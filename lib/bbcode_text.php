<?php

if (!defined('BLARG')) {
    die();
}

// Misc things that get replaced in text.

function loadSmilies()
{
    global $smilies, $smiliesReplaceOrig, $smiliesReplaceNew;

    $rSmilies = Query('SELECT * FROM {smilies} ORDER BY length(code) DESC');
    $smilies = [];

    while ($smiley = Fetch($rSmilies)) {
        $smilies[] = $smiley;
    }

    $smiliesReplaceOrig = $smiliesReplaceNew = [];
    for ($i = 0; $i < count($smilies); $i++) {
        $smiliesReplaceOrig[] = "/(?<!\w)".preg_quote($smilies[$i]['code'], '/')."(?!\w)/";
        $smiliesReplaceNew[] = '<img class="smiley" alt="" src="'.resourceLink('img/smilies/'.$smilies[$i]['image']).'" />';
    }
}

//Main post text replacing.
function postDoReplaceText($s, $parentTag, $parentMask)
{
    global $postNoSmilies, $postPoster, $smiliesReplaceOrig, $smiliesReplaceNew;

    if ($postPoster) {
        $s = preg_replace("'/me '", '<b>* '.htmlspecialchars($postPoster).'</b> ', $s);
    }

    //Smilies
    if (!$postNoSmilies) {
        if (!isset($smiliesReplaceOrig)) {
            LoadSmilies();
        }
        $s = preg_replace($smiliesReplaceOrig, $smiliesReplaceNew, $s);
    }

    //Automatic links
    if (!($parentMask & TAG_NOAUTOLINK)) {
        $s = preg_replace_callback('@(?:(?:http|ftp)s?://|\bwww\.)[\w\-/%&?=+#~\'\@*^$\.,;!:]+[\w\-/%&?=+#~\'\@*^$]@i', 'bbcodeURLAuto', $s);
    }

    //Plugin bucket for allowing plugins to add replacements.
    $bucket = 'postMangler';
    include __DIR__.'/pluginloader.php';

    return $s;
}
