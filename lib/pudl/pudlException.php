<?php


////////////////////////////////////////////////////////////////////////////////
// GENERIC PUDL EXCEPTION - ALL OTHER EXCEPTIONS INHERIT THIS CLASS
////////////////////////////////////////////////////////////////////////////////
class pudlException extends Exception
{
    public function __construct($pudl, $message = null, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->pudl = $pudl;

        if ($pudl instanceof pudl) {
            $pudl->decache()->destring();
        }
    }

    public $pudl;
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN A CONNECTION ERROR OCCURS
////////////////////////////////////////////////////////////////////////////////
class pudlConnectionException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN A DATA TYPE IS INVALID FOR THE GIVEN CALL
////////////////////////////////////////////////////////////////////////////////
class pudlTypeException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WITH THE DATA VALUE IS INVALID FOR THE GIVEN CALL
////////////////////////////////////////////////////////////////////////////////
class pudlValueException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN THE GIVEN FUNCTION CALL IS INVALID
////////////////////////////////////////////////////////////////////////////////
class pudlFunctionException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN THE GIVEN CLASS METHOD CALL IS INVALID
////////////////////////////////////////////////////////////////////////////////
class pudlMethodException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN A REQUIRED PHP EXTENSION IS NOT INSTALLED/ENABLED
////////////////////////////////////////////////////////////////////////////////
class pudlExtensionException extends pudlException
{
}

////////////////////////////////////////////////////////////////////////////////
// THROWN WHEN A RECURSION LIMIT IS REACHED
////////////////////////////////////////////////////////////////////////////////
class pudlRecursionException extends pudlException
{
}
