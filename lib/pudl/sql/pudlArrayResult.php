<?php


//NOTE: THIS IS ONLY A PARTIAL IMPLEMENTATION AND WILL BE FINISHED LATER

class pudlArrayResult extends pudlResult
{
    ////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    ////////////////////////////////////////////////////////////////////////////
    public function __construct(pudl $pudl, $array)
    {
        parent::__construct($pudl);

        if ($array instanceof pudlObject) {
            $array = $array->raw();
        } elseif (!is_array($array)) {
            $array = (array) $array;
        }

        $this->array = $array;
    }

    ////////////////////////////////////////////////////////////////////////////
    // FREE THE RESULT DATA
    ////////////////////////////////////////////////////////////////////////////
    public function free()
    {
        $this->array = false;
        $this->pos = 0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GET A CELL BY ROW/COLUMN
    // TODO: CURRENTLY DOESN'T WORK
    ////////////////////////////////////////////////////////////////////////////
    public function cell($row = 0, $column = 0)
    {
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GET THE TOTAL NUMBER OF ROWS
    ////////////////////////////////////////////////////////////////////////////
    public function count()
    {
        return is_array($this->array) ? count($this->array) : 0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GET THE TOTAL NUMBER OF FIELDS
    ////////////////////////////////////////////////////////////////////////////
    public function fields()
    {
        if (!is_array($this->array)) {
            return false;
        }
        $array = current($this->array);

        return is_array($array) ? count($array) : false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // LIST ALL OF THE FIELDS
    ////////////////////////////////////////////////////////////////////////////
    public function listFields()
    {
        if (!is_null($this->keys)) {
            return $this->keys;
        }
        if (!is_array($this->array)) {
            return false;
        }

        $array = current($this->array);
        if ($array instanceof pudlObject) {
            $array = $array->raw();
        }

        $this->keys = [];
        $keys = array_keys((array) $array);

        if (!is_array($keys)) {
            return false;
        }

        foreach ($keys as $item) {
            $this->keys[] = (object) ['name' => $item];
        }

        return $this->keys;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GET A LIST OF ALL THE AVAILABLE KEYS / FIELDS
    ////////////////////////////////////////////////////////////////////////////
    public function getField($column)
    {
        $fields = $this->listFields();
        if (!is_array($fields)) {
            return false;
        }
        if (!array_key_exists($column, $fields)) {
            return false;
        }

        return $fields[$column];
    }

    ////////////////////////////////////////////////////////////////////////////
    // SET TO A DIFFERENT ROW
    ////////////////////////////////////////////////////////////////////////////
    public function seek($row)
    {
        if (!is_array($this->array)) {
            return;
        }
        if ($row < 0 || $row >= count($this->array)) {
            return;
        }
        $this->pos = $row;
    }

    ////////////////////////////////////////////////////////////////////////////
    // GET THE CURRENT ROW
    ////////////////////////////////////////////////////////////////////////////
    public function row()
    {
        if (!is_array($this->array)) {
            return false;
        }

        return ($this->pos++ === 0)
            ? current($this->array)
            : next($this->array);
    }

    ////////////////////////////////////////////////////////////////////////////
    // ERROR CODE - 0 FOR NO ERROR
    ////////////////////////////////////////////////////////////////////////////
    public function error()
    {
        return 0;
    }

    ////////////////////////////////////////////////////////////////////////////
    // ERROR MESSAGE - EMPTY STRING
    ////////////////////////////////////////////////////////////////////////////
    public function errormsg()
    {
        return '';
    }

    ////////////////////////////////////////////////////////////////////////////
    // MEMBER VARIABLES
    ////////////////////////////////////////////////////////////////////////////
    /**
     * @var array|false
     */        private $array = false;
    /**
     * @var ?array
     */            private $keys = null;
    /**
     * @var int
     */                private $pos = 0;
}
