<?php


trait pudlCte
{
    ////////////////////////////////////////////////////////////////////////////
    // CHECK IF WE'RE IN A CTE STATE
    ////////////////////////////////////////////////////////////////////////////
    public function isCte()
    {
        return $this->cte_alias !== null;
    }

    ////////////////////////////////////////////////////////////////////////////
    // SET THE CTE STATE
    ////////////////////////////////////////////////////////////////////////////
    public function cte($alias, $query)
    {
        if ($query instanceof pudlStringResult) {
            $query = (string) $query;
        }

        $this->cte_alias = $alias;
        $this->cte_query = $query;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////
    // INTERNAL SQL QUERY BUILDER FOR CTE, AND RESET CTE STATE
    ////////////////////////////////////////////////////////////////////////////
    private function _cte($query = null)
    {
        $return = 'WITH '
                    .$this->_table($this->cte_alias)
                    .' AS '
                    .$this->cte_query
                    .' ';

        if ($query !== null) {
            $return .= $query;
        }

        $this->cte_alias = null;
        $this->cte_query = null;

        return $return;
    }

    ////////////////////////////////////////////////////////////////////////////
    // MEMBER VARIABLES
    ////////////////////////////////////////////////////////////////////////////
    /**
     * @var ?string
     */    private $cte_alias = null;
    /**
     * @var ?string
     */    private $cte_query = null;
}
