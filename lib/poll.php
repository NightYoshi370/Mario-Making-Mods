<?php/*
[8:07 AM] <shibs> when you are using SQL you should really throw the data into a ctor for a class that handles that data
[8:07 AM] <shibs> rather than use stray code over and over again
[8:08 AM] <shibs> it would make so much stuff easier
[8:08 AM] <shibs> you can define a method for the class like, addOption to class Thread as long as this->isPoll is true
[8:08 AM] <shibs> rather than
[8:08 AM] <shibs> SQL query to see if its a poll
[8:09 AM] <shibs> SQL query to see if thread exists
[8:09 AM] <shibs> SQL query to add the poll option
[8:09 AM] <shibs> SQL query to modify the thread data to include the poll
*/

class Poll {
	var $thread;

	function __construct($thread) {
		$this->thread = $thread;
	}

	function CheckPoll() {
		if($thread['poll']) {
				$poll = Fetch(Query("SELECT p.*,
										(SELECT COUNT(DISTINCT user) FROM {pollvotes} pv WHERE pv.poll = p.id) as users,
										(SELECT COUNT(*) FROM {pollvotes} pv WHERE pv.poll = p.id) as votes
									 FROM {poll} p
									 WHERE p.id={0}", $thread['poll']));

				if(!$poll)	return false;
				else		return true;
		} else
			return false;
	}
}