<?php

if (php_sapi_name() !== 'cli') {
    die("This script is only intended for CLI usage.\n");
}

require __DIR__.'/../language.php';

$langnames = ['nl_NL', 'es_ES', 'pl_PL'];
$langs = [];

foreach ($langnames as $langname) {
    $languagePack = [];
    importLanguagePack($langname.'.txt');
    $langs[$langname] = $languagePack;
}

foreach ($languagePack as $key => $val) {
    echo $key."\t\t";
    foreach ($langnames as $langname) {
        echo $langs[$langname][$key]."\t";
    }
    echo "\n";
}
