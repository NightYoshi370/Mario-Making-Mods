<?php

if (!defined('BLARG')) {
    die();
}
//ToDo: Make this not a bunch of array's.
//Rather, allow something like the neat code of editprofile.php

$settings = [
    // SEO stuff
    'boardname'       => ['type' => 'text', 'name' => 'Board name', 'category' => 'Board identity'],
    'metaDescription' => ['type' => 'text', 'name' => 'Meta description', 'category' => 'Board identity'],
    'metaTags'        => ['type' => 'text', 'name' => 'Meta tags', 'category' => 'Board identity'],

    'dateformat'         => ['type' => 'text', 'default' => 'm-d-y, h:i a', 'name' => 'Default date format', 'category' => 'Presentation'],
    'guestLayouts'       => ['type' => 'boolean', 'default' => '0', 'name' => 'Show post layouts to guests', 'category' => 'Presentation'],
    'defaultTheme'       => ['type' => 'theme', 'name' => 'Default board theme', 'category' => 'Presentation'],
    'defaultLayout'      => ['type' => 'layout', 'default' => 'modern', 'name' => 'Board layout', 'category' => 'Presentation'],
    'showGender'         => ['type' => 'boolean', 'default' => '1', 'name' => 'Color usernames based on gender', 'category' => 'Presentation'],
    'defaultLanguage'    => ['type' => 'language', 'default' => 'en_US', 'name' => 'Board language', 'category' => 'Presentation'],
    'tagsDirection'      => ['type' => 'options', 'options' => ['Left' => 'Left', 'Right' => 'Right'], 'default' => 'Right', 'name' => 'Direction of thread tags', 'category' => 'Presentation'],
    'alwaysMinipic'      => ['type' => 'boolean', 'default' => '0', 'name' => 'Show minipics everywhere', 'category' => 'Presentation'],
    'showExtraSidebar'   => ['type' => 'boolean', 'default' => '1', 'name' => 'Show extra info in post sidebar', 'category' => 'Presentation'],
    'profilePreviewText' => [
        'type'    => 'textbbcode',
        'default' => "This is a sample post. You [b]probably[/b] [i]already[/i] [u]know[/u] what this is for.

[quote=Goomba][quote=Mario]Woohoo! [url=http://www.mariowiki.com/Super_Mushroom]That's what I needed![/url][/quote]Oh, nooo! *stomp*[/quote]

Well, what more could you [url=http://en.wikipedia.org]want to know[/url]? Perhaps how to do the classic infinite loop?
[source=c]while(true){
    printf(\"Hello World!
\");
}[/source]",
        'name'     => 'Post preview text',
        'category' => 'Presentation',
    ],

    'postLayoutType'              => ['type' => 'options', 'options' => ['0' => 'Signature', '1' => 'Post header + signature', '2' => 'Post header + signature + sidebars'], 'default' => '2', 'name' => 'Post layout type', 'category' => 'Functionality'],
    'postAttach'                  => ['type' => 'boolean', 'default' => '0', 'name' => 'Allow post attachments', 'category' => 'Functionality'],
    'customTitleThreshold'        => ['type' => 'integer', 'default' => '100', 'name' => 'Custom title threshold (posts)', 'category' => 'Functionality'],
    'oldThreadThreshold'          => ['type' => 'integer', 'default' => '3', 'name' => 'Old thread threshold (months)', 'category' => 'Functionality'],
    'viewcountInterval'           => ['type' => 'integer', 'default' => '10000', 'name' => 'Viewcount report interval', 'category' => 'Functionality'],
    'ajax'                        => ['type' => 'boolean', 'default' => '1', 'name' => 'Enable AJAX', 'category' => 'Functionality'],
    'ownerEmail'                  => ['type' => 'text', 'name' => 'Owner email address', 'help' => 'This email address will be shown to IP-banned users and on other occasions.', 'category' => 'Functionality'],
    'mailResetSender'             => ['type' => 'text', 'name' => 'Password Reset email sender', 'help' => 'Email address used to send the pasword reset e-mails. If left blank, the password reset feature is disabled.', 'category' => 'Functionality'],
        'floodProtectionInterval' => [
            'type'     => 'integer',
            'default'  => '10',
            'name'     => 'Minimum time between user posts (seconds)',
            'category' => 'Functionality',
        ],
        'nofollow' => [
            'type'     => 'boolean',
            'default'  => '0',
            'name'     => 'Add rel=nofollow to all user-posted links',
            'category' => 'Functionality',
        ],
        'maintenance' => [
            'type'     => 'text',
            'default'  => '0',
            'name'     => 'Maintenance mode',
            'help'     => 'Leave Text blank for no Maintenance Mode',
            'category' => 'Functionality',
            'rootonly' => 1,
        ],

        'rssTitle' => [
            'type'     => 'text',
            'default'  => 'Blargboard RSS',
            'name'     => 'RSS feed title',
            'category' => 'Information',
        ],
        'rssDesc' => [
            'type'     => 'text',
            'default'  => 'A news feed for Blargboard',
            'name'     => 'RSS feed description',
            'category' => 'Information',
        ],

        'DisReg' => [
            'type'     => 'boolean',
            'default'  => '0',
            'name'     => 'Turn off registration',
            'help'     => 'Usefull when your site is hit with a spam attack.',
            'category' => 'Registration',
        ],

        'newsForum' => [
            'type'     => 'forum',
            'default'  => '0',
            'name'     => 'Latest News forum',
            'category' => 'Forum settings',
        ],
        'anncForum' => [
            'type'     => 'forum',
            'default'  => '0',
            'name'     => 'Announcements forum',
            'category' => 'Forum settings',
        ],
        'StaffanncForum' => [
            'type'     => 'forum',
            'default'  => '0',
            'name'     => 'Staff Announcements forum',
            'category' => 'Forum settings',
        ],
        'trashForum' => [
            'type'     => 'forum',
            'default'  => '0',
            'name'     => 'Trash forum',
            'category' => 'Forum settings',
        ],
        'secretTrashForum' => [
            'type'     => 'forum',
            'default'  => '0',
            'name'     => 'Deleted threads forum',
            'category' => 'Forum settings',
        ],

        'WebHookName' => [
            'type'     => 'text',
            'default'  => 'BlargReport',
            'name'     => 'Webhook username',
            'category' => 'Discord settings',
        ],
        'ForumWebhook' => [
            'type'     => 'text',
            'default'  => '',
            'name'     => 'Main Post Report webhook link',
            'category' => 'Discord settings',
        ],
        'webhookimage' => [
            'type'     => 'text',
            'default'  => 'https://maxcdn.icons8.com/Share/icon/Logos//discord_logo1600.png',
            'name'     => 'Webhook profile picture',
            'category' => 'Discord settings',
        ],
        'helpwebhook' => [
            'type'     => 'text',
            'default'  => '',
            'name'     => 'Help bot webhook link',
            'category' => 'Discord settings',
        ],
        'devwebhook' => [
            'type'     => 'text',
            'default'  => '',
            'name'     => 'DevBot webhook link',
            'category' => 'Discord settings',
        ],
        'logwebhook' => [
            'type'     => 'text',
            'default'  => '',
            'name'     => 'Log webhook link',
            'category' => 'Discord settings',
        ],

        'defaultGroup' => [
            'type'     => 'group',
            'default'  => 0,
            'name'     => 'Group for new users',
            'category' => 'Group settings',
            'rootonly' => 1,
        ],
        'rootGroup' => [
            'type'     => 'group',
            'default'  => 4,
            'name'     => 'Group for root users',
            'category' => 'Group settings',
            'rootonly' => 1,
        ],
        'bannedGroup' => [
            'type'     => 'group',
            'default'  => -1,
            'name'     => 'Group for banned users',
            'category' => 'Group settings',
            'rootonly' => 1,
        ],

        'homepageText' => [
            'type'     => 'texthtml',
            'default'  => 'Welcome to Blargboard.<br><br>Fill this with relevant info.',
            'name'     => 'Homepage contents',
            'category' => 'Homepage contents',
        ],
        'faqText' => [
            'type'     => 'texthtml',
            'default'  => 'Blargboard FAQ. Put your rules and stuff here.',
            'name'     => 'FAQ contents',
            'category' => 'FAQ contents',
        ],
    ];
