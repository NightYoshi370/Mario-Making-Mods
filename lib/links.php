<?php

if (!defined('BLARG')) {
    die();
}

$ishttps = ($_SERVER['SERVER_PORT'] == 443);
$serverport = ($_SERVER['SERVER_PORT'] == ($ishttps ? 443 : 80)) ? '' : ':'.$_SERVER['SERVER_PORT'];

function urlNamify($urlname)
{
    $urlname = strtolower($urlname);
    $urlname = str_replace('&', 'and', $urlname);
    $urlname = preg_replace('/[^a-zA-Z0-9]/', '-', $urlname);
    $urlname = preg_replace('/-+/', '-', $urlname);
    $urlname = preg_replace('/^-/', '', $urlname);
    $urlname = preg_replace('/-$/', '', $urlname);

    return $urlname;
}

function actionLink($action, $id = '', $args = '', $urlname = '')
{
    $boardroot = URL_ROOT;
    if (!empty($boardroot)) {
        $boardroot = './';
    }

    if ($action == MAIN_PAGE) {
        $action = '';
    } elseif ($id) {
        $action .= '/';
    } else {
        $action .= '';
    }

    if ($id) {
        if ($urlname) {
            $id .= '-'.urlNamify($urlname);
        }
        $id .= '';
    } else {
        $id = '';
    }

    return $boardroot.$action.$id.($args ? '?'.$args : '');
}

function pageLink($page, $params = [], $extra = '')
{
    global $router;

    return $router->generate($page, $params).($extra != '' ? '?'.$extra : '');
}

// Page generators
function pageLinkTag($text, $page, $params = [], $extra = '', $title = '')
{
    return '<a href="'.pageLink($page, $params, $extra).'" title="'.$title.'">'.htmlentities($text).'</a>';
}
function pageLinkTagUnescaped($text, $page, $params = [], $extra = '', $title = '')
{
    return '<a href="'.pageLink($page, $params, $extra).'" title="'.$title.'">'.$text.'</a>';
}
function pageLinkTagItem($text, $page, $params = [], $extra = '', $title = '')
{
    return '<li><a href="'.pageLink($page, $params, $extra).'" title="'.$title.'">'.htmlentities($text).'</a></li>';
}
function pageLinkTagConfirm($text, $prompt, $page, $params = [], $extra = '')
{
    return '<a onclick="return confirm(\''.htmlentities($prompt).'\'); " href="'.pageLink($page, $params, $extra).'">'.htmlentities($text).'</a>';
}
function pageLinkTagItemConfirm($text, $prompt, $page, $params = [], $extra = '')
{
    return '<li><a onclick="return confirm(\''.htmlentities($prompt).'\'); " href="'.pageLink($page, $params, $extra).'">'.htmlentities($text).'</a></li>';
}

// Action generators
function actionLinkTag($text, $action, $id = '', $args = '', $urlname = '')
{
    return '<a href="'.htmlentities(actionLink($action, $id, $args, $urlname)).'">'.$text.'</a>';
}
function actionLinkTagItem($text, $action, $id = '', $args = '', $urlname = '')
{
    return '<li><a href="'.htmlentities(actionLink($action, $id, $args, $urlname)).'">'.$text.'</a></li>';
}
function actionLinkTagConfirm($text, $prompt, $action, $id = '', $args = '')
{
    return '<a onclick="return confirm(\''.$prompt.'\'); " href="'.htmlentities(actionLink($action, $id, $args)).'">'.$text.'</a>';
}
function actionLinkTagItemConfirm($text, $prompt, $action, $id = '', $args = '')
{
    return '<li><a onclick="return confirm(\''.$prompt.'\'); " href="'.htmlentities(actionLink($action, $id, $args)).'">'.$text.'</a></li>';
}

function getForm($action, $id = '')
{
    $ret = '<form method="GET"><input type="hidden" value="'.$action.'">';

    if (!empty($id)) {
        $ret .= '<input type="hidden" name="id" value="'.htmlspecialchars($id).'">';
    }

    return $ret;
}

function resourceLink($what)
{
    return URL_ROOT.$what;
}

function themeResourceLink($what)
{
    global $theme;

    return URL_ROOT.'themes/'.$theme.'/'.$what;
}

function getMinipicTag($user)
{
    if (!$user['picture']) {
        return '';
    }
    $pic = str_replace('$root/', DATA_URL, $user['picture']);
    $minipic = '<img src="'.htmlspecialchars($pic).'" alt="" class="minipic" /> ';

    return $minipic;
}

function prettyRainbow($s)
{
    $r = mt_rand(0, 359);
    $s = str_replace('&nbsp;', ' ', $s);
    $s = html_entity_decode($s);
    $len = strlen($s);
    $out = '';
    for ($i = 0; $i < $len;) {
        $c = $s[$i++];

        if ($c == ' ') {
            $out .= '&nbsp;';
            continue;
        }

        $ord = ord($c);
        if ($ord & 0x80) {        // UTF8 stuff
            if ($ord & 0x40) {
                $c .= $s[$i++];
                if ($ord & 0x20) {
                    $c .= $s[$i++];
                    if ($ord & 0x10) {
                        $c .= $s[$i++];
                        if ($ord & 0x08) {
                            $c .= $s[$i++];
                            if ($ord & 0x04) {
                                $c .= $s[$i++];
                            }
                        }
                    }
                }
            }
        } else {
            if ($c == '<') {
                $c = '&lt;';
            } elseif ($c == '>') {
                $c = '&gt;';
            }
        }

        $out .= '<span style="color:hsl('.$r.',100%,80.4%);">'.$c.'</span>';
        $r += 31;
        $r %= 360;
    }

    return $out;
}

$poptart = mt_rand(0, 359);
$dorainbow = -1;

function userLink($user, $showMinipic = false, $customID = false, $array = false)
{
    global $usergroups;
    global $poptart, $dorainbow, $newToday;
    global $luckybastards;

    if ($dorainbow == -1) {
        $dorainbow = false;

        if ($newToday >= 600) {
            $dorainbow = true;
        }
    }

    $fgroup = $usergroups[$user['primarygroup']];
    $fsex = $user['sex'];
    $fname = ($user['displayname'] ? $user['displayname'] : $user['name']);
    $fname = htmlspecialchars($fname);

    $isbanned = $fgroup['id'] == Settings::get('bannedGroup');

    $minipic = '';
    if (($showMinipic || Settings::get('alwaysMinipic')) && $showMinipic !== 'false') {
        $minipic = getMinipicTag($user);
    }

    if (!Settings::get('showGender')) {
        $fsex = 2;
    }

    if ($fsex == 0) {
        $scolor = 'color_male';
    } elseif ($fsex == 1) {
        $scolor = 'color_female';
    } else {
        $scolor = 'color_unspec';
    }

    $classing = ' style="color: '.htmlspecialchars($fgroup[$scolor]).';"';

    $bucket = 'userLink';
    include __DIR__.'/pluginloader.php';

    if (!$isbanned && $luckybastards && in_array($user['id'], $luckybastards)) {
        $classing = ' style="text-shadow:0px 0px 4px;"';
        $fname = prettyRainbow($fname);
    } elseif ($dorainbow) {
        if (!$isbanned) {
            $classing = ' style="color:hsl('.$poptart.',100%,80.4%);"';
        }
        $poptart += 31;
        $poptart %= 360;
    }

    $fname = $minipic.$fname;

    if ($customID) {
        $classing .= " id=\"$customID\"";
    }

    $title = htmlspecialchars($user['name']).' ('.$user['id'].') ['.htmlspecialchars($fgroup['title']).']';
    if ($user['id'] == 0) {
        return "<strong$classing class=\"userlink fake\">$fname</strong>";
    }

    if ($array) {
        return ['link' => pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]), 'text' => $fname];
    }

    return pageLinkTagUnescaped("<span $classing class=\"userlink\" title=\"".htmlentities($title)."\">$fname</span>", 'profile', ['id' => $user['id'], 'name' => slugify($user['name'])]);
}

function profileLink($user)
{
    $fname = ($user['displayname'] ? $user['displayname'] : $user['name']);
    $fname = str_replace(' ', '-', htmlspecialchars($fname));

    $bucket = 'profileLink';
    include __DIR__.'/pluginloader.php';

    return pageLink('profile', ['id' => $user['id'], 'name' => slugify($fname)]);
}

function userLinkById($id)
{
    global $userlinkCache;

    if (!isset($userlinkCache[$id])) {
        $rUser = Query('SELECT u.(_userfields) FROM {users} u WHERE u.id={0}', $id);
        if (NumRows($rUser)) {
            $userlinkCache[$id] = getDataPrefix(Fetch($rUser), 'u_');
        } else {
            $userlinkCache[$id] = ['id' => 0, 'name' => 'Unknown User', 'sex' => 0, 'primarygroup' => -1];
        }
    }

    return UserLink($userlinkCache[$id]);
}

function makeThreadLink($thread)
{
    $tags = ParseThreadTags($thread['title']);

    $link = actionLinkTag($tags[0], 'thread', $thread['id'], '', HasPermission('forum.viewforum', $thread['forum'], true) ? $tags[0] : '');
    $tags = $tags[1];

    if (Settings::get('tagsDirection') === 'Left') {
        return $tags.' '.$link;
    } else {
        return $link.' '.$tags;
    }
}

function makeThreadLinkNoTags($thread)
{
    $tags = ParseThreadTags($thread['title']);

    $link = actionLinkTag($tags[0], 'thread', $thread['id'], '', HasPermission('forum.viewforum', $thread['forum'], true) ? $tags[0] : '');

    return $link;
}

function makeAnnounceLink($thread)
{
    $tags = ParseThreadTags($thread['title']);

    $link = actionLink('thread', $thread['id'], '', HasPermission('forum.viewforum', $thread['forum'], true) ? $tags[0] : '');

    return ['tags' => $tags[1], 'link' => $link, 'name' => $tags[0]];
}

function makeFromUrl($url, $from)
{
    if ($from == 0) {
        $url = preg_replace('@(?:&amp;|&|\?)\w+=$@', '', $url);

        return $url;
    } else {
        return $url.$from;
    }
}

function pageLinks($url, $epp, $from, $total)
{
    if ($total <= $epp) {
        return '';
    }

    $url = htmlspecialchars($url);

    if ($from < 0) {
        $from = 0;
    }
    if ($from > $total - 1) {
        $from = $total - 1;
    }
    $from -= $from % $epp;

    $numPages = (int) ceil($total / $epp);
    $page = (int) ceil($from / $epp) + 1;

    $first = ($from > 0) ? '
			<li class="page-item first">
				<a class="page-link" href="'.makeFromUrl($url, 0).'" aria-label="First">
					<span aria-hidden="true">&#x00AB;</span>
					<span class="sr-only">First</span>
				</a>
			</li>' : '';
    $prev = $from - $epp;
    if ($prev < 0) {
        $prev = 0;
    }
    $prev = ($from > 0) ? '
			<li class="page-item previous">
				<a class="page-link" href="'.makeFromUrl($url, $prev).'" aria-label="Previous">
					<span aria-hidden="true">&#x2039;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>' : '';
    $next = $from + $epp;
    $last = ($numPages * $epp) - $epp;
    if ($next > $last) {
        $next = $last;
    }
    $next = ($from < $total - $epp) ? '
			<li class="page-item next">
				<a class="page-link" href="'.makeFromUrl($url, $next).'" aria-label="Next">
					<span aria-hidden="true">&#x203A;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>' : '';
    $last = ($from < $total - $epp) ? '
			<li class="page-item last">
				<a class="page-link" href="'.makeFromUrl($url, $last).'" aria-label="Last">
					<span aria-hidden="true">&#x00BB;</span>
					<span class="sr-only">Last</span>
				</a>
			</li>' : '';

    $pageLinks = [];
    for ($p = $page - 5; $p < $page + 5; $p++) {
        if ($p < 1 || $p > $numPages) {
            continue;
        }

        if ($p == $page || ($from == 0 && $p == 1)) {
            $pageLinks[] = "<li class=\"page-item active\"><span class=\"page-link\">$p <span class=\"sr-only\">(current)</span></span></li>";
        } else {
            $pageLinks[] = '<li class="page-item"><a class="page-link" href="'.makeFromUrl($url, (($p - 1) * $epp))."\">$p</a></li>";
        }
    }

    $style = '
	<nav aria-label="Page navigation example">
		<ul class="pagination pg-blue">
			'.$first.$prev.implode($pageLinks, '').$next.$last.'
		</ul>
	</nav>';

    return $style;
}

function pageLinksInverted($url, $epp, $from, $total)
{
    if ($total <= $epp) {
        return '';
    }

    $url = htmlspecialchars($url);

    if ($from < 0) {
        $from = 0;
    }
    if ($from > $total - 1) {
        $from = $total - 1;
    }
    $from -= $from % $epp;

    $numPages = (int) ceil($total / $epp);
    $page = (int) ceil($from / $epp) + 1;

    $first = ($from > 0) ? '
			<li class="page-item first">
				<a class="page-link" href="'.makeFromUrl($url, 0).'" aria-label="First">
					<span aria-hidden="true">&#x00BB;</span>
					<span class="sr-only">First</span>
				</a>
			</li>' : '';
    $prev = $from - $epp;
    if ($prev < 0) {
        $prev = 0;
    }
    $prev = ($from > 0) ? '
			<li class="page-item previous">
				<a class="page-link" href="'.makeFromUrl($url, $prev).'" aria-label="Previous">
					<span aria-hidden="true">&#x203A;</span>
					<span class="sr-only">Previous</span>
				</a>
			</li>' : '';
    $next = $from + $epp;
    $last = ($numPages * $epp) - $epp;
    if ($next > $last) {
        $next = $last;
    }
    $next = ($from < $total - $epp) ? '
			<li class="page-item next">
				<a class="page-link" href="'.makeFromUrl($url, $next).'" aria-label="Next">
					<span aria-hidden="true">&#x2039;</span>
					<span class="sr-only">Next</span>
				</a>
			</li>' : '';
    $last = ($from < $total - $epp) ? '
			<li class="page-item last">
				<a class="page-link" href="'.makeFromUrl($url, $last).'" aria-label="Last">
					<span aria-hidden="true">&#x00AB;</span>
					<span class="sr-only">Last</span>
				</a>
			</li>' : '';

    $pageLinks = [];
    for ($p = $page + 5; $p >= $page - 5; $p--) {
        if ($p < 1 || $p > $numPages) {
            continue;
        }

        if ($p == $page || ($from == 0 && $p == 1)) {
            $pageLinks[] = '<li class="page-item active"><span class="page-link">'.($numPages + 1 - $p).'<span class="sr-only">(current)</span></span></li>';
        } else {
            $pageLinks[] = '<li class="page-item"><a class="page-link" href="'.makeFromUrl($url, (($p - 1) * $epp)).'">'.($numPages + 1 - $p).'</a></li>';
        }
    }

    $style = '
	<nav aria-label="Page navigation example">
		<ul class="pagination pg-blue">
			'.$last.$next.implode($pageLinks, '').$prev.$first.'
		</ul>
	</nav>';

    return $style;
}

function absoluteActionLink($action, $id = 0, $args = '')
{
    global $serverport;

    return getServerDomainNoSlash().dirname($_SERVER['PHP_SELF']).substr(actionLink($action, $id, $args), 1);
}

function absolutePageLink($page, $params = [], $extra = '')
{
    global $serverport;

    return getServerDomainNoSlash().dirname($_SERVER['PHP_SELF']).pageLink($page, $params, $extra);
}

function getRequestedURL()
{
    return htmlspecialchars($_SERVER['REQUEST_URI']);
}

function getServerDomainNoSlash($https = false)
{
    global $serverport;

    return ($https ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$serverport;
}

function getServerDomain($https = false)
{
    return getServerDomainNoSlash($https).'/';
}

function getServerURL($https = false)
{
    return getServerURLNoSlash($https).'/';
}

function getServerURLNoSlash($https = false)
{
    global $serverport;

    return ($https ? 'https' : 'http').'://'.$_SERVER['SERVER_NAME'].$serverport.substr(URL_ROOT, 0, strlen(URL_ROOT) - 1);
}

function getFullRequestedURL($https = false)
{
    return getServerURL($https).$_SERVER['REQUEST_URI'];
}

function newRedir($url)
{
    die(header('Location: '.$url));
}

// ----------------------------------------------------------------------------
// --- Smarty interface
// ----------------------------------------------------------------------------

function smarty_function_actionLink($params, $template)
{
    return htmlspecialchars(actionLink($params['page'], ($params['id'] ?: ''), $params['args'], $params['urlname']));
}

function smarty_function_resourceLink($params, $template)
{
    return htmlspecialchars(resourceLink($params['url']));
}
function smarty_function_pageLink($params, $template)
{
    $passParams = isset($params['params']) ? $params['params'] : [];

    return pageLink($params['name'], $passParams);
}
