<?php

if (!defined('BLARG')) {
    die();
}

// ----------------------------------------------------------------------------
// --- General layout functions
// ----------------------------------------------------------------------------

function RenderTemplate($template, $options = null)
{
    global $tpl, $loguser, $plugintemplates, $plugins, $mobileLayout, $isAndroid;

    if (array_key_exists($template, $plugintemplates)) {
        $plugin = $plugintemplates[$template];
        $self = $plugins[$plugin];

        $tplroot = BOARD_ROOT.'/plugins/'.$self['dir'].'/layout/';
    } else {
        $tplroot = BOARD_ROOT.'/layout/';
    }

	$tplname = '';

    if ($isAndroid) {
		// All of the checks for mobile layout
        if(isset($loguser['layout']) && !empty($loguser['layout'])) {
			$tplname = $tplroot.$loguser['layout'].'/android/'.$template.'.tpl';
			if(!file_exists($tplname)) {
				$tplname = $tplroot.$loguser['layout'].'/mobile/'.$template.'.tpl';
			}
		}

		if((empty($tplname) || (!empty($tplname) && !file_exists($tplname))) && !empty(Settings::get('defaultLayout')) && Settings::get('defaultLayout') !== 'modern') {
			$tplname = $tplroot.Settings::get('defaultLayout').'/android/'.$template.'.tpl';
			if(!file_exists($tplname)) {
				$tplname = $tplroot.Settings::get('defaultLayout').'/mobile/'.$template.'.tpl';
			}
		}

	    if (empty($tplname) || (!empty($tplname) && !file_exists($tplname))) {
			$tplname = $tplroot.'modern/android/'.$template.'.tpl';
			if(!file_exists($tplname)) {
				$tplname = $tplroot.'modern/mobile/'.$template.'.tpl';
			}
		}
    } else if ($mobileLayout) {
		// All of the checks for mobile layout
        if(isset($loguser['layout']) && !empty($loguser['layout'])) {
			$tplname = $tplroot.$loguser['layout'].'/mobile/'.$template.'.tpl';
		}

		if((empty($tplname) || (!empty($tplname) && !file_exists($tplname))) && !empty(Settings::get('defaultLayout')) && Settings::get('defaultLayout') !== 'modern') {
			$tplname = $tplroot.Settings::get('defaultLayout').'/mobile/'.$template.'.tpl';
		}

	    if (empty($tplname) || (!empty($tplname) && !file_exists($tplname))) {
			$tplname = $tplroot.'modern/mobile/'.$template.'.tpl';
		}
	}

	if((empty($tplname) || (!empty($tplname) && !file_exists($tplname))) && isset($loguser['layout']) && !empty($loguser['layout'])) {
		$tplname = $tplroot.$loguser['layout'].'/templates/'.$template.'.tpl';
	}

	if((empty($tplname) || (!empty($tplname) && !file_exists($tplname))) && !empty(Settings::get('defaultLayout')) && Settings::get('defaultLayout') !== 'modern') {
		$tplname = $tplroot.Settings::get('defaultLayout').'/templates/'.$template.'.tpl';
	}

    if ((empty($tplname) || (!empty($tplname) && !file_exists($tplname)))) {
		$tplname = $tplroot.'modern/templates/'.$template.'.tpl';
	}


    if ($options && count($options)) {
        $tpl->assign($options);
    }

    $tpl->display($tplname);
}

function smarty_function_RenderTemplate($params, &$smarty) {
	if(!$params['options'] || !is_array($params['options'])) {
		$params['options'] = [];
	}

	return RenderTemplate($params['template'], $params['options']);
}

function mfl_forumBlock($fora, $catid, $selID, $indent)
{
    $ret = '';

    foreach ($fora[$catid] as $forum) {
        $ret .=
        '				<option value="'.$forum['id'].'"'.($forum['id'] == $selID ? ' selected="selected"' : '').'>'
        .str_repeat('&nbsp; &nbsp; ', $indent).htmlspecialchars($forum['title'])
        .'</option>
';
        if (!empty($fora[-$forum['id']])) {
            $ret .= mfl_forumBlock($fora, -$forum['id'], $selID, $indent + 1);
        }
    }

    return $ret;
}

function makeForumList($fieldname, $selectedID, $allowNone = false)
{
    global $loguserid, $loguser, $forumBoards;

    $viewableforums = ForumsWithPermission('forum.viewforum');
    $viewhidden = HasPermission('user.viewhiddenforums');

    $noneOption = '';
    if ($allowNone) {
        $noneOption = '<option value=0>'.__('(none)').'</option>';
    }

    $rCats = Query('SELECT id, name, board FROM {categories} ORDER BY board, corder, id');
    $cats = [];
    while ($cat = Fetch($rCats)) {
        $cats[$cat['id']] = $cat;
    }

    $rFora = Query(
        '	SELECT
							f.id, f.title, f.catid
						FROM
							{forums} f
						WHERE f.id IN ({0c})'.(!$viewhidden ? ' AND f.hidden=0' : '')." AND f.redirect=''
						ORDER BY f.forder, f.id", $viewableforums
    );

    $fora = [];
    while ($forum = Fetch($rFora)) {
        $fora[$forum['catid']][] = $forum;
    }

    $theList = '';
    foreach ($cats as $cid=>$cat) {
        if (empty($fora[$cid])) {
            continue;
        }

        $cname = $cat['name'];
        if ($cat['board']) {
            $cname = $forumBoards[$cat['board']].' - '.$cname;
        }

        $theList .=
        '			<optgroup label="'.htmlspecialchars($cname).'">
'.mfl_forumBlock($fora, $cid, $selectedID, 0).
        '			</optgroup>
';
    }

    return "<select id=\"$fieldname\" class=\"form-control z-depth-1\" name=\"$fieldname\">$noneOption$theList</select>";
}

function forumCrumbs($forum)
{
    global $forumBoards;
    $ret = [pageLink('board') => __('Forums')];

    if (!empty($forum['board'])) {
        $ret[pageLink('board', ['id' => $forum['board']])] = $forumBoards[$forum['board']];
    }

    if (!isset($forum['id'])) {
        return $ret;
    }

    $parents = Query('SELECT id,title FROM {forums} WHERE l<{0} AND r>{1} ORDER BY l', $forum['l'], $forum['r']);
    while ($p = Fetch($parents)) {
        $public = HasPermission('forum.viewforum', $p['id'], true);
        $ret['/'.actionLink('forum', $p['id'], '', $public ? $p['title'] : '')] = $p['title'];
    }

    $public = HasPermission('forum.viewforum', $forum['id'], true);
    $ret['/'.actionLink('forum', $forum['id'], '', $public ? $forum['title'] : '')] = $forum['title'];

    return $ret;
}

function doThreadPreview($tid, $maxdate = 0)
{
    global $loguser, $ckeditor_color;

    $review = [];
    $ppp = $loguser['postsperpage'] ?: 20;

    $rPosts = Query(
        'SELECT
						{posts}.id, {posts}.date, {posts}.num, {posts}.deleted, {posts}.options, {posts}.mood, {posts}.ip,
						{posts_text}.text, {posts_text}.text, {posts_text}.revision,
						u.(_userfields), u.(posts)
					FROM {posts}
					LEFT JOIN {posts_text} on {posts_text}.pid = {posts}.id AND {posts_text}.revision = {posts}.currentrevision
					LEFT JOIN {users} u on u.id = {posts}.user
					WHERE thread={0} AND deleted=0'.($maxdate ? ' AND {posts}.date<={1}' : '').'
					ORDER BY date DESC LIMIT 0, {2u}', $tid, $maxdate, $ppp
    );

    while ($post = Fetch($rPosts)) {
        $pdata = ['id' => $post['id']];

        $poster = getDataPrefix($post, 'u_');
        $pdata['userlink'] = UserLink($poster);

        $pdata['posts'] = $post['num'].'/'.$poster['posts'];

        $nosm = $post['options'] & 2;
        $pdata['contents'] = CleanUpPost($post['text'], $poster['name'], $nosm);

        $review[] = $pdata;
    }

    RenderTemplate('threadreview', ['review' => $review, 'ckeditor' => $ckeditor_color]);
}

function makeCrumbs($path, $links = '', $dropdown = '')
{
    global $layout_crumbs, $layout_actionlinks, $layout_dropdown;

    if (count($path) != 0) {
        $pathPrefix = [pageLink('home') => '<i class="fa fa-home"></i>'];

        $bucket = 'breadcrumbs';
        include __DIR__.'/pluginloader.php';

        $path = $pathPrefix + $path;
    }

    $layout_crumbs = $path;
    $layout_actionlinks = $links;
    $layout_dropdown = $dropdown;
}

// parent=0: index listing
function makeForumListing($parent, $board = '', $template = 'forumlist', $category = '')
{
    global $loguserid, $loguser, $usergroups;

    $viewableforums = ForumsWithPermission('forum.viewforum');
    $viewhidden = HasPermission('user.viewhiddenforums');

    $rFora = Query(
        '	SELECT f.*,
							c.name cname,
							'.($loguserid ? '(NOT ISNULL(i.fid))' : '0').' ignored,
							(SELECT COUNT(*) FROM {threads} t'.($loguserid ? ' LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={0}' : '').'
								WHERE t.forum=f.id AND t.lastpostdate>'.($loguserid ? 'IFNULL(tr.date,0)' : time() - 900).') numnew,
							lu.(_userfields)
						FROM {forums} f
							LEFT JOIN {categories} c ON c.id=f.catid
							'.($loguserid ? 'LEFT JOIN {ignoredforums} i ON i.fid=f.id AND i.uid={0}' : '').'
							LEFT JOIN {users} lu ON lu.id=f.lastpostuser
						WHERE f.id IN ({1c}) AND '.($parent == 0 ? 'c.board={2} AND f.catid>0' : 'f.catid={3}').(!$viewhidden ? ' AND f.hidden=0' : '').(!empty((int) $category) ? ' AND c.id={4} ' : '').'
						ORDER BY c.corder, c.id, f.forder, f.id',
        $loguserid,
        $viewableforums,
        $board,
        -$parent,
        (int) $category
    );
    if (!NumRows($rFora)) {
        if ($template == 'array') {
            return [];
        } else {
            return;
        }
    }

    $f = Fetch(Query('SELECT MIN(l) minl, MAX(r) maxr FROM {forums} WHERE '.($parent == 0 ? 'board={0}' : 'catid={1}'), $board, -$parent));

    $rSubfora = Query(
        '	SELECT f.*,
							'.($loguserid ? '(NOT ISNULL(i.fid))' : '0').' ignored,
							(SELECT COUNT(*) FROM {threads} t'.($loguserid ? ' LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={0}' : '').'
								WHERE t.forum=f.id AND t.lastpostdate>'.($loguserid ? 'IFNULL(tr.date,0)' : time() - 900).') numnew
						FROM {forums} f
							'.($loguserid ? 'LEFT JOIN {ignoredforums} i ON i.fid=f.id AND i.uid={0}' : '').'
						WHERE f.id IN ({1c}) AND f.l>{2} AND f.r<{3} AND f.catid!={4}'.(!$viewhidden ? ' AND f.hidden=0' : '').'
						ORDER BY f.forder, f.id',
        $loguserid,
        $viewableforums,
        $f['minl'],
        $f['maxr'],
        -$parent
    );
    $subfora = [];
    while ($sf = Fetch($rSubfora)) {
        $subfora[-$sf['catid']][] = $sf;
    }

    $rMods = Query(
        'SELECT 
						p.(arg, applyto, id),
						u.(_userfields)
					FROM {permissions} p
					LEFT JOIN {users} u ON p.applyto=1 AND p.id=u.id
					WHERE SUBSTR(p.perm,1,4)={0} AND p.arg!=0 AND p.value=1
					GROUP BY p.applyto, p.id, p.arg
					ORDER BY p.applyto, p.id',
        'mod.'
    );
    $mods = [];
    while ($mod = Fetch($rMods)) {
        $mods[$mod['p_arg']][] = $mod['p_applyto'] ? getDataPrefix($mod, 'u_') : ['groupid' => $mod['p_id']];
    }

    $categories = [];
    while ($forum = Fetch($rFora)) {
        $skipThisOne = false;
        $bucket = 'forumListMangler';
        include __DIR__.'/pluginloader.php';
        if ($skipThisOne) {
            continue;
        }

        if (!isset($categories[$forum['catid']])) {
            $categories[$forum['catid']] = ['id' => $forum['catid'], 'name' => ($parent == 0) ? $forum['cname'] : 'Subforums', 'forums' => []];
        }

        $fdata = ['id' => $forum['id']];

        if ($forum['redirect']) {
            $redir = $forum['redirect'];
            if ($redir[0] == ':') {
                $redir = explode(':', $redir);
                $fdata['link'] = actionLinkTag($forum['title'], $redir[1], $redir[2], $redir[3], $redir[4]);
                $forum['numthreads'] = '-';
                $forum['numposts'] = '-';

                if ($redir[1] == 'board') {
                    $tboard = $redir[2];
                    $f = Fetch(Query('SELECT MIN(l) minl, MAX(r) maxr FROM {forums} WHERE board={0}', $tboard));

                    $forum['numthreads'] = 0;
                    $forum['numposts'] = 0;
                    $sforums = Query(
                        '	SELECT f.id, f.numthreads, f.numposts, f.lastpostid, f.lastpostuser, f.lastpostdate,
											'.($loguserid ? '(NOT ISNULL(i.fid))' : '0').' ignored,
											(SELECT COUNT(*) FROM {threads} t'.($loguserid ? ' LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={0}' : '').'
												WHERE t.forum=f.id AND t.lastpostdate>'.($loguserid ? 'IFNULL(tr.date,0)' : time() - 900).') numnew,
											lu.(_userfields)
										FROM {forums} f
											'.($loguserid ? 'LEFT JOIN {ignoredforums} i ON i.fid=f.id AND i.uid={0}' : '').'
											LEFT JOIN {users} lu ON lu.id=f.lastpostuser
										WHERE f.l>={1} AND f.r<={2}',
                        $loguserid,
                        $f['minl'],
                        $f['maxr']
                    );
                    while ($sforum = Fetch($sforums)) {
                        $forum['numthreads'] += $sforum['numthreads'];
                        $forum['numposts'] += $sforum['numposts'];

                        if (!HasPermission('forum.viewforum', $sforum['id'])) {
                            continue;
                        }

                        if (!$sforum['ignored']) {
                            $forum['numnew'] += $sforum['numnew'];
                        }

                        if ($sforum['lastpostdate'] > $forum['lastpostdate']) {
                            $forum['lastpostdate'] = $sforum['lastpostdate'];
                            $forum['lastpostid'] = $sforum['lastpostid'];
                            $forum['lastpostuser'] = $sforum['lastpostuser'];
                            foreach ($sforum as $key=>$val) {
                                if (substr($key, 0, 3) != 'lu_') {
                                    continue;
                                }
                                $forum[$key] = $val;
                            }
                        }
                    }
                }
            } else {
                $fdata['link'] = '<a href="'.htmlspecialchars($redir).'">'.parseBBCode($forum['title']).'</a>';
            }
        } else {
            $fdata['link'] = actionLinkTag(
                htmlspecialchars($forum['title']),
                'forum',
                $forum['id'],
                '',
                HasPermission('forum.viewforum', $forum['id'], true) ? parseBBCode($forum['title']) : ''
            );
        }

        $fdata['ignored'] = $forum['ignored'];

        $newstuff = 0;
        $localMods = '';
        $subforaList = '';

        $newstuff = $forum['ignored'] ? 0 : $forum['numnew'];
        if ($newstuff > 0) {
            $fdata['new'] = '<div class="statusIcon new">'.$newstuff.' </div>';
        }

        $fdata['description'] = parseBBCode($forum['description']);

        if (isset($mods[$forum['id']])) {
            foreach ($mods[$forum['id']] as $user) {
                if ($user['groupid']) {
                    $localMods .= htmlspecialchars($usergroups[$user['groupid']]['name']).', ';
                } else {
                    $localMods .= UserLink($user).', ';
                }
            }
        }

        if ($localMods) {
            $fdata['localmods'] = substr($localMods, 0, -2);
        }

        if (isset($subfora[$forum['id']])) {
            foreach ($subfora[$forum['id']] as $subforum) {
                $link = actionLinkTag(
                    __($subforum['title']),
                    'forum',
                    $subforum['id'],
                    '',
                    HasPermission('forum.viewforum', $subforum['id'], true) ? $subforum['title'] : ''
                );

                if ($subforum['ignored']) {
                    $link = '<span class="ignored">'.$link.'</span>';
                } elseif ($subforum['numnew'] > 0) {
                    $link = '<div class="statusIcon new"></div> '.$link;
                }

                $subforaList .= $link.', ';
            }
        }

        if ($subforaList) {
            $fdata['subforums'] = substr($subforaList, 0, -2);
        }

        $fdata['threads'] = $forum['numthreads'];
        $fdata['posts'] = $forum['numposts'];

        if ($forum['lastpostdate']) {
            $user = getDataPrefix($forum, 'lu_');

            $fdata['lastpostdate'] = formatdate($forum['lastpostdate']);
            $fdata['lastpostuser'] = UserLink($user);
            $fdata['lastpostlink'] = actionLink('post', $forum['lastpostid']);
        } else {
            $fdata['lastpostdate'] = 0;
        }

        $categories[$forum['catid']]['forums'][$forum['id']] = $fdata;
    }

    if ($template == 'array') {
        return $categories;
    } else {
        RenderTemplate($template, ['categories' => $categories]);
    }
}

function makeSubForumListing($parent, $board = '')
{
    makeForumListing($parent, $board, 'subforumlist');
}

function makeThreadListing($threads, $pagelinks, $dostickies = true, $showforum = false)
{
    global $loguserid, $loguser, $misc;

    $threadlist = [];
    while ($thread = Fetch($threads)) {
        $tdata = ['id' => $thread['id']];
        $starter = getDataPrefix($thread, 'su_');
        $last = getDataPrefix($thread, 'lu_');
        $fid = $thread['forum'];

        $ispublic = HasPermission('forum.viewforum', $thread['forum'], true);
        $tdata['link'] = MakeAnnounceLink($thread);
        $tdata['description'] = parseBBCode($thread['description']);

        $NewIcon = '';

        if ($thread['closed']) {
            $NewIcon = 'off';
        }
        if ($thread['replies'] >= $misc['hotcount']) {
            $NewIcon .= 'hot';
        }
        if ((!$loguserid && $thread['lastpostdate'] > time() - 900)
            || ($loguserid && $thread['lastpostdate'] > $thread['readdate'])
        ) {
            $NewIcon .= 'new';
            if ($loguserid) {
                $tdata['gotonew'] = actionLinkTag(
                    '<img src="'.resourceLink('img/gotounread.png').'" alt="[go to first unread post]">',
                    'post',
                    '',
                    'tid='.$thread['id'].'&time='.(int) $thread['readdate']
                );
            }
        }

        if ($NewIcon) {
            $tdata['new'] = '<div class="statusIcon '.$NewIcon.'"></div>';
        }

        $tdata['sticky'] = $thread['sticky'];

        if ($thread['icon']) {
            if (startsWith($thread['icon'], 'fa')) {
                $tdata['icon'] = '<i class="fa fa-lg '.htmlspecialchars($thread['icon']).'"></i>';
            } else {
                //This is a hack, but given how icons are stored in the DB, I can do nothing about it without breaking DB compatibility.
                if (startsWith($thread['icon'], 'img/')) {
                    $thread['icon'] = resourceLink($thread['icon']);
                }
                $tdata['icon'] = '<img src="'.htmlspecialchars($thread['icon']).'" alt="" class="smiley" style="max-width:32px; max-height:32px;">';
            }
        }

        if ($thread['poll']) {
            $tdata['poll'] = '<img src="'.resourceLink('img/poll.png').'" alt="[poll]">';
        }

        $n = 4;
        $total = $thread['replies'];

        $ppp = $loguser['postsperpage'];
        if (!$ppp) {
            $ppp = 20;
        }

        $numpages = floor($total / $ppp);
        $pl = [];
        if ($numpages <= $n * 2) {
            for ($i = 1; $i <= $numpages; $i++) {
                $pl[] = ['link' => actionLink('thread', $thread['id'], 'from='.($i * $ppp), $urlname), 'text' => $i + 1];
            }
        } else {
            for ($i = 1; $i < $n; $i++) {
                $pl[] = ['link' => actionLink('thread', $thread['id'], 'from='.($i * $ppp), $urlname), 'text' => $i + 1];
            }
            $pl[] = ['disabled' => true, 'text' => '&hellip;'];
            for ($i = $numpages - $n + 1; $i <= $numpages; $i++) {
                $pl[] = ['link' => actionLink('thread', $thread['id'], 'from='.($i * $ppp), $urlname), 'text' => $i + 1];
            }
        }
        if (count($pl)) {
            $pageLinks = '<li class="page-item"><a class="page-link" href="'.actionLink('thread', $thread['id'], '', $urlname).'">1</a></li>';
            foreach ($pl as $pageLink) {
                $pageLinks .= '<li class="page-item '.($pageLink['disabled'] ? 'disabled' : '').'">
								<a class="page-link" '.($pageLink['link'] ? 'href="'.$pageLink['link'].'"' : '').'>
									'.$pageLink['text'].'
								</a>
								</li>';
            }
            $tdata['pagelinks'] = $pageLinks;
        }

        if ($showforum) {
            $tdata['forumlink'] = actionLinkTag(htmlspecialchars($thread['f_title']), 'forum', $thread['f_id'], '', $ispublic ? $thread['f_title'] : '');
        }

        $tdata['startuser'] = UserLink($starter);

        $tdata['replies'] = $thread['replies'];
        $tdata['views'] = $thread['views'];

        $tdata['postdate'] = formatdate($thread['date']);
        $tdata['lastpostdate'] = formatdate($thread['lastpostdate']);
        $tdata['lastpostuser'] = UserLink($last);
        $tdata['lastpostlink'] = pageLink('post', ['id' => $thread['lastpostid']]);

        $tdata['quicklinks']['twitter'] = [
            'url'  => 'http://twitter.com/share?text='.$threadtags[0].'&url='.getServerDomainNoSlash().actionLink('thread', $tdata['id']).'/&via=MarioMakingMods',
            'icon' => 'twitter',
            'text' => __('Tweet'), ];

        if ($loguserid) {
            if (FetchResult('SELECT COUNT(*) FROM {favorites} WHERE user={0} AND thread={1}', $loguserid, $tid) > 0) {
                $tdata['quicklinks']['star'] = [
                    'url'  => actionLink('favorites', $tdata['id'], 'action=remove&token='.$loguser['token']),
                    'icon' => 'star-o',
                    'text' => __('Unstar Thread'), ];
            } else {
                $tdata['quicklinks']['star'] = [
                    'url'  => actionLink('favorites', $tdata['id'], 'action=add&token='.$loguser['token']),
                    'icon' => 'star',
                    'text' => __('Star Thread'), ];
            }

            $notclosed = (!$thread['closed'] || HasPermission('mod.closethreads', $fid));

            // we also check mod.movethreads because moving threads is done on editthread
            if ((HasPermission('forum.renameownthreads', $fid) && $thread['user'] == $loguserid)
             || (HasPermission('mod.renamethreads', $fid) || HasPermission('mod.movethreads', $fid))
                && $notclosed
            ) {
                $tdata['quicklinks']['edit'] = ['url' => actionLink('editthread', $thread['id']), 'icon' => 'pencil', 'text' => __('Edit')];
            }
        }

        $threadlist[$tdata['id']] = $tdata;
    }

    RenderTemplate('threadlist', ['threads' => $threadlist, 'pagelinks' => $pagelinks, 'dostickies' => $dostickies, 'showforum' => $showforum]);
}

function makeAnncBar($anncboard = '')
{
    global $loguserid;

    switch ($anncboard) {
    case 'staff':
        $cacheName = 'staffanncBar';
        $anncforum = Settings::get('StaffanncForum');
        $headName = __('Staff Announcements');
        break;
    case 'depot':
        $cacheName = 'depotanncBar';
        $anncforum = 3;
        $headName = __('Project Announcements');
        break;
    default:
        $cacheName = 'anncBar';
        $anncforum = Settings::get('anncForum');
        $headName = __('Announcements');
    }

    $annc = Query(
        '	SELECT 
						t.id, t.title, t.icon, t.poll, t.forum,
						t.date anncdate,
						'.($loguserid ? 'tr.date readdate,' : '').'
						u.(_userfields)
					FROM 
						{threads} t 
						'.($loguserid ? 'LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={1}' : '').'
						LEFT JOIN {users} u ON u.id=t.user
					WHERE forum={0}
					ORDER BY anncdate DESC LIMIT 1', $anncforum, $loguserid
    );

    if ($annc && NumRows($annc)) {
        $annc = Fetch($annc);
        $adata = [];

        $adata['new'] = '';
        if ((!$loguserid && $annc['anncdate'] > (time() - 900))
            || ($loguserid && $annc['anncdate'] > $annc['readdate'])
        ) {
            $adata['new'] = '<div class="statusIcon new"></div>';
        }

        if ($annc['poll']) {
            $adata['poll'] = '<img src="'.resourceLink('img/poll.png').'" alt="Poll"/>';
        }
        $adata['links'] = MakeAnnounceLink($annc);
        $adata['name'] = $headName;

        $user = getDataPrefix($annc, 'u_');
        $adata['user'] = UserLink($user);
        $adata['date'] = formatdate($annc['anncdate']);

        RenderTemplate('anncbar', ['annc' => $adata, 'link' => $link]);
    }
}
