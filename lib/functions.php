<?php

// AcmlmBoard XD support - Handy snippets
// TODO organize better
if (!defined('BLARG')) {
    die();
}

function endsWith($a, $b)
{
    return substr($a, strlen($a) - strlen($b)) == $b;
}

function endsWithIns($a, $b)
{
    return endsWith(strtolower($a), strtolower($b));
}

function startsWith($a, $b)
{
    return substr($a, 0, strlen($b)) == $b;
}

function startsWithIns($a, $b)
{
    return startsWith(strtolower($a), strtolower($b));
}

function Alert($s, $t = 'Notice', $a = 'success')
{
    RenderTemplate(
        'messagebox',
        ['msgtitle'       => $t,
                'message' => $s,
                'alert'   => $a, ]
    );
}

function smarty_function_Alert($params, &$smarty) {
    if (!isset($params['title']) || (isset($params['title']) && empty($params['title']))) {
		$params['title'] = __("Notice");
	}

	if (!isset($params['message']) || (isset($params['message']) && empty($params['message']))) {
		$smarty->trigger_error("You cannot make an alert with a blank message");
	}

	if (!isset($params['color']) || (isset($params['color']) && empty($params['color']))) {
		$params['color'] = 'success';
	}

	return Alert($params['message'], $params['title'], $params['color']);
}

function Kill($s, $t = 'Error', $a = 'danger')
{
    Alert($s, $t, $a);

    throw new KillException();
}

// This function is only used for the edit forums page
function dieAjax($what)
{
    global $ajaxPage;

    echo $what;
    $ajaxPage = true;
    exit;

    throw new KillException();
}

// returns FALSE if it fails.
function QueryURL($url)
{
    if (function_exists('curl_init')) {
        $page = curl_init($url);
        if ($page === false) {
            return false;
        }

        curl_setopt($page, CURLOPT_TIMEOUT, 10);
        curl_setopt($page, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($page, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($page, CURLOPT_USERAGENT, 'Blargboard/'.BLARG_VERSION);

        $result = curl_exec($page);
        curl_close($page);

        return $result;
    } elseif (ini_get('allow_url_fopen')) {
        return file_get_contents($url);
    } else {
        return false;
    }
}

function OldRedirect($s, $t, $n)
{
    RenderTemplate(
        'oldRedirect', [
        'action'       => $s,
        'redirectLink' => $t,
        'redirectText' => format(__('You will now be redirected to {0}&hellip;'), '<a href="'.$t.'">'.$n.'</a>'),
        ]
    );

    throw new KillException();
}

function SendSystemPM($to, $message, $title)
{
    global $db;
    $userID = 197;

    $pid = mt_rand();
    $pxist = FetchResult('SELECT * FROM {posts} WHERE id = {0}', $pid);
    $pnuke = FetchResult("SELECT * FROM {nuked} WHERE id = {0} AND type = 'pm'", $pid);
    while ($pnuke == $pid or $pxist == $pid) {
        while ($pxist == $pid) {
            $pid = mt_rand();
            $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
        }
        $pid = mt_rand();
        $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
        $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'pm'", $pid);
    }

    $db->insert('pmsgs_text', ['pid' => $pid, 'title' => $title, 'text' => $message]);
    $db->insert('pmsgs', [
        'id' => $pid, 'userto' => $to, 'userfrom' => $userID, 'date' => $db->time(),
        'ip' => '127.0.0.1', 'msgread' => 0, 'drafting' => 0,
    ]);

    SendNotification('pm', $pid, $to);
}

function format()
{
    $argc = func_num_args();
    if ($argc == 1) {
        return func_get_arg(0);
    }
    $args = func_get_args();
    $output = $args[0];
    for ($i = 1; $i < $argc; $i++) {
        // TODO kill that hack
        $splicethis = preg_replace("'\{([0-9]+)\}'", '&#x7B;\\1&#x7D;', $args[$i]);
        $output = str_replace('{'.($i - 1).'}', $splicethis, $output);
    }

    return $output;
}

// TODO NUKE
// This function can be found in userbadges.php.
// So basically, templatize that page then we can remove this function
function write()
{
    $argc = func_num_args();
    if ($argc == 1) {
        echo func_get_arg(0);

        return;
    }
    $args = func_get_args();
    $output = $args[0];
    for ($i = 1; $i < $argc; $i++) {
        // TODO kill that hack
        $splicethis = preg_replace("'\{([0-9]+)\}'", '&#x7B;\\1&#x7D;', $args[$i]);
        $output = str_replace('{'.($i - 1).'}', $splicethis, $output);
    }
    echo $output;
}

function LoadPostToolbar()
{
    echo '<script type="text/javascript">window.addEventListener("load", hookUpControls, false);</script>';
}

function TimeUnits($sec)
{
    if ($sec < 60) {
        return $sec.' sec.';
    }
    if ($sec < 3600) {
        return floor($sec / 60).' min.';
    }
    if ($sec < 86400) {
        return floor($sec / 3600).' hour'.($sec >= 7200 ? 's' : '');
    }

    return floor($sec / 86400).' day'.($sec >= 172800 ? 's' : '');
}

function cdate($format, $date = 0)
{
    global $loguser;
    if ($date == 0) {
        $date = time();
    }

    return gmdate($format, $date + $loguser['timezone']);
}

function Report($stuff, $hidden = 0, $severity = 0)
{
    global $db;
    $full = getFullRequestedURL();
    $here = substr($full, 0, strrpos($full, '/'));

    $db->insert('reports', ['ip' => $_SERVER['REMOTE_ADDR'], 'user' => (int) $loguserid, 'time' => $db->time(), 'text' => str_replace('#HERE#', $here, $stuff), 'hidden' => $hidden, 'severity' => $severity, 'request' => 'NULL']);
}

function Shake()
{
    $cset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789';
    $salt = '';
    $chct = strlen($cset) - 1;
    while (strlen($salt) < 16) {
        $salt .= $cset[mt_rand(0, $chct)];
    }

    return $salt;
}

function BytesToSize($size, $retstring = '%01.2f&nbsp;%s')
{
    $sizes = ['B', 'KiB', 'MiB'];
    $lastsizestring = end($sizes);
    foreach ($sizes as $sizestring) {
        if ($size < 1024) {
            break;
        }
        if ($sizestring != $lastsizestring) {
            $size /= 1024;
        }
    }
    if ($sizestring == $sizes[0]) {
        $retstring = '%01d %s';
    } // Bytes aren't normally fractional
    return sprintf($retstring, $size, $sizestring);
}

function makeThemeArrays()
{
    global $themes, $themefiles;
    $themes = [];
    $themefiles = [];
    $dir = @opendir('themes');
    while ($file = readdir($dir)) {
        if ($file != '.' && $file != '..') {
            $themefiles[] = $file;
            $name = explode("\n", @file_get_contents("./themes/$file/themeinfo.txt"));
            $themes[] = trim($name[0]);
        }
    }
    closedir($dir);
}

function getdateformat()
{
    global $loguserid, $loguser;

    if ($loguserid) {
        return $loguser['dateformat'].', '.$loguser['timeformat'];
    } else {
        return Settings::get('dateformat');
    }
}

function formatdate($date)
{
    return cdate(getdateformat(), $date);
}
function formatdatenow()
{
    // Acmlmboard theme function. Oh well
    return cdate(getdateformat());
}
function relativedate($date)
{
    return '<time class="timeago" datetime="'.date(DATE_ATOM, $date).'">'.formatdate($date).'</time>';
}

function formatBirthday($b)
{
    if (empty($b)) {
        return '';
    }

    return format(__('{0} ({1} old)'), date('F j, Y', $b), Plural(floor((time() - $b) / 86400 / 365.2425), 'year'));
}

function getSexName($sex)
{
    $sexes = [
        0 => __('Male'),
        1 => __('Female'),
        2 => __('N/A'),
    ];

    return $sexes[$sex];
}

function formatIP($ip)
{
    global $loguser;

    $res = '<nobr>'.$ip.' '.IP2C($ip).'</nobr>';
    if (HasPermission('admin.ipsearch')) {
        $ip = ip2long_better($ip);

        return actionLinkTag($res, 'ipquery', $ip);
    } else {
        return $res;
    }
}

function ip2long_better($ip)
{
    return ip2long($ip);
    $v = explode('.', $ip);

    return ($v[0] * 16777216) + ($v[1] * 65536) + ($v[2] * 256) + $v[3];
}

function long2ip_better($ip)
{
    return long2ip((float) $ip);
}

//TODO: Optimize it so that it can be made with a join in online.php and other places.
function IP2C($ip)
{
    // Only used on Last Known Browsers page
    global $dblink;
    //This nonsense is because ips can be greater than 2^31, which will be interpreted as negative numbers by PHP.
    $ipl = ip2long($ip);
    $r = Fetch(
        Query(
            'SELECT * 
				 FROM {ip2c}
				 WHERE ip_from <= {0s} 
				 ORDER BY ip_from DESC
				 LIMIT 1',
            sprintf('%u', $ipl)
        )
    );
}

function getBirthdaysText($ret = true)
{
    global $luckybastards, $loguser;

    $luckybastards = [];
    $today = gmdate('m-d', time() + $loguser['timezone']);

    $rBirthdays = Query('SELECT u.birthday, u.(_userfields) FROM {users} u WHERE u.birthday > 0 AND u.primarygroup!={0} ORDER BY u.name', Settings::get('bannedGroup'));
    $birthdays = [];
    while ($user = Fetch($rBirthdays)) {
        $b = $user['birthday'];
        if (gmdate('m-d', $b) == $today) {
            $luckybastards[] = $user['u_id'];
            if ($ret) {
                $y = gmdate('Y') - gmdate('Y', $b);
                $birthdays[] = UserLink(getDataPrefix($user, 'u_'))." ($y)";
            }
        }
    }
    if (!$ret) {
        return '';
    }
    if (count($birthdays)) {
        $birthdaysToday = implode(', ', $birthdays);
    }
    if (isset($birthdaysToday)) {
        return __('We wish a happy birthday to: ').$birthdaysToday;
    } else {
        return '';
    }
}

function getKeywords($stuff)
{
    $common = ['the', 'and', 'that', 'have', 'for', 'not', 'this'];

    $stuff = strtolower($stuff);
    $stuff = str_replace('\'s', '', $stuff);
    $stuff = preg_replace('@[^\w\s]+@', '', $stuff);
    $stuff = preg_replace('@\s+@', ' ', $stuff);

    $stuff = explode(' ', $stuff);
    $stuff = array_unique($stuff);
    $finalstuff = '';
    foreach ($stuff as $word) {
        if (strlen($word) < 3 && !is_numeric($word)) {
            continue;
        }
        if (in_array($word, $common)) {
            continue;
        }

        $finalstuff .= $word.' ';
    }

    return substr($finalstuff, 0, -1);
}

function forumRedirectURL($redir)
{
    if ($redir[0] == ':') {
        $redir = explode(':', $redir);

        return actionLink($redir[1], $redir[2], $redir[3], $redir[4]);
    } else {
        return $redir;
    }
}

function entity_fix__callback($matches)
{
    if (!isset($matches[2])) {
        return '';
    }

    $num = $matches[2][0] === 'x' ? hexdec(substr($matches[2], 1)) : (int) $matches[2];

    // we don't allow control characters, characters out of range, byte markers, etc
    if ($num < 0x20 || $num > 0x10FFFF || ($num >= 0xD800 && $num <= 0xDFFF) || $num == 0x202D || $num == 0x202E) {
        return '';
    } else {
        return '&#'.$num.';';
    }
}

function utfmb4_fix($string)
{
    $i = 0;
    $len = strlen($string);
    $new_string = '';
    while ($i < $len) {
        $ord = ord($string[$i]);
        if ($ord < 128) {
            $new_string .= $string[$i];
            $i++;
        } elseif ($ord < 224) {
            $new_string .= $string[$i].$string[$i + 1];
            $i += 2;
        } elseif ($ord < 240) {
            $new_string .= $string[$i].$string[$i + 1].$string[$i + 2];
            $i += 3;
        } elseif ($ord < 248) {
            // Magic happens.
            $val = (ord($string[$i]) & 0x07) << 18;
            $val += (ord($string[$i + 1]) & 0x3F) << 12;
            $val += (ord($string[$i + 2]) & 0x3F) << 6;
            $val += (ord($string[$i + 3]) & 0x3F);
            $new_string .= '&#'.$val.';';
            $i += 4;
        }
    }

    return $new_string;
}

function utfmb4String($string)
{
    return utfmb4_fix(preg_replace_callback('~(&#(\d{1,7}|x[0-9a-fA-F]{1,6});)~', 'entity_fix__callback', $string));
}

function checknumeric(&$var)
{
    if (!is_numeric($var)) {
        $var = 0;

        return false;
    }

    return true;
}

function timeunits2($sec)
{
    $d = floor($sec / 86400);
    $h = floor($sec / 3600) % 24;
    $m = floor($sec / 60) % 60;
    $s = $sec % 60;
    $ds = ($d > 1 ? 's' : '');
    $hs = ($h > 1 ? 's' : '');
    $str = ($d ? "$d day$ds " : '').($h ? "$h hour$hs " : '').($m ? "$m min. " : '').($s ? "$s sec." : '');
    if (substr($str, -1) == ' ') {
        $str = substr_replace($str, '', -1);
    }

    return $str;
}

function percentageOf($number, $everything, $decimals = 2)
{
    return round($number / $everything * 100, $decimals);
}

//To remove all the hidden text not displayed on a webpage
function strip_html_tags($str)
{
    $invitags = []; //Start declaring all invisible tags

    $invitags[] = '@<head[^>]*?>.*?</head>@siu';
    $invitags[] = '@<style[^>]*?>.*?</style>@siu';
    $invitags[] = '@<script[^>]*?.*?</script>@siu';
    $invitags[] = '@<noscript[^>]*?.*?</noscript>@siu';

    $str = preg_replace('/(<|>)\1{2}/is', '', $str);
    foreach ($invitags as $invitag) {
        $str = preg_replace($invitag, '', $str);
    }
    $str = replaceWhitespace($str);
    $str = strip_tags($str);

    return $str;
}

//To replace all types of whitespace with a single space
function replaceWhitespace($str)
{
    $result = $str;
    foreach ([
    '  ', " \t",  " \r",  " \n",
    "\t\t", "\t ", "\t\r", "\t\n",
    "\r\r", "\r ", "\r\t", "\r\n",
    "\n\n", "\n ", "\n\t", "\n\r",
    ] as $replacement) {
        $result = str_replace($replacement, $replacement[0], $result);
    }

    return $str !== $result ? replaceWhitespace($result) : $result;
}

function drawminibar($width, $height, $progress, $image = '/gfx/lib/bar/minibar.png')
{
    if ($width != 0) {
        $on = round($progress * 100 / $width);
    } else {
        $on = 0;
    }

    return "<img src='{$image}' style='float: left; width: {$on}%; height: {$height}px'>";
}

function checkForImage(&$image, $external, $file)
{
    if ($image) {
        return;
    }

    if ($external) {
        if (file_exists(DATA_DIR.$file)) {
            $image = DATA_URL.$file;
        }
    } else {
        if (file_exists($file)) {
            $image = $file;
        }
    }
}

function changewithinrange($num, $max, $min)
{
    if ($num > $max) {
        return $max;
    } elseif ($num < $min) {
        return $min;
    } else {
        return $num;
    }
}

function checkbox($field, $name, $check = false)
{
    return '
		<div class="custom-control custom-checkbox custom-control-inline">
			<input type="checkbox" '.($value ? 'checked' : '').' class="custom-control-input" id="'.$field.'" name="'.$field.'">
			<label class="custom-control-label" for="'.$field.'">'.$name.'</label>
		</div>';
}
