<?php

if (!defined('BLARG')) {
    die();
}

// notification formatting callbacks for each notification type
$NotifFormat = [
    'pm'             => 'FormatNotif_PM',
    'profilecomment' => 'FormatNotif_ProfileComment',
    'report'         => 'FormatNotif_Report',
    'favorite'       => 'FormatNotif_Favorite',
];

// plugins should use an init hook to extend $NotifFormat

function FormatNotif_Favorite($id, $args)
{
    global $loguserid;

    $thread = Fetch(
        Query(
            '	SELECT 
								t.*,
								u.(_userfields) 
							FROM {threads} t 
							LEFT JOIN {users} u ON u.id=t.lastposter 
							WHERE t.id={0}', $id
        )
    );
    $userdata = getDataPrefix($thread, 'u_');

    if (!HasPermission('forum.viewforum', $thread['forum'])) {
        return;
    }

    return format(__('{0} responded to {1}'), UserLink($userdata), makeThreadLinkNoTags($thread));
}

function FormatNotif_PM($id, $args)
{
    global $loguserid;

    $staffpm = '';
    if (HasPermission('admin.viewstaffpms')) {
        $staffpm = ' OR p.userto=-1';
    }

    $pm = Fetch(
        Query(
            "	SELECT 
							p.id,
							pt.title pmtitle, 
							u.(_userfields) 
						FROM {pmsgs} p 
						LEFT JOIN {pmsgs_text} pt ON pt.pid=p.id 
						LEFT JOIN {users} u ON u.id=p.userfrom 
						WHERE p.id={0} AND (p.userto={1}{$staffpm})",
            $id,
            $loguserid
        )
    );
    $userdata = getDataPrefix($pm, 'u_');

    //return '<a href="'.actionLink('showprivate', $pm['id']).'">'
    //.format(__('New private message from {0}'), UserLink($userdata))
    //.'</a>';

    return actionLinkTag(format(__('New private message from {0}'), UserLink($userdata)), 'showprivate', $pm['id']);
}

function FormatNotif_Report($id, $args)
{
    global $loguserid;

    $staffpm = '';
    if (HasPermission('admin.viewstaffpms')) {
        $staffpm = ' OR p.userto=-1';
    }

    $pm = Fetch(
        Query(
            "	SELECT 
							p.id,
							pt.title pmtitle, 
							u.(_userfields) 
						FROM 
							{pmsgs} p 
							LEFT JOIN {pmsgs_text} pt ON pt.pid=p.id 
							LEFT JOIN {users} u ON u.id=p.userfrom 
						WHERE 
							p.id={0} AND (p.userto={1}{$staffpm})",
            $id,
            $loguserid
        )
    );
    $userdata = getDataPrefix($pm, 'u_');

    return __('New post report from ').UserLink($userdata)."\n".
        actionLinkTag(htmlspecialchars($pm['pmtitle']), 'showprivate', $pm['id']);
}

function FormatNotif_ProfileComment($id, $args)
{
    global $loguserid, $loguser;

    return __('New comments in ').actionLinkTag(__('your profile'), 'profile', $loguserid, '', $loguser['name']);
}

function notifsort($a, $b)
{
    if ($a['date'] == $b['date']) {
        return 0;
    }

    return ($a['date'] > $b['date']) ? -1 : 1;
}

function GetNotifications()
{
    global $loguserid, $NotifFormat;
    if (!$loguserid) {
        return [];
    }

    $notifs = [];

    // TODO do it better!
    $staffnotif = '';
    if (HasPermission('admin.viewstaffpms')) {
        $staffnotif = 'OR user=-1';
    }

    $ndata = Query("SELECT type, id, date, args FROM {notifications} WHERE user={0} {$staffnotif} ORDER BY date DESC", $loguserid);
    while ($n = Fetch($ndata)) {
        $ncb = $NotifFormat[$n['type']];
        if (function_exists($ncb)) {
            $ndesc = $ncb($n['id'], $n['args'] ? unserialize($n['args']) : null);

            $notifs[] = [
                'date'          => $n['date'],
                'formattedDate' => relativedate($n['date']),
                'text'          => $ndesc,
            ];
        }
    }

    return $notifs;
}

// type: notification type (private messages, profile comments, etc)
// id: identifier for this notification, should be unique (for example, for PMs the PM ID is used as an ID)
// args: notification-specific args, used by the format callbacks
function SendNotification($type, $id, $user, $args = null)
{
    $argstr = $args ? serialize($args) : '';
    $now = time();

    Query(
        '
		INSERT INTO {notifications} (type,id,user,date,args) VALUES ( {0} , {1} , {2} , {3} , {4} )
		ON DUPLICATE KEY UPDATE date={3}, args={4}',
        $type,
        $id,
        $user,
        $now,
        $argstr
    );

    $bucket = 'sendNotification';
    include __DIR__.'/pluginloader.php';
}

function DismissNotification($type, $id, $user)
{
    //In standard blargboard, it doesn't check if the notif actually exists before deleting it.
    //Just as a fail safe for the future, I've added a check whether the notification actually exists before deleting it
    //Who knows what the future of MySQL holds for litle old blargboard here. :P
    $checkRows = Query('SELECT * FROM {notifications} WHERE type={0} AND id={1} AND user={2}', $type, $id, $user);
    if (NumRows($checkRows)) {
        Query('DELETE FROM {notifications} WHERE type={0} AND id={1} AND user={2}', $type, $id, $user);
    }

    $bucket = 'dismissNotification';
    include __DIR__.'/pluginloader.php';
}
