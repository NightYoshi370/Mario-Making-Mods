<?php

if (!defined('BLARG')) {
    die();
}
require __DIR__.'/email.php';

function do403()
{
    header('HTTP/2.0 403 Forbidden');
    header('Status: 403 Forbidden');
    die('403 Forbidden');
}

function do404()
{
    header('HTTP/2.0 404 Not Found');
    header('Status: 404 Not Found');
    die('404 Not Found');
}

// spamdexing in referrals/useragents
if (isset($_SERVER['HTTP_REFERER'])) {
    if (stristr($_SERVER['HTTP_REFERER'], '<a href=')
        || stristr($_SERVER['HTTP_USER_AGENT'], '<a href=')
    ) {
        do403();
    }
}

// spamrefreshing
if (isset($_SERVER['HTTP_REFERER'])) {
    if (stristr($_SERVER['HTTP_REFERER'], 'refreshthis.com')) {
        do403();
    }
}

if ($isBot) {
    // keep SE bots out of certain pages that don't interest them anyway
    // TODO move that code to those individual pages
    $forbidden = ['register', 'login', 'online', 'referrals', 'records', 'lastknownbrowsers'];
    if (in_array($_GET['page'], $forbidden)) {
        do403();
    }
}

//Todo: Detect User Power Level Change, as well as other minor things such as if the MySQL database has some odd paramiters.
