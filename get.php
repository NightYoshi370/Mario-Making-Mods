<?php

define('BLARG', 1);
require __DIR__.'/lib/common.php';

if (isset($_GET['uploader'])) {
    $table = '{uploader}';
} else {
    $table = '{uploadedfiles}';
}

if (isset($_GET['id'])) {
    $entry = Fetch(Query("SELECT * FROM $table WHERE id = {0}", $_GET['id']));
} elseif (isset($_GET['file'])) {
    $entry = Fetch(Query("SELECT * FROM $table WHERE filename = {0}", $_GET['file']));
} else {
    die('Nothing specified.');
}

if (!$entry) {
    die(__('Unknown file ID.'));
}
if ($entry['deldate'] != 0) {
    die(__('No such file.'));
}

$path = DATA_DIR.'uploads/'.$entry['physicalname'];
if (!file_exists($path)) {
    die(__('No such file.'));
}

// TODO detect/store MIME type instead of all that junk?
$fsize = filesize($path);
$parts = pathinfo($entry['filename']);
$ext = strtolower($parts['extension']);
$download = true;

switch ($ext) {
    case 'gif': $ctype = 'image/gif'; $download = false; break;
    case 'bmp': $ctype = 'image/bmp'; $download = false; break;
    case 'apng':
    case 'png': $ctype = 'image/png'; $download = false; break;
    case 'jpeg':
    case 'jpg': $ctype = 'image/jpg'; $download = false; break;
    case 'css': $ctype = 'text/css'; $download = false; break;
    case 'txt': $ctype = 'text/plain'; $download = false; break;
    case 'pdf': $ctype = 'application/pdf'; $download = false; break;
    case 'mp3': $ctype = 'audio/mpeg'; $download = false; break;
    default: $ctype = 'application/force-download'; break;
}

$cachetime = 604800; // 1 week. Should be more than okay. Uploaded files aren't supposed to change.
header('Pragma: public');
header('Expires: 0');
header("Cache-Control: private, post-check={$cachetime}, pre-check=999999999, min-fresh={$cachetime}, max-age={$cachetime}");
header('Content-Type: '.$ctype);

if ($download) {
    header('Content-Disposition: attachment; filename="'.addslashes($entry['filename']).'";');
    if (!$isBot) {
        $db->updateId('uploadedfiles', ['downloads' => pudl::increment()], 'id', $entry['id']);
    }
} else {
    header('Content-Disposition: attachment; filename="'.addslashes($entry['filename']).'"');
}

header('Content-Transfer-Encoding: binary');
header('Content-Length: '.$fsize);

readfile($path);
