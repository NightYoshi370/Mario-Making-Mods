	{if $pagelinks}<div class="smallFonts pages">Pages: {$pagelinks}</div>{/if}
	
	<table class="table table-bordered table-striped">
		<tr class="header1">
			<th>Search results</th>
		</tr>
		<tr class="header0">
			<th>{$resultstext}</th>
		</tr>

		{if $nresults>0}
		{foreach $results as $result}

		<tr class="cell{cycle values='0,1'}">
			<td>
				{$result.link}<br>
				{$result.description}<br>
				<small>By {$result.user} on {$result.formattedDate}</small>
			</td>
		</tr>
		
		{/foreach}
		{/if}
	</table>
	
	{if $pagelinks}<div class="smallFonts pages">Pages: {$pagelinks}</div>{/if}
