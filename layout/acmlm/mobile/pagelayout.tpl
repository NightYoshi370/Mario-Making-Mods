
	<table class="table table-bordered table-striped">
		<tr><td class="center" id="boardHeader">
			<h3>{$boardname}</h3>
			{$layout_views}, {$layout_time}
		</td></tr>
		<tr><td class="center"><ul class="pipemenu">
			{foreach $sidelinks as $cat=>$links}{foreach $links as $url=>$texts}
				<li> <a href="{$url|escape}" class="bold">{$texts.text}</a></li>
			{/foreach}{/foreach}
		</ul></td></tr>
		<tr><td class="center">
			{if $loguserid}{$loguserlink}: {/if}

			<ul class="pipemenu">
				{if $loguserid}
					{$numnotifs=count($notifications)}
					<li class="dropdown">
						<span class="dropdown-toggle" data-toggle="dropdown">
							Notifications
							<span id="notifCount" class="badge {if $numnotifs}badge-danger{else}badge-secondary{/if}">{$numnotifs}</span>
							<span class="caret"></span>
						</span>
						<ul class="dropdown-menu" style="color:black;">
							{if $numnotifs}
								{foreach $notifications as $notif}
									<li id="notifText">{$notif.text}<br><small>{$notif.formattedDate}</small></li>
								{/foreach}
							{else}
								<li id="notifText">You have no new notifications.</li>
							{/if}
						</ul>
					</li>
				{/if}
				{foreach $layout_userpanel as $url=>$texts}<li><a href="{$url|escape}">{$texts.text}</a></li>{/foreach}
				{if $loguserid}<li><a href="#" onclick="$('#logout').submit(); return false;">Log out</a></li>{/if}
			</ul>
		</td></tr>
	</table>

	{capture "breadcrumbs"}{if $layout_crumbs || $layout_actionlinks || $layout_dropdown}
	<nav aria-label="breadcrumb" class="navbar navbar-expand-sm" style="margin-bottom: 0px; border-radius: 0px;">
		{if $layout_crumbs && count($layout_crumbs)}
			<ul class="crumbLinks mr-auto navbar-nav breadcrumb d-none d-md-inline-flex">
				{foreach $layout_crumbs as $url=>$text}<li><a href="{$url|escape}">{$text}</a></li>{/foreach}
			</ul>
		{/if}
		{if ($layout_actionlinks && count($layout_actionlinks)) || ($layout_dropdown && count($layout_dropdown))}
			<ul class="ml-auto navbar-nav pipemenu">
				{if $layout_actionlinks && count($layout_actionlinks)}
					{foreach $layout_actionlinks as $url=>$texts}
						<li><a {if $url}href="{$url|escape}"{/if}
							{if $texts.onclick} onclick="{$texts.onclick}"{/if}>
							{$texts.text}
						</a></li>
					{/foreach}
				{/if}
				{if $layout_dropdown && count($layout_dropdown)}
					{foreach $layout_dropdown as $cat=>$links}
						<li><span class="dropdown">
							<span class="dropdown-toggle" data-toggle="dropdown">
								{$cat}<span class="caret"></span>
							</span>
							<ul class="dropdown-menu">
								{foreach $links as $url=>$texts}
									<li {if $texts.active}class="active"{/if}><a href="{$url|escape}">{$texts.text}</a></li>
								{/foreach}
							</ul>
						</span></li>
					{/foreach}
				{/if}
			</ul>
		{/if}
	</nav>
	{/if}{/capture}

	{$smarty.capture.breadcrumbs}<br>
  
		<div class="row content">
			{if $sidebar}
				<div class="col-lg-2 col-md-3 col-sm-4 sidenav">
					{$sidebar}
				</div>
			{/if}
			<div class="{if $sidebar}col-lg-10 col-md-9 col-sm-8{else}col-sm-12{/if} text-left">
				{$layout_contents}
			</div>
		</div>

	{$smarty.capture.breadcrumbs}
	<table class="table table-bordered">
		<tr><td>
			<span style="float: left;">
				{$layout_credits}
			</span>
			<span style="float:right; text-align: right;">
				<ul class="pipemenu">
					<li><a href="{actionLink page='memberlist'}">Member List</a></li>
					<li><a href="{actionLink page='online'}">Online Users</a></li>
					<li><a href="{actionLink page='records'}">Records</a></li>
					<li><a href="{actionLink page='calendar'}">Calendar</a></li>
					<li><a href="{actionLink page='privacypolicy'}">Privacy policy</a></li>
					{if HasPermission('admin.viewadminpanel')}
						<li><a href="{actionLink page='admin'}">Admin</a></li>
					{/if}
				</ul>
			</span>
		</td></tr>
	</table>
