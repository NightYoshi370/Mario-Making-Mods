	<table class="layout-table">
		<tr>
			<td style="width: 50%; vertical-align: top; padding-right: 0.5em;">
				<table class="outline margin homepage" style="text-align: center;">
					<tr class="cell1">
						<td style="padding:5px;">
							<h1>Welcome to Mario Making Mods</h1>
							We are a community dedicated to bringing you a home for the hacking of Super Mario Maker (Wii U & 3DS).
							Our community is a place for everyone to post their projects, get help and support, and find tutorials.
							Join us to share your very own creations, or feel free to just browse the site and check out some of the awesome projects and tutorials found here.
							<h1>Super Mario Maker Mods</h1>
							We have all sorts of crazy mods for Super Mario Maker, 
							ranging from Super Mario Odyssey in 8-bit style, 
							to the new 32-bit series of custom themes. 
							Whatever you are interested in, you might find it in our Depot. 
							Over at the Depot, we have download links to some of the best mods out there for Super Mario Maker & a great tutorial for newcomers. 
							It is connected with the forum, so you can check out both if you'd wish.
							<h1>Wiki</h1>
							At the Mario Making Mods Wiki, you'll find lots of up-to-date information about all the technical parts of Super Mario Maker. 
							You can submit your own findings there, too!
							<h1>Chat and Hang-Out with our community</h1>
							Come take a look at our Discord Server! It's a great central for quick and general discussions.<br>
							At the Discord server, we have many active members from our forums, who love to help people and generally chat with you!
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 50%; vertical-align: top; padding-left: 0.5em;">
				<table class="outline margin lastactivity">
					<tr class="header1">
						<th>Recent activity</th>
					</tr>
					{foreach $lastactivity as $item}
					<tr class="cell{cycle values='0,1'}">
						<td style="padding:5px;">
							{$item.description}<br>
							<span class="smallFonts">{$item.formattedDate}</span>
						</td>
					</tr>
					{/foreach}
				</table>
			</td>
		</tr>
	</table>
