<div class="row">
	<div class="col-sm-3 col-md-3 col-lg-2">
		<ul class="nav flex-column">
			<li class="nav-item active"><a class="nav-link">{"Edit Profile"|__}</a></li>
			<li class="nav-item"><a class="nav-link" href="{actionLink page='editavatars'}">{"Mood Avatars"|__}</a></li>
			<li class="nav-item"><a class="nav-link" href="{actionLink page='shop'}">{"Item Shop"|__}</a></li>
		</ul>
	</div>
	<div class="col-sm-9 col-md-9 col-lg-10">
		<div class="nav-tabs-wrapper"><ul id="profile-nav" class="nav nav-tabs dragscroll horizontal" role="tablist">
			{foreach $pages as $pageid=>$pname}
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#{$pageid}">{$pname}</a></li>
			{/foreach}
		</ul></div>
		<div class="tab-content">
			<div class="tab-pane fade show active">
				<table class="table table-bordered table-outlined">
					<tr><th>Welcome to the Edit Profile page</th></tr>
					<tr><td>Click on the left to go to different pages<br>Click on the above tabs to edit other parts</td></tr>
				</table>
			</div>
			{foreach $pages as $pageid=>$pname}
				<div id="{$pageid}" class="tab-pane fade">
					<table class="table table-bordered table-outlined">

						{foreach $categories.{$pageid} as $catid=>$cname}
							<tr class="header0"><th colspan=2>{$cname}</th></tr>

							{foreach $fields.{$catid} as $fieldid=>$field}
								<tr class="cell{cycle values='0,1'}">

									{if $field.type == 'themeselector'}
										<td class="themeselector" colspan=2>
											{$field.html}
										</td>
									{else}
										<td class="cell2 center" style="width:20%;">
											{$field.caption}
											{if $field.hint}<br><small>{$field.hint}</small>{/if}
										</td>
										<td>{$field.html}</td>
									{/if}
								</tr>
							{/foreach}
						{/foreach}
						<tr class="cell{cycle values='0,1'}">
							<td class="cell2"></td>
							<td>{$btnEditProfile}</td>
						</tr>
					</table>
				</div>
			{/foreach}
		</div>
	</div>
</div>