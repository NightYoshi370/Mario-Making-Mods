	<div class="row">
		<div class="col-md-6 col-lg-8 col-uhd-9">
			{foreach $profileParts as $name=>$fields}
				<table class="table table-bordered table-striped profiletable">
					{if is_array($fields)}
						<thead><tr class="header1"><th colspan=2>{$name}</th></tr></thead>
						<tbody>
							{foreach $fields as $label=>$val}
								<tr class="cell{cycle values='0,1'}">
									<td class="bold" style="width:15%;">{$label}</td>
									<td>{$val}</td>
								</tr>
							{/foreach}
						</tbody>
					{else}
						<thead><tr><th>{$name}</th></tr></thead>
						<tbody><tr><td>{$fields}</td></tr></tbody>
					{/if}
				</table>
			{/foreach}
		</div>

		<div class="col-md-6 col-lg-4 col-uhd-3">
			<table class="table table-bordered table-sm">
				<thead><tr class="header1"><th>RPG Status</th></tr></thead>
				<tbody><tr class="cell1"><td><img src="{$rpgstatus}" alt="RPG Status" title="RPG Status for {$username}" /></td></tr></tbody>
			</table>

			{if $equipitems}
				<table class="table table-bordered table-striped table-sm">
					<thead><tr class="header1"><th colspan=2>Equipped Items</th></tr></thead>
					<tbody>
						{foreach $equipitems as $equipitem}
							<tr>
								<td class="cell2">{$equipitem.name}</td>
								<td>{$equipitem.desc}</td>
							</tr>
						{/foreach}
					</tbody>
				</table>
			{/if}
			{if $badgers}
				<table class="table table-bordered table-striped table-sm">
					<thead><tr class="header1"><th>User Badges</th></tr></thead>
					<tbody>
						{foreach $badgers as $badger}
							<tr><td>{$badger}</td></tr>
						{/foreach}
					</tbody>
				</table>
			{/if}
		</div>
	</div>
	<table class="table table-bordered table-striped">
		<thead><tr><th colspan=2>Comments</th></tr></thead>
		<tbody>
			{if $commentField}
				<tr><td colspan=2>{$commentField}</td></tr>
			{else if !$loguserid}
				<tr><td colspan=2>You need to be logged in to post profile comments here.</td></tr>
			{/if}

			{if $pagelinks}<tr><td colspan=2>{$pagelinks}</td></tr>{/if}

			{foreach $comments as $cmt}
				<tr>
					<td>
						{$cmt.userlink}<br>
						<small>{$cmt.formattedDate}</small>
					</td>
					<td>
						{if $cmt.deleteLink}<small style="float: right; margin: 0px 4px;">{$cmt.deleteLink}</small>{/if}
						{$cmt.text}
					</td>
				</tr>
			{foreachelse}
				<tr><td colspan=2>No comments.</td></tr>
			{/foreach}

			{if $pagelinks}<tr><td colspan=2>{$pagelinks}</td></tr>{/if}
		</tbody>
	</table>