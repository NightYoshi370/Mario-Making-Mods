	<table class="table table-bordered table-striped">
		<thead><tr><th colspan=2>{"Reply"|__}</th></tr></thead>
		<tbody><tr>
			<td>Reply:</td>
			<td>
				<noscript><small>{$fields.noscript}</small></noscript>
				{$fields.text}
				{if $fields.ckeditor}{$fields.ckeditor}{/if}
			</td>
		</tr></tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><div class="btn-group">
						{$fields.btnPost}
						{$fields.btnPreview}
				</div></td>
			</tr>
			<tr>
				<td>Options:</td>
				<td>
					{$fields.mood}
					{$fields.nopl}
					{$fields.nosm}
					{$fields.lock}
				</td>
			</tr>
		</tfoot>
	</table>
