	<div class="row">
		<div class="col-md-6">
			<h3 class="center bold">Latest News</h2>
			{foreach $news as $post}
				<table class="table table-striped table-bordered">
					<thead><tr><th style="text-align:left!important;" colspan="2">
						<h4 style="display: inline; font-weight: bold;">{$post.title}</h4>
						<span style='float:right;text-align:right;font-weight:normal;'>
							Posted by {$post.userlink}<br>
							{$post.formattedDate}
						</span>
					</th></tr></thead>
					<tbody><tr><td colspan=2>{$post.text}</td></tr></tbody>
					<tfoot><tr>
						<td>{$post.comments}</td>
						<td>{$post.replylink}</td>
					</tr></tfoot>
				</table>
			{/foreach}
		</div>
		<div class="col-md-6">
			<table class="table table-striped table-bordered center">
				<tr><td>{$home_text}</td></tr>
			</table>
			<table class="table table-striped table-bordered">
				<thead><tr><th>Recent activity</th></tr></thead>
				<tbody>
					{foreach $lastactivity as $item}
						<tr class="cell{cycle values='0,1'}">
							<td style="padding:5px;">
								{$item.user} posted in {$item.link}
								<span class="smallFonts">{$item.formattedDate}</span>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>