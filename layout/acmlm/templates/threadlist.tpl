	{if $pagelinks}{$pagelinks}{/if}

	<style>#ThreadInput { width: 80%; }</style>

	<script>
		function SearchFunction() {
			// Declare variables 
			var input, filter, table, tr, td, i;
			input = document.getElementById("ThreadInput");
			filter = input.value.toUpperCase();
			table = document.getElementById("threadlist");
			tr = table.getElementsByTagName("tr");

			// Loop through all table rows, and hide those who don't match the search query
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[2];
				if (td) {
					if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	</script>

	<table class="table table-bordered">
		<tr class="header1"><th>Search</th></tr>
		<tr><td class="cell2"><center><input type="text" id="ThreadInput" onkeyup="SearchFunction()" placeholder="Search for threads on this page..."><noscript><br>
		<small>(Please enable JavaScript in order to use the search feature)</small></noscript></center></td></tr>
	</table>

	<table class="table table-sm table-striped" id="threadlist">
		<tr class="header1">
			<th style="width:32px;">&nbsp;</th>
			<th style="width:32px;">&nbsp;</th>
			{if $showforum}<th>Forum</th>{/if}
			<th>Title</th>
			<th style="width: 200px;">Started by</th>
			<th style="width: 140px;">
				<span style="float: left; width: 60px;">Replies</span>
				<span style="float: right; width: 60px; text-align: right;">Views</span>
			</th>
			<th style="width: 200px;">Last post</th>
		</tr>
		{foreach $threads as $thread}
		{if $dostickies && !$thread@first && $laststicky != $thread.sticky}
		<tr class="header0"><th colspan={if $showforum}8{else}7{/if} style="height:5px;"></th></tr>
		{/if}
		{$laststicky=$thread.sticky}
		<tr>
			<td class="cell2 newMarker">{$thread.new}</td>
			<td class="threadIcon" style="border-right:0px none;">{$thread.icon}</td>
			<td style="border-left:0px none;">
				{$thread.gotonew}
				{$thread.poll}
				{$thread.link.tags}<a href="{$thread.link.link}">{$thread.link.name}</a>
				{if $thread.pagelinks}<span style="float: right;">
					<ul class="pagination pagination-sm justify-content-end" style="margin-bottom: 0;">{$thread.pagelinks}</ul>
				</span>{/if}<br><i>{$thread.description}</i>
			</td>
			{if $showforum}<td class="center">{$thread.forumlink}</td>{/if}
			<td class="center">{$thread.startuser}</td>
			<td class="smallFonts">
				<span style="float: left;">Replies:</span> <span style="float: right; text-align:right;">{$thread.replies}</span><br>
				<span style="float: left;">Views:</span> <span style="float: right; text-align:right;">{$thread.views}</span>
			</td>
			<td class="center smallFonts">
				{$thread.lastpostdate}<br>
				by {$thread.lastpostuser} <a href="{$thread.lastpostlink}">&raquo;</a>
			</td>
		</tr>
		{/foreach}
	</table>
	
	{if $pagelinks}{$pagelinks}{/if}
