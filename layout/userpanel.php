<?php

if (!defined('BLARG')) {
    die();
}

$userMenu = [];

if ($loguserid) {
    $userMenu[profileLink($loguser)] = ['icon' => 'user-circle-o', 'devider' => true, 'text' => __('View My Profile')];
    if (HasPermission('user.editprofile')) {
        $userMenu[actionLink('editprofile')] = ['icon' => 'pencil', 'text' => __('Edit profile')];
    }

    $userMenu[actionLink('private')] = ['icon' => 'envelope', 'text' => __('Private messages')];
    $userMenu[actionLink('favorites')] = ['icon' => 'star-o', 'text' => __('Favorites')];

    $bucket = 'userMenu';
    include __DIR__.'/../lib/pluginloader.php';
} else {
    $userMenu[pageLink('login')] = ['icon' => 'sign-in', 'text' => __('Login')];
    $userMenu[actionLink('register')] = ['icon' => 'user-circle-o', 'text' => __('Register')];

    $bucket = 'guestMenu';
    include __DIR__.'/../lib/pluginloader.php';
}

$layout_userpanel = $userMenu;
