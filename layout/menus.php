<?php

if (!defined('BLARG')) {
    die();
}

$headerlinks = [
    pageLink('home')  => ['link'  => ['home'], 'icon' => 'home', 'text' => __('Home')],
    pageLink('board') => ['link'  => ['board', 'search', 'lastposts', 'ranks', 'forum'], 'text' => __('Forums'), 'dropdown' => [
        pageLink('board')         => ['icon' => 'bars', 'text' => __('Index'), 'devider' => true],
        pageLink('lastposts')     => ['icon' => 'group', 'text' => __('Latest Posts')],
        actionLink('activeusers') => ['text' => __('Active users')],
        actionLink('ranks')       => ['icon' => 'trophy', 'text' => __('Ranks'), 'devider' => true],
        pageLink('search')        => ['icon' => 'search', 'text' => __('Search')],
    ]],
    pageLink('faq')   => ['icon'  => 'question', 'link' => ['faq'], 'text' => __('FAQ')],
    pageLink('depot') => ['icon'  => 'download', 'link' => ['depot'], 'text' => __('Depot')],
    pageLink('wiki')  => ['icon'  => 'file-text', 'link' => ['wiki'], 'text' => __('Wiki')],
];

$sidelinks = [
    Settings::get('menuMainName') => [
        pageLink('home')                            => ['icon' => 'home', 'text' => __('Home')],
        pageLink('board')                           => ['icon' => 'bars', 'text' => __('Forums')],
        actionLink('faq')                           => ['icon' => 'question', 'text' => __('FAQ')],
        actionLink('memberlist')                    => ['icon' => 'group', 'text' => __('Member list')],
        actionLink('ranks')                         => ['icon' => 'trophy', 'text' => __('Ranks')],
        actionLink('online')                        => ['icon' => 'eye', 'text' => __('Online Users')],
        actionLink('lastposts')                     => ['icon' => 'group', 'text' => __('Latest Posts')],
        actionLink('search')                        => ['icon' => 'search', 'text' => __('Search')],
        pageLink('depot')                           => ['icon' => 'download', 'link' => ['depot'], 'text' => __('Depot')],
        pageLink('wiki')                            => ['icon' => 'file-text', 'link' => ['wiki'], 'text' => __('Wiki')],
        'https://www.youtube.com/c/MarioMakingMods' => ['icon' => 'youtube-play', 'text' => __('Youtube')],
        'https://twitter.com/MarioMakingMods'       => ['icon' => 'twitter', 'text' => __('Twitter')],
    ],
];
