	<table class="table table-striped table-bordered" id="post{$post.id}">
		<tr><td>
			{$post.userlink} -
			<small>
				<span id="meta_{$post.id}">
					{if $post.type == $smarty.const.POST_SAMPLE}
						Preview
					{else}
						{if $post.type == $smarty.const.POST_PM}Sent{else}Posted{/if} on {$post.formattedDate}
						{if $post.threadlink} in {$post.threadlink}{/if}
					{/if}
				</span>
				<span id="dyna_{$post.id}" style="display: none;">
					blarg
				</span>
			</small>
		</td></tr>

		<tr><td id="post_{$post.id}">{$post.contents}</td></tr>

		{if count($post.links) > 0}
			<tr><td>
				{if $post.id}<a href="{actionLink page='post' id=$post.id}">#{$post.id}</a>{/if}

				{if $post.type == $smarty.const.POST_NORMAL}
					<div class="dropdown dropleft float-right">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
							Options
						</button>
						<div class="dropdown-menu">
							{foreach $post.links as $link}
								<a class="dropdown-item" {if $link.onclick}onclick="{$link.onclick}"{/if} {if $link.link}href="{$link.link}"{/if}>
									{if $link.icon}<i class="fa fa-{$link.icon}"></i>{/if}
									{if $link.text}{$link.text}{/if}
								</a>
							{/foreach}
						</div>
					</div>
					{foreach $post.links_extra as $link}
						{$link}
					{/foreach}
				{else if $post.type == $smarty.const.POST_DELETED_SNOOP}
					<div class="right">
						Post deleted: 
							{if $post.links.undelete}{$post.links.undelete}{/if}
							{if $post.links.close}{if $post.links.undelete} | {/if}{$post.links.close}{/if}
					</div>
				{/if}
			</td></tr>
		{/if}
	</table>