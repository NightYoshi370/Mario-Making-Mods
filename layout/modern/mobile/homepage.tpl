	<table class="table table-striped table-bordered" style="text-align: center;">
		<tr><td>
			{$home_text}
		</td></tr>
	</table>

	<table class="table table-striped table-bordered">
		<thead><tr><th>Recent Content</th></tr></thead>
		<tbody>
			{foreach $lastactivity as $item}
				<tr><td>
					<b>{$item.link}</b><br>
					<span class="smallFonts">{$item.user} - {$item.formattedDate} in {$item.forum}</span>
				</td></tr>
			{/foreach}
		</tbody>
	</table>

	{if $pagelinks}{$pagelinks}{/if}
	{foreach $news as $post}
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th style="text-align:left!important;">
						{if $post.links.edit || $post.links.delete}
							<span style='float:right;text-align:right;font-weight:normal;'>
								<ul class="pipemenu">
									{if $post.links.edit}<li>{$post.links.edit}{/if}
									{if $post.links.delete}<li>{$post.links.delete}{/if}
								</ul>
							</span>
						{/if}
						<h3>{$post.title}</h3>
						<span style="font-weight:normal;">Posted on {$post.formattedDate} by {$post.userlink}</span>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr><td>{$post.text}</td></tr>
				<tr><td>{$post.comments}. {$post.replylink}</td></tr>
			</tbody>
		</table>
	{/foreach}
	{if $pagelinks}{$pagelinks}{/if}