	<table class="table table-bordered table-striped">
		<thead><tr><th>Search options</th></tr></thead>
		<tbody>
			<tr><td>
				<label for="sort">Sort by</label><br>
				{$fields.sortBy}
			</td></tr>
			<tr><td>
				<label for="order">Order</label><br>
				{$fields.order}
			</td></tr>
			<tr><td>
				<label for="group">Group</label><br>
				{$fields.group}
			</td></tr>
			<tr><td>{$fields.name}</td></tr>
		</tbody>
		<tfoot><tr><td>{$fields.btnSearch}</td></tr></tfoot>
	</table>
