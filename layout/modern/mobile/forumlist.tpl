{foreach $categories as $cat}
	<table class="table table-bordered table-striped table-sm">
		<thead><tr><th class="center">{$cat.name}</th></tr></thead>
		<tbody>
			{foreach $cat.forums as $forum}
				<tr><td class="center">
					<h4>{$forum.new} {$forum.link}</h4>
					{if $forum.description}{$forum.description}<br>{/if}
					{if $forum.localmods}Moderated by: {$forum.localmods}<br>{/if}

					<small>
						{plural num=$forum.threads what='thread'}, {plural num=$forum.posts what='post'}<br>

						{if $forum.lastpostdate}Last post: {$forum.lastpostdate}, by {$forum.lastpostuser} <a href="{$forum.lastpostlink}">&raquo;</a>{else}&mdash;{/if}
					</small>

					{if $forum.subforums}<br>Subforums: {$forum.subforums}{/if}
				</td></tr>
			{/foreach}
		</tbody>
    </table>
{/foreach}