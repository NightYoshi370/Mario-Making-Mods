
	{foreach $ranks as $rank}
		<table class="table table-bordered table-striped">
			<thead><tr><th>{$rank.rank}</th></tr></thead>
			<tbody>
				<tr><td>{$rank.users}</td></tr>
				<tr><td><small>Requires {$rank.posts} posts</small></td></tr>
			</tbody>
		</table>
	{/foreach}
