
		<div class="nav-tabs-wrapper"><ul id="profile-nav" class="nav nav-tabs dragscroll horizontal" role="tablist">
			{foreach $pages as $pageid=>$pname}
				<li class="nav-item"><a data-toggle="tab" class="nav-link" href="#{$pageid}">{$pname}</a></li>
			{/foreach}
		</ul></div>
		<div class="tab-content">
			<div class="tab-pane fade show active">
				<table class="table table-bordered table-outlined">
					<tr><th>Welcome to the Edit Profile page</th></tr>
					<tr><td>Click on the left to go to different pages<br>Click on the above tabs to edit other parts</td></tr>
				</table>
			</div>
			{foreach $pages as $pageid=>$pname}
				<div id="{$pageid}" class="tab-pane fade">
					<table class="table table-bordered table-outlined">

						{foreach $categories.{$pageid} as $catid=>$cname}
							<tr class="header0"><th>{$cname}</th></tr>

							{foreach $fields.{$catid} as $fieldid=>$field}
								<tr class="cell{cycle values='0,1'}">

									{if $field.type == 'themeselector'}
										<td class="themeselector">
											{$field.html}
										</td>
									{else}
										<td>
											{if $field.caption}<strong>{$field.caption}</strong><br>{/if}
											{$field.html}
											{if $field.hint}<br><small>{$field.hint}</small>{/if}
										</td>
									{/if}
								</tr>
							{/foreach}
						{/foreach}
						<tr class="cell{cycle values='0,1'}">
							<td>{$btnEditProfile}</td>
						</tr>
					</table>
				</div>
			{/foreach}
		</div>
