	<table class="table table-striped table-bordered">
		<thead><tr><th>{$poll.question}</th></tr></thead>
		<tbody>
			{foreach $poll.options as $option}
				<tr class="cell{cycle values='0,1'}"><td>
					<strong>{$option.label}</strong>
					<div class="pollbarContainer">
						{if $option.votes}
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="{$option.percent}"
								aria-valuemin="0" aria-valuemax="100" style="background-color: {$option.color}; width: {$option.percent}%;">
									&nbsp;{$option.votes} ({$option.percent}%)
								</div>
							</div>
						{else}
							&nbsp;0 (0%)
						{/if}
					</div>
				</td></tr>
			{/foreach}
			<tr class="cell{cycle values='0,1'}"><td><small>
				{if $poll.multivote}
					Multiple voting is allowed.
				{else}
					Multiple voting is not allowed.
				{/if}
				{if $poll.voters == 1}
					1 user has voted so far.
				{else}
					{$poll.voters} users have voted so far.
				{/if}
				{if $poll.multivote}
					Total votes: {$poll.votes}.
				{/if}
			</small></td></tr>
		</tbody>
	</table>
