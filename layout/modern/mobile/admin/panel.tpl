	<table class="table table-striped table-bordered">
		<thead><tr><th>Administration tools</th></tr>
		<tbody>
			{foreach $adminLinks as $link=>$text}
				<tr style="text-align: center;"><td>
					<a href="{$link|escape}">{$text}</a>
				</td></tr>
			{/foreach}
		</tbody>
	</table>

	<table class="table table-striped table-bordered">
		<thead><tr><th>Information</th></tr></thead>
		<tbody>
			{foreach $adminInfo as $label=>$contents}
				<tr><td>
					<strong>{$label}</strong><br>
					{$contents}
				</td></tr>
			{/foreach}
		</tbody>
	</table>