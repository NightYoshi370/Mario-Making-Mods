	{foreach $settingfields as $cat=>$settings}
		<table class="table table-bordered table-striped">
			<thead><tr><th>{if $cat}{$cat}{else}Settings{/if}</th></tr></thead>
			<tbody>
				{foreach $settings as $setting}
					<tr><td>
						{if $setting.name}<b>{$setting.name}</b><br>{/if}
						{if $setting.field}{$setting.field}{/if}
					</td></tr>
				{/foreach}
			</tbody>
		</table>
	{/foreach}

	<div class="center">{$fields.btnSaveExit}{$fields.btnSave}</div>