	Search results &mdash; {plural num=$numUsers what='user'} found<br>
	{if $pagelinks}{$pagelinks}{/if}

	{foreach $users as $user}
				<div class="card hoverable" style="width: 100%; margin: 0 auto;">
					{if $user.avatar}<img src="{$user.avatar}" class="card-img-top" style="width:100%;">{/if}
					<div class="card-body">
						<h6 class="card-title">{$user.link}{$user.isonline}</h6>
						{if $user.title}<p class="card-subtitle">{$user.title}</p>{/if}
					</div>
					<ul class="list-group list-group-flush"><li class="list-group-item">{$user.group}</li></ul>
						<div class="card-body">
							Posts: {$user.posts} ({$user.average})<br>
							Since: {$user.regdate}<br>
							{if $user.from}From: {$user.from}<br>{/if}
							{if $user.birthday}Birthday: {$user.birthday}{/if}
						</div>
						<div class="card-footer d-flex justify-content-between">
							{foreach $user.quicklinks as $quicklink}
								<a href="{$quicklink.link}" title="{$quicklink.text}"><i class="fa fa-{$quicklink.icon}"></i></a>
							{/foreach}
						</div>
				</div><br>
	{/foreach}

	{if $pagelinks}<div class="smallFonts pages">{$pagelinks}</div>{/if}
