	<ul id="profile-nav" class="nav nav-tabs" role="tablist">
		<li class="nav-item"><a class="nav-link active" href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Information</a></li>
		<li class="nav-item"><a class="nav-link" href="#comments" role="tab" id="comments-tab" data-toggle="tab" aria-controls="comments">Comments</a></li>
		<li class="nav-item"><a class="nav-link" href="#recent" role="tab" id="recent-tab" data-toggle="tab" aria-controls="recent">Recent Activity</a></li>
	</ul><br>
	<div id="profile-nav-content" class="tab-content">
		<div role="tabpanel" class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
			{foreach $profileParts as $name=>$fields}
				<table class="table table-bordered table-striped profiletable">
					{if is_array($fields)}
						<thead><tr class="header1"><th colspan=2>{$name}</th></tr></thead>
						<tbody>
							{foreach $fields as $label=>$val}
								<tr><td>
									<h4>{$label}</h4>
									{$val}
								</td></tr>
							{/foreach}
						</tbody>
					{else}
						<thead><tr><th>{$name}</th></tr></thead>
						<tbody><tr><td>{$fields}</td></tr></tbody>
					{/if}
				</table>
			{/foreach}
		</div>
		<div role="tabpanel" class="tab-pane fade" id="comments" aria-labelledby="comments-tab">

			{if $commentField}
				{$commentField}
			{else if !$loguserid}
				You need to be logged in to post profile comments here.
			{/if}

			{if $pagelinks}{$pagelinks}{/if}

			{foreach $comments as $cmt}
				<table class="table table-striped table-bordered">
					<tr><td>
						<div class="media">
							<div class="media-body">
								<h4 class="media-heading">{$cmt.userlink} <small>{$cmt.formattedDate}</small></h4>
								{if $cmt.deleteLink}<small style="float: right; margin: 0px 4px;">{$cmt.deleteLink}</small>{/if}
								{$cmt.text}
							</div>
						</div>
					</td></tr>
				</table>
			{foreachelse}
				No comments.
			{/foreach}

			{if $pagelinks}{$pagelinks}{/if}
		</div>
		<div role="tabpanel" class="tab-pane fade" id="recent" aria-labelledby="recent-tab">
			{foreach $lastactivity as $post}
				<table class="table table-bordered table-striped">
				<tr><td class="cell2">{$post.contents}</td></tr>
				<tr class="header0"><th style="text-align: left !important;">
					Posted on {$post.formattedDate}
					{if $post.threadlink} in {$post.threadlink}{/if}
					{if $post.revdetail} ({$post.revdetail}){/if}
				</th></tr></table>
			{/foreach}
		</div>
	</div>
