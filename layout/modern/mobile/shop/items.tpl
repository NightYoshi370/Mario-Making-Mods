
{foreach $itemlists as $item}
	{$colspan = 0}
	{foreach $item.atrlist as $atr}
		{if empty($atr.st)}{else}{$colspan = $colspan+1}{/if}
	{/foreach}
	<table class="table table-bordered table-striped">
		<thead><tr><th colspan="{$colspan}">{$item.itemLink}</th></tr></thead>
		<tbody {$item.color}>
			<tr><td colspan="{$colspan}">{$item.desc}</td></tr>
			<tr>
				{foreach $item.atrlist as $atr}
					{if !empty($atr.st)}<td style="text-align: center;" {$atr.cl}>{$atr.st} {$atr.name}</td>{/if}
				{/foreach}
			</tr>
		</tbody>
		<tfoot><tr><td colspan="{count($item.atrlist)}">
			{if $item.comm}<span style="text-align: left;">{$item.comm}</span>{/if}
			<span style="text-align: right">{plural num=$item.coins what='coin'}</span>
		</td></tr></tfoot>
	</table>
{/foreach}