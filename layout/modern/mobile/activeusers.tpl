<table class="table table-bordered table-striped">
	<tr class="header1">
		<th style="width: 30px;">#</th>
		<th>Username</th>
		<th style="width: 50px;">Posts</th>
	</tr>
	{foreach $rows as $row}
		<tr>
			<td>{$row.i}</td>
			<td>{$row.userlink}</td>
			<td>{$row.num}</td>
		</tr>
	{/foreach}
	<tr><th colspan="7"></th></tr>
	<tr>
		<td></td>
		<td class="bold">Total</td>
		<td>{$post_total}</td>
	</tr>
</table>