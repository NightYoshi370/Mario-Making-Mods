	<div style="height: auto; text-align:center;" id="boardHeader">
		<a href="{actionLink page='home'}">
			<img id="theme_banner" src="{$layout_logopic}" alt="{$boardname}" title="{$boardname}" style="max-width:100%;">
		</a>
	</div>

	<nav class="navbar bg-dark navbar-dark sticky-top" style="margin-bottom: 0px; border-radius: 0px;">
		{if $layout_crumbs && count($layout_crumbs)>1}
			{$crumburls=array_keys($layout_crumbs)}
			{$prevcrumb=$crumburls[count($crumburls)-2]}
			{$thiscrumb=$crumburls[count($crumburls)-1]}
			<a class="navbar-brand" href="{$prevcrumb|escape}" style="margin-right: 0"><i class="fa fa-reply" aria-hidden="true"></i></a>
			<b class="d-inline-block text-truncate navbar-menuname">{$layout_crumbs[$thiscrumb]}</b>
		{/if}
		<button class="navbar-toggler" style="float:right; text-align:right;" type="button" data-toggle="collapse" data-target="#myNavbar">
			{if $loguserid}
				{$numnotifs=count($notifications)}
				{if $numnotifs}
					<span id="notifCount" class="badge badge-danger">{$numnotifs}</span>
				{else}
					<span class="navbar-toggler-icon"></span>
				{/if}
			{else}
				<span class="navbar-toggler-icon"></span>
			{/if}
		</button>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				{foreach $headerlinks as $url=>$texts}
					{if $texts.dropdown}
						<li class="nav-item dropdown {foreach $texts.link as $link}{if $currentPage === $link}active{/if}{/foreach}">
							<a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="{$url}">
								{$texts.text} <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								{foreach $texts.dropdown as $ddurl=>$ddtexts}
									<li class="dropdown-item"><a class="dropdown-link" href="{$ddurl|escape}">{$ddtexts.text}</a></li>
									{if $ddtexts.devider}
										<li class="dropdown-divider"></li>
									{/if}
								{/foreach}
							</ul>
						</li>
					{else}
						<li class="nav-item {foreach $texts.link as $link}{if $currentPage === $link}active{/if}{/foreach}">
							<a class="nav-link" href="{$url|escape}">{$texts.text}</a>
						</li>
					{/if}
				{/foreach}
			</ul>
			<ul class="nav navbar-nav ml-auto">
				<li class="nav-item dropdown">
					{if $loguserid}
						<a class="nav-link nav-dropdown dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="{$logusertext.link}">
							{$logusertext.text} <span class="caret"></span>
						</a>
					{else}
						<a class="nav-link dropdown-toggle" data-toggle="dropdown">
							Welcome Guest <span class="caret"></span>
						</a>
					{/if}
					<ul class="dropdown-menu">
						{foreach $layout_userpanel as $url=>$texts}
							<li class="dropdown-item"><a class="dropdown-link" href="{$url|escape}">{$texts.text}</a></li>
							{if $texts.devider}
								<li class="dropdown-divider"></li>
							{/if}
						{/foreach}
						{if $loguserid}
							<li class="dropdown-divider"></li>
							<li class="dropdown-item"><a href="#" class="dropdown-link" onclick="$('#logout').submit(); return false;">Log out</a></li>
						{/if}
					</ul>
				</li>
				{if $loguserid}
					{$numnotifs=count($notifications)}
					<li id="notifMenuContainer" class="nav-item dropdown {if $numnotifs}hasNotifs{else}noNotif{/if}">
						<a id="notifMenuButton" class="nav-link dropdown-toggle" data-toggle="dropdown">
							Alerts
							<span id="notifCount" class="badge {if $numnotifs}badge-danger{else}badge-secondary{/if}">{$numnotifs}</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" style="right: 0; left: auto;">
							{if $numnotifs}
								{foreach $notifications as $notif}
									<li id="notifText" class="dropdown-item">{$notif.text}<br><small>{$notif.formattedDate}</small></li>
								{/foreach}
							{else}
								<li id="notifText" class="dropdown-item">You have no new Alerts.</li>
							{/if}
						</ul>
					</li>
				{/if}
			</ul>
		</div>
	</nav>

	{capture "breadcrumbs"}{if ($layout_actionlinks && count($layout_actionlinks)) || ($layout_dropdown && count($layout_dropdown))}
		<div class="bg-light navbar-light center" id="breadBar" style="white-space: nowrap;">
			{if $layout_actionlinks && count($layout_actionlinks)}
				{foreach $layout_actionlinks as $url=>$texts}
					<a {if $url}href="{$url|escape}"{/if}
						class="btn {if $texts.active}btn-primary{/if}"
						{if $texts.onclick} onclick="{$texts.onclick}"{/if}>
						{if $texts.icon}<i class="fa fa-{$texts.icon}"></i> {/if}{$texts.text}
					</a>
				{/foreach}
			{/if}
			{if $layout_dropdown && count($layout_dropdown)}
				{foreach $layout_dropdown as $cat=>$links}
					<span class="dropdown">
						<button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
							{$cat} <span class="caret"></span>
						</button>
						<div class="dropdown-menu">
							{foreach $links as $url=>$texts}
								<a class="dropdown-item {if $texts.active}active{/if}" href="{$url|escape}">{$texts.text}</a>
							{/foreach}
						</div>
					</span>
				{/foreach}
			{/if}
		</div>
	{/if}{/capture}

	{$smarty.capture.breadcrumbs}

	<div class="container-fluid">
		<br>
		{$layout_contents}
	</div>


<footer class="page-footer elegant-color darken-3">
	{$smarty.capture.breadcrumbs}
    <div class="container">
		<div class="row">
			<div class="col-md-12 py-0">
				<div class="mb-5 flex-center">
					<a href="https://www.youtube.com/c/MarioMakingMods" style="color: white!important;">
						<i class="fa fa-youtube fa-lg white-text mr-md-5 mr-3 fa-3x"></i>
					</a>
					<a href="https://twitter.com/MarioMakingMods" style="color: white!important;">
						<i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-3x"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
    <div class="footer-copyright text-center py-3">{$layout_credits}</div>
</footer>