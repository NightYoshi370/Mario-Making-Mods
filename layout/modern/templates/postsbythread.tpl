	<div class="smallFonts margin">
		Timeframe:
		<ul class="pipemenu">
		{foreach $timelinks as $link}
			<li>{$link}
		{/foreach}
		</ul><br>
		Posts {$from} in threads {$during}:
	</div>

	<table class="table table-bordered table-striped">
		<tr class="header1">
			<th class="center" style="width:30px;"></th>
			<th style="width:200px;"></th>
			<th class="center">Thread</th>
			<th class="center" style="width:70px;">Posts</th>
			<th class="center" style="width:90px;">Thread total</th>
		</tr>
		{foreach $rows as $row}
			<tr>
				<td class="cell2 center">{$row.i}</td>
				<td class="cell2 center">{$row.forum}</td>
				<td class="cell1">{if $row.icon}<img src="{$row.icon}">{/if}{$row.thread}</td>
				<td class="cell1 center"><b>{$row.cnt}</b></td>
				<td class="cell1 center">{$row.replies}</td>
			</tr>
		{/foreach}
	</table>