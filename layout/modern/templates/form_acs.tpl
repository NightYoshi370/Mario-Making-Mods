	<table class="table table-bordered table-striped">
		<thead><tr><th>Settings</th></tr></thead>
		<tbody>
			<tr><td>{$fields.past} days ago</td></tr>
			<tr><td>
				<div class="checkbox">
					<label>{$fields.easymode} Easy Mode</label>
				</div>
				<small>Without this, at most 5 posts per day in any single thread are counted.</small>
			</td></tr>
			<tr><td>{$fields.submit}</td></tr>
		</tbody>
	</table>