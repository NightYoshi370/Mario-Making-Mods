	<table class="table table-bordered table-striped">
		<thead><tr class="header1"><th colspan=2>Thread review</th></tr></thead>
		<tbody>
			{foreach $review as $post}
				<tr>
					<td class="cell2" style="width: 200px;">
						{$post.userlink}<br>
						<span class="smallFonts">Posts: {$post.posts}</span>
					</td>
					<td>
						<button style="float:right;" class="btn btn-primary" onclick="{if $ckeditor}insertQuoteCK({$post.id});{else}insertQuote({$post.id});{/if}">Quote</button>
						{$post.contents}
					</td>
				</tr>
			{/foreach}
		</table>
	</table>
