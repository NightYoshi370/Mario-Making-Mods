	<table class="table table-bordered table-striped" id="wikidiff">
		<tr><td><h1>{$nicetitle}</h1></td></tr>
		<tr><td>Viewing the difference between revisions {$previous} (previous) and {$current} (current)</td></tr>
		<tr><td>(revisions: &nbsp;1&nbsp;{$revList})</td></tr>
		<tr><td>{$diff}</td></tr>
	</table>