	<script>
		function preview(user,item,cat,name){
			document.getElementById('prev').src='gfx/rpgstatus.php?u='+user+'&it='+item+'&ct='+cat+'&'+Math.random();
			document.getElementById('pr').innerHTML='Equipped with<br>'+name+'<br>---------->';
		}
	</script>
	<div class="row">
		<div class="col-sm-4"><img src="/gfx/rpgstatus.php?u={$loguserid}"></div>
		<div class="col-sm-4" id="pr"></div>
		<div class="col-sm-4"><img src="" id="prev"></div>
	</div>
	<br>
	<table class="table table-bordered table-striped">
		<thead><tr>
				<th style="width: 140px;">Commands</th>
				<th>Item</th>
				{foreach $atrlist as $atr}
					<th style="width: 50px;">{$atr}</th>
				{/foreach}
				<th style="width: 6%;"><img src="/img/coin.gif"></th>
		</tr></thead>
		<tbody>
			{foreach $itemlists as $itemlist}
				<tr {$itemlist.color}>
					<td align="center">{$itemlist.comm}</td>
					<td>{$itemlist.itemLink} - <small>{$itemlist.desc}</small></td>
					{foreach $itemlist.atrlist as $atr}
						<td style="text-align: center;" {$atr.cl}>{$atr.st}</td>
					{/foreach}
					<td align="right">{$itemlist.coins}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>