<div class="row">
	<div class="col-sm-3 col-md-2 col-lg-1">
		<ul class="nav nav-pills nav-stacked">
			<li><a href="{actionLink page='editprofile'}/{$loguserid}">Edit Profile</a></li>
			<li><a href="{actionLink page='editavatars'}">Mood Avatars</a></li>
			<li class="active"><a href="{actionLink page='shop'}">Item Shop</a></li>
		</ul>
	</div>
	<div class="col-sm-9 col-md-10 col-lg-11"><br>
	<img src="gfx/rpgstatus.php?u={$loguserid}">
	<br><br>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>Shop</th>
						<th>Item equipped</th>
					</tr>
				</thead>
				<tbody>
					{foreach $shoplists as $shoplist}
						<tr>
							<td>
								{$shoplist.link}
								<br><small>{$shoplist.description}</small>
							</td>
							<td>{$shoplist.curent}</td>
						</tr>
					{/foreach}
				</tbody>
			</table>
	</div>
</div>