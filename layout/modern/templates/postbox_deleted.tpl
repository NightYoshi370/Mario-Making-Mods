	<table class="table table-bordered" id="post{$post.id}">
		<tr>
			<td class="side userlink" id="{$post.id}">
				{$post.userlink}
			</td>
			<td class="smallFonts meta right">
				<div style="float:left">
					Posted on {$post.formattedDate}, deleted by {$post.deluserlink}{if $post.delreason}: {$post.delreason}{/if}
				</div>
					{if $post.links.undelete}{$post.links.undelete}{/if}
					{if $post.links.view}{$post.links.view}{/if}
					#{$post.id}
					{if $post.ip}{$post.ip}{/if}
			</td>
		</tr>
	</table>
