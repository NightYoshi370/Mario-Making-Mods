	{if $pagelinks}<div class="smallFonts pages">{$pagelinks}</div>{/if}
	
	{foreach $threads as $thread}
		<table class="table table-bordered {if $thread@index is odd}table-striped{/if}">
			<thead>
				<tr><th {if $thread.gotonew || $thread.poll}colspan=2{/if}>

					{if $dostickies && $thread.sticky}<i class="fa fa-map-pin"></i>{/if}
					{if $thread.new}{$thread.new}{/if}
					{if $thread.icon}{$thread.icon}{/if}
					<a href="{$thread.link.link}">{$thread.link.name}</a>
					{if $thread.link.tags}{$thread.link.tags}{/if}
				</th></tr>
				{if $thread.description}
					<tr><th {if $thread.gotonew || $thread.poll}colspan=2{/if}>
						{$thread.description}
					</th></tr>
				{/if}
			</thead>
			<tbody><tr>
				{if $thread.gotonew || $thread.poll}
					<td class="cell2" style="width:40px;">
						{if $thread.gotonew}{$thread.gotonew}<br>{/if}
						{if $thread.poll}{$thread.poll}{/if}
					</td>
				{/if}
					<td>
						<div style="float: right;">
							<div class="btn-group">
								{foreach $thread.quicklinks as $quicklink}
									<a href="{$quicklink.url}" class="btn btn-sm btn-link" title="{$quicklink.text}">
										<i class="fa fa-{$quicklink.icon} fa-2x"></i>
									</a>
								{/foreach}
							</div>
						</div>
						<div style="float: left;">
							Posted on {$thread.postdate} by {$thread.startuser} {if $showforum}in {$thread.forumlink}{/if}<br>
							Last Post: {$thread.lastpostuser} on {$thread.lastpostdate} <a href="{$thread.lastpostlink}">&raquo;</a><br>
							Thread ID: {$thread.id}
						</div>
					</td>
			</tr></tbody>
			<tfoot><tr><td {if $thread.gotonew || $thread.poll}colspan=2{/if}>
				<span style="float: left;">
					<i class="fa fa-comments-o"></i> {plural num=$thread.replies what='reply'}
					<i class="fa fa-eye"></i> {plural num=$thread.views what='view'}
				</span>
				{if $thread.pagelinks}<span style="float: right;">
					<ul class="pagination pagination-sm justify-content-end" style="margin-bottom: 0;">{$thread.pagelinks}</ul>
				</span>{/if}
			</td></tr></tfoot>
		</table>
	{/foreach}
	
	{if $pagelinks}<div class="smallFonts pages">{$pagelinks}</div>{/if}
