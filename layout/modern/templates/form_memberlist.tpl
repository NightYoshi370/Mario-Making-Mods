	<table class="table table-bordered table-striped">
		<thead><tr><th colspan=2>Search options</th></tr></thead>
		<tbody>
			<tr><td colspan=2><div class="input-group">
				<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user-circle-o fa-fw"></i></span></div>
				{$fields.name}
			</div></td></tr>
			<tr><td class="center" style="width:20%;">Sort by</td><td>{$fields.sortBy}</td></tr>
			<tr><td class="center">{"Order"|__}</td><td>{$fields.order}</td></tr>
			<tr><td class="center">{"Group"|__}</td><td>{$fields.group}</td></tr>
			<tr><td class="center">{"Last View"|__}</td><td>{$fields.lastview}</td></tr>
		</tbody>
		<tfoot><tr><td class="center" colspan=2>{$fields.btnSearch}</td></tr></tfoot>
	</table>
