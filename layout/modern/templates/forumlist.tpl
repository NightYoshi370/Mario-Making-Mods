{foreach $categories as $cat}
    <table class="table table-bordered table-striped">
        <thead><tr><th style="text-align:center;">{$cat.name}</th></tr></thead>
        <tbody><tr><td style="text-align: center;">
			<div class="row">
				{foreach $cat.forums as $forum}
					<div class="
						{if $cat.forums|@count > 4}col-sm-6 col-md-3
						{elseif $cat.forums|@count == 4}col-sm-6 col-md-3
						{elseif $cat.forums|@count == 3}col-sm-4
						{elseif $cat.forums|@count == 2}col-sm-6
						{else}col-sm-12
						{/if}
					">
						<h3>{$forum.new} {$forum.link}</h3>
						{if $forum.description}{$forum.description}<br>{/if}
						{if $forum.localmods}Moderated by: {$forum.localmods}<br>{/if}

						<small>
							{plural num=$forum.threads what='thread'}, {plural num=$forum.posts what='post'}<br>
							{if $forum.lastpostdate}Last post: {$forum.lastpostdate}, by {$forum.lastpostuser} <a href="{$forum.lastpostlink}">&raquo;</a>{else}&mdash;{/if}
						</small>

						{if $forum.subforums}<br>Subforums: {$forum.subforums}{/if}
					</div>
				{/foreach}
			</div>
        </td></tr></tbody>
    </table>
{/foreach}