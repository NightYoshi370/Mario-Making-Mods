	<div class="row">
		<div class="col-sm-4">
			<table class="table table-bordered table-striped">
				<thead><tr><th>Most Recent</th></tr></th>
				<tbody>
					{foreach $recent as $thread}
						<tr><td>
							{$thread.title}<br>
							{$thread.userlink} - {$thread.formattedDate}
						</td></tr>
					{/foreach}
				</tbody>
			</table>
		</div>
		<div class="col-sm-4">
			<table class="table table-bordered table-striped">
				<thead><tr><th>Most Viewed</th></tr></th>
				<tbody>
					{foreach $popular as $thread}
						<tr><td>
							{$thread.title}<br>
							{$thread.userlink} - {$thread.formattedDate}
						</td></tr>
					{/foreach}
				</tbody>
			</table>
		</div>
		<div class="col-sm-4">
			<table class="table table-bordered table-striped">
				<thead><tr><th>Most Liked</th></tr></th>
				<tbody>
					{foreach $liked as $thread}
						<tr><td>
							{$thread.title}<br>
							{$thread.userlink} - {$thread.formattedDate}
						</td></tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>