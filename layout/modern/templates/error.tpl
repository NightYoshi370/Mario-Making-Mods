	<table class="table table-bordered table-striped">
		<tr class="header1"><th>404 - Not found</th></tr>
		<tr class="cell0">
			<td>
				<br>
				The page you are looking for was not found.<br>
				Requested URL: {$error}<br><br>
				<a href="{pageLink name='home'}">Return to the Homepage</a><br>
				<a href="javascript:history.go(-1)">Return to the previous page</a><br>
				<br>
			</td>
		</tr>
	</table>

