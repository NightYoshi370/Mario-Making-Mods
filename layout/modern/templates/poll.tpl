	<table class="table table-striped table-bordered">
		<thead><tr><th colspan=2>{$poll.question}</th></tr></thead>
		<tbody>
			{foreach $poll.options as $option}
				<tr>
					<td class="cell2" style="width: 20%;">
						<strong>{$option.label}</strong>
					</td>
					<td style="width: 80%">
						<div class="pollbarContainer">
							{if $option.votes}
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="{$option.percent}"
									aria-valuemin="0" aria-valuemax="100" style="background-color: {$option.color}; width: {$option.percent}%;">
										&nbsp;{$option.votes} ({$option.percent}%)
									</div>
								</div>
							{else}
								&nbsp;0 (0%)
							{/if}
						</div>
					</td>
				</tr>
			{/foreach}
			<tr><td colspan=2><small>
				{if $poll.multivote}Multiple voting is allowed.
				{else}				Multiple voting is not allowed.
				{/if}
				{plural num=$poll.voters what='user'} has voted so far
				{if $poll.multivote}Total votes: {$poll.votes}{/if}
			</small></td></tr>
		</tbody>
	</table>
