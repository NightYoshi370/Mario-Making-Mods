<div class="container">
	<header>
		<h4 class="display-4 mb-4 text-center">{$months} {$year}</h4>
		<div class="row d-none d-sm-flex p-1 bg-dark text-white">
			{foreach $days as $day}<h5 class="col-sm p-1 text-center">{$day}</h5>{/foreach}
		</div>
	</header>
	<div class="row border border-right-0 border-bottom-0">
		{$grid}
	</div>
</div>