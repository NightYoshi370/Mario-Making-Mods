<table class="table table-striped table-bordered">
	<thead><tr><th>New Wiki Page</th></tr></thead>
	<tbody>
		<tr><td>{$fields.title}</td></tr>
		<tr><td>{$fields.text}</td></tr>
	</tbody>
	<tfoot>
		<tr><td>
			<div class="btn-group">
				{$fields.submit}
				{$fields.preview}
			</div>

			{$fields.contentbox}
			{if $fields.special}{$fields.special}{/if}
			{if $fields.deleted}{$fields.deleted}{/if}

			{$fields.token}
		</td></tr>
	</tfoot>
</table>