		<table class="table table-bordered table-striped">
			<tr class="cell2">
				<td colspan="3">
					To add, fill in both bottom fields and apply.<br />
					To edit, change either code or image fields to <em>not</em> match their hidden counterparts.
				</td>
			</tr>
			<tr class="header1">
				<th>
					Code
				</th>
				<th colspan="2">
					Image
				</th>
			</tr>
			{$smileyList}
			<tr class="header0">
				<th colspan="3">
					Add
				</th>
			</tr>
			<tr class="cell2">
				<td>
					{$code_add}
				</td>
				<td colspan="2">
					{$image_add}
				</td>
			</tr>

			<tr class=\"cell2\">
				<td colspan=\"3\">
					{$apply}
					{$token}
				</td>
			</tr>
		</table>