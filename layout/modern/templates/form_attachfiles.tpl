	<table class="table table-bordered table-striped">
		<thead><tr><th>Attach files</th></tr></thead>
		<tbody>
			{foreach $files as $file}
				<tr><td>{$file}</td></tr>
			{/foreach}
			<tr><td>{$fields.newFile}</td></tr>
		</tbody>
		<tfoot><tr><td>
			{$fields.btnSave} Maximum file size is {$fileCap}.
		</td><tr></tfoot>
	</table>