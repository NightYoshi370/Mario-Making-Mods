<table class="table table-bordered table-striped">
	<thead><tr>
		<th style="width: 30px;">#</th>
		<th>Username</th>
		<th style="width: 150px;">Registered on</th>
		<th style="width: 50px;">Posts</th>
		<th style="width: 50px;">Total</th>
	</tr></thead>
	<tbody>
		{foreach $rows as $row}
			<tr>
				<td>{$row.i}</td>
				<td>{$row.userlink}</td>
				<td>{$row.register}</td>
				<td>{$row.num}</td>
				<td>{$row.posts}</td>
			</tr>
		{/foreach}
	</tbody>
	<tfoot><tr>
		<td></td>
		<td colspan="2"><b>Total</b></td>
		<td>{$post_total}</td>
		<td>{$post_overall}</td>
	</tr></tfoot>
</table>