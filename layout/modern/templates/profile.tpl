	<ul id="profile-nav" class="nav nav-tabs" role="tablist">
		<li class="nav-item"><a class="nav-link active" href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Information</a></li>
		<li class="nav-item"><a class="nav-link" href="#comments" role="tab" id="comments-tab" data-toggle="tab" aria-controls="comments">Comments</a></li>
		<li class="nav-item"><a class="nav-link" href="#recent" role="tab" id="recent-tab" data-toggle="tab" aria-controls="recent">Recent Activity</a></li>
	</ul><br>
	<div id="profile-nav-content" class="tab-content">
		<div role="tabpanel" class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
			<div class="row">
				<div class="col-md-6 col-lg-8 col-uhd-9">
					{foreach $profileParts as $name=>$fields}
						<table class="table table-bordered table-striped profiletable">
							{if is_array($fields)}
								<thead><tr class="header1"><th colspan=2>{$name}</th></tr></thead>
								<tbody>
									{foreach $fields as $label=>$val}
										<tr class="cell{cycle values='0,1'}">
											<td class="cell2 center" style="width:15%;">{$label}</td>
											<td>{$val}</td>
										</tr>
									{/foreach}
								</tbody>
							{else}
								<thead><tr><th>{$name}</th></tr></thead>
								<tbody><tr><td>{$fields}</td></tr></tbody>
							{/if}
						</table>
					{/foreach}
				</div>

				<div class="col-md-6 col-lg-4 col-uhd-3">
					<table class="table table-bordered table-sm">
						<thead><tr class="header1"><th>RPG Status</th></tr></thead>
						<tbody><tr class="cell1"><td><img src="{$rpgstatus}" alt="RPG Status" title="RPG Status for {$username}" /></td></tr></tbody>
					</table>
					{if $equipitems}
						<table class="table table-bordered table-striped table-sm">
							<thead><tr class="header1"><th colspan=2>Equipped Items</th></tr></thead>
							<tbody>
								{foreach $equipitems as $equipitem}
									<tr>
										<td class="cell2">{$equipitem.name}</td>
										<td>{$equipitem.desc}</td>
									</tr>
								{/foreach}
							</tbody>
						</table>
					{/if}
					{if $badgers}
						<table class="table table-bordered table-striped table-sm">
							<thead><tr class="header1"><th>User Badges</th></tr></thead>
							<tbody>
								{foreach $badgers as $badger}
									<tr><td>{$badger}</td></tr>
								{/foreach}
							</tbody>
						</table>
					{/if}
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade" id="comments" aria-labelledby="comments-tab">

			{if $commentField}
				{$commentField}
			{else if !$loguserid}
				You need to be logged in to post profile comments here.
			{/if}

			{if $pagelinks}{$pagelinks}{/if}

			{foreach $comments as $cmt}
				<table class="table table-striped table-bordered">
					<tr><td><div class="media">
						<div class="media-left">
							{if $cmt.avatar}<img src="{$cmt.avatar}" class="media-object" style="max-width: 60px; width:60px;">
							{else}			<div 										  style="    width: 60px;"></div>
							{/if}
						</div>
						<div class="media-body">
							<h4 class="media-heading">{$cmt.userlink} <small>{$cmt.formattedDate}</small></h4>
							{if $cmt.deleteLink}<small style="float: right; margin: 0px 4px;">{$cmt.deleteLink}</small>{/if}
							{$cmt.text}
						</div>
					</div></td></tr>
				</table>
			{foreachelse}
				No comments.
			{/foreach}

			{if $pagelinks}{$pagelinks}{/if}
		</div>
		<div role="tabpanel" class="tab-pane fade" id="recent" aria-labelledby="recent-tab">
			{foreach $lastactivity as $post}
				<table class="table table-bordered table-striped">
					<tr><td class="cell2">{$post.contents}</td></tr>
					<tr class="header0"><th style="text-align: left !important;">
						Posted on {$post.formattedDate}
						{if $post.threadlink} in {$post.threadlink}{/if}
						{if $post.revdetail} ({$post.revdetail}){/if}
					</th></tr>
				</table>
			{/foreach}
		</div>
	</div>