	<table class="table table-bordered table-striped">
		<tr class="header1">
			<th>New reply</th>
		</tr>
		<tr class="cell0">
			<td>
				<noscript><small>{$fields.noscript}</small></noscript>
				{$fields.text}
				{if $fields.ckeditor}{$fields.ckeditor}{/if}
			</td>
		</tr>
		<tr class="cell2">
			<td>
				<div class="btn-group">
					{$fields.btnPost}
					{$fields.btnPreview}
				</div>
				{$fields.mood}
				{$fields.nopl}
				{$fields.nosm}
				{$fields.lock}
				{$fields.stick}
				{$fields.question}
			</td>
		</tr>
	</table>
