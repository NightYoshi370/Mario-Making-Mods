	<div class="alert alert-{$alert} alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		<span class="alert-link">{$msgtitle}</span>: {$message}
	</div>