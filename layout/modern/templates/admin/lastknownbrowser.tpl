	<table class="table table-striped table-bordered table-condensed">
		<thead><tr>
			<th>ID</th>
			<th>Name</th>
			<th>IP</th>
			<th>Last known browser</th>
		</tr></thead>
		<tbody>
			{foreach $items as $item}
				<tr>
					<td>{$item.id}</td>
					<td>{$item.name}</td>
					<td>{$item.ip}</td>
					<td>{$item.lkb}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>