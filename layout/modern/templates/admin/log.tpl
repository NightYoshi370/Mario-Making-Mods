	<table class="table table-striped table-bordered table-condensed">
		<thead><tr>
			<th>{"Time"|__}</th>
			<th>{"Event"|__}</th>
		</tr></thead>
		<tbody>
			{foreach $rows as $row}
				<tr>
					<td>{$row.time}</td>
					<td>{$row.event}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>