	<table class="table table-striped table-bordered">
		<thead>
			<tr><th colspan="4">{"Table Status"|__}</th></tr>
			<tr>
				<th>{'Name'|__}</th>
				<th>{'Rows'|__}</th>
				<th>{'Overhead'|__}</th>
				<th>{'Final Status'|__}</th>
			</tr>
		</thead>
		<tbody>
			{foreach $tablelist as $row}
				<tr>
					<td>{$row.Name}</td>
					<td>{$row.Rows}</td>
					<td>{$row.overhead}</td>
					<td>{$row.status}</td>
				</tr>
			{/foreach}
		</tbody>
		<tfoot><tr><td colspan="4">Excess trimmed: {$total} bytes</td></tr></tfoot>
	</table>