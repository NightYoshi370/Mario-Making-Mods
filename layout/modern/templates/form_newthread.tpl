	<table class="table table-striped table-bordered">
		<tr class="header1"><th>New thread</th></tr>
			<tr><td id="threadTitleContainer">{$fields.title}</td></tr>
			<tr><td>{$fields.description}</td></tr>
			<tr><td>{$fields.icon}</td></tr>
		<tr class="header0"><th style="height:5px;"></th></tr>
			<tr class="pollModeOff"><td>{$fields.btnAddPoll}</td></tr>
			<tr class="pollModeOn"><td>{$fields.pollQuestion}</td></tr>
			<tr class="pollModeOn"><td>
				<strong>Poll Options</strong><br>
				{$fields.pollOptions}
			</td></tr>
			<tr class="pollModeOn"><td>{$fields.pollMultivote}</td></tr>
			<tr class="pollModeOn"><td>{$fields.btnRemovePoll}</td></tr>
		<tr class="header0"><th style="height:5px;"></th></tr>
			<tr><td>
				<noscript><small>{$fields.noscript}</small></noscript>
				{$fields.text}
				{if $fields.ckeditor}{$fields.ckeditor}{/if}
			</td></tr>
		<tfoot><tr><td>
			<div class="btn-group">
				{$fields.btnPost}
				{$fields.btnPreview}
			</div>
			{$fields.mood}
			{$fields.nopl}
			{$fields.nosm}
			{$fields.lock}
			{$fields.stick}
		</td></tr></tfoot>
	</table>
