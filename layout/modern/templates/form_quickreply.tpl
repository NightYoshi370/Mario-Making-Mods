	<table class="table table-bordered table-striped table-condensed" id="quickreply">
		<thead><tr><th>Quick reply</th></tr></thead>
		<tbody>
			<tr><td>
				<noscript><small>{$fields.noscript}</small></noscript>
				{$fields.text}
				{if $fields.ckeditor}{$fields.ckeditor}{/if}
			</td></tr>
			<tr><td>
				<div class="btn-group">
					{$fields.btnPost}
					{$fields.btnPreview}
				</div>
				{$fields.mood}
				{$fields.nopl}
				{$fields.nosm}
				{$fields.lock}
				{$fields.stick}
				{$fields.question}
			</td></tr>
		</tbody>
	</table>
