	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th style="text-align:left!important;">
					{if $post.links.edit || $post.links.delete}
						<span style='float:right;text-align:right;font-weight:normal;'>
							<ul class="pipemenu">
								{if $post.links.edit}<li>{$post.links.edit}{/if}
								{if $post.links.delete}<li>{$post.links.delete}{/if}
							</ul>
						</span>
					{/if}
					<span style="font-size:125%;">{$post.title}</span><br>
					<span style="font-weight:normal;font-size:97%;">Posted on {$post.formattedDate} by {$post.userlink}</span>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr><td style="padding:10px;">{$post.text}</td></tr>
			<tr><td>{$post.comments}. {$post.replylink}</td></tr>
		</tbody>
	</table>
