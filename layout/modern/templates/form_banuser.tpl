	<table class="table table-bordered table-striped">
		<thead><tr><th>Ban {$fields.target}</th></tr></thead>
		<tbody>
			<tr><td>
				<video autoplay loop>
					<source src="https://mariomods.net/img/ban.mp4" type="video/mp4">
				</video>
			</td></tr>
			<tr><td>{$fields.duration}</td></tr>
			<tr><td><div class="form-group" style="margin-bottom: 0;">{$fields.reason}</div></td></tr>
		</tbody>
		<tfoot><tr><td>{$fields.btnBanUser}</td></tr></tfoot>
	</table>
