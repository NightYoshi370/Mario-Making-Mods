	<table class="table table-bordered table-striped">
		<thead><tr><th>Register</th></tr></thead>
		<tbody>
			<tr><td>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user-circle-o fa-fw"></i></span></div>
					{$fields.username}
				</div>
			</td></tr>
			<tr><td>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key fa-fw"></i></span></div>
					{$fields.password}
				</div><br>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key fa-fw"></i></span></div>
					{$fields.password2}
				</div>
			</td></tr>
			<tr><td>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span></div>
					{$fields.email}
				</div>
				<small>Specifying an email address is a requirement, but your email is made private by default.</small>
			</td></tr>
			<tr><td>Gender: {$fields.sex}</td></tr>
			<tr><td>
				{$fields.readfaq}<br>
				{$fields.btnRegister}
				{$fields.autologin}
			</td></tr>
		</tbody>
	</table>
