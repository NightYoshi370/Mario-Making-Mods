	<table class="table table-bordered table-striped">
		<thead><tr><th>Progress</th></tr></thead>
		<tbody><tr><td>
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="{$option.percent}"
				aria-valuemin="0" aria-valuemax="100" style="background-color: {$color}; width: {$percent}%;">
					{$percent}%
				</div>
			</div>
		</td></tr></tbody>
	</table>
