	{if $nresults>0}
		{if $pagelinks}{$pagelinks}{/if}

		<table class="table table-bordered table-striped">
			<thead><tr><th>Search results &mdash; {$resultstext}</th></tr></thead>
			<tbody>
				{foreach $results as $result}
					<tr><td>
						{$result.link}<br>
						{if $result.description}{$result.description}<br>{/if}
						<small>
							Post 
							{if $result.user}by {$result.user}{/if} 
							{if $result.formattedDate}on {$result.formattedDate}{/if} 
							{if $result.forum}in {$result.forum}{/if}</small>
					</td></tr>
				{/foreach}
			</tbody>
		</table>

		{if $pagelinks}{$pagelinks}{/if}
	{else}
		{Alert title='No results found' message='Please try again'}
	{/if}