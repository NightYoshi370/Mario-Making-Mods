	<table class="table table-bordered">
		<thead><tr><th>{$action}</th></tr></thead>
		<tbody><tr><td>
			{$redirectText}
			<div class="progress">
				<div class="progress-bar progress-bar-striped active" role="progressbar" id="theBar" style="width: 0%">
					0%
				</div>
			</div>
		</td></tr></tbody>
	</table>
	<meta http-equiv="REFRESH" content="5;URL={$redirectLink}" />
	<script>
		var progressBar = $('.progress-bar');
		var percentVal = 0;
		var target = "{$redirectLink}"

		window.setInterval(function(){
			if (percentVal >= 100) {
				return;
			}

			percentVal += 3;
			if (percentVal >= 100) {
				percentVal = 100;
			}
			progressBar.css("width", percentVal+ '%').attr("aria-valuenow", percentVal+ '%').text(percentVal+ '%'); 
		}, 130);

		if(percentVal >= 100) {
			document.location = target;
		}
	</script>
