	<table class="table table-bordered table-striped table-condensed">
		<thead><tr><th colspan=2>Search</th></tr></thead>
		<tbody>
			<tr><td class="cell2">
				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search fa-fw"></i></span></div>
					{$fields.terms}
				</div>
			</td></tr>
			<tr><td>{$fields.searchin}</td></tr>
			<tr><td>{$fields.engine}</td></tr>
		</tbody>
		<tfoot><tr><td>{$fields.btnSubmit}</td></tr></tfoot>
	</table>