<table class="table table-bordered table-striped">
	<thead><tr><th colspan="2">{"Highest Numbers"|__}</th></tr></thead>
	<tbody>
		{foreach $rewards as $reward}
			<tr>
				<td class="cell2">{$reward.title}</td>
				<td>{$reward.value}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

<table class="table table-bordered table-striped">
	<tbody>
		<tr class="header0">
			<th colspan="7" style="cursor:pointer;" onclick="$('#fulltables').toggle();">
				{"Table Status (click to toggle details)"|__}
			</th>
		</tr>
		<tr class="header1">
			<th width="15%">{"Name"|__}</th>
			<th width="14%">{"Rows"|__}</th>
			<th width="14%">{"Avg. data/row"|__}</th>
			<th width="14%">{"Data size"|__}</th>
			<th width="14%">{"Index size"|__}</th>
			<th width="14%">{"Unused data"|__}</th>
			<th width="14%">{"Total size"|__}</th>
		</tr>
	</tbody>
	<tbody id="fulltables" style="display:none;">
		{foreach $tablestats as $row}{if $row.name == 'Total'}{else}
			<tr>
				<td class="cell2">{$row.name}</td>
				<td>{$row.rows}</td>
				<td>{$row.avg}</td>
				<td>{$row.datlen}</td>
				<td>{$row.idx}</td>
				<td>{$row.datfree}</td>
				<td>{$row.dattot}</td>
			</tr>
		{/if}{/foreach}
		<tr class="header1">
			<th colspan="7" style="height: 8px;"></th>
		</tr>
	</tbody>
	<tbody>
		<tr class="cell2">
			<td class="bold">{"Total"|__}</td>
			<td>{$tablestatsTotal.rows|sp}</td>
			<td>{$tablestatsTotal.avg|sp}</td>
			<td>{$tablestatsTotal.datlen|sp}</td>
			<td>{$tablestatsTotal.idx|sp}</td>
			<td>{$tablestatsTotal.datfree|sp}</td>
			<td>{$tablestatsTotal.datlen + $tablestatsTotal.idx|sp}</td>
		</tr>
	</tbody>
</table>

<table class="table table-bordered">
	<thead><tr><th>This month's daily stats</th></tr></thead>
	<tbody><tr><td>
		<table class="table table-bordered table-striped">
			<thead><tr>
				<th>Date</th>
				<th>Total users</th>
				<th>Total threads</th>
				<th>Total posts</th>
				<th>New users</th>
				<th>New threads</th>
				<th>New posts</th>
			</tr></thead>
			<tbody>
				{foreach $dailystatrows as $dailystatrow}
					<tr>
						<td>{$dailystatrow.date}</td>
						<td>{$dailystatrow.utotal}</td>
						<td>{$dailystatrow.ttotal}</td>
						<td>{$dailystatrow.ptotal}</td>
						<td>{$dailystatrow.unew}</td>
						<td>{$dailystatrow.tnew}</td>
						<td>{$dailystatrow.pnew}</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	</td></tr></tbody>
</table>