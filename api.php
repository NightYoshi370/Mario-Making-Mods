<?php

define('BLARG', 1);
require __DIR__.'/lib/common.php';
header('Cache-Control: no-cache');
header('Content-type: text/json');

$page = $_GET['page'];
$discordUserID = 0;
$userID = 0;

if (isset($_GET['id'])) {
    $userID = (int)$_GET['id'];
}

if (isset($_GET['discordUserID'])) {
    $discordUserID = (int)$_GET['discordUserID'];
}

$bucket = 'api';
require BOARD_ROOT.'lib/pluginloader.php';

if (!isset($apireturn) && file_exists(__DIR__.'/callbacks/api/'.$page.'.php')) {
    include __DIR__.'/callbacks/api/'.$page.'.php';
}

if (!isset($apireturn)) {
    die($apireturn);
} else {
    die(['error' => 404, 'code' => 'Requested API not found. Try again']);
}