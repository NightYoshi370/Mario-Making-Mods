<?php

$submissions = __('<ul>
					<li>Name of Theme
					<li>Platform (WiiU/3DS)
					<li>Game it replaces (SMB1/SMB3/SMW/NSMBU)
					<li>Screenshots/Video
					<li>Download link
				</ul>');

Alert(__('You can submit your custom levels and tilesets here, or browse and download those created by other people.'), __('Welcome to the Mario Making Mods Depot!'));

$sidebarshow = true;
$rFora = Query('SELECT * FROM {forums} WHERE id = 3');
if (NumRows($rFora)) {
    if (HasPermission('forum.viewforum', 3)) {
        $forum = Fetch($rFora);
    } else {
        Kill(__('You do not have the permission to view the depot.'));
    }
} else {
    Kill(__('Forum not found'));
}

$depoturl = 'depot/hacks';

Alert($submissions, __('Specify the following in your submission'), 'info');
$fid = $forum['id'];

$Query = "	SELECT 
				t.*,
				p.id pid, p.date,
				su.(_userfields),
				COUNT(f.thread) AS liked
			FROM {threads} t
			LEFT JOIN {posts} p ON p.thread=t.id AND p.id=t.firstpostid
			LEFT JOIN {users} su ON su.id=t.user
			LEFT JOIN {favorites} f ON f.thread=t.id
			WHERE t.forum=$fid AND p.deleted=0 AND t.depothide=0";

$mostpopular = Query("	SELECT 
							t.*,
							su.(_userfields)
						FROM {threads} t
						LEFT JOIN {users} su ON su.id=t.user
						WHERE t.forum=$fid AND t.depothide=0
						ORDER BY views DESC LIMIT 5");
$mostrecent = Query("	SELECT 
							t.*,
							su.(_userfields)
						FROM {threads} t
						LEFT JOIN {users} su ON su.id=t.user
						WHERE t.forum=$fid AND t.depothide=0
						ORDER BY t.date DESC LIMIT 5");
$mostliked = Query('SELECT 
						t.*,
						su.(_userfields)
					FROM {threads} t
					LEFT JOIN {users} su ON su.id=t.user
					LEFT JOIN ( SELECT COUNT(*) AS liked, thread FROM favorites GROUP BY thread ) AS r ON r.thread = t.id
					WHERE t.forum={0u} AND t.depothide=0
					ORDER BY liked DESC, t.date ASC LIMIT 5', $fid);

makeAnncBar('depot');
MakeCrumbs([pageLink('depot') => __('Depot')]);

$popular = [];
while ($thread = Fetch($mostpopular)) {
    $pdata = [];

    $starter = getDataPrefix($thread, 'su_');

    $tags = ParseThreadTags($thread['title']);

    $pdata['title'] = $tags[1].'<img src="'.$thread['icon'].'"><a href="'.pageLink('entry', [
                'id'   => $thread['id'],
                'name' => slugify($tags[0]),
            ]).'">'.$tags[0].'</a>';

    $pdata['formattedDate'] = formatdate($thread['date']);
    $pdata['userlink'] = UserLink($starter);

    $popular[] = $pdata;
}

$recent = [];
while ($thread = Fetch($mostrecent)) {
    $pdata = [];

    $starter = getDataPrefix($thread, 'su_');
    $last = getDataPrefix($thread, 'lu_');

    $tags = ParseThreadTags($thread['title']);

    $pdata['title'] = $tags[1].'<img src="'.$thread['icon'].'"><a href="'.pageLink('entry', [
                'id'   => $thread['id'],
                'name' => slugify($tags[0]),
            ]).'">'.$tags[0].'</a>';

    $pdata['formattedDate'] = formatdate($thread['date']);
    $pdata['userlink'] = UserLink($starter);

    $recent[] = $pdata;
}

$liked = [];
while ($thread = Fetch($mostliked)) {
    $pdata = [];

    $starter = getDataPrefix($thread, 'su_');
    $last = getDataPrefix($thread, 'lu_');

    $tags = ParseThreadTags($thread['title']);

    $pdata['title'] = $tags[1].'<img src="'.$thread['icon'].'"><a href="'.pageLink('entry', [
                'id'   => $thread['id'],
                'name' => slugify($tags[0]),
            ]).'">'.$tags[0].'</a>';

    $pdata['formattedDate'] = formatdate($thread['date']);
    $pdata['userlink'] = UserLink($starter);

    $liked[] = $pdata;
}

RenderTemplate('depot_home', ['recent' => $recent, 'liked' => $liked, 'popular' => $popular]);
