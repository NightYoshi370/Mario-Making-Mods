<?php

//  AcmlmBoard XD - Thread display page
//  Access: all
if (!defined('BLARG')) {
    die();
}

$sidebarshow = true;
$showconsoles = false;

if (isset($ckeditor_color)) {
    echo '<script src="/ckeditor/ckeditor.js"></script>';
}

if (isset($_GET['pid'])) {
    header('HTTP/1.1 301 Moved Permanently');
    header('Status: 301 Moved Permanently');
    die(header('Location: '.actionLink('depost', $_GET['pid'])));
}

$tid = $pageParams['id'];
if (!$tid) {
    Kill(__('Enter a thread ID, and try again'), __('Unknown thread ID'));
}

$thread = $db->row('threads', ['id' => $tid]);
if (!$thread) {
    Kill(__('Unknown thread ID.'));
}

$forum = $db->row('forums', ['id' => $thread['forum']]);
if (!$forum) {
    Kill(__('Unknown forum ID.'));
}
if (!HasPermission('forum.viewforum', $thread['forum'])) {
    Kill(__('You may not access this forum.'));
}

$fid = $thread['forum'];

if ($tnext = FetchResult('SELECT min(t.date), t.forum fid FROM threads t LEFT JOIN forums f ON f.id=t.forum WHERE f.id={0} AND t.date > {1}', $fid, $thread['date'])) {
    $tnext = $db->row('threads', ['date' => $tnext]);
}

if ($tprev = FetchResult('SELECT max(t.date), t.forum fid FROM threads t LEFT JOIN forums f ON f.id=t.forum WHERE f.id={0} AND t.date < {1}', $fid, $thread['date'])) {
    $tprev = $db->row('threads', ['date' => $tprev]);
}

$threadtags = ParseThreadTags($thread['title']);
$title = $threadtags[0];
$urlname = HasPermission('forum.viewforum', $fid, true) ? $title : '';

if (isset($_GET['vote'])) {
    CheckPermission('forum.votepolls', $fid);

    if (!$loguserid) {
        Kill(__("You can't vote without logging in."));
    }
    if ($thread['closed']) {
        Kill(__("Poll's closed!"));
    }
    if (!$thread['poll']) {
        Kill(__('This is not a poll.'));
    }
    if ($loguser['token'] != $_GET['token']) {
        Kill(__('Invalid token.'));
    }

    $vote = (int) $_GET['vote'];

    $doublevote = FetchResult('SELECT doublevote FROM {poll} WHERE id={0}', $thread['poll']);
    $existing = FetchResult('SELECT COUNT(*) FROM {pollvotes} WHERE poll={0} and choiceid={1} and user={2}', $thread['poll'], $vote, $loguserid);
    if ($doublevote) {
        //Multivote.
        if ($existing) {
            Query('delete FROM {pollvotes} WHERE poll={0} and choiceid={1} and user={2}', $thread['poll'], $vote, $loguserid);
        } else {
            Query('INSERT into {pollvotes} (poll, choiceid, user) values ({0}, {1}, {2})', $thread['poll'], $vote, $loguserid);
        }
    } else {
        //Single vote only?
        //Remove any old votes BY this user on this poll, then add a new one.
        Query('delete FROM {pollvotes} WHERE poll={0} and user={1}', $thread['poll'], $loguserid);
        if (!$existing) {
            Query('INSERT into {pollvotes} (poll, choiceid, user) values ({0}, {1}, {2})', $thread['poll'], $vote, $loguserid);
        }
    }

    $ref = $_SERVER['HTTP_REFERER'] ?: actionLink('depotentry', $tid, '', $urlname);
    die(header('Location: '.$ref));
}

if (isset($_GET['post'])) {
    $postget = FetchResult('SELECT pt.text FROM {posts} p LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision WHERE p.thread={0} AND p.deleted=0 AND p.id={1} ORDER BY p.date ASC LIMIT 1', $tid, $_GET['post']);
    if ($postget && $postget != -1) {
        $postget = parseBBCode($postget);
        $postget = strip_html_tags($postget);
        $postget = preg_replace('@\[.*?\]@s', '', $postget);
        $postget = preg_replace('@\s+@', ' ', $postget);

        $postget = explode(' ', $postget);
        if (count($postget) > 30) {
            $postget = array_slice($postget, 0, 30);
            $postget[29] .= '...';
        }
        $postget = implode(' ', $postget);

        $metaStuff['description'] = htmlspecialchars($postget);
    }
} else {
    $firstpost = FetchResult('SELECT pt.text FROM {posts} p LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision WHERE p.thread={0} AND p.deleted=0 ORDER BY p.date ASC LIMIT 1', $tid);
    if ($firstpost && $firstpost != -1) {
        $firstpost = parseBBCode($firstpost);
        $firstpost = strip_html_tags($firstpost);
        $firstpost = preg_replace('@\[.*?\]@s', '', $firstpost);
        $firstpost = preg_replace('@\s+@', ' ', $firstpost);

        $firstpost = explode(' ', $firstpost);
        if (count($firstpost) > 30) {
            $firstpost = array_slice($firstpost, 0, 30);
            $firstpost[29] .= '...';
        }
        $firstpost = implode(' ', $firstpost);

        $metaStuff['description'] = htmlspecialchars($firstpost);
    }
}
$metaStuff['tags'] = getKeywords(strip_tags($thread['title']));

$db->updateId('threads', ['views' => pudl::increment()], 'id', $tid);

$links = [];

if ($fid == 3) {
    if (!empty($thread['downloadtheme3ds'])) {
        $links[htmlspecialchars($thread['downloadtheme3ds'])] = ['text' => __('Download 3DS Theme')];
    }

    if (!empty($thread['downloadthemewiiu'])) {
        $links[htmlspecialchars($thread['downloadthemewiiu'])] = ['text' => __('Download WiiU Theme')];
    }
    if (!empty($thread['downloadcostumewiiu'])) {
        $links[htmlspecialchars($thread['downloadcostumewiiuwiiu'])] = ['text' => __('Download WiiU Costume')];
    }

    if ($tprev) {
        $links[pageLink('entry', ['id' => $tprev['id'], 'name' => htmlspecialchars($tprev['title'])])] = ['text' => __('Previous Entry')];
    }
    if ($tnext) {
        $links[pageLink('entry', ['id' => $tnext['id'], 'name' => htmlspecialchars($tnext['title'])])] = ['text' => __('Next Entry')];
    }

    $depoturl = 'depot/hacks';
    $tweetlink = pageLink('entry', ['id' => $tid, 'name' => '']);
} elseif ($fid == 7) {
    if (!empty($thread['downloadlevel3ds'])) {
        $links[$thread['downloadlevel3ds']] = ['text' => __('Download 3DS Level')];
    }

    if (!empty($thread['downloadlevel3ds'])
     && !empty($thread['downloadlevelwiiu'])
     && substr($thread['downloadlevel3ds'], 0, 22) === 'https://smmdb.ddns.net'
     && strpos($thread['downloadlevelwiiu'], '://') == false) {
        $links[str_ireplace('3ds', 'zip', $thread['downloadlevel3ds'])] = ['text' => __('Download WiiU Level')];
        $links['https://supermariomakerbookmark.nintendo.net/courses/'.$thread['downloadlevelwiiu']] = ['text' => __('Super Mario Maker BookMark')];
    } elseif (!empty($thread['downloadlevelwiiu'])) {
        if (!strpos($thread['downloadlevelwiiu'], '://')) {
            $links[$thread['downloadlevelwiiu']] = ['text' => __('Download WiiU Level')];
        } else {
            $links['https://supermariomakerbookmark.nintendo.net/courses/'.$thread['downloadlevelwiiu']] = ['text' => __('Super Mario Maker Bookmark')];
        }
    }

    if ($tprev) {
        $links[pageLink('lvlentry', ['id' => $tprev['id'], 'name' => htmlspecialchars($tprev['title'])])] = ['text' => __('Previous Entry')];
    }
    if ($tnext) {
        $links[pageLink('lvlentry', ['id' => $tnext['id'], 'name' => htmlspecialchars($tnext['title'])])] = ['text' => __('Next Entry')];
    }

    $depoturl = 'depot/level';
    $tweetlink = pageLink('lvlentry', ['id' => $tid, 'name' => '']);
} else {
    die(header('Location: /'.actionLink('thread', $tid, '', $urlname)));
}

$links['http://twitter.com/share?text='.$threadtags[0].'&url='.getServerDomainNoSlash().$tweetlink.'/&via=MarioMakingMods'] = ['icon' => 'twitter', 'text' => __('Tweet')];

if ($loguserid) {
    $notclosed = (!$thread['closed'] || HasPermission('mod.closethreads', $fid));

    if (FetchResult('SELECT COUNT(*) FROM {favorites} WHERE user={0} AND thread={1}', $loguserid, $tid) > 0) {
        $links[actionLink('favorites', $tid, 'action=remove&token='.$loguser['token'])] = ['icon' => 'star-o', 'text' => __('Unstar Entry')];
    } else {
        $links[actionLink('favorites', $tid, 'action=add&token='.$loguser['token'])] = ['icon' => 'star', 'text' => __('Star Entry')];
    }

    // we also check mod.movethreads because moving threads is done on editthread
    if ((HasPermission('forum.renameownthreads', $fid) && $thread['user'] == $loguserid) ||
        (HasPermission('mod.renamethreads', $fid) || HasPermission('mod.movethreads', $fid))
        && $notclosed) {
        $links[actionLink('editdepotentry', $tid)] = ['icon' => 'pencil', 'text' => __('Edit Depot Entry')];
    }

    if (HasPermission('mod.closethreads', $fid)) {
        if ($thread['closed']) {
            $links[actionlink('editdepotentry', $tid, 'action=open&key='.$loguser['token'])] = ['icon' => 'unlock', 'text' => __('Open')];
        } else {
            $links[actionLink('editdepotentry', $tid, 'action=close&key='.$loguser['token'])] = ['icon' => 'lock', 'text' => __('Close')];
        }
    }

    if (HasPermission('forum.postreplies', $fid)) {
        // allow the user to directly post in a closed thread if they have permission to open it
        if ($notclosed) {
            $links[actionLink('newcomment', $tid, '', $urlname)] = ['icon' => 'comment', 'text' => __('New Comment')];
        } elseif ($thread['closed']) {
            $links[] = ['text' => __('Entry closed')];
        }
    }
}

if (!isset($ckeditor_color)) {
    LoadPostToolbar();
}

if ($fid == 3) {
    MakeCrumbs([
        pageLink('depot')     => __('Depot'),
        pageLink('haxsdepot') => __('Super Mario Maker Mods'),
        pageLink('entry', ['id' => $tid, 'name' => $urlname]) => $threadtags[0],
    ], $links);
} elseif ($fid == 7) {
    MakeCrumbs([
        pageLink('depot')      => __('Depot'),
        pageLink('leveldepot') => __('Super Mario Maker Levels'),
        pageLink('lvlentry', ['id' => $tid, 'name' => $urlname]) => $threadtags[0],
    ], $links);
}

if ($thread['progress']) {
    RenderTemplate('progress_mod', ['percent' => $thread['progress'], 'color' => 'blue']);
}

if ($thread['poll']) {
    $poll = Fetch(Query('SELECT p.*,
							(SELECT COUNT(DISTINCT user) FROM {pollvotes} pv WHERE pv.poll = p.id) as users,
							(SELECT COUNT(*) FROM {pollvotes} pv WHERE pv.poll = p.id) as votes
						 FROM {poll} p
						 WHERE p.id={0}', $thread['poll']));

    if (!$poll) {
        Kill(__('Poll not found'));
    }

    $totalVotes = $poll['users'];

    $rOptions = Query('SELECT pc.*,
							(SELECT COUNT(*) FROM {pollvotes} pv WHERE pv.poll = {0} AND pv.choiceid = pc.id) as votes,
							(SELECT COUNT(*) FROM {pollvotes} pv WHERE pv.poll = {0} AND pv.choiceid = pc.id AND pv.user = {1}) as myvote
					   FROM {poll_choices} pc
					   WHERE poll={0}', $thread['poll'], $loguserid);
    $defaultColors = [
                  '#0000B6', '#00B600', '#00B6B6', '#B60000', '#B600B6', '#B66700', '#B6B6B6',
        '#676767', '#6767FF', '#67FF67', '#67FFFF', '#FF6767', '#FF67FF', '#FFFF67', '#FFFFFF', ];

    $pdata = [];
    $pdata['question'] = htmlspecialchars($poll['question']);
    $pdata['options'] = [];

    while ($option = Fetch($rOptions)) {
        $odata = [];

        $odata['color'] = htmlspecialchars($option['color']);
        if ($odata['color'] == '') {
            $odata['color'] = $defaultColors[($option['id'] + 9) % 15];
        }

        $chosen = $option['myvote'] ? '&#x2714;' : '';

        if ($loguserid && (!$thread['closed'] || HasPermission('mod.closethreads', $fid)) && HasPermission('forum.votepolls', $fid)) {
            $label = $chosen.' '.actionLinkTag(htmlspecialchars($option['choice']), 'depotentry', $thread['id'], 'vote='.$option['id'].'&token='.$loguser['token'], $urlname);
        } else {
            $label = $chosen.' '.htmlspecialchars($option['choice']);
        }
        $odata['label'] = $label;

        $odata['votes'] = $option['votes'];
        if ($totalVotes > 0) {
            $width = (100 * $odata['votes']) / $totalVotes;
            $odata['percent'] = sprintf('%.4g', $width);
        } else {
            $odata['percent'] = 0;
        }

        $pdata['options'][] = $odata;
    }

    $pdata['multivote'] = $poll['doublevote'];
    $pdata['votes'] = $poll['votes'];
    $pdata['voters'] = $totalVotes;

    RenderTemplate('poll', ['poll' => $pdata]);
}

Query('INSERT into {threadsread} (id,thread,date) values ({0}, {1}, {2}) on duplicate key UPDATE date={2}', $loguserid, $tid, time());

$total = $thread['replies']; //+1 for the OP
$ppp = $loguser['threadsperpage'];
if (!$ppp) {
    $ppp = 50;
}
if (isset($_GET['from'])) {
    $from = $_GET['from'];
} else {
    $from = 0;
}

$rFirstPost = Query('
			SELECT
				p.*,
				pt.text, pt.revision, pt.user AS revuser, pt.date AS revdate,
				u.(_userfields), u.(rankset,title,picture,posts,postheader,signature,signsep,lastposttime,lastactivity,regdate,globalblock,fulllayout),
				ru.(_userfields),
				du.(_userfields)
			FROM
				{posts} p
				LEFT JOIN {posts_text} pt ON pt.pid = p.id AND pt.revision = p.currentrevision
				LEFT JOIN {users} u ON u.id = p.user
				LEFT JOIN {users} ru ON ru.id=pt.user
				LEFT JOIN {users} du ON du.id=p.deletedby
			WHERE thread={1}
			ORDER BY date ASC LIMIT 1', $loguserid, $tid, $from, $ppp);

    while ($FirstPost1 = Fetch($rFirstPost)) {
        $FirstPost1['closed'] = $thread['closed'];
        $FirstPost1['firstpostid'] = $thread['firstpostid'];
        MakePost($FirstPost1, POST_DEPOT_MAIN, ['tid'=>$tid, 'fid'=>$fid]);
    }

$rPosts = Query('
			SELECT
				p.*,
				pt.text, pt.revision, pt.user AS revuser, pt.date AS revdate,
				u.(_userfields), u.(rankset,title,picture,posts,postheader,signature,signsep,lastposttime,lastactivity,regdate,globalblock,fulllayout),
				ru.(_userfields),
				du.(_userfields),
				t.title AS threadname
			FROM
				{posts} p
				LEFT JOIN {posts_text} pt ON pt.pid = p.id AND pt.revision = p.currentrevision
				LEFT JOIN {users} u ON u.id = p.user
				LEFT JOIN {users} ru ON ru.id=pt.user
				LEFT JOIN {users} du ON du.id=p.deletedby
				LEFT JOIN {threads} t ON t.id=p.thread
			WHERE thread={1} AND p.deleted <> 1 AND p.id <> {4u} 
			ORDER BY date ASC LIMIT {2u}, {3u}', $loguserid, $tid, $from, $ppp, $thread['firstpostid']);
$numonpage = NumRows($rPosts);

$pagelinks = PageLinks(actionLink('depotentry', $tid, 'from=', $urlname), $ppp, $from, $total);

    if (NumRows($rPosts)) {
        $comments = [];
        while ($post = Fetch($rPosts)) {
            $post['closed'] = $thread['closed'];
            $post['firstpostid'] = $thread['firstpostid'];
            $comments[] = MakePost($post, POST_PROFILE, ['tid'=>$tid, 'fid'=>$fid]);
        }
        RenderTemplate('postbox_comments', ['pagelinks' => $pagelinks, 'posts' => $comments]);
    }

if ($loguserid && HasPermission('forum.postreplies', $fid) && !$thread['closed']) {
    $ninja = FetchResult('SELECT id FROM {posts} WHERE thread={0} ORDER BY date DESC LIMIT 0, 1', $tid);

    $mod_lock = '';
    if (HasPermission('mod.closethreads', $fid)) {
        if (!$thread['closed']) {
            $mod_lock = checkbox('lock', __('Close thread', 1));
        } else {
            $mod_lock = checkbox('unlock', __('Open thread', 1));
        }
    }

    $moodOptions = '<option selected="selected" value="0">'.__('[Default avatar]')."</option>\n";
    $rMoods = Query('SELECT mid, name FROM {moodavatars} WHERE uid={0} ORDER BY mid asc', $loguserid);
    while ($mood = Fetch($rMoods)) {
        $moodOptions .= '<option value="'.$mood['mid'].'">'.htmlspecialchars($mood['name']).'</option>';
    }

    $fields = [
        'text' => '<textarea name="text" id="text" rows="10" class="form-control"></textarea>',
        'mood' => '<select size=1 name="mood">'.$moodOptions.'</select>',
        'nopl' => checkbox('nopl', __('Disable post layout', 1)),
        'nosm' => checkbox('nosm', __('Disable smilies', 1)),
        'lock' => $mod_lock,

        'btnPost'    => '<input type="submit" class="btn btn-primary" name="actionpost" value="'.__('Post').'">',
        'btnPreview' => '<input type="submit" class="btn btn-default" name="actionpreview" value="'.__('Preview').'">',
    ];

    if (isset($ckeditor_color)) {
        $fields['ckeditor'] = "<script>CKEDITOR.replace( 'text', {uiColor: '$ckeditor_color', languages: 'en'});</script>";
    }

    echo '<form action="'.htmlentities(actionLink('newreply', $tid)).'" method="post">
			<input type="hidden" name="ninja" value="'.$ninja.'">';

    RenderTemplate('form_quickreply', ['fields' => $fields]);

    echo '</form>';
}
