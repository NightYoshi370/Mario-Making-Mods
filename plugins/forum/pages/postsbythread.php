<?php

if (!defined('BLARG')) {
    die();
}
    checknumeric($_GET['id']);

    $qstrings = [];
if ($_GET['id']) {
    $qstrings[] = "p.user = {$_GET['id']}";
    $by = 'by '.FetchResult("SELECT name FROM users WHERE id = {$_GET['id']}");
} else {
    $by = '';
}

    $_GET['posttime'] = isset($_GET['posttime']) ? (int) $_GET['posttime'] : 86400;

if ($_GET['posttime'] == '0') {
    $_GET['posttime'] = 9999999999;
}

    $qstrings[] = 'p.date > '.(time() - $_GET['posttime']);
    $during = ' during the last '.timeunits2($_GET['posttime']);

if (empty($qstrings)) {
    $qwhere = '1';
} else {
    $qwhere = implode(' AND ', $qstrings);
}

    $posters = Query(
        "SELECT
						t.id, t.replies, t.title, t.forum, t.icon, f.id, f.title ftitle, COUNT(p.id) cnt
					FROM {threads} t
					LEFT JOIN {forums} f ON f.id = t.forum
					LEFT JOIN {posts}  p ON t.id = p.thread
					WHERE {$qwhere}
					GROUP BY t.id
					ORDER BY cnt DESC, t.date DESC
					LIMIT 1000"
    );

    $spans = [3600 => __('1 hour'), 86400 => __('1 day'), 259200 => __('3 days'), 604800 => __('1 week'), 31557600 => __('Last year')];
    if ($qid) {
        $spans[0] = __('Total');
    }

    $options = [];
    foreach ($spans as $span => $desc) {
        if ($span == $_GET['posttime']) {
            $options[] = $desc;
        } else {
            $options[] = actionLinkTag($desc, 'postsbythread', $qid, 'posttime='.$span);
        }
    }

    $rows = [];
    $i = 1;
    while ($t = fetch($posters)) {
        $rowdata = [];

        if (!HasPermission('forum.viewforum', $t['forum'])) {
            $rowdata['icon'] = '';
            $rowdata['forum'] = '(restricted forum)';
            $rowdata['thread'] = '(private thread)';
        } else {
            if ($t['icon']) {
                $rowdata['icon'] = $t['icon'];
            }

            $tags = ParseThreadTags($t['title']);

            $rowdata['forum'] = actionLinkTag(htmlspecialchars($t['ftitle']), 'forum', $t['forum']);
            $rowdata['thread'] = $tags[1].actionLinkTag(htmlspecialchars($tags[0]), 'thread', $t['id']);
        }

        $rowdata['i'] = $i;
        $rowdata['cnt'] = $t['cnt'];
        $rowdata['replies'] = $t['replies'] + 1;

        $rows[] = $rowdata;
        $i++;
    }

    RenderTemplate(
        'postsbythread', [
        'timelinks' => $options,
        'from'      => $by,
        'during'    => $during,
        'rows'      => $rows,
        'id'        => $qid,
        ]
    );
