<?php

if (!defined('BLARG')) {
    die();
}
    checknumeric($_GET['id']);

    $qstrings = [];
if ($_GET['id']) {
    $qstrings[] = "p.user = {$_GET['id']}";
    $by = 'by '.FetchResult("SELECT name FROM users WHERE id = {$_GET['id']}");
} else {
    $by = '';
}

    $_GET['posttime'] = isset($_GET['posttime']) ? (int) $_GET['posttime'] : 86400;

if ($_GET['posttime'] == '0') {
    $_GET['posttime'] = 9999999999;
}

    $qstrings[] = 'p.date > '.(time() - $_GET['posttime']);
    $during = ' during the last '.timeunits2($_GET['posttime']);
    $timeid = '&time='.$_GET['posttime'];

if (!empty($qstrings)) {
    $qwhere = ' WHERE '.implode(' AND ', $qstrings);
} else {
    $qwhere = '';
}

    $posters = Query(
        "
					SELECT
						f.*,
						COUNT(p.id) AS cnt
					FROM forums f
					LEFT JOIN threads t ON f.id = t.forum
					LEFT JOIN posts   p ON t.id = p.thread
					{$qwhere}
					GROUP BY f.id
					ORDER BY cnt DESC"
    );

    $userposts = FetchResult("SELECT COUNT(*) FROM posts p $qwhere");

    $qid = ($_GET['id'] ? $_GET['id'] : '');

    $spans = [3600 => __('1 hour'), 86400 => __('1 day'), 259200 => __('3 days'), 604800 => __('1 week'), 31557600 => __('Last year')];
    if ($qid) {
        $spans[0] = __('Total');
    }

    $options = [];
    foreach ($spans as $span => $desc) {
        if ($span == $_GET['posttime']) {
            $options['Posts within'] = $desc;
        } else {
            $options[] = actionLinkTag($desc, 'postsbyforum', $qid, 'posttime='.$span);
        }
    }

    $dropdownlinks = [];
    foreach ($spans as $span=>$desc) {
        $dropdownlinks['Posts within'][actionLink('postsbyforum', $qid, 'posttime='.$span)] = ['text' => $desc, 'active' => ($span == $_GET['posttime'] ? true : false)];
    }

    MakeCrumbs([actionLink('postsbyforum') => __('Posts by forum')], [], $dropdownlinks);

    $rows = [];
    $i = 1;

    while ($f = Fetch($posters)) {
        $rowdata = [];

        if (!HasPermission('forum.viewforum', $f['id'])) {
            $rowdata['link'] = '(restricted)';
            $rowdata['viewall'] = '(<s><b>view</b></s>)';
        } else {
            $rowdata['link'] = actionLinkTag($f['title'], 'forum', $f['id']);
            $rowdata['viewall'] = '('.actionLinkTag('View', 'postsbyuser', $_GET['id'], '&forum='.$f['id'].$timeid).')';
        }
        $rowdata['cnt'] = $f['cnt'];
        $rowdata['numposts'] = $f['numposts'];
        $rowdata['i'] = $i;
        $rowdata['percentage'] = percentageOf($f['cnt'], $userposts);

        $rows[] = $rowdata;
        $i++;
    }

    RenderTemplate(
        'postsbyforum', [
        'timelinks' => $options,
        'from'      => $by,
        'during'    => $during,
        'rows'      => $rows,
        'id'        => $qid,
        'userposts' => $userposts,
        ]
    );
