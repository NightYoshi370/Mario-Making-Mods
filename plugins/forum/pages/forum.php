<?php

if (!defined('BLARG')) {
    die();
}

if (!isset($pageParams['id'])) {
    Kill(__('Forum ID unspecified.'));
}

$fid = $pageParams['id'];
$forum = $db->row('forums', ['id' => $fid]);
if (!$forum || !HasPermission('forum.viewforum', $fid)) {
    Kill(__('Unknown forum ID.'));
}

checknumeric($fid);

if ($forum['redirect']) {
    die(header('Location: /'.forumRedirectURL($forum['redirect'])));
}

$title = htmlentities($forum['title']);
$urlname = HasPermission('forum.viewforum', $fid, true) ? $title : '';

$links = [];
if ($loguserid) {
    if ($_GET['action'] == 'markasread') {
        Query(
            'REPLACE INTO {threadsread} (id,thread,date) SELECT {0}, {threads}.id, {1} FROM {threads} WHERE {threads}.forum={2}',
            $loguserid,
            time(),
            $fid
        );

        die(header('Location: /'.actionLink('board', $forum['board'])));
    }

    $links[pageLink('forum', ['id' => $fid, 'name' => $urlname], 'action=markasread')] = ['icon' => 'ok', 'text' => __('Mark forum read')];

    if (isset($_GET['ignore'])) {
        $isIgnored = FetchResult('SELECT COUNT(*) FROM {ignoredforums} WHERE uid={0} AND fid={1}', $loguserid, $fid) == 1;
        if ($isIgnored) {
            Query('DELETE FROM {ignoredforums} WHERE uid={0} AND fid={1}', $loguserid, $fid);
            Alert(format(__('{0} is no longer ignored'), '<a class="alert-link" href="'.pageLink('forum', ['id' => $fid, 'name' => $urlname]).'">'.$title.'</a>'));
        } else {
            $db->insert('ignoredforums', ['uid' => $loguserid, 'fid' => $fid]);
            Alert(format(__('{0} is now ignored'), '<a class="alert-link" href="'.pageLink('forum', ['id' =>$fid, 'name' => $urlname]).'">'.$title.'</a>'));
        }
    }

    $isIgnored = FetchResult('SELECT COUNT(*) FROM {ignoredforums} WHERE uid={0} AND fid={1}', $loguserid, $fid) == 1;
    if ($isIgnored) {
        $ignoretext = ['icon' => 'eye-open', 'text' => format(__('Unignore "{0}"'), $title)];
    } else {
        $ignoretext = ['icon' => 'eye-close', 'text' => format(__('Ignore "{0}"'), $title)];
    }

    $links[pageLink('forum', ['id' => $fid, 'name' => $urlname], 'ignore')] = $ignoretext;

    if (HasPermission('forum.postthreads', $fid)) {
        $links[actionLink('newthread', $fid, '', $urlname)] = ['icon' => 'comment', 'text' => __('Post thread')];
    }
}

$dropdownlink = [];

$metaStuff['description'] = htmlspecialchars(strip_tags($forum['description']));
$metaStuff['tags'] = getKeywords(strip_tags($forum['title']));

$OnlineUsersFid = $fid;
MakeCrumbs(forumCrumbs($forum), $links);

if (!$mobileLayout) {
    $statData = Fetch(Query('SELECT

		(SELECT COUNT(*) FROM {threads}) AS totalnumThreads,
		(SELECT COUNT(*) FROM {posts}) AS totalnumPosts,
		(SELECT COUNT(*) FROM {threads} WHERE forum = {0}) AS numThreads,
		(SELECT COUNT(*) FROM {posts} p LEFT JOIN {threads} t ON t.id=p.thread WHERE t.forum = {0}) AS numPosts,

		(SELECT COUNT(*) FROM {users}) AS numUsers,
		(SELECT COUNT(*) FROM {users} WHERE lastposttime > {4}) AS numActive,

		(SELECT COUNT(*) FROM {posts} WHERE date > {2}) AS totalnewPostLastHour,
		(SELECT COUNT(*) FROM {posts} WHERE date > {1}) AS totalnewPostToday,
		(SELECT COUNT(*) FROM {posts} WHERE date > {3}) AS totalnewPostLastWeek,

		(SELECT COUNT(*) FROM {posts} p LEFT JOIN {threads} t ON t.id=p.thread WHERE t.forum = {0} AND p.date > {2}) AS newPostLastHour,
		(SELECT COUNT(*) FROM {posts} p LEFT JOIN {threads} t ON t.id=p.thread WHERE t.forum = {0} AND p.date > {1}) AS newPostToday,
		(SELECT COUNT(*) FROM {posts} p LEFT JOIN {threads} t ON t.id=p.thread WHERE t.forum = {0} AND p.date > {3}) AS newPostLastWeek,

		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {2}) AS totalnewThreadLastHour,
		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {1}) AS totalnewThreadToday,
		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {3}) AS totalnewThreadLastWeek,

		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {2} AND forum = {0}) AS newThreadLastHour,
		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {1} AND forum = {0}) AS newThreadToday,
		(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {3} AND forum = {0}) AS newThreadLastWeek',
							$fid,
							time() - 86400,
							time() - 3600,
							time() - 604800,
							time() - 2592000
    ));

    $statData['pctActive'] = $statData['numUsers'] ? ceil((100 / $statData['numUsers']) * $statData['numActive']) : 0;
    $statData['birthday'] = getBirthdaysText();
	$statData['online'] = getOnlineUsersText();

    $lastUser = Query('SELECT u.(_userfields) FROM {users} u ORDER BY u.regdate DESC LIMIT 1');
    if (NumRows($lastUser)) {
        $lastUser = getDataPrefix(Fetch($lastUser), 'u_');
        $statData['lastUserLink'] = UserLink($lastUser);
    }

    RenderTemplate('boardstats', ['stats' => $statData]);
}

makeAnncBar(false);

makeForumListing($fid);

$total = $forum['numthreads'];
$tpp = $loguser['threadsperpage'];
if (isset($_GET['from'])) {
    $from = (int) $_GET['from'];
} else {
    $from = 0;
}

if (!$tpp) {
    $tpp = 50;
}

$viewunlisted = HasPermission('forum.unlistedthreads', $forum);
$viewhidden = HasPermission('forum.hiddenthreads', $forum);

if ($_GET['direction'] == 'ASC') {
    $direction = 'ASC';
} else {
    $direction = 'DESC';
}

if ($_GET['sort'] == 'name') {
    $sort = 'title';
} elseif ($_GET['sort'] == 'title') {
    $sort = 'title';
} elseif ($_GET['sort'] == 'date') {
    $sort = 'date';
} else {
    $sort = 'lastpostdate';
}

if (!$viewunlisted) {
    $rThreads = Query(' SELECT
							t.*,
							'.($loguserid ? 'tr.date readdate,' : '').'
							su.(_userfields),
							lu.(_userfields)
						FROM
							{threads} t
							'.($loguserid ? 'LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={3}' : '')."
							LEFT JOIN {users} su ON su.id=t.user
							LEFT JOIN {users} lu ON lu.id=t.lastposter
						WHERE forum={0} AND t.unlisted=0 AND t.hidden=0
						ORDER BY sticky DESC, $sort $direction LIMIT {1u}, {2u}", $fid, $from, $tpp, $loguserid
    );
} else {
    $rThreads = Query(
        'SELECT
							t.*,
							'.($loguserid ? 'tr.date readdate,' : '').'
							su.(_userfields),
							lu.(_userfields)
						FROM
							{threads} t
							'.($loguserid ? 'LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={3}' : '').'
							LEFT JOIN {users} su ON su.id=t.user
							LEFT JOIN {users} lu ON lu.id=t.lastposter
						WHERE forum={0}'.(!$viewhidden ? ' AND t.hidden=0' : '')."
						ORDER BY sticky DESC, $sort $direction LIMIT {1u}, {2u}", $fid, $from, $tpp, $loguserid
    );
}

$pagelinks = PageLinks(pageLink('forum', ['id' => $fid, 'name' => $urlname], 'from='), $tpp, $from, $total);

$ppp = $loguser['postsperpage'];
if (!$ppp) {
    $ppp = 20;
}

if (NumRows($rThreads)) {
    makeThreadListing($rThreads, $pagelinks);
} elseif (!HasPermission('forum.postthreads', $fid)) {
    Alert(__('You cannot start any threads here.'), __('Empty forum'));
} elseif ($loguserid) {
    Alert(format(__('Would you like to {0}?'), actionLinkTag(__('post something'), 'newthread', $fid)), __('Empty forum'));
} else {
    Alert(format(__('{0} so you can post something.'), pageLinkTag(__('Log in'), 'login')), __('Empty forum'));
}

ForumJump();

function fj_forumBlock($fora, $catid, $selID, $indent)
{
    $ret = '';

    foreach ($fora[$catid] as $forum) {
        if ($forum['redirect']) {
            $forumlink = forumRedirectURL($forum['redirect']);
        } else {
            $forumlink = pageLink('forum', ['id' => $forum['id'], 'name' => $urlname]);
        }

        $ret .=
        '				<option value="'.htmlentities($forumlink)
        .'"'.($forum['id'] == $selID ? ' selected="selected"' : '').'>'
        .str_repeat('&nbsp; &nbsp; ', $indent).htmlspecialchars($forum['title'])
        .'</option>
';
        if (!empty($fora[-$forum['id']])) {
            $ret .= fj_forumBlock($fora, -$forum['id'], $selID, $indent + 1);
        }
    }

    return $ret;
}

$sidebar = '';

$categories = makeForumListing(0, ($forum['board'] ? $forum['board'] : ''), 'array');
if (count($categories) && !$acmlmboardLayout) {
    foreach ($categories as $category) {
        $sidebar .= '<table class="table table-bordered table-striped table-sm">
						<thead><tr><th class="center">'.$category['name'].'</th></tr></thead>
						<tbody>';
        foreach ($category['forums'] as $forum) {
            $sidebar .= '<tr><td class="center">'.$forum['link'].'</td></tr>';
        }

        $sidebar .= '</tbody>
    </table>';
    }
}

function ForumJump()
{
    global $fid, $loguserid, $loguser, $forum;

    $viewableforums = ForumsWithPermission('forum.viewforum');
    $viewhidden = HasPermission('user.viewhiddenforums');

    $rCats = Query('SELECT id, name FROM {categories} WHERE board={0} ORDER BY corder, id', $forum['board']);
    $cats = [];
    while ($cat = Fetch($rCats)) {
        $cats[$cat['id']] = $cat['name'];
    }

    $rFora = Query(
        '	SELECT
							f.id, f.title, f.catid, f.redirect
						FROM
							{forums} f
						WHERE f.id IN ({0c})'.(!$viewhidden ? ' AND f.hidden=0' : '').'
						ORDER BY f.forder, f.id', $viewableforums
    );

    $fora = [];
    while ($forum = Fetch($rFora)) {
        $fora[$forum['catid']][] = $forum;
    }

    $theList = '';
    foreach ($cats as $cid=>$cname) {
        if (empty($fora[$cid])) {
            continue;
        }

        $theList .=
        '			<optgroup label="'.htmlspecialchars($cname).'">
'.fj_forumBlock($fora, $cid, $fid, 0).
        '			</optgroup>
';
    }

    $theList = '<select onchange="document.location=this.options[this.selectedIndex].value;">'
        .($forum['board'] ? '<option value="'.actionLink('board').'">Back to main forums</option>' : '')
        .$theList
        .'</select>';

    RenderTemplate('forumjump', ['forumlist' => $theList]);
}
