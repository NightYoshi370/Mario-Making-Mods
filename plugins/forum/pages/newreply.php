<?php

if (!defined('BLARG')) {
    die();
}

if (isset($ckeditor_color)) {
    echo '<script src="/ckeditor/ckeditor.js"></script>';
}

require BOARD_ROOT.'lib/upload.php';

if (!$loguserid) { //Not logged in?
    Kill(__('You must be logged in to post.'));
}

if (isset($_POST['id'])) {
    $_GET['id'] = $_POST['id'];
}

if (!isset($_GET['id'])) {
    Kill(__('Thread ID unspecified.'));
}

$tid = (int) $_GET['id'];
checknumeric($tid);

$thread = $db->row('threads', ['id' => $tid]);
if (!$thread) {
	Kill(__('Unknown thread ID.'));
}

$forum = $db->row('forums', ['id' => $thread['forum']]);
if ($forum) {
    $fid = $forum['id'];

    if (!HasPermission('forum.viewforum', $fid)) {
        Kill(__('You may not access this forum.'));
    }

    if (!HasPermission('forum.postreplies', $fid)) {
        Kill(__('You may not post in this forum.'));
    }

    $isHidden = !HasPermission('forum.viewforum', $fid, true);
    $OnlineUsersFid = $fid;
} else {
    Kill('Unknown forum ID.');
}

if ($thread['closed'] && !HasPermission('mod.closethreads', $fid)) {
    Kill(__('This thread is locked.'));
}

if (!isset($ckeditor_color)) {
    LoadPostToolbar();
}

$tags = ParseThreadTags($thread['title']);
$urlname = $isHidden ? '' : $tags[0];
$title = htmlspecialchars($urlname).' &raquo; '.__('New reply');
MakeCrumbs(
	forumCrumbs($forum) + [actionLink('thread', $tid, '', $urlname) => $tags[0], actionLink('newreply', $tid, '', $urlname) => __('New reply')]);

$attachs = [];

if (isset($_POST['saveuploads'])) {
    $attachs = HandlePostAttachments(0, false);
} elseif (isset($_POST['actionpreview'])) {
    $attachs = HandlePostAttachments(0, false);

    $previewPost['text'] = $_POST['text'];
    $previewPost['num'] = $loguser['posts'] + 1;
    $previewPost['posts'] = $loguser['posts'] + 1;
    $previewPost['id'] = 0;
    $previewPost['options'] = 0;
    if ($_POST['nopl']) {
        $previewPost['options'] |= 1;
    }
    if ($_POST['nosm']) {
        $previewPost['options'] |= 2;
    }
    $previewPost['mood'] = (int) $_POST['mood'];
    $previewPost['has_attachments'] = !empty($attachs);
    $previewPost['preview_attachs'] = $attachs;

    foreach ($loguser as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }

    $previewPost['u_posts']++;

    MakePost($previewPost, POST_SAMPLE);
} elseif (isset($_POST['actionpost'])) {
    //Now check if the post is acceptable.
    $rejected = false;

    if (!trim($_POST['text'])) {
        Alert(__('Enter a message and try again.'), __('Your post is empty.'));
        $rejected = true;
    } elseif ($thread['lastposter'] == $loguserid && $thread['lastpostdate'] >= time() - 86400 && !HasPermission('forum.doublepsot', $forum)) {
        Alert(__("You can't double post until it's been at least one day."), __('Sorry'));
        $rejected = true;
    } else {
        $lastPost = time() - $loguser['lastposttime'];
        if ($lastPost < Settings::get('floodProtectionInterval')) {
            //Check for last post the user posted.
            $lastPost = Fetch(
                Query(
                    'SELECT p.id,p.thread,pt.text FROM {posts} p LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision 
				WHERE p.user={0} ORDER BY p.date DESC LIMIT 1', $loguserid
                )
            );

            //If it looks similar to this one, assume the user has double-clicked the button.
            if ($lastPost['thread'] == $tid && $lastPost['text'] == $_POST['text']) {
                $pid = $lastPost['id'];
                die(header('Location: '.actionLink('post', $pid)));
            }

            $rejected = true;
            Alert(__("You're going too damn fast! Slow down a little."), __('Hold your horses.'));
        }
    }

    if (!$rejected) {
        $ninja = FetchResult('SELECT id FROM {posts} WHERE thread={0} ORDER BY date DESC LIMIT 0, 1', $tid);
        if (isset($_POST['ninja']) && $_POST['ninja'] != $ninja) {
            Alert(__('You might want to review the post made while you were typing before you submit yours.'), __("Someone posted before you."));
            $rejected = true;
        }
    }

    if (!$rejected) {
        $bucket = 'checkPost';
        include './lib/pluginloader.php';
    }

    if (!$rejected) {
        $post = utfmb4String($_POST['text']);

        $options = 0;
        if ($_POST['nopl']) {
            $options |= 1;
        }
        if ($_POST['nosm']) {
            $options |= 2;
        }

        $db->updateId('users', ['posts' => pudl::increment(), 'lastposttime' => $db->time()], 'id', $loguserid);

        // Post Random ID
        $pid = mt_rand();
        $pxist = FetchResult('SELECT * FROM {posts} WHERE id = {0}', $pid);
        $pnuke = FetchResult("SELECT * FROM {nuked} WHERE id = {0} AND type = 'post'", $pid);
        while ($pnuke == $pid or $pxist == $pid) {
            while ($pxist == $pid) {
                $pid = mt_rand();
                $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
            }
            $pid = mt_rand();
            $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
            $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'post'", $pid);
        }

        $attachs = HandlePostAttachments($pid, true);
        $db->insert(
            'posts', [
				'id'              => $pid,
				'thread'          => $tid,
				'user'            => $loguserid,
				'date'            => $db->time(),
				'ip'              => $_SERVER['REMOTE_ADDR'],
				'num'             => $loguser['posts'] + 1,
				'options'         => $options,
				'mood'            => (int) $_POST['mood'],
				'has_attachments' => (!empty($attachs) ? 1 : 0),
            ]
        );
        $db->insert(
			'posts_text', [
				'pid'      => $pid,
				'text'     => $post,
				'revision' => 0,
				'user'     => $loguserid,
				'date'     => $db->time()
			]
		);

        $db->updateId(
            'forums', [
				'numposts'     => pudl::increment(),
				'lastpostdate' => $db->time(),
				'lastpostuser' => $loguserid,
				'lastpostid'   => $pid,
            ], 'id', $fid
        );

        $updateThread = [
			'lastposter'   => $loguserid,
			'lastpostdate' => $db->time(),
			'replies'      => pudl::increment(),
			'lastpostid'   => $pid
		];

        if (HasPermission('mod.closethreads', $forum['id'])) {
            if ($_POST['lock']) {
                $updateThread['closed'] = 1;
            } elseif ($_POST['unlock']) {
                $updateThread['closed'] = 0;
            }
        }

        $db->updateId('threads', $updateThread, 'id', $tid);

        Report('New reply by '.$loguser['name'].' in '.$thread['title'].' ('.$forum['title'].') -> '.getServerDomainNoSlash().actionLink('post', $pid), $isHidden);

        $bucket = 'newreply';
        include BOARD_ROOT.'lib/pluginloader.php';

        $rFavorites = Query('SELECT * FROM {favorites} WHERE thread = {0}', $tid);
        while ($stars = Fetch($rFavorites)) {
            SendNotification('favorite', $tid, $stars['user']);
        }

        $c = rand(100, 500);
        $rBonus = Query('UPDATE {usersrpg} SET spent = spent - {0} WHERE id = {1}', $c, $loguserid);

        if ($acmlmboardLayout) {
            OldRedirect(format(__('Posted! (Gained {0} bonus coins)'), $c), actionLink('post', $pid), __('the thread'));
        } else {
            newRedir(pageLink('post', ['id' => $pid]));
        }
    } else {
        $attachs = HandlePostAttachments(0, false);
    }
}

$prefill = htmlspecialchars($_POST['text']);

if ($_GET['quote']) {
    $getquote = (int) $_GET['quote'];
    $rQuote = Query(
        '
				SELECT
					p.id, p.deleted, pt.text,
					t.forum fid, 
					u.name poster,
					u.displayname displayposter
				FROM {posts} p
					LEFT JOIN {posts_text} pt on pt.pid = p.id and pt.revision = p.currentrevision
					LEFT JOIN {threads} t on t.id=p.thread
					LEFT JOIN {users} u on u.id=p.user
				WHERE p.id={0}', $getquote
    );

    if (NumRows($rQuote)) {
        $quote = Fetch($rQuote);

        if (!empty($quote['displayposter'])) {
            $poaster = htmlspecialchars($quote['displayposter']);
        } else {
            $poaster = htmlspecialchars($quote['poster']);
        }

        //SPY CHECK!
        if (!HasPermission('forum.viewforum', $quote['fid'])) {
            $poaster = 'your mom';
            $quote['text'] = __('Nice try kid, but no.');
        }

        if ($quote['deleted']) {
            $quote['text'] = __('(deleted post)');
        }

        if (isset($ckeditor_color)) {
            $prefill = '[quote="'.$poaster.'" id="'.$quote['id'].'"]'.htmlspecialchars(nl2br($quote['text'])).'[/quote]';
            $prefill = str_replace('/me', '[b]* '.htmlspecialchars($poaster).'[/b]', $prefill);
        } else {
            $prefill = '[quote="'.$poaster.'" id="'.$quote['id'].'"]'.htmlspecialchars($quote['text']).'[/quote]';
            $prefill = str_replace('/me', '[b]* '.htmlspecialchars($poaster).'[/b]', $prefill);
        }
    }
}

function getCheck($name)
{
    if (isset($_POST[$name]) && $_POST[$name]) {
        return true;
    } else {
        return false;
    }
}

$moodSelects = [];
if ($_POST['mood']) {
    $moodSelects[(int) $_POST['mood']] = 'selected="selected" ';
}
$moodOptions = '<option '.$moodSelects[0].'value="0">'.__('[Default avatar]')."</option>\n";

$rMoods = Query('select mid, name from {moodavatars} where uid={0} order by mid asc', $loguserid);

while ($mood = Fetch($rMoods)) {
    $moodOptions .= format(
        '
	<option {0} value="{1}">{2}</option>
',
        $moodSelects[$mood['mid']],
        $mood['mid'],
        htmlspecialchars($mood['name'])
    );
}

$ninja = FetchResult('SELECT id FROM {posts} WHERE thread={0} ORDER BY date DESC LIMIT 0, 1', $tid);

$fields = [
    'text' => "<textarea class=\"form-control\" name=\"text\" id=\"text\" rows=\"10\">$prefill</textarea>",
    'mood' => '<select size=1 name="mood">'.$moodOptions.'</select>',
    'nopl' => checkbox('nopl', __('Disable post layout', 1), getCheck('nopl')),
    'nosm' => checkbox('nosm', __('Disable smilies', 1), getCheck('nosm')),

    'btnPost'    => '<input class="btn btn-primary" type="submit" name="actionpost" value="'.__('Post').'">',
    'btnPreview' => '<input class="btn btn-default" type="submit" name="actionpreview" value="'.__('Preview').'">',
];

if (HasPermission('mod.closethreads', $fid)) {
    if (!$thread['closed']) {
        $fields['lock'] = checkbox('lock', __('Close thread', 1), getCheck('lock'));
    } else {
        $fields['lock'] = checkbox('unlock', __('Open thread', 1), getCheck('unlock'));
    }
}

if (isset($ckeditor_color)) {
    $fields['ckeditor'] = "<script>CKEDITOR.replace( 'text', {lang: 'en', uiColor: '$ckeditor_color'});</script>";
    $fields['noscript'] = __('Please enable Javascript in order to view the WYSIWYG editor.');
} else {
    $fields['noscript'] = __('Please enable Javascript in order to view the BBCode Toolbar.');
}

echo '
	<form name="postform" action="'.htmlentities(actionLink('newreply', $tid))."\" method=\"post\" enctype=\"multipart/form-data\">
		<input type=\"hidden\" name=\"ninja\" value=\"$ninja\">";

RenderTemplate('form_newreply', ['fields' => $fields]);

PostAttachForm($attachs);

echo '
		</form>
	<script type="text/javascript">
		document.postform.text.focus();
	</script>
';

doThreadPreview($tid);
