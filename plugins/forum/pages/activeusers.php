<?php

if (!defined('BLARG')) {
    die();
}

    $time = (int) $_GET['time'];
if ($time < 1) {
    $time = 86400;
}
    checknumeric($time);

    $users = Query(
        'SELECT u.*, COUNT(*) num 
					FROM {users} u 
					LEFT JOIN {posts} p ON p.user=u.id 
					WHERE p.date > '.(time() - $time).' 
					GROUP BY u.id ORDER BY num DESC'
    );

    $post_total = 0;
    $post_overall = 0;
    $i = 1;

    $spans = [3600 => __('1 hour'), 86400 => __('1 day'), 259200 => __('3 days'), 604800 => __('1 week')];
    $dropdownlinks = [];
    foreach ($spans as $span=>$desc) {
        $dropdownlinks['Active users within'][actionLink('activeusers', '', 'time='.$span.'&show='.$show.$fparam)] = ['text' => $desc, 'active' => ($span == $time ? true : false)];
    }

    $title = __('Active Users');
    MakeCrumbs([pageLink('board') => __('Forums'), actionLink('activeusers') => __('Active Users')], [], $dropdownlinks);

    $rows = [];
    while ($user = Fetch($users)) {
        $rowdata = [];

        $post_total += $user['num'];
        $post_overall += $user['posts'];

        $rowdata['i'] = $i;
        $rowdata['userlink'] = UserLink($user);
        $rowdata['register'] = cdate($loguser['dateformat'], $user['regdate']);
        $rowdata['num'] = $user['num'];
        $rowdata['posts'] = $user['posts'];

        $rows[] = $rowdata;
        $i++;
    }

    RenderTemplate(
        'activeusers', [
        'post_total'   => $post_total,
        'post_overall' => $post_overall,
        'rows'         => $rows,
        ]
    );
