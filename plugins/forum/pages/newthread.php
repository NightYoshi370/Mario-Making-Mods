<?php

//  MakerBoard - Thread submission/preview page
//  Access: users
//  TODO: ungroup forcage of forum ID from this. Instead, if the user forgot to insert which forum he wants, there'll be a list of options for him to pick from
if (!defined('BLARG')) {
    die();
}

require BOARD_ROOT.'lib/upload.php';

$title = __('New thread');

if (!$loguserid) { //Not logged in?
    Kill(__('You must be logged in to post.'));
}

if (isset($_POST['id'])) {
    $_GET['id'] = $_POST['id'];
}

if (!isset($_GET['id'])) {
    Kill(__('Forum ID unspecified.'));
}

$fid = (int) $_GET['id'];
checknumeric($fid);

$rFora = Query('SELECT * FROM {forums} WHERE id={0}', $fid);
if (NumRows($rFora)) {
    if (!HasPermission('forum.viewforum', $fid)) {
        Kill(__('You may not access this forum.'));
    } elseif (!HasPermission('forum.postthreads', $fid)) {
        Kill($loguser['banned'] ? __('You may not post in this forum because you are banned.') : __('You may not post threads in this forum.'));
    } else {
        $forum = Fetch($rFora);
    }
} else {
    Kill(__('Unknown forum ID.'));
}

if ($forum['locked']) {
    Kill(__('This forum is locked.'));
}

if (!isset($_POST['poll']) || isset($_GET['poll'])) {
    $_POST['poll'] = $_GET['poll'];
}

$isHidden = !HasPermission('forum.viewforum', $fid, true);
$urlname = $isHidden ? '' : $forum['title'];

$OnlineUsersFid = $fid;

MakeCrumbs(forumCrumbs($forum) + ['' => __('New thread')]);

$attachs = [];

if (isset($_POST['saveuploads'])) {
    $attachs = HandlePostAttachments(0, false);
} elseif (isset($_POST['actionpreview'])) {
    $attachs = HandlePostAttachments(0, false);

    if ($_POST['poll']) {
        $options = [];

        $pdata = [];
        $pdata['question'] = htmlspecialchars($_POST['pollQuestion']);
        $pdata['options'] = [];

        $noColors = 0;
        $defaultColors = [
                      '#0000B6', '#00B600', '#00B6B6', '#B60000', '#B600B6', '#B66700', '#B6B6B6',
            '#676767', '#6767FF', '#67FF67', '#67FFFF', '#FF6767', '#FF67FF', '#FFFF67', '#FFFFFF', ];

        $totalVotes = 0;
        foreach ($_POST['pollOption'] as $i=>$opt) {
            $opt = ['choice'=>$opt, 'color'=>$_POST['pollColor'][$i], 'votes' => rand(1, 10)];
            $totalVotes += $opt['votes'];
            $options[] = $opt;
        }

        $pops = 0;
        foreach ($options as $option) {
            $odata = [];

            $odata['color'] = htmlspecialchars($option['color']);
            if ($odata['color'] == '') {
                $odata['color'] = $defaultColors[($pops + 9) % 15];
            }

            $votes = $option['votes'];

            $label = htmlspecialchars($option['choice']);
            $odata['label'] = $label;

            $odata['votes'] = $option['votes'];
            if ($totalVotes > 0) {
                $width = (100 * $odata['votes']) / $totalVotes;
                $odata['percent'] = sprintf('%.4g', $width);
            } else {
                $odata['percent'] = 0;
            }

            $pdata['options'][] = $odata;
            $pops++;
        }

        $pdata['multivote'] = $_POST['multivote'] ? 1 : 0;
        $pdata['votes'] = $totalVotes;
        $pdata['voters'] = $totalVotes;

        RenderTemplate('poll', ['poll' => $pdata]);
    }

    $previewPost['text'] = $_POST['text'];
    $previewPost['num'] = $loguser['posts'] + 1;
    $previewPost['posts'] = $loguser['posts'] + 1;
    $previewPost['id'] = 0;
    $previewPost['options'] = 0;
    if ($_POST['nopl']) {
        $previewPost['options'] |= 1;
    }
    if ($_POST['nosm']) {
        $previewPost['options'] |= 2;
    }
    $previewPost['mood'] = (int) $_POST['mood'];
    $previewPost['has_attachments'] = !empty($attachs);
    $previewPost['preview_attachs'] = $attachs;

    foreach ($loguser as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }

    $previewPost['u_posts']++;

    MakePost($previewPost, POST_SAMPLE);
} elseif (isset($_POST['actionpost'])) {
    $titletags = parseThreadTags($_POST['title']);
    $trimmedTitle = trim(str_replace('&nbsp;', ' ', $titletags[0]));

    //Now check if the thread is acceptable.
    $rejected = false;

    if (!trim($_POST['text'])) {
        Alert(__('Enter a message and try again.'), __('Your post is empty.'));
        $rejected = true;
    } elseif (!$trimmedTitle) {
        Alert(__('Enter a thread title and try again.'), __('Your thread is unnamed.'));
        $rejected = true;
    } elseif ($_POST['poll']) {
        $optionCount = 0;
        foreach ($_POST['pollOption'] as $po) {
            if ($po) {
                $optionCount++;
            }
        }

        if ($optionCount < 2) {
            Alert(__('You need to enter at least two options to make a poll.'), __('Invalid poll.'));
            $rejected = true;
        }

        if (!$rejected && !$_POST['pollQuestion']) {
            Alert(__('You need to enter a poll question to make a poll.'), __('Invalid poll.'));
            $rejected = true;
        }
    } else {
        $lastPost = time() - $loguser['lastposttime'];
        if ($lastPost < Settings::get('floodProtectionInterval')) {
            //Check for last thread the user posted.
            $lastThread = Fetch(Query('SELECT * FROM {threads} WHERE user={0} ORDER BY id DESC LIMIT 1', $loguserid));

            //If it looks similar to this one, assume the user has double-clicked the button.
            if ($lastThread['forum'] == $fid && $lastThread['title'] == $_POST['title']) {
                newRedir(actionLink('thread', $lastThread['id']));
            }

            $rejected = true;
            Alert(__("You're going too damn fast! Slow down a little."), __('Hold your horses.'));
        }
    }

    $titleMatch = $db->row('threads', ['title' => $_POST['title']]);
    if ($titleMatch) {
        Alert(__("Please pick another one and try again."), __('Thread title has already been taken.'));
        $rejected = true;
    }

    if (!$rejected) {
        $bucket = 'checkPost';
		include BOARD_ROOT.'lib/pluginloader.php';
    }

    if (!$rejected) {
        $post = utfmb4String($_POST['text']);

        $options = 0;
        if ($_POST['nopl']) {
            $options |= 1;
        }
        if ($_POST['nosm']) {
            $options |= 2;
        }

        $iconurl = $_POST['iconurl'];

        $closed = 0;
        $sticky = 0;
        if (HasPermission('mod.closethreads', $forum['id'])) {
            $closed = ($_POST['lock'] == 'on') ? 1 : 0;
        }
        if (HasPermission('mod.stickthreads', $forum['id'])) {
            $sticky = ($_POST['stick'] == 'on') ? 1 : 0;
        }

        if ($_POST['poll']) {
            $doubleVote = ($_POST['multivote']) ? 1 : 0;
            $db->insert('poll', ['question' => $_POST['pollQuestion'], 'doublevote' => $doubleVote]);
            $pod = $db->insertId();
            foreach ($_POST['pollOption'] as $i=>$opt) {
                if ($opt) {
                    $pollColor = filterPollColors($_POST['pollColor'][$i]);
                    $db->insert('poll_choices', ['poll' => $pod, 'choice' => $opt, 'color' => $pollColor]);
                }
            }
        } else {
            $pod = 0;
        }

        // Thread Random ID
        $tid = mt_rand();
        $txist = FetchResult('SELECT * from {threads} WHERE id = {0}', $tid);
        $tnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'thread'", $tid);
        while ($tnuke == $tid or $txist == $tid) {
            while ($txist == $tid) {
                $tid = mt_rand();
                $txist = FetchResult('SELECT * from {threads} WHERE id = {0}', $tid);
            }
            $tid = mt_rand();
            $txist = FetchResult('SELECT * from {threads} WHERE id = {0}', $tid);
            $tnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'thread'", $tid);
        }

        // Post Random ID
        $pid = mt_rand();
        $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
        $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'post'", $pid);
        while ($pnuke == $pid or $pxist == $pid) {
            while ($pxist == $pid) {
                $pid = mt_rand();
                $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
            }
            $pid = mt_rand();
            $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
            $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'post'", $pid);
        }

        $db->insert(
            'threads', [
            'id'     => $tid, 'date' => $db->time(), 'forum' => $fid, 'user' => $loguserid, 'title' => $_POST['title'],
            'icon'   => $iconurl, 'lastpostdate' => $db->time(), 'lastposter' => $loguserid, 'closed' => $closed,
            'sticky' => $sticky, 'poll' => $pod, 'description' => $_POST['description'], 'firstpostid' => $pid, 'lastpostid' => $pid,
            ]
        );
        $db->updateId('users', ['posts' => pudl::increment(), 'lastposttime' => $db->time()], 'id', $loguserid);

        $attachs = HandlePostAttachments($pid, true);
        $db->insert(
            'posts', [
            'id'  => $pid, 'thread' => $tid, 'user' => $loguserid, 'date' => $db->time(), 'ip' => $_SERVER['REMOTE_ADDR'],
            'num' => $loguser['posts'] + 1, 'options' => $options, 'mood' => (int) $_POST['mood'], 'has_attachments' => (!empty($attachs) ? 1 : 0),
            ]
        );
        $db->insert('posts_text', ['pid' => $pid, 'text' => $post, 'revision' => 0, 'user' => $loguserid, 'date' => $db->time()]);

		$db->updateId('forums', ['numthreads' => pudl::increment(), 'numthreads' => pudl::increment(), 'lastpostdate' => $db->time(), 'lastpostuser' => $loguserid, 'lastpostid' => $pid], 'id', $fid);

        Report('New '.($_POST['poll'] ? 'poll' : 'thread').' by [b]'.$loguser['name'].'[/]: [b]'.$_POST['title'].'[/] ('.$forum['title'].') -> [g]#HERE#?tid='.$tid, ($isHidden ? 1 : 0));

        //newthread bucket
        $postingAsUser = $loguser;
        $thread['title'] = $_POST['title'];
        $thread['id'] = $tid;
        $bucket = 'newthread';
        include BOARD_ROOT.'lib/pluginloader.php';

        // bonus shit
        $c = rand(250, 750);
        $rBonus = Query('UPDATE {usersrpg} SET spent = spent - {0} WHERE id = {1}', $c, $loguserid);

        if ($acmlmboardLayout == true) {
            OldRedirect(format(__('Posted! (Gained {0} bonus coins)'), $c), '/'.actionLink('thread', $tid), __('the thread'));
        } else {
            newRedir('/'.actionLink('thread', $tid));
        }
    } else {
        $attachs = HandlePostAttachments(0, false);
    }
}

// Let the user try again.
$prefill = htmlspecialchars($_POST['text']);
$trefill = htmlspecialchars($_POST['title']);
$beefill = htmlspecialchars($_POST['description']);

if (!isset($_POST['iconid'])) {
    $_POST['iconid'] = 0;
}

function getCheck($name)
{
    if (isset($_POST[$name]) && $_POST[$name]) {
        return true;
    } else {
        return false;
    }
}

$iconCustomChecked = ($_POST['iconid'] == 255) ? 'checked="checked"' : '';

$i = 1;
$icons = '';
while (is_file('img/icons/icon'.$i.'.png')) {
    $checked = ($_POST['iconid'] == $i) ? 'checked="checked" ' : '';
    $icons .= '';
    $i++;
}

if ($_POST['addpoll']) {
    $_POST['poll'] = 1;
} elseif ($_POST['deletepoll']) {
    $_POST['poll'] = 0;
} elseif ($_POST['pollAdd']) {
    $_POST['pollOption'][] = '';
    $_POST['pollColor'][] = '';
} elseif ($_POST['pollRemove']) {
    $i = array_keys($_POST['pollRemove']);
    $i = $i[0];

    array_splice($_POST['pollOption'], $i, 1);
    array_splice($_POST['pollColor'], $i, 1);
}

echo '<style type="text/css">';
if ($_POST['poll']) {
    echo '.pollModeOff { display: none; }';
} else {
    echo '.pollModeOn { display: none; }';
}
echo '</style>';

$pollSettings = '<div id="pollOptions">';
if (!isset($_POST['pollOption'])) {
    $pollSettings .= '<div class="polloption">
		<input type="text" name="pollOption[0]" size=48 maxlength=40>
		&nbsp;Color: <input type="text" name="pollColor[0]" size=10 maxlength=7 class="color {hash:true,required:false,pickerFaceColor:\'black\',pickerFace:3,pickerBorder:0,pickerInsetColor:\'black\',pickerPosition:\'left\',pickerMode:\'HVS\'}">
		&nbsp; <input type="submit" name="pollRemove[0]" value="&#xD7;" onclick="removeOption(this.parentNode);return false;">
	</div>';
    $pollSettings .= '<div class="polloption">
		<input type="text" name="pollOption[1]" size=48 maxlength=40>
		&nbsp;Color: <input type="text" name="pollColor[1]" size=10 maxlength=7 class="color {hash:true,required:false,pickerFaceColor:\'black\',pickerFace:3,pickerBorder:0,pickerInsetColor:\'black\',pickerPosition:\'left\',pickerMode:\'HVS\'}">
		&nbsp; <input type="submit" name="pollRemove[1]" value="&#xD7;" onclick="removeOption(this.parentNode);return false;">
	</div>';
} else {
    foreach ($_POST['pollOption'] as $i=>$opt) {
        $color = htmlspecialchars($_POST['pollColor'][$i]);
        $opttext = htmlspecialchars($opt);

        $pollSettings .= '<div class="polloption">
		<input type="text" name="pollOption['.$i.']" value="'.$opttext.'" size=48 maxlength=40>
		&nbsp;Color: <input type="text" name="pollColor['.$i.']" value="'.$color.'" size=10 maxlength=7 class="color {hash:true,required:false,pickerFaceColor:\'black\',pickerFace:3,pickerBorder:0,pickerInsetColor:\'black\',pickerPosition:\'left\',pickerMode:\'HVS\'}">
		&nbsp; <input type="submit" name="pollRemove['.$i.']" value="&#xD7;" onclick="removeOption(this.parentNode);return false;">
	</div>';
    }
}
$pollSettings .= '</div>';
$pollSettings .= '<input type="submit" name="pollAdd" value="'.__('Add option').'" onclick="addOption();return false;">';

$moodSelects = [];
if ($_POST['mood']) {
    $moodSelects[(int) $_POST['mood']] = 'selected="selected"';
}
$moodOptions = '<option '.$moodSelects[0].' value="0">'.__('[Default avatar]').'</option>';
$rMoods = Query('SELECT mid, name FROM {moodavatars} WHERE uid={0} ORDER BY mid ASC', $loguserid);
while ($mood = Fetch($rMoods)) {
    $moodOptions .= '<option '.$moodSelects[$mood['mid']].' value="'.$mood['mid'].'">'.htmlspecialchars($mood['name']).'</option>';
}

$fields = [
    'title'         => '<input type="text" class="form-control" name="title" placeholder="'.__('Title').'" style="width: 80%;" maxlength="60" value="'.$trefill.'">',
    'description'   => '<input type="text" class="form-control" name="description" placeholder="'.__('Thread Description').'" style="width: 80%;" maxlength="50" value="'.$beefill.'">',
    'icon'          => '<input type="text" id="iconurl" class="form-control" name="iconurl" placeholder="'.__('Thread Icon URL').'" style="width: 80%;" maxlength="100" value="'.htmlspecialchars($_POST['iconurl']).'">',

    'pollQuestion'  => '<input type="text" class="form-control" name="pollQuestion" placeholder="'.__('Poll Question').'" style="width: 80%;" value="'.htmlspecialchars($_POST['pollQuestion']).'" size=80 maxlength="100">',
    'pollOptions'   => $pollSettings,
    'pollMultivote' => checkbox('multivote', __('Multivote', 1), $_POST['multivote']),

    'text'          => "<textarea name=\"text\" id=\"text\" class=\"form-control\" rows=\"10\" cols=\"80\">$prefill</textarea>",
    'mood'          => '<select size=1 name="mood">'.$moodOptions.'</select>',
    'nopl'          => checkbox('nopl', __('Disable post layout', 1), getCheck('nopl')),
    'nosm'          => checkbox('nosm', __('Disable smilies', 1), getCheck('nosm')),

    'btnPost'       => '<input type="submit" class="btn btn-primary" name="actionpost" value="'.__('Post').'">',
    'btnPreview'    => '<input type="submit" class="btn btn-secondary" name="actionpreview" value="'.__('Preview').'">',
    'btnAddPoll'    => '<input type="submit" class="btn btn-primary" name="addpoll" value="'.__('Add poll').'" onclick="addPoll();return false;">',
    'btnRemovePoll' => '<input type="submit" class="btn btn-danger" name="deletepoll" value="'.__('Remove poll').'" onclick="removePoll();return false;">',
];

if (HasPermission('mod.closethreads', $forum['id'])) {
    $fields['lock'] = checkbox('lock', __('Close Thread', 1), getCheck('lock'));
}

echo '
	<form name="postform" action="'.htmlentities(actionLink('newthread', $fid)).'" method="post" enctype="multipart/form-data">';

RenderTemplate('form_newthread', ['fields' => $fields, 'pollMode' => (int) $_POST['poll']]);

PostAttachForm($attachs);

echo '
		<input type="hidden" name="poll" id="pollModeVal" value="'.((int) $_POST['poll']).'">
	</form>
	<script type="text/javascript">
		document.postform.text.focus();
	</script>
	<script src="'.resourceLink('js/threadtagging.js').'"></script>
	<script src="'.resourceLink('js/polleditor.js').'"></script>
';

LoadPostToolbar();
