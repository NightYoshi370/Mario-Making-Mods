<?php
    checknumeric($_GET['id']);

    $qstrings = [];

if ($_GET['id']) {
    $qstrings[] = "user = {$_GET['id']}";
    $from = FetchResult("SELECT name FROM users WHERE id = {$_GET['id']}");
} else {
    $from = 'the board';
}

    $_GET['posttime'] = isset($_GET['posttime']) ? (int) $_GET['posttime'] : 86400;

if (!$_GET['posttime']) { // All posts
    $_GET['posttime'] = 2592000;
}

if ($_GET['posttime']) {
    $qstrings[] = 'date > '.(time() - $_GET['posttime']);
    $during = ' during the last '.timeunits2($_GET['posttime']);
} else {
    $during = '';
}

if (!empty($qstrings)) {
    $qwhere = ' WHERE '.implode(' AND ', $qstrings);
} else {
    $qwhere = '';
}

    $posts = Query(
        "
		SELECT COUNT(*) AS cnt, FROM_UNIXTIME(date,'%k') AS hour 
		FROM {posts} 
		{$qwhere} 
		GROUP BY hour"
    );

    $title = 'Posts by time of day';

    $qid = ($_GET['id'] ? $_GET['id'] : '');

    $spans = [3600 => __('1 hour'), 86400 => __('1 day'), 259200 => __('3 days'), 604800 => __('1 week'), 31557600 => __('Last year'), 0 => __('All-time')];
    $dropdownlinks = [];
    foreach ($spans as $span=>$desc) {
        $dropdownlinks['Posts within'][actionLink('postsbytime', $qid, 'posttime='.$span)] = ['text' => $desc, 'active' => ($span == $_GET['posttime'] ? true : false)];
    }

    $title = __('Posts by time of day');
    MakeCrumbs([actionLink('postsbytime') => __('Posts by time')], [actionLink('postsbyforum') => ['text' =>__('Posts by forum')]], $dropdownlinks);

    $postshour = array_fill(0, 24, 0);
    $max = 0;
    while ($h = Fetch($posts)) {
        if (($postshour[$h['hour']] = $h['cnt']) > $max) {
            $max = $h['cnt'];
        }
    }

    if ($theme == 'yule') {
        $rpgimagesetminibar = '/gfx/lib/bar/jul/';
    } else {
        $rpgimagesetminibar = '/gfx/lib/bar/acmlm/';
    }

    $rows = [];
    for ($i = 0; $i < 24; $i++) {
        $rowdata = [];

        $rowdata['time'] = sprintf('%1$02d:00 - %1$02d:59', $i);
        $rowdata['bar'] = drawminibar($max, 8, $postshour[$i], $rpgimagesetminibar.'bar-on.png');
        $rowdata['posts'] = $postshour[$i];

        $rows[] = $rowdata;
    }

    RenderTemplate('postsbytime', ['rows' => $rows]);
