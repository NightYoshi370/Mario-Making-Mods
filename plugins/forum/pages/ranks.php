<?php

if (!defined('BLARG')) {
    die();
}

loadRanksets();
if (count($ranksetData) == 0) {
    Kill(__('No ranksets were found.'));
}

if (!isset($pageParams['id'])) {
    $rankset = $loguser['rankset'];
    if (!$rankset || !isset($ranksetData[$rankset])) {
        $rankset = array_keys($ranksetData);
        $rankset = $rankset[0];
    }

    die(header('Location: '.pageLink('ranks', ['id' => $rankset])));
}

$where = '';
if ($_GET['showyour']) {
    $where = " WHERE u.rankset = '".$loguser['rankset']."' ";
}

$rankset = $pageParams['id'];

$ranksets = [];
foreach ($ranksetNames as $name => $ranktitle) {
    if (isset($ranksetData[$rankset]) && $name == $rankset) {
        $title = $ranktitle;
        MakeCrumbs([pageLink('board') => __('Forums'), pageLink('ranks') => __('Ranks'), pageLink('ranks', ['id' => $name]) => $ranktitle]);
    }

    $ranksets[] = pageLinkTag($ranktitle, 'ranks', ['id' => $name], ($_GET['inactive'] ? 'inactive=1' : ''));
}

usort($ranksets, 'strnatcasecmp');
RenderTemplate('ranks_list', ['ranksets' => $ranksets]);

if (!isset($ranksetData[$rankset])) {
    MakeCrumbs([pageLink('board') => __('Forums'), pageLink('ranks') => __('Ranks')]);
    Kill(__('Please select a rankset from the list above.'), __('Rankset unspecified<br>'), 'info');
}

$users = [];
$rUsers = Query("SELECT u.(_userfields), u.(posts,lastposttime,rankset) FROM {users} u $where ORDER BY id ASC");
while ($user = Fetch($rUsers)) {
    $users[$user['u_id']] = getDataPrefix($user, 'u_');
}

$ranks = $ranksetData[$rankset];

$ranklist = [];
for ($i = 0; $i < count($ranks); $i++) {
    $rdata = [];

    $rank = $ranks[$i];
    $nextRank = $ranks[$i + 1];
    if ($nextRank['num'] == 0) {
        $nextRank['num'] = $ranks[$i]['num'] + 1;
    }
    $members = [];
    $inactive = 0;
    $total = 0;
    foreach ($users as $user) {
        if ($user['posts'] >= $rank['num'] && $user['posts'] < $nextRank['num']) {
            $total++;
            if (($user['lastposttime'] > time() - 2592000) || $_GET['inactive']) {
                $members[] = UserLink($user);
            } else {
                $inactive++;
            }
        }
    }
    if ($inactive) {
        $members[] = $inactive.' inactive';
    }

    $showRank = HasPermission('admin.viewallranks') || $loguser['posts'] >= $rank['num'] || count($members) > 0;
    if ($showRank) {
        $rdata['rank'] = getRankHtml($rankset, $rank);
    } else {
        $rdata['rank'] = '???';
    }

    if (count($members) == 0) {
        $members = '&nbsp;';
    } else {
        $members = implode(', ', $members);
    }

    $rdata['posts'] = $showRank ? $rank['num'] : '???';

    $rdata['numUsers'] = $total;
    $rdata['users'] = $members;

    $ranklist[] = $rdata;
}

RenderTemplate('ranks', ['ranks' => $ranklist]);
