<?php

if (!defined('BLARG')) {
    die();
}

if ($pageParams['id']) {
    $rPost = Query('SELECT id, date, thread FROM {posts} where id={0}', $pageParams['id']);
} elseif (isset($_GET['tid']) && isset($_GET['time'])) {
    $rPost = Query(
        'select id,date,thread from {posts} where thread={0} AND date>{1} ORDER BY date LIMIT 1',
        $_GET['tid'],
        $_GET['time']
    );
} else {
    Kill(__('Please specify a post ID and try again.'), __('Unspecified Post ID'));
}

if (NumRows($rPost)) {
    $post = Fetch($rPost);
} else {
    Kill(__('Unknown post ID.'));
}

$pid = $post['id'];

$thread = $db->row('threads', ['id' => $post['thread']]);
if (!$thread) {
    Kill(__('Unknown thread ID.'));
}

if (!HasPermission('forum.viewforum', $thread['forum'])) {
    Kill(__('You do not have the permission to see this post'));
}

$tags = ParseThreadTags($thread['title']);

$ppp = $loguser['postsperpage'];
if (!$ppp) {
    $ppp = 20;
}

$from = (floor(FetchResult('SELECT COUNT(*) FROM {posts} WHERE thread={1} AND date<={2} AND id!={0}', $pid, $thread['id'], $post['date']) / $ppp)) * $ppp;
$url = actionLink('thread', $thread['id'], $from ? "from=$from&post=$pid#post$pid" : "post=$pid#post$pid", HasPermission('forum.viewforum', $thread['forum'], true) ? $tags[0] : '');

header('HTTP/1.1 301 Moved Permanently');
header('Status: 301 Moved Permanently');
die(header('Location: /'.$url));
