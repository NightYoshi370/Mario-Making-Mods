<?php
if (!defined('BLARG')) die();

$settings = [
    'username' => ['type' => 'text', 'name' => 'Custom Webhook username', 'help' => 'Leave this blank to use the default username'],
    'webhook' => ['type' => 'text', 'name' => 'Webhook link'],
    'image' => ['type' => 'text', 'name' => 'Custom Webhook avatar'],
    'embeds' => ['type' => 'boolean', 'default' => '1', 'name' => 'Use Embeds']
];
