<?php

if ($isHidden) {
    return;
}

$thename = $loguser['name'];
if ($loguser['displayname']) {
    $thename = $loguser['displayname'];
}

$link = getServerDomainNoSlash().actionLink('post', $pid);

$msg = "Post edited by $thename: ".$thread['title'].'('.$forum['title'].") -- $link";

ThreadReport($msg);
GeneralReport($msg);
