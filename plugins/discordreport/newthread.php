<?php

if ($isHidden) {
    return;
}

$thename = $loguser['name'];
if ($loguser['displayname']) {
    $thename = $loguser['displayname'];
}

$link = getServerDomainNoSlash().actionLink('thread', $tid, '', $thread['title']);

$msg = "New thread by $thename: ".$thread['title'].' ('.$forum['title'].") -- $link";

ThreadReport($msg);
GeneralReport($msg);
