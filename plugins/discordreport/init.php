<?php

$webhook = Settings::pluginGet('webhook');

function DiscordTextReport($content) {
	$webhook = [];
	
	$webhook['username'] = Settings::pluginGet('username');
	$webhook['avatar_url'] = Settings::pluginGet('image');
	$webhook['content'] = str_replace('@', '[at]', $content);

	return PostToDiscord($webhook);
}

function PostToDiscord($webhook)
{
	global $webhook;

    $curl = curl_init($webhook);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($content));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    return curl_exec($curl);
}
