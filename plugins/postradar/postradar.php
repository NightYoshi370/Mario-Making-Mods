<?php

if (!$loguserid) {
    Kill('You must be logged in to use the post radar.');
}

if (isset($_GET['remove'])) {
    $remove = checknumeric((int) $_GET['remove']);
    $rUser = Query('SELECT * FROM {users} WHERE id={0}', $remove);
    if (NumRows($rUser)) {
        Query('DELETE FROM {postradar} WHERE stalkee={0} and stalker={1}', $remove, $loguserid);
        Alert(__('User removed.'));
    } else {
        Alert("User doesn't exist.");
    }
} else if (isset($_POST['add'])) {
	addUserRadar(htmlspecialchars($_POST['add']));
}

$count = $db->rows('postradar', ['stalker' => $loguserid]);

if (count($count) > 0) {
	Write('
		<table class="outline margin">
			<tr class="header1">
				<th style="width: 20%;">Name</th>
				<th style="width: 5%;">Posts</th>
				<th style="width: 65%;">Difference</th>
			</tr>
	');

    $rEntries = Query('SELECT pr.stalkee, u.(_userfields) FROM {postradar} pr LEFT JOIN {users} u ON pr.stalkee = u.id WHERE pr.stalker={0} order by u.posts DESC', $loguserid);
    $c = 0;
    while ($user = Fetch($rEntries)) {
        $delta = '';
        if ($user['posts'] == $loguser['posts']) {
            $delta = 'Equal postcount.';
        } elseif ($user['posts'] > $loguser['posts']) {
            $delta = format('<strong>{1}</strong> posts behind.', UserLink($user), $user['posts'] - $loguser['posts']);
        } elseif ($user['posts'] < $loguser['posts']) {
            $delta = format('<strong>{1}</strong> posts ahead.', UserLink($user), $loguser['posts'] - $user['posts']);
        }

        Write('
		<tr class="cell{0}">
			<td>
				{1}
				<sup>[<a href="?page=postradar&remove={2}" title="Remove {5}">r</a>]</sup>
			</td>
			<td>
				{3}
			</td>
			<td>
				{4}
			</td>
		</tr>
', $c, UserLink($user), $user['id'], $user['posts'], $delta, $user['name']);

        $c = ($c + 1) % 2;
    }
	Write('</table>');
}
Write(
'
	<form action="/?page=postradar" method="post">
		<table class="outline margin width50">
			<tr class="header0">
				<th colspan="2">Post radar</th>
			</tr>
			<tr class="cell1">
				<td>
					Current users
				</td>
				<td>
					{0} of 5
				</td>
			</tr>
			<tr class="header1">
				<th colspan="2">Add a user</th>
			</tr>
			<tr class="cell0">
				<td>Name</td>
				<td>
					<input name="add" type="text" />
				</td>
			</tr>
			<tr class="cell0">
				<td>&nbsp;</td>
				<td>
					<input type="submit" value="Add user" />
				</td>
			</tr>
		</table>
	</form>
', $count);

function addUserRadar($stalkee) {
	global $loguserid, $db;

	$count = $db->rows('postradar', ['stalker' => $loguserid]);
    if (count($count) > 5) {
		Alert(__("Please remove a user and try again."), __('You can only have up to 5 users in your post radar at a time'));
		return;
	}

	if (empty($stalkee)) {
		Alert(__("Please check the name and try again"), __("Your username field is empty"));
		return;
	}

    if (strtolower($stalkee) == strtolower($loguser['name']) || (!empty($loguser['displayname']) && strtolower($stalkee) == strtolower($loguser['displayname']))) {
		Alert(__("Please enter a different user and try again"), __("You may not add yourself to the post radar list."));
		return;
	}

	$user = $db->row('users', [['name' => $stalkee, 'displayname' => $stalkee]]);
	if (!$user) {
		Alert(__('User not found.'));
		return;
	}

	$exist = $db->row('postradar', [ 'stalkee' => $user['id'], 'stalker' => $loguserid ]);
	if ($exist) {
		Alert("This user has already been added");
		return;
	}

	$db->insert('postradar', [ 'stalkee' => $user['id'], 'stalker' => $loguserid ]);
	Alert(__('This user has been added to your post radar.'));
}