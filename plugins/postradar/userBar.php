<?php

if ($loguserid) {
    $rEntries = Query('SELECT id, name, displayname, colorset, powerlevel, posts FROM postradar LEFT JOIN users ON postradar.user = users.id WHERE userid='.$loguserid.' order by posts DESC');
    if (NumRows($rEntries)) {
        $radar = "<ul class=\"pipemenu\">\r\n";
        while ($user = Fetch($rEntries)) {
            $delta = '';
            if ($user['posts'] == $loguser['posts']) {
                $radar .= '<li>'.UserLink($user)." (0)</li>\r\n";
            } elseif ($user['posts'] > $loguser['posts']) {
                $radar .= format("<li>{0} ({1} behind)</li>\r\n", UserLink($user), $user['posts'] - $loguser['posts']);
            } elseif ($user['posts'] < $loguser['posts']) {
                $radar .= format("<li>{0} ({1} ahead)</li>\r\n", UserLink($user), $loguser['posts'] - $user['posts']);
            }
        }
        Write(
    '
		<div class="header1 outline cell2 smallFonts margin" style="text-align: center">
			<a href="/?page=postradar" style="float: right;">Edit</a>
			Post Radar: {0}
		</div>
	', $radar);
    }
}
