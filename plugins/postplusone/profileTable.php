<?php

$received = $user['postplusones'];
if ($user['postplusones']) {
    $received .= ' ['.actionLinkTag('View...', 'listplusones', $user['id']).']';
}

$res = query(
    'SELECT COUNT(*) as ct, u.(_userfields)
FROM postplusones l
LEFT JOIN posts p on l.post=p.id
LEFT JOIN users u on u.id = l.user
WHERE p.user={0}
group BY l.user
ORDER BY COUNT(*) DESC
LIMIT 6', $user['id']
);

$plusoners = [];

while ($row = fetch($res)) {
    if (count($plusoners) == 5) {
        $plusoners[] = 'more...';
    } else {
        $plusoners[] = userLink(getDataPrefix($row, 'u_')).' ('.$row['ct'].')';
    }
}

if (count($plusoners)) {
    $received .= '<br/>'.__('From:').' '.implode(', ', $plusoners);
}

$profileParts[__('General information')][__('Total stars received')] = $received;

$given = $user['postplusonesgiven'];

$res = query(
    'SELECT COUNT(*) as ct, u.(_userfields)
FROM postplusones l
LEFT JOIN posts p on l.post=p.id
LEFT JOIN users u on u.id = p.user
WHERE l.user={0}
group BY p.user
ORDER BY COUNT(*) DESC
LIMIT 6', $user['id']
);

$plusoners = [];

while ($row = fetch($res)) {
    if (count($plusoners) == 5) {
        $plusoners[] = 'more...';
    } else {
        $plusoners[] = userLink(getDataPrefix($row, 'u_')).' ('.$row['ct'].')';
    }
}

if (count($plusoners)) {
    $given .= '<br/>'.__('To:').' '.implode(', ', $plusoners);
}

$profileParts[__('General information')][__('Total stars given')] = $given;
