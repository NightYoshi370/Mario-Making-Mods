<?php

$ajaxPage = true;

CheckPermission('user.voteposts');

$pid = (int) $pageParams['id'];

$post = Fetch(Query('SELECT * FROM {posts} WHERE id = {0}', $pid));
if (!$post) {
    die('Unknown post');
}
if (!$loguserid) {
    die('You must be logged on in order to star posts!');
}
if ($post['user'] == $loguserid) {
    die('You may not star your own posts!');
}
if ($_GET['key'] != $loguser['token']) {
    die('Nope!');
}

$thread = Fetch(Query('SELECT * FROM {threads} WHERE id = {0}', $post['thread']));
if (!$thread) {
    die('Unknown thread');
}

if (!HasPermission('forum.viewforum', $thread['forum'])) {
    die('Nice try hacker kid, but no.');
}

$vote = $db->row('postplusones', ['post' => $pid, 'user' => $loguserid]);
if (!$vote) {
    $db->updateId('posts', ['postplusones' => pudl::increment()], 'id', $pid);
    $db->updateId('users', ['postplusones' => pudl::increment()], 'id', $post['user']);
    $db->updateId('users', ['postplusonesgiven' => pudl::increment()], 'id', $loguserid);
    $db->insert('postplusones', ['user' => $loguserid, 'post' => $pid]);
    $post['postplusones']++;
    $starimg = 'star';
} else {
    $db->updateId('posts', ['postplusones' => pudl::_increment(-1)], 'id', $pid);
    $db->updateId('users', ['postplusones' => pudl::_increment(-1)], 'id', $post['user']);
    $db->updateId('users', ['postplusonesgiven' => pudl::_increment(-1)], 'id', $loguserid);
    Query('DELETE FROM {postplusones} WHERE user = {0} AND post = {1}', $loguserid, $pid);
    $post['postplusones']--;
    $starimg = 'star-o';
}

$url = pageLink('plusone', ['id' => $pid], 'key='.$loguser['token']);

echo "<span onclick=\"$(this.parentElement).load('$url'); return false;\"><i class=\"fa fa-$starimg\"></i></span> ".$post['postplusones'];
