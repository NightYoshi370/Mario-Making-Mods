<?php

if (!defined('BLARG')) {
    die();
}

$title = 'Wiki &raquo; Recent changes';

$links = [];
$links[pageLink('wikichanges')] = ['text' => __('Recent changes')];
if ($canedit) {
    $links[pageLink('wikicreate')] = ['text' => __('Create page')];
}

$mydatefmt = 'm-d-Y';
if ($loguserid) {
    $mydatefmt = $loguser['dateformat'];
}

$time = (int) $_GET['time'];
if (!$time) {
    $time = 86400;
}

$spans = [86400 => 'Today', 604800 => 'This week', 2592000 => 'This month'];
$dropdownlinks = [];
foreach ($spans as $span => $desc) {
    $dropdownlinks['Changes within'][pageLink('wikichanges', [], 'time='.$span)] = ['text' => $desc, 'active' => ($span == $time ? true : false)];
}

MakeCrumbs([pageLink('wiki') => 'Wiki', pageLink('wikichanges') => 'Recent changes'], $links, $dropdownlinks);

$today = cdate($mydatefmt, time());
$yesterday = cdate($mydatefmt, time() - 86400);
$lastts = 'lol';

$mindate = time() - $time;
$changes = Query(
    '	SELECT
						pt.*,
						u.(_userfields)
					FROM
						{wiki_pages_text} pt
						LEFT JOIN {users} u ON u.id=pt.user
					WHERE
						pt.date > {0}
					ORDER BY pt.date DESC',
    $mindate
);
if (!NumRows($changes)) {
    Kill(__('No changes to display'));
}

echo '
	<table class="table table-bordered table-striped">
		<tr class="header1">
			<th>Page</th>
			<th style="width:100px;">&nbsp;</th>
		</tr>';

while ($change = Fetch($changes)) {
    $date = $change['date'];
    $ts = cdate($mydatefmt, $date);
    if ($ts == $today) {
        $ts = __('Today');
    } elseif ($ts == $yesterday) {
        $ts = __('Yesterday');
    }

    if ($ts != $lastts) {
        $lastts = $ts;
        echo '
			<tr class="header0">
				<th colspan="2">'.$ts.'</th>
			</tr>';
    }

    $user = getDataPrefix($change, 'u_');
    $userlink = userLink($user);
    $date = formatdate($date);

    $links = actionLinkTagItem('View page', 'wiki', $change['id'], 'rev='.$change['revision']);
    $changetext = 'Page '.actionLinkTag(htmlspecialchars(url2title($change['id'])), 'wiki', $change['id']);
    if ($change['revision'] > 1) {
        $changetext .= ' edited by '.$userlink.' on '.$date.' (revision '.$change['revision'].')';
        $links .= actionLinkTagItem(__('View Changes'), 'wikidiff', $change['id'], 'rev='.$change['revision']);
    } else {
        $changetext .= ' created by '.$userlink.' on '.$date;
    }

    echo '
		<tr class="cell'.$c.'">
			<td>'.$changetext.'</td>
			<td><ul class="pipemenu">'.$links.'</ul></td>
		</tr>';
}
echo '</table>';
