<?php

if (!defined('BLARG')) {
    die();
}

$ptitle = title2url($_POST['title']);
$page = [
    'id'       => $ptitle,
    'revision' => 0,
    'flags'    => 0,
    'text'     => '',
    'new'      => 2,
    'canedit'  => $canedit,
];

if (isset($_POST['saveaction']) && !preg_match('@[^_]@', $ptitle)) {
    Kill(__('You must enter a page title.'));
}

if (!$page['canedit']) {
    Kill(__('You may not create pages.'));
}

$urltitle = $ptitle; //urlencode($ptitle);
$nicetitle = htmlspecialchars(url2title($ptitle));
$title = 'Wiki &raquo; New page';

$links = [];

$links[pageLink('wikichanges')] = ['text' => __('Recent changes')];

if (isset($_POST['saveaction'])) {
    if ($_POST['token'] !== $loguser['token']) {
        die('No.');
    }

    if (!$ptitle) {
        Kill(__('Enter a title and try again.'));
    }

    $rev = $page['revision'];

    $flags = $page['flags'];
    setflag($flags, WIKI_PFLAG_NOCONTBOX, $_POST['nocontbox']);
    if (HasPermission('wiki.makepagesspecial')) {
        setflag($flags, WIKI_PFLAG_SPECIAL, $_POST['special']);
    }
    if (HasPermission('wiki.deletepages')) {
        setflag($flags, WIKI_PFLAG_DELETED, $_POST['deleted']);
    }

    if ($_POST['text'] !== $page['text']) {
        $rev++;
        Query(
            'INSERT INTO {wiki_pages_text} (id,revision,date,user,text) VALUES ({0},{1},UNIX_TIMESTAMP(),{2},{3})',
            $page['id'],
            $rev,
            $loguserid,
            $_POST['text']
        );
    }

    Query(
        'INSERT INTO {wiki_pages} (id,revision,flags) VALUES ({0},{1},{2}) ON DUPLICATE KEY UPDATE revision={1}, flags={2}',
        $page['id'],
        $rev,
        $flags
    );

    $bucket = 'wikixd_pageedit';
    include 'lib/pluginloader.php';

    die(header('Location: /'.actionLink('wiki', $page['id'])));
}

MakeCrumbs([pageLink('wiki') => 'Wiki', pageLink('wikicreate') => 'New page'], $links);

if (isset($_POST['previewaction'])) {
    echo '<table class="table table-bordered">';

    $page['text'] = $_POST['text'];

    setflag($page['flags'], WIKI_PFLAG_NOCONTBOX, $_POST['nocontbox']);
    if (HasPermission('wiki.makepagesspecial')) {
        setflag($page['flags'], WIKI_PFLAG_SPECIAL, $_POST['special']);
    }
    if (HasPermission('wiki.deletepages')) {
        setflag($page['flags'], WIKI_PFLAG_DELETED, $_POST['deleted']);
    }

    echo '	<thead><tr><th>'.$nicetitle.'</th></tr></thead>
			<tbody><tr><td>'.wikiFilter($page['text'], $page['flags'] & WIKI_PFLAG_NOCONTBOX).'</td></tr></tbody>
		  </table>';
}

echo '<form action="'.pageLink('wikicreate').'" method="POST" name="editform">';

$fields = [];

$fields['title'] = '<input type="text" name="title" value="'.$nicetitle.'" placeholder="'.__('Title').'" class="form-control" />';
$fields['text'] = '<textarea name="text" id="text" class="form-control" rows="20" onkeydown="tabfixor(this,event);">'.htmlspecialchars($page['text']).'</textarea>';
$fields['submit'] = '<input type="submit" name="saveaction" class="btn btn-primary" value="'.__('Save').'" />';
$fields['preview'] = '<input type="submit" name="previewaction" class="btn btn-default" value="'.__('Preview').'" />';
$fields['token'] = '<input type="hidden" name="token" value="'.$loguser['token'].'" />';

$fields['contentbox'] = '<label class="checkbox-inline"><input type="checkbox" name="nocontbox" '.(($page['flags'] & WIKI_PFLAG_NOCONTBOX) ? ' checked="checked"' : '').'/> Disable contents box</label> ';
if (HasPermission('wiki.makepagesspecial')) {
    $fields['special'] = '<label class="checkbox-inline"><input type="checkbox" name="special" '.(($page['flags'] & WIKI_PFLAG_SPECIAL) ? ' checked="checked"' : '').'/> Special page</label> ';
}
if (HasPermission('wiki.deletepages')) {
    $fields['deleted'] = '<label class="checkbox-inline"><input type="checkbox" name="deleted" '.(($page['flags'] & WIKI_PFLAG_DELETED) ? ' checked="checked"' : '').'/> Deleted</label> ';
}

RenderTemplate(
    'wikicreate', [
    'title'  => $nicetitle,
    'fields' => $fields,
    ]
);
echo '</form>
<script type="text/javascript">
	document.editform.text.focus();
	window.addEventListener("load",  hookUpControls, false);
	function tabfixor(t,e)
	{
		var code = e.keyCode||e.which;
		if (code == 9)
		{
			e.preventDefault();
			t.focus();
			if (document.selection)
				document.selection.createRange().text += \'\t\';
			else
			{
				var oldpos = t.selectionEnd;
				t.value = t.value.substring(0, t.selectionEnd) + \'\t\' + t.value.substring(t.selectionEnd, t.value.length);
				t.selectionStart = t.selectionEnd = oldpos + 1;
			}
		}
	}
</script>';

function setflag(&$flags, $f, $b)
{
    if ($b) {
        $flags |= $f;
    } else {
        $flags &= ~$f;
    }
}
