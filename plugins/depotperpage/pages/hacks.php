<?php

$submissions = __('<ul>
					<li>Name of Theme
					<li>Platform (WiiU/3DS)
					<li>Game it replaces (SMB1/SMB3/SMW/NSMBU)
					<li>Screenshots/Video
					<li>Download link
				</ul>');

Alert(__('You can submit your custom levels and tilesets here, or browse and download those created by other people.'), __('Welcome to the Mario Making Mods Depot!'));

$getArgs = [];

$console = '';
$style = '';
$smmtheme = '';
$command = '';

if ($http->get('console')) {
    switch ($http->get('console')) {
        case '3ds':
            $console = '3ds';
            $command .= " AND t.downloadtheme3ds <> '' ";
            break;
        case 'wiiu':
            $console = 'wiiu';
            $command .= " AND (t.downloadthemewiiu <> '' OR t.downloadcostumewiiu <> '') ";
            break;
    }
    $getArgs[] = 'console='.$console;
}

if ($http->get('style')) {
    switch ($http->get('style')) {
        case 'smb1':
            $style = 'smb1';
            $command .= " AND t.style = 'smb1' ";
            break;
        case 'smb3':
            $style = 'smb3';
            $command .= " AND t.style = 'smb3' ";
            break;
        case 'smw':
            $style = 'smw';
            $command .= " AND t.style = 'smw' ";
            break;
        case 'nsmbu':
            $style = 'nsmbu';
            $command .= " AND t.style = 'nsmbu' ";
            break;
        case 'custom':
            $style = 'custom';
            $command .= " AND t.style = 'custom' ";
            break;
    }
    $getArgs[] = 'style='.$style;
}

if ($http->get('theme')) {
    switch ($http->get('theme')) {
        case 'grass':
            $smmtheme = 'grass';
            $command .= " AND t.theme = 'grass' ";
            break;
        case 'under':
            $smmtheme = 'under';
            $command .= " AND t.theme = 'under' ";
            break;
        case 'water':
            $smmtheme = 'water';
            $command .= " AND t.theme = 'water' ";
            break;
        case 'castle':
            $smmtheme = 'castle';
            $command .= " AND t.theme = 'castle' ";
            break;
        case 'ghost':
            $smmtheme = 'ghost';
            $command .= " AND t.theme = 'ghost' ";
            break;
        case 'airship':
            $smmtheme = 'airship';
            $command .= " AND t.theme = 'airship' ";
            break;
    }
    $getArgs[] = 'theme='.$smmtheme;
}

if ($http->get('hackname')) {
    $command .= " AND t.title like '%".htmlspecialchars($http->get('hackname'))."%' ";
    $getArgs[] = 'hackname='.$http->get('hackname');
    $prevfield['hackname'] = $http->get('hackname');
} else {
    $prevfield['hackname'] = '';
}

$sidebarshow = true;
$rFora = Query('SELECT * FROM {forums} WHERE id = 3');
if (NumRows($rFora)) {
    if (HasPermission('forum.viewforum', $forum['id'])) {
        $forum = Fetch($rFora);
    } else {
        Kill(__('You do not have the permission to view the depot.'));
    }
}

$showconsoles = true;
$depoturl = 'depot/hacks';

$numThemes = FetchResult('SELECT COUNT(*) FROM {threads} t WHERE t.forum = 3 AND t.depothide=0 '.$command);

Alert($submissions, __('Specify the following in your submission'), 'info');
$fid = $forum['id'];

$total = $forum['numthreads'];

if (isset($_GET['depotpage'])) {
    $depotpage = (int) $_GET['depotpage'];
} else {
    $depotpage = 0;
}

$tpp = $loguser['depotperpage'];
if (!$tpp) {
    $tpp = 12;
}

$rThreads = Query("	SELECT 
						t.*,
						p.id pid, p.date,
						pt.text,
						su.(_userfields),
						lu.(_userfields)
					FROM 
						{threads} t
						LEFT JOIN {posts} p ON p.thread=t.id AND p.id=t.firstpostid
						LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision
						LEFT JOIN {users} su ON su.id=t.user
						LEFT JOIN {users} lu ON lu.id=t.lastposter
					WHERE t.forum={0} AND p.deleted=0 AND t.depothide=0 $command
					ORDER BY lastpostdate DESC LIMIT {1u}, {2u}", $fid, $depotpage, $tpp);

$numonpage = NumRows($rThreads);

$getArgs[] = 'depotpage=';
$pagelinks = PageLinks(pageLink('haxsdepot', [], implode('&', $getArgs)), $tpp, $depotpage, $numThemes);

$links = [];
if ($loguserid) {
    if (HasPermission('forum.postthreads', $fid)) {
        $links[actionLink('newthread', $fid, '', $urlname)] = ['text' => __('Post new submission')];
    }
}
makeAnncBar('depot');
MakeCrumbs([pageLink('depot') => __('Depot'), pageLink('haxsdepot') => __('Super Mario Maker Mods')], $links);

$posts = [];
while ($thread = Fetch($rThreads)) {
    $pdata = [];

    $starter = getDataPrefix($thread, 'su_');
    $last = getDataPrefix($thread, 'lu_');

    $pdata['screenshots'] = $thread['screenshot'];

    if ((strpos($pdata['screenshots'], 'https://www.youtube.com/') !== false) || (strpos($pdata['screenshots'], 'https://youtu.be/') !== false)) {
        $pdata['screenshot'] = str_replace('/watch?v=', '/embed/', '<iframe width="280" height="157" src="'.htmlspecialchars($pdata['screenshots']).'" frameborder="0" allowfullscreen></iframe>');
    } elseif (substr($pdata['screenshots'], -4) == '.mp4') {
        $pdata['screenshot'] = '<video width="280" height="157" controls><source src="'.htmlspecialchars($pdata['screenshots']).'" type="video/mp4">Your browser does not support the video tag.</video>';
    } elseif (substr($pdata['screenshots'], -4) == '.mp3' || substr($pdata['screenshots'], -4) == '.wav') {
        //Audio controls

        if (substr($pdata['screenshots'], -4) == '.mp3') {
            $pdata['audio/type'] = 'mpeg';
        } elseif (substr($pdata['screenshots'], -4) == '.wav') {
            $pdata['audio/type'] = 'wav';
        }

        $pdata['screenshot'] = '<audio controls><source src="'.htmlspecialchars($pdata['screenshots']).'" type="audio/'.$pdata['audio/type'].'">Your browser does not support the audio tag.</audio>';
    } elseif (!empty($pdata['screenshots'])) {
        $pdata['screenshot'] = '<img src="'.htmlspecialchars($pdata['screenshots']).'" style="max-width: 280px; max-height: 157px;">';
    } elseif (preg_match('/\[youtube\](.*?)\[\/youtube\]/', $pdata['text']) === 1) {
        $pdata['screenshots'] = '2';
        preg_match('(\[youtube\](.*?)\[\/youtube\])', $pdata['text'], $match);
        $pdata['screenshot'] = str_replace('/watch?v=', '/embed/', '<iframe width="280" height="157" src="'.htmlspecialchars($match[1]).'" frameborder="0" allowfullscreen></iframe>');
    }
    $pdata['description'] = parseBBCode($thread['description']);

    $tags = ParseThreadTags($thread['title']);

    $pdata['download'] = '';
    if (!empty($thread['downloadtheme3ds'])) {
        $pdata['download'] .= '<a href="'.htmlspecialchars($thread['downloadtheme3ds']).'">Download 3DS Theme</a>';
    }
    if (!empty($thread['downloadtheme3ds']) && (!empty($thread['downloadthemewiiu']) || !empty($thread['downloadcostumewiiu']))) {
        $pdata['download'] .= ' | ';
    }
    if (!empty($thread['downloadthemewiiu'])) {
        $pdata['download'] .= '<a href="'.htmlspecialchars($thread['downloadthemewiiu']).'">Download WiiU Theme</a>';
    } elseif (!empty($thread['downloadcostumewiiu'])) {
        $pdata['download'] .= '<a href="'.htmlspecialchars($thread['downloadcostumewiiu']).'">Download WiiU Costume</a>';
    }

    if ($loguser['layout'] == 'acmlm') {
        $pdata['title'] = $tags[1].'<img src="'.$thread['icon'].'"><a href="'.pageLink('entry', [
                    'id'   => $thread['id'],
                    'name' => slugify($tags[0]),
                ]).'">'.$tags[0].'</a>';
    } else {
        $pdata['title'] = '<img src="'.$thread['icon'].'"><a href="'.pageLink('entry', [
                    'id'   => $thread['id'],
                    'name' => slugify($tags[0]),
                ]).'">'.$tags[0].'</a><br>'.$tags[1];
    }

    $pdata['formattedDate'] = formatdate($thread['date']);
    $pdata['userlink'] = UserLink($starter);
    $pdata['text'] = CleanUpPost($thread['text'], $starter['name'], false, false);

    if ($loguser['layout'] == 'acmlm') {
        if (!$thread['replies']) {
            $comments = __('No comments yet');
        } else {
            $comments = Plural($thread['replies'], __('comment'));
        }
    } else {
        if (!$thread['replies']) {
            $comments = __('No comments yet');
        } else {
            $comments = actionLinkTag(Plural($thread['replies'], __('comment')), 'depost', $thread['lastpostid']).' (last by '.UserLink($last).')';
        }
    }
    $pdata['comments'] = $comments;

    if ($thread['closed']) {
        $newreply = __('Comments closed.');
    } elseif (!$loguserid) {
        $newreply = format(__('{0} to post a comment.'), pageLinkTag(__('Log in'), 'login'));
    } elseif (HasPermission('forum.postthreads', $fid)) {
        $newreply = actionLinkTag(__('Post a comment'), 'newcomment', $thread['id']);
    }
    $pdata['replylink'] = $newreply;

    $posts[] = $pdata;
}

RenderTemplate('depot', ['posts' => $posts, 'pagelinks' => $pagelinks]);
