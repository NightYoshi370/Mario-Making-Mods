<main class="game">
  <div class="game__background"></div>
  <div class="game__foreground">
    <div class="foreground__mario"></div>
    <div class="foreground__ground"></div>
  </div>
</main>