<?php

//  AcmlmBoard XD - Board Settings editing page
//  Access: administrators
if (!defined('BLARG')) {
    die();
}

$title = __('Edit settings');

CheckPermission('admin.editsettings');

$plugin = 'main';
if (isset($_GET['id'])) {
    $plugin = $_GET['id'];
}
if (isset($_POST['_plugin'])) {
    $plugin = $_POST['_plugin'];
}

if (isset($_GET['field'])) {
    $htmlfield = $_GET['field'];
    if (!isset($settings[$htmlfield])) {
        Kill(__('No.'));
    }
    if ($settings[$htmlfield]['type'] != 'texthtml') {
        Kill(__('No.'));
    }

    $htmlname = $settings[$htmlfield]['name'];
} else {
    $htmlfield = null;
}

if (!ctype_alnum($plugin)) {
    Kill(__('No.'));
}

if ($plugin == 'main') {
    MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('editsettings') => __('Edit settings')]);
} else {
    MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('pluginmanager') => __('Plugin manager'), '' => $plugins[$plugin]['name']]);
}

$settings = Settings::getSettingsFile($plugin);
$oursettings = Settings::$settingsArray[$plugin];
$invalidsettings = [];

if (isset($_POST['_plugin'])) {
    if ($_POST['key'] !== $loguser['token']) {
        Kill(__('No.'));
    }

    //Save the settings.
    $valid = true;

    foreach ($_POST as $key => $value) {
        if ($key == '_plugin') {
            continue;
        }

        //Don't accept unexisting settings.
        if (!isset($settings[$key])) {
            continue;
        }

        // don't save settings if the user isn't allowed to change them
        if ($settings[$key]['rootonly'] && !$loguser['root']) {
            continue;
        }

        //Save the entered settings for re-editing
        $oursettings[$key] = $value;

        if (!Settings::validate($value, $settings[$key]['type'], $settings[$key]['options'])) {
            $valid = false;
            $invalidsettings[$key] = true;
        } else {
            Settings::$settingsArray[$plugin][$key] = $value;
        }
    }

    if ($valid) {
        Settings::save($plugin);
        if (isset($_POST['_exit'])) {
            if ($plugin == 'main') {
                if (HasPermission('admin.viewadminpanel')) {
                    $linkredirect = pageLink('admin');
                    $linkname = __('the admin panel');
                } else {
                    $linkredirect = pageLink('home');
                    $linkname = __('the home page');
                }
            } else {
                $linkredirect = actionLink('pluginmanager');
                $linkname = __('the plugin manager');
            }

            if ($acmlmboardLayout) {
                OldRedirect(__('Edited!'), $linkredirect, $linkname);
            } else {
                die(header('Location: '.$linkredirect));
            }
        } else {
            Alert(__('Settings were successfully saved!'));
        }
    } else {
        Alert(__('Settings were not saved because there were invalid values. Please correct them and try again.'));
    }
}

echo '
    <form action="'.htmlentities(actionLink('editsettings'))."\" method=\"post\">
        <input type=\"hidden\" name=\"_plugin\" value=\"$plugin\">
        <input type=\"hidden\" name=\"key\" value=\"{$loguser['token']}\">";

$settingfields = [];
$settingfields[] = ''; // ensures the uncategorized entries come first

foreach ($settings as $name => $data) {
    if ($data['rootonly'] && !$loguser['root']) {
        continue;
    }

    if ($data['type'] == 'texthtml' && $htmlfield == null) {
        continue;
    }
    if ($htmlfield != null && $htmlfield != $name) {
        continue;
    }

    $sdata = [];

    $sdata['name'] = $name;
    if (isset($data['name'])) {
        $sdata['name'] = $data['name'];
    }

    $type = $data['type'];
    $help = $data['help'];
    $options = $data['options'];
    $value = $oursettings[$name];
    $placeholder = $sdata['name'];

    $input = '[Bad setting type]';

    $value = htmlspecialchars($value);

    switch ($type) {
    case 'boolean':
        $input = makeSelect($name, $value, [1=>'Yes', 0=>'No']);
        break;
    case 'options':
        $input = makeSelect($name, $value, $options);
        break;
    case 'integer':
    case 'float':
    case 'text':
        $input = "<input class=\"form-control z-depth-1\" placeholder=\"$placeholder\" type=\"text\" id=\"$name\" name=\"$name\" value=\"$value\" />";
        break;
    case 'password':
        $input = "<input class=\"form-control z-depth-1\" placeholder=\"$placeholder\" type=\"password\" id=\"$name\" name=\"$name\" value=\"$value\" />";
        break;
    case 'textbox':
    case 'textbbcode':
        $input = "<textarea class=\"form-control z-depth-1\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" rows=\"8\">\n$value</textarea>";
        break;
    case 'texthtml':
        $input = "<textarea class=\"form-control z-depth-1\" placeholder=\"$placeholder\" id=\"$name\" name=\"$name\" rows=\"30\">\n$value</textarea>";
        break;
    case 'forum':
        $input = makeForumList($name, $value, true);
        break;
    case 'group':
        $input = makeSelect($name, $value, $grouplist);
        break;
    case 'theme':
        $input = makeThemeList($name, $value);
        break;
    case 'layout':
        $input = makeLayoutList($name, $value);
        break;
    case 'language':
        $input = makeLangList($name, $value);
    }

    $sdata['field'] = $input;

    if ($invalidsettings[$name]) {
        $sdata['name'] = "<span style=\"color: #f44;\">{$sdata['name']} (invalid)</span>";
    }

    if ($help) {
        $sdata['name'] .= "<br><small>$help</small>";
    }

    $settingfields[$data['category']][] = $sdata;
}

if (!$settingfields['']) {
    unset($settingfields['']);
}

$fields = [
    'btnSaveExit' => '<input type="submit" name="_exit" value="'.__('Save and Exit').'">',
    'btnSave'     => '<input type="submit" name="_action" value="'.__('Save').'">',
];

RenderTemplate('form_settings', ['settingfields' => $settingfields, 'htmlfield' => $htmlfield, 'fields' => $fields]);

echo '
    </form>';

function makeSelect($fieldName, $checkedIndex, $choicesList, $extras = '')
{
    $checks[$checkedIndex] = ' selected="selected"';
    foreach ($choicesList as $key=>$val) {
        $options .= format(
            '
                        <option value="{0}"{1}>{2}</option>', $key, $checks[$key], $val
        );
    }
    $result = format(
        '
                    <select class="form-control z-depth-1" id="{0}" name="{0}" size="1" {1} >{2}
                    </select>',
        $fieldName,
        $extras,
        $options
    );

    return $result;
}

function makeThemeList($fieldname, $value)
{
    $themes = [];
    $dir = @opendir('themes');
    while ($file = readdir($dir)) {
        if ($file != '.' && $file != '..') {
            $name = explode("\n", @file_get_contents('./themes/'.$file.'/themeinfo.txt'));
            $themes[$file] = trim($name[0]);
        }
    }
    closedir($dir);

    return makeSelect($fieldname, $value, $themes);
}

function makeLayoutList($fieldname, $value)
{
    $layouts = [];
    $dir = @opendir('layout');
    while ($layout = readdir($dir)) {
        if (!endsWith($layout, '.php') && $layout != '.' && $layout != '..') {
            $layouts[$layout] = @file_get_contents('./layout/'.$layout.'/info.txt');
        }
    }
    closedir($dir);

    return makeSelect($fieldname, $value, $layouts);
}

function makeLangList($fieldname, $value)
{
    $data = [];
    $dir = @opendir('lib/lang');
    while ($file = readdir($dir)) {
        //print $file;
        if (endsWith($file, '_lang.php')) {
            $file = substr($file, 0, strlen($file) - 9);
            $data[$file] = $file;
        }
    }
    $data['en_US'] = 'en_US';
    closedir($dir);

    return makeSelect($fieldname, $value, $data);
}
