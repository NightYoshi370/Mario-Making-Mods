<?php

if (!defined('BLARG')) {
    die();
}

if ($http->post('action') === 'logout') {
    setcookie('logsession', '', 2147483647, URL_ROOT, '', false, true);
    $db->updateId('users', ['loggedin' => 0], 'id', $loguserid);
    Query('DELETE FROM {sessions} WHERE id={0}', doHash($_COOKIE['logsession'].SALT));

    die(header('Location: '.URL_ROOT));
} elseif ($http->post('action') === 'login') {
    $okay = false;
    $passwordfalse = false;
    $userfalse = false;
    $pass = $http->post('pass');
    $name = htmlspecialchars($http->post('name'));

    $user = $db->row('users', [['name' => $name, 'email' => $name, 'displayname' => $name]]);
    if ($user) {
        $sha = doHash($pass.SALT.$user['pss']);

        if ($user['password'] === $sha) {
            $okay = true;
        } else {
            $passwordfalse = true;
        }
    } else {
        $userfalse = true;
    }

    // auth plugins
    if (!$okay) {
        $bucket = 'login';
        include BOARD_ROOT.'lib/pluginloader.php';

        Report('A visitor from [b]'.$_SERVER['REMOTE_ADDR'].'[/] tried to log in as [b]'.$name.'[/].', 1);

        if ($userfalse == true) {
            Alert(
                __(
                    'This can be because of many reasons.
					<br><ul><li>The user does not have a registered account. Please <a href="'.actionLink('register').'" class="alert-link">register one here</a>.</li>
					<li>You perhaps mistyped the name? Double check. Keep in mind that this can be either a username, a display name or an email address.</li></ul>
					If you require further assistance, please contact NightYoshi370.'
                ), __('Invalid name inserted.'), 'danger'
            );
        } elseif ($passwordfalse == true) {
            Alert(
                __(
                    'Please double check that you have not mistyped it and try again.<br><br>

			If you need a password reset, you can either contact the staff, or use the Lost Password function (email required to be set before-hand).'
                ), __('You have entered the wrong password')
            );
        }
    } else {
        //TODO: Tie sessions to IPs if user has enabled it
        $sessionID = Shake();
        setcookie('logsession', $sessionID, 2147483647, URL_ROOT, '', false, true);
        $db->insert('sessions', ['id' => doHash($sessionID.SALT), 'user' => $user['id'], 'autoexpire' => $http->post('session') ? 0 : 1]);

        Report('[b]'.htmlspecialchars($user['name']).'[/] logged in.', 1);

        $rLogUser = Query('SELECT id, pss, password FROM {users} WHERE 1');
        $matches = [];

        while ($testuser = Fetch($rLogUser)) {
            if ($testuser['id'] == $user['id']) {
                continue;
            }

            $sha = doHash($http->post('pass').SALT.$testuser['pss']);
            if ($testuser['password'] === $sha) {
                $matches[] = $testuser['id'];
            }
        }

        if (count($matches) > 0) {
            Query('INSERT INTO {passmatches} (date,ip,user,matches) VALUES (UNIX_TIMESTAMP(),{0},{1},{2})', $_SERVER['REMOTE_ADDR'], $user['id'], implode(',', $matches));
        }

        if ($user['layout'] == 'acmlm') {
            OldRedirect(format(__('You are now logged in. Welcome back {0}!'), htmlspecialchars($user['name'])), URL_ROOT, __('the home page'));
        } else {
            die(header('Location: '.URL_ROOT));
        }
    }
}

$title = __('Log in');
MakeCrumbs([pageLink('login') => __('Log in')], [actionLink('register') => ['text' => __('Register')]]);

$fields = [];
$fields['username'] = '<input type="text" class="form-control" name="name" size=24 maxlength=20 placeholder="'.__('User Name').'">';
$fields['password'] = '<input type="password" class="form-control" name="pass" size=24 placeholder="'.__('Password').'">';
$fields['session'] = checkbox('session', __('Stay logged in'));
$fields['btnLogin'] = '<input type="submit" name="actionlogin" class="btn btn-primary" value="'.__('Log in').'">';

if (!empty(Settings::get('mailResetSender'))) {
    $fields['btnForgotPass'] = actionLinkTag(__('Forgot your Password?'), 'lostpass');
}

echo '<form name="loginform" action="'.htmlentities(pageLink('login')).'" method="post">';

RenderTemplate('form_login', ['fields' => $fields]);

echo '</form>
	<script type="text/javascript">
		document.loginform.name.focus();
	</script>';
