<?php
if (!defined('BLARG')) die();

$links = [];
if(HasPermission('admin.editsettings'))
	$links[actionLink('editsettings', '', 'field=homepageText')] = ['icon' => 'pencil', 'text' => __('Edit home page')];

$viewableforums = ForumsWithPermission('forum.viewforum');

// timestamp => data
$lastPosts = [];
$news = [];
$maxitems = 10;

$lastposts = Query('SELECT
                        t.(title,forum,lastpostdate,lastpostid),
						f.(title,id),
                        u.(_userfields)
                    FROM
                        {threads} t
                        LEFT JOIN {forums} f ON f.id=t.forum
                        LEFT JOIN {users} u ON u.id=t.lastposter
                    WHERE f.id IN ({0c}) AND f.offtopic=0
                    ORDER BY t.lastpostdate DESC
                    LIMIT {1u}', $viewableforums, $maxitems);

while ($lp = Fetch($lastposts)) {
    $user = getDataPrefix($lp, 'u_');
	$forum = actionLinkTag($lp['f_title'], 'forum', $lp['f_id'], '', $lp['f_title']);
    
	$tags = ParseThreadTags($lp['t_title']);
	if (Settings::get("tagsDirection") === 'Left') {
		$link = $tags[1].' '.pageLinkTag($tags[0], 'post', ['id' => $lp['t_lastpostid']]);
	} else {
		$link = pageLinkTag($tags[0], 'post', ['id' => $lp['t_lastpostid']]).' '.$tags[1];
	}

    $lastActivity[$lp['t_lastpostdate']] = ['user' => UserLink($user), 'link' => $link, 'forum' => $forum, 'formattedDate' => relativedate($lp['t_lastpostdate'])];
}

$bucket = 'lastactivity';
require BOARD_ROOT.'lib/pluginloader.php';

krsort($lastActivity);
$lastActivity = array_slice($lastActivity, 0, $maxitems);

$forum = $db->row('forums', ['id' => Settings::get('newsForum')]);
if ($forum && HasPermission('forum.viewforum', $forum['id'])) {
    $total = $forum['numthreads'];

	if (isset($_GET['from'])) $from = (int) $_GET['from'];
	else 					  $from = 0;

	$tpp = 5;

	$links['/API/rss.php'] = ['icon' => 'rss-square', 'text' => __('RSS feed')];
	if (HasPermission('forum.postthreads', $forum['id'])) {
		$links[actionLink('newthread', $forum['id'])] = ['text' => __('Post new')];
	}

	$rThreads = Query('	SELECT 
							t.id, t.title, t.closed, t.replies, t.lastpostid,
							p.id pid, p.date,
							pt.text,
							su.(_userfields),
							lu.(_userfields)
						FROM {threads} t
                        LEFT JOIN {posts} p ON p.thread=t.id AND p.id=t.firstpostid
                        LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision
                        LEFT JOIN {users} su ON su.id=t.user
                        LEFT JOIN {users} lu ON lu.id=t.lastposter
						WHERE t.forum={0} AND p.deleted=0
						ORDER BY p.date DESC LIMIT {1u}, {2u}', $forum['id'], $from, $tpp);

	$numonpage = NumRows($rThreads);

	$pagelinks = PageLinks(pageLink('home', [], 'from='), $tpp, $from, $total);

	while ($thread = Fetch($rThreads)) {
		$pdata = [];

		$starter = getDataPrefix($thread, 'su_');
		$last = getDataPrefix($thread, 'lu_');

		$tags = ParseThreadTags($thread['title']);

		$pdata['title'] = $tags[0];
		$pdata['formattedDate'] = formatdate($thread['date']);
		$pdata['userlink'] = UserLink($starter);
		$pdata['text'] = CleanUpPost($thread['text'], $starter['name'], false, false);

		if (!$thread['replies'])			$comments = 'No comments yet';
		else								$comments = pageLinkTag(plural($thread, __('post')), 'post', ['id' => $thread['lastpostid']]);

		if ($thread['replies'] == 1)		$comments .= ' '.format(__('(by {0})'), UserLink($last));
		else if ($thread['replies'] >= 2)	$comments .= ' '.format(__('(last by {0})'), UserLink($last));

		$pdata['comments'] = $comments;

		if (!$loguserid) 											$newreply = __('You must have an account in order to post a comment.');
		else if (HasPermission('forum.postreplies', $forum['id']))	$newreply = actionLinkTag(__('Post a comment'), 'newreply', $thread['id']);
		$pdata['replylink'] = $newreply;

		$modlinks = [];
		if (($loguserid == $starter['id'] && HasPermission('forum.editownposts', $forum['id'])) || HasPermission('mod.editposts', $forum['id'])) {
			$modlinks['edit'] = actionLinkTag(__('Edit'), 'editpost', $thread['pid']);
		}
		if (($loguserid == $starter['id'] && HasPermission('forum.deleteownposts', $forum['id'])) || HasPermission('mod.deleteposts', $forum['id'])) {
			$modlinks['delete'] = actionLinkTag(__('Delete'), 'edit', $thread['pid'], 'delete=1&key='.$loguser['token']);
		}

		$news[] = $pdata;
	}
}

MakeCrumbs([], $links);
RenderTemplate('homepage', [
	'lastactivity' => $lastActivity,
	'home_text'    => parseBBCode(Settings::get('homepageText')),
	'pagelinks'    => $pagelinks,
	'news'         => $news
]);