<?php

if (!defined('BLARG')) {
    die();
}

if (!$loguser['root']) {
    Kill(__("You're not an administrator. There is nothing for you here."));
}

MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('optimize') => __('Optimize tables')]);

$rStats = Query('show table status');
while ($stat = Fetch($rStats)) {
    $tables[$stat['Name']] = $stat;
}

$tablelist = [];
$total = 0;
foreach ($tables as $table) {
	$rowdata = [];

	$rowdata['Name'] = $table['Name'];
	$rowdata['Rows'] = $table['Rows'];

    $rowdata['overhead'] = $table['Data_free'];
    $total += $rowdata['overhead'];

    $rowdata['status'] = __('OK');
    if ($overhead > 0) {
        Query('OPTIMIZE TABLE `{'.$table['Name'].'}`');
        $rowdata['status'] = '<strong>'.__('Optimized').'</strong>';
    }

	$tablelist[] = $rowdata;
}

RenderTemplate('admin/optimize', ['total' => $total, 'tablelist' => $tablelist]);
