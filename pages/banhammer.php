<?php
if (!defined('BLARG')) die();
CheckPermission('admin.banusers');

$id = (int) $pageParams['id'];
if (!$id) Kill('Please specify a user id.');

$user = Fetch(Query('SELECT u.(_userfields) FROM {users} u WHERE u.id={0}', $id));
if (!$user) {
    Kill('Invalid user ID.');
}

if ($usergroups[$user['u_primarygroup']]['rank'] >= $loguserGroup['rank']) {
    Kill(__('You may not ban a user whose level is equal to or above yours.'));
}

if ($_POST['ban']) {
    if ($_POST['token'] !== $loguser['token']) {
        Kill('No.');
    }

    if ($_POST['permanent']) {
        $time = 0;
        $expire = 0;
        $bantitle = __('Banned permanently');
    } else {
        $time = $_POST['time'] * $_POST['timemult'];
        $expire = $db->time() + $time;
        $bantitle = format(__('Banned until {0}'), formatdate($expire));
    }

    if (trim($_POST['reason'])) {
        $bantitle .= __(': ').$_POST['reason'];
    }

    $db->updateId(
        'users', [
        'tempbanpl'    => $user['u_primarygroup'], 'tempbantime' => $expire,
        'primarygroup' => Settings::get('bannedGroup'), 'title' => $bantitle,
        ], 'id', $id
    );

    Report(
        $loguser['name'].' banned '.$user['u_name'].($expire ? ' for '.TimeUnits($time) : ' permanently').
        ($_POST['reason'] ? ': '.$_POST['reason'] : '.'), true
    );

    die(header('Location: '.pageLink('profile', ['id' => $id, 'name' => $user['u_name']])));
} elseif ($user['u_primarygroup'] == Settings::get('bannedGroup')) {
    if ($_GET['token'] !== $loguser['token']) {
        Kill('No.');
    }

	Report(format(__('{0} unbanned {1}.'), $loguser['name'], $user['u_name']), true);
    Query('UPDATE {users} SET primarygroup = tempbanpl, tempbantime = {0}, title = {1} WHERE id = {2}', 0, '', $id);

    die(header('Location: '.pageLink('profile', ['id' => $id, 'name' => slugify($user['u_name'])])));
}

$uname = htmlspecialchars($user['u_displayname'] ? $user['u_displayname'] : $user['u_name']);

$title = format(__('Ban {0}'), $uname);
MakeCrumbs([pageLink('profile', ['id' => $id, 'name' => $user['u_name']]) => $uname, actionLink('banhammer', $id) => __('Ban')]);

$timemult_options = [1 => __('seconds'), 60 => __('minutes'), 3600 => __('hours'), 86400 => __('days'), 604800 => __('weeks')];

foreach ($timemult_options as $time => $text) {
    $timemult .= '<option value="'.$time.'">'.$text.'</option>';
}

$duration = '
<label><input type="radio" name="permanent" value="0"> '.__('For:').'</label>
	<input type="text" name="time" size="4" maxlength="2">
	<select name="timemult">
		'.$timemult.'
	</select>
	<br>
<label><input type="radio" name="permanent" value="1" checked="checked"> '.__('Permanent').'</label>';

$userlink = userLink(getDataPrefix($user, 'u_'));
$fields = [
    'target'   => $userlink,
    'duration' => $duration,
    'reason'   => '<input type="text" name="reason" class="form-control" size=80 maxlength=200 placeholder="'.__('Reason').'">',

    'btnBanUser' => '<input type="submit" class="btn btn-primary" name="ban" value="'.__('Ban user').'">',
];

echo '<form action="'.pageLink('ban', ['id' => $id, ]).'" method="POST">';
RenderTemplate('form_banuser', ['fields' => $fields]);
echo '<input type="hidden" name="token" value="'.$loguser['token'].'">
	</form>';
