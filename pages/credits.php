<?php

$title = __('Credits');
MakeCrumbs([actionLink('credits') => __('Credits')]);

$software = [
    'NightYoshi370'   => 'The lead developer of MakerBoard',
    'StapleButter'    => 'Made Blargboard, prequel of MakerBoard.',
    'Dirbaio'         => 'Lead developer of ABXD 3.0, prequel of Blargboard.',
    'Oman Computar'   => 'New Router',
    'Sean Murphy'     => 'URL Slugify code',
    'Toni Lähdekorpi' => 'Wiki Markup',
    'darkain'         => 'PUDL (MySQL engine)',
];

$visual = [
    'PickPen'                       => 'Depot icons',
    'Samplasion & MoonlightCapital' => 'Italian Translation',
    'Ryan McGeary'                  => 'TimeAgo',
    'GamesWithIsaac'                => 'Ranksets',
    'mdo'                           => 'Lead developer of Bootstrap',
];

foreach ($software as $devs => $contribution) {
    $softwarelist .= '<tr class="cell1"><td class="cell2">'.$devs.'</td><td>'.$contribution.'</td></tr>';
}

foreach ($visual as $people => $graphics) {
    $graphiclist .= '<tr class="cell1"><td class="cell2">'.$people.'</td><td>'.$graphics.'</td></tr>';
}

$credits = '
	<table class="table table-bordered table-striped">
		<thead><tr class="header1"><th colspan=2>Software Credits</th></tr></thead>
		<tbody>'.$softwarelist.'</tbody>
	</table>

	<table class="table table-bordered table-striped">
		<thead><tr class="header1"><th colspan=2>Graphic Designers</th></tr></thead>
		<tbody>'.$graphiclist.'</tbody>
	</table>';

echo $credits;
