<?php

if (!defined('BLARG')) {
    die();
}

// Some servers use one response, some use another. For safety, use both.
header('HTTP/2.0 404 Not Found');
header('Status: 404 Not Found');

$title = __('404 - Not found');
$metaStuff['description'] = 'The page you are looking for was not found.';

$request = getServerDomain().$_SERVER['REQUEST_URI'];

RenderTemplate('error', ['error' => $request]);
