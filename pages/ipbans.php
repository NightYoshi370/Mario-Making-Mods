<?php

if (!defined('BLARG')) {
    die();
}

CheckPermission('admin.manageipbans');

$title = __('IP bans');
MakeCrumbs([actionLink('admin') => __('Admin'), pageLink('ipbans') => __('IP bans')]);

if (isset($_POST['actionadd'])) {
    if (isIPBanned($_POST['ip'])) {
        Alert('Already banned IP!');
    } else {
        $db->insert('ipbans', ['ip' => $_POST['ip'], 'reason' => $_POST['reason'], 'date' => ((int) $_POST['days'] > 0 ? $db->time() + ((int) $_POST['days'] * 86400) : 0)]);
        Alert(__('Added.'), __('Notice'));
    }
} elseif ($_GET['action'] == 'delete') {
    $rIPBan = Query('delete from {ipbans} where ip={0} limit 1', $_GET['ip']);
    Alert(__('Removed.'), __('Notice'));
}

$rIPBan = Query('select * from {ipbans} order by date desc, ip asc');

$banList = '';
while ($ipban = Fetch($rIPBan)) {
    if ($ipban['date']) {
        $date = formatdate($ipban['date']).' ('.TimeUnits($ipban['date'] - time()).' left)';
    } else {
        $date = __('Permanent');
    }

    $banList .= '
	<tr>
		<td>'.htmlspecialchars($ipban['ip']).'</td>
		<td>'.htmlspecialchars($ipban['reason'])."</td>
		<td>$date</td>
		<td><a href=\"".actionLink('ipbans', '', 'ip='.htmlspecialchars($ipban['ip']).'&action=delete').'">&#x2718;</a></td>
	</tr>';
}

echo '
<table class="outline margin">
	<tr class="header1">
		<th>'.__('IP').'</th>
		<th>'.__('Reason').'</th>
		<th>'.__('Date')."</th>
		<th>&nbsp;</th>
	</tr>
	$banList
</table>

<form action=\"".htmlentities(actionLink('ipbans')).'" method="post">
	<table class="outline margin">
		<tr class="header1">
			<th colspan="2">
				'.__('Add').'
			</th>
		</tr>
		<tr>
			<td class="cell2">
				'.__('IP').'
			</td>
			<td class="cell0">
				<input type="text" name="ip" style="width: 98%;" maxlength="45" />
			</td>
		</tr>
		<tr>
			<td class="cell2">
				'.__('Reason').'
			</td>
			<td class="cell1">
				<input type="text" name="reason" style="width: 98%;" maxlength="100" />
			</td>
		</tr>
		<tr>
			<td class="cell2">
				'.__('For').'
			</td>
			<td class="cell1">
				<input type="text" name="days" size="13" maxlength="13" /> '.__('days').'
			</td>
		</tr>
		<tr class="cell2">
			<td></td>
			<td>
				<input type="submit" name="actionadd" value="'.__('Add').'" />
			</td>
		</tr>
	</table>
</form>';
