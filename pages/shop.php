<style>
    .disabled {color:#888888}
    .higher   {color:#abaffe}
    .equal    {color:#ffea60}
    .lower    {color:#ca8765}
</style>

<?php
    $title = __('Item shop');

if (!$loguserid) {
    Kill('You need to be logged on, in order to purchase items from the shop.');
}

    // Same thing for checking if we have access to the item shop / shop editor
    $_GET['id'] = isset($_GET['id']) ? (int) $_GET['id'] : 0;
    $_GET['cat'] = isset($_GET['cat']) ? (int) $_GET['cat'] : 0;
    $_GET['action'] = isset($_GET['action']) ? $_GET['action'] : '';

    // Add shop editor specific actions here
    $shopedit = in_array($_GET['action'], ['edit', 'save']);
    $canedit = HasPermission('admin.manageShop');

if (!HasPermission('user.itemShop')) {
    Kill(__('You may not view the item shop.'));
} elseif ($shopedit && !$canedit) {
    Kill(__('You may not edit the item shop!'));
}

$rURPG = $db->row('usersrpg', ['id' => $loguserid]);
if (!$rURPG) {
    $db->insert('usersrpg', ['id' => $loguserid]);
}

    $user = Fetch(
        Query(
            'SELECT u.name, u.posts, u.regdate, r.*
							FROM {users} u
                            LEFT JOIN {usersrpg} r ON u.id=r.id
                            WHERE u.id = {0u}', $loguserid
        )
    );

    $st = getstats($user);
    $coins = $st['GP'];

    switch ($_GET['action']) {
    case 'buy':
        $item = $db->row('items', ['id' => $_GET['id']]);
        $realshop = FetchResult('SELECT COUNT(*) FROM {itemcateg} WHERE id = {0c}', $item['cat']);

        if ($item['coins'] <= $coins && $realshop) {
            if (!$user['eq'.$item['cat']]) {
                $pitem = ['coins' => 0];
            } else {
                $pitem = $db->row('items', ['id' => $user['eq'.$item['cat']]]);
            }

            Query(
                "UPDATE {usersrpg} SET 
                        eq{$item['cat']} = {$_GET['id']},
                        spent  = spent  - {$pitem['coins']}  * 0.6 + {$item['coins']}
                    WHERE id = {$loguser['id']}"
            );

            Alert(format(__('The {0} has been bought and equipped!'), $item['name']));
        }
        break;
    case 'sell':
        $pitem = $db->row('items', ['id' => $user['eq'.$_GET['cat']]]);
        if ($pitem) {
            Query(
                "	UPDATE {usersrpg} SET
							eq{$_GET['cat']} = 0,
							spent = spent - {0} * 0.6
						WHERE id = {1}", $pitem['coins'], $loguserid
            );

            Alert(format(__('The {0} has been unequipped and sold.'), $pitem['name']));
        }
        break;
    default:
        Alert(
            __(
                "Here is where you can buy items (such as weapons, armor, and more) using your coins. In order to obtain coins, you'll need to post on the board.
				In the future, these items will allow you to battle another person with your items."
            ), __('Welcome to the item shop!')
        );
    }

    $cat = $_GET['cat'];

    $leftcrumbs = [
        actionLink('memberlist') => __('Member List'),
        actionLink('shop')       => __('Item Shop'),
    ];
    $rightcrumbs = [];

    switch ($_GET['action']) {
    case 'save':
        if (!$_GET['id']) {
            Kill(__('Please specify a item and try again.'), __('No item specified'));
        }

        $_POST['cat'] = isset($_POST['cat']) ? (int) $_POST['cat'] : 0;

        if ($_GET['id'] != -1 && isset($_POST['delete']) && $_POST['delete']) {
            // Make sure the item exists before doing anything with it
            $item = $db->row('items', ['id' => $_GET['id']]);
            if ($item) {
                Query('DELETE FROM {items} WHERE id = {0c}', $_GET['id']);
                // Remove deleted item from users' inventories
                for ($i = 1; $i < 7; $i++) {
                    Query("UPDATE {usersrpg} SET eq{$i} = 0 WHERE eq{$i} = {0c}", $_GET['id']);
                }

                if ($acmlmboardLayout) {
                    OldRedirect(format(__('The item {0} has deleted!'), $item['name']), __('the shop'), actionLink('shop', '', 'action=items&cat='.$_POST['cat']));
                } else {
                    newRedir(actionLink('shop', '', 'action=items&cat='.$_POST['cat']));
                }
            } else {
                Kill(__("You can't delete an item which does not exist."));
            }
        }

        $_POST['name'] = isset($_POST['name']) ? $_POST['name'] : '';
        $_POST['desc'] = isset($_POST['desc']) ? $_POST['desc'] : '';
        $_POST['coins'] = isset($_POST['coins']) ? (int) $_POST['coins'] : 0;
        $_POST['hidden'] = isset($_POST['hidden']) ? (int) $_POST['hidden'] : 0;

        $realshop = $db->row('itemcateg', ['id' => $cat]);
        if (!$realshop) {
            Kill(__('This category does not exist!'));
        }
        if (!trim($_POST['name'])) {
            Kill(__('Please insert a name and try again.'), __('The item must have a name.'));
        }
        if ($_POST['coins'] < 0) {
            Kill(__('You cannot save items with negative cost!'));
        }

        $set = [];
        $stype = '';
        for ($i = 0; $i < 9; $i++) {
            $status = (isset($_POST[$stat[$i]]) && trim($_POST[$stat[$i]])) ? (string) $_POST[$stat[$i]] : '0';
            $mode = strtolower(substr($status, 0, 1)) == 'x' ? 'm' : 'a'; // only the x operator in the first character matters
            $val = ($mode == 'm') ? substr($status, 1) * 100 : $status; // m -> .2 decimal; a -> int
            $set["s{$stat[$i]}"] = (int) $val;
            $stype .= $mode;
        }
        $set['name'] = stripslashes($_POST['name']);
        $set['desc'] = stripslashes($_POST['desc']);
        $set['stype'] = $stype;
        $set['coins'] = $_POST['coins'];
        $set['cat'] = $_POST['cat'];
        $set['hidden'] = $_POST['hidden'];

        if ($_GET['id'] == -1) {
            $db->insert('items', $set);
            $id = $db->insertid();
        } else {
            $db->updateId('items', $set, 'id', $_GET['id']);
            $id = $_GET['id'];
        }
        newRedir(actionLink('shop', $id, 'action=desc'));

        break;
    case 'edit':
        if (!$_GET['id']) {
            Kill(__('Please specify a item and try again.'), __('No item specified'));
        }

        $item = Fetch(Query("SELECT * FROM {items} WHERE id={0} union SELECT * FROM {items} WHERE id='-1'", $_GET['id']));
        if ($_GET['id'] == -1) { // Default to the category specified via url (and not to the default value 99)
            $item['cat'] = $_GET['cat'];
        }
        $_GET['cat'] = $item['cat'];

        $statlist = '';
        $shops = query('SELECT * FROM {itemcateg} ORDER BY corder');
        while ($shop = fetch($shops)) {
            $catlist .= '<option value="'.$shop['id'].'"'.(($shop['id'] == $item['cat']) || ($item['cat'] == 99 && isset($_GET['cat']) && $shop['id'] == $_GET['cat']) ? "selected='selected'" : '').'>'.$shop['name'].'</option>';
        }
        for ($i = 0; $i < 9; $i++) {
            $st = $item['s'.$stat[$i]];
            if (substr($item['stype'], $i, 1) == 'm') {
                $st = vsprintf('x%1.2f', $st / 100);
            } else {
                if ($st > 0) {
                    $st = '+'.$st;
                }
            }
            $itst = $item['s'.$stat[$i]];
            $eqst = $eqitem['s'.$stat[$i]];
            if (!$color) {
                if ($itst > 0) {
                    $cl = 'higher';
                } elseif ($itst == 0) {
                    $cl = 'equal';
                } elseif ($itst < 0) {
                    $cl = 'lower';
                }
            } else {
                $cl = '';
            }

            $statlist .= "    <td class=\"b cell2 align=\"center\"'><input type=\"text\" name='$stat[$i]' size='4' value='$st'></td>";
            $stathdr .= '        <td class="b cell1" align="center" width="6%">'.$stat[$i].'</td>';
        }

        $rightcrumbs[actionLink('shop')] = ['text' => __('Return to shop list')];
        $rightcrumbs[actionLink('shop', '', 'action=items&cat='.$_GET['cat'])] = ['text' => __('Return to item list')];

        $fields = [
            'name'     => '<input class="form-control" type="text" name="name" value="'.$item['name'].'" placeholder="'.__('Item name').'">',
            'desc'     => '<input class="form-control" type="text" name="desc" value="'.$item['desc'].'" placeholder="'.__('Description').'">',
            'categ'    => "<select name='cat' class='form-control'>$catlist</select>",
            'cost'     => '<input class="form-control" type="text" name="coins" value="'.$item['coins'].'" placeholder="'.__('Cost').'">',
            'hide'     => '<label><input type="checkbox" name="hidden" '.($item['hidden'] ? 'checked' : '').'> '.__('Hidden Item').'</label>',
            'stathdr'  => $stathdr,
            'statlist' => $statlist,

            'btnDelete' => '<input type="submit" name="delete" value="'.__('Delete Item').'">',
            'btnSave'   => '<input type="submit" name="Save" value="'.__('Save Item').'">',
        ];
        echo "<form action='".actionLink('shop', $item['id'], 'action=save')."' method='POST'>";
        RenderTemplate('shop/edit', ['fields' => $fields]);
        echo '</form>';

        break;
    case 'desc':
        checknumeric($_GET['id']);
        $item = $db->row('items', ['id' => $_GET['id']]);
        $statlist = '';
        for ($i = 0; $i < 9; $i++) {
            $st = $item['s'.$stat[$i]];
            if (substr($item['stype'], $i, 1) == 'm') {
                $st = vsprintf('x%1.2f', $st / 100);
                if ($st == 100) {
                    $st = '&nbsp;';
                }
            } else {
                if ($st > 0) {
                    $st = '+'.$st;
                }
                if (!$st) {
                    $st = '&nbsp;';
                }
            }
            $itst = $item['s'.$stat[$i]];
            $eqst = $eqitem['s'.$stat[$i]];

            if ($canedit && $item['id'] != 0) {
                $rightcrumbs[actionLink('shop', $item['id'], 'action=edit')] = ['text' => format(__('Edit {0}'), $item['name'])];
                $rightcrumbs[actionLink('shop', $item['id'], 'action=delete')] = ['text' => format(__('Delete {0}'), $item['name'])];
            }

            if (!$color) {
                if ($itst > 0) {
                    $cl = 'higher';
                } elseif ($itst == 0) {
                    $cl = 'equal';
                } elseif ($itst < 0) {
                    $cl = 'lower';
                }
            } else {
                $cl = '';
            }

            $statlist .= "    <td class=\"b cell2 align=\"center\" $cl'>$st</td>";
            $stathdr .= '    <td class=\"b cell1 align=\"center\" width=6%>'.$stat[$i].'</td>';
        }

        RenderTemplate(
            'shop/desc', [
            'name'     => $item['name'],
            'stathdr'  => $stathdr,
            'statlist' => $statlist,
            'desc'     => $item['desc'],
                ]
        );

        break;
    case 'items':
        // TODO: Allow view-all-items & Equiped Items view
        $realshop = $db->row('itemcateg', ['id' => $cat]);
        if (!$realshop) {
            Kill(__('This category does not exist!'));
        }

        $eqitem = Fetch(
            Query(
                "                   SELECT i.*
                                    FROM {items} i
                                    INNER JOIN {usersrpg} r ON i.id = r.eq$cat
                                    WHERE r.id={0}", $loguserid
            )
        );

        $leftcrumbs[actionLink('shop', '', 'action=items&cat='.$cat)] = __($realshop['name']);
        $rightcrumbs[actionLink('shop')] = ['text' => __('Return to shop list')];
        if ($canedit) {
            $rightcrumbs[actionLink('shop', -1, 'action=edit&cat='.$cat)] = ['text' => __('Add a new item')];
        }

        $atrlist = [];
        for ($i = 0; $i < 9; $i++) {
            $atrlist[] = $stat[$i];
        }

        $seehidden = (int) $canedit;

        $items = Query(
            'SELECT * FROM {items}
                            WHERE cat={0} AND hidden <= {1}
                            ORDER BY coins, name', $cat, $seehidden
        );

        $itemlist = [];
        while ($item = fetch($items)) {
            $sublist = [];
            $buy = actionLinkTag(__('Buy'), 'shop', $item['id'], 'action=buy');
            $sell = actionLinkTag(__('Sell'), 'shop', '', 'action=sell&cat='.$cat);
            $preview = "<a href='javascript:;' onclick=\"preview($loguserid,$item[id],$cat,'".addslashes($item['name'])."')\">Preview</a>";

            if ($mobileLayout) {
                if ($item['id'] == $eqitem['id']) {
                    $sublist['comm'] = $sell;
                } elseif ($item['id'] && $item['coins'] <= $coins) {
                    $sublist['comm'] = $buy.' | '.$preview;
                } else {
                    $sublist['comm'] = $preview;
                }
            } else {
                if ($item['id'] == $eqitem['id']) {
                    $sublist['comm'] = $sell;
                } elseif ($item['id'] && $item['coins'] <= $coins) {
                    $sublist['comm'] = $buy;
                }
            }

            if ($item['id'] == $eqitem['id'] && $item['id'] !== 0) {
                $sublist['color'] = ' class="equal"';
            } elseif ($item['coins'] > $coins || $item['id'] == 0) {
                $sublist['color'] = ' class="disabled"';
            } else {
                $sublist['color'] = '';
            }

            $sublist['atrlist'] = [];
            for ($i = 0; $i < 9; $i++) {
                $artlist = [];
                $st = $item['s'.$stat[$i]];
                if (substr($item['stype'], $i, 1) == 'm') {
                    $st = vsprintf('x%1.2f', $st / 100);
                    if ($st == 100) {
                        $st = '';
                    }
                } else {
                    if ($st > 0) {
                        $st = '+'.$st;
                    }
                    if (!$st) {
                        $st = '';
                    }
                }
                $itst = $item['s'.$stat[$i]];
                $eqst = $eqitem['s'.$stat[$i]];

                if (!$color && substr($item['stype'], $i, 1) == substr($eqitem['stype'], $i, 1)) {
                    if ($itst > $eqst) {
                        $cl = 'higher';
                    } elseif ($itst == $eqst) {
                        $cl = 'equal';
                    } elseif ($itst < $eqst) {
                        $cl = 'lower';
                    }
                } else {
                    $cl = '';
                }

                $artlist['cl'] = $cl;
                $artlist['st'] = $st;
                $artlist['name'] = $stat[$i];
                $sublist['atrlist'][] = $artlist;
            }

            $sublist['itemLink'] = actionLinkTag($item['name'], 'shop', $item['id'], 'action=desc');
            $sublist['coins'] = $item['coins'];
            $sublist['desc'] = $item['desc'];

            $itemlist[] = $sublist;
        }

        RenderTemplate(
            'shop/items', [
            'loguserid'  => $loguserid,
            'returnLink' => $returnLink,
            'edit'       => $edit,
            'atrlist'    => $atrlist,
            'itemlists'  => $itemlist, ]
        );

        break;
    default:
        $shops = Query('SELECT * FROM {itemcateg} ORDER BY corder');
        $eq = $db->row('usersrpg', ['id' => $loguserid]);
        $eqitems = getresultsbykey(Query('SELECT id, name FROM {items} WHERE id IN ({0u})', $eq), 'id', 'name');

        $shoplist = [];
        while ($shop = fetch($shops)) {
            $shopdesc = [];
            $id = $eq["eq{$shop['id']}"];

            $shopdesc['curent'] = actionLinkTag($eqitems[$id], 'shop', $id, 'action=desc#'.$id);
            $shopdesc['id'] = $shop['id'];
            $shopdesc['name'] = $shop['name'];
            $shopdesc['description'] = $shop['description'];
            $shopdesc['link'] = actionLinkTag($shop['name'], 'shop', '', 'action=items&cat='.$shop['id'].'#status');

            $shoplist[] = $shopdesc;
        }

        RenderTemplate('shop/home', ['loguserid' => $loguserid, 'shoplists' => $shoplist]);

        break;

    }

    MakeCrumbs($leftcrumbs, $rightcrumbs);
