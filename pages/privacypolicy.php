<table class="table table-bordered table-striped">
    <thead><tr><th>What we collect</th><tr></thead>
    <tbody><tr><td><ul>
        <li>IP Address. We use this to make sure that users do not register alternate accounts</li>
        <li>Emails: We use this to allow users to recover their password</li>
        <li>Cookies: We use this to allow users to log in</li>
    </ul></td></tr></tbody>
</table>

<table class="table table-bordered table-striped">
    <thead><tr><th>How we use the collected data</th><tr></thead>
    <tbody><tr><td><ul>
        <li>We DO NOT sell or supply customer information to third parties.</li>
        <li>We DO NOT share customer information with outside parties who may wish to market their products to you.</li>
        <li>You DO NOT have to take any action or instruct us to keep your information confidential. We will protect your privacy automatically.</li>
        <li>We are committed to helping you protect your privacy.</li>
    </ul></td></tr></tbody>
</table>