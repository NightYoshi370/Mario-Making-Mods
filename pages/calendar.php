<?php

$title = __('Calendar');

$now = getdate(time());
$year = $now['year'];
$month = $now['mon'];
$day = $now['mday'];

if ((int) $_GET['month']) {
    $month = (int) $_GET['month'];
    $day = 0;
}

if ((int) $_GET['year']) {
    $year = (int) $_GET['year'];
    $day = 0;
}

checknumeric($year);
checknumeric($month);
checknumeric($day);

MakeCrumbs([actionlink('calendar') => __('Calendar')]);

$d = getdate(mktime(0, 0, 0, $month, 1, $year));
$i = 1 - $d['wday'];
$d = getdate(mktime(0, 0, 0, $month + 1, 0, $year));
$max = $d['mday'];

$users = Query('SELECT u.(_userfields), u.birthday as u_birthday FROM {users} u WHERE birthday != 0 ORDER BY name');
$cells = [];
while ($user = Fetch($users)) {
    $user = getDataPrefix($user, 'u_');
    $d = getdate($user['birthday']);
    if ($d['mon'] == $month) {
        $dd = $d['mday'];
        $age = $year - $d['year'];
        if ($age < 0) {
            $age = -$age;
            $cells[$dd][] = format(__('{0} is born in {1}'), Userlink($user), Plural($age, 'year'));
        } elseif ($age == 0) {
            $cells[$dd][] = format(__('{0} is born'), Userlink($user));
        } else {
            $cells[$dd][] = format(__('{0} turns {1}'), Userlink($user), $age);
        }
    }
}

$events = Query('SELECT * FROM {calendar} ORDER BY day, month, year');
while ($event = Fetch($events)) {
    $eventday = $event['day'];
    $eventmonth = $event['month'];
    $eventyear = $event['year'];

    if ($eventmonth == $month && (($eventyear && $eventyear == $year) || !$eventyear)) {
        $cells[$eventday][] = htmlspecialchars($event['event']);
    }
}

while ($i <= $max) {
    for ($dn = 0; $dn <= 6; $dn++) {
        $mondath = $days[$dn];
        $dd = $i + $dn;
        if ($dd < 1 || $dd > $max) {
            $grid .= '
			<div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-light text-muted"></div>';
        } else {
            if (is_array($cells[$dd]) && count($cells[$dd])) {
                $caevents = implode('<br>', $cells[$dd]);
            } else {
                $caevents = '<p class="d-sm-none">No events</p>';
            }

            $grid .= "
			<div class=\"day col-sm p-2 border cell border-left-0 border-top-0 text-truncate\">
				<h5 class=\"row align-items-center\">
					<span class=\"date col-1\">$dd</span>
					<small class=\"col d-sm-none text-center text-muted\">$mondath</small>
					<span class=\"col-1\"></span>
				</h5>
				$caevents
			</div>";
        }
    }
    $grid .= '<div class="w-100"></div>';
    $i += 7;
}

$monthChoice = [];
for ($i = 1; $i <= 12; $i++) {
    if ($i == $month) {
        $monthChoice[] = $months[$i];
    } else {
        $monthChoice[] = actionLinkTag($months[$i], 'calendar', 0, "month=$i");
    }
}

RenderTemplate(
    'calendar', [
    'days'         => $days,
    'months'       => $months[$month],
    'year'         => $year,
    'grid'         => $grid,
    'monthChoices' => $monthChoice,
    ]
);
