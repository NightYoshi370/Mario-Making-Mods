<?php

if (!defined('BLARG')) {
    die();
}

$title = __('Mood avatars');

if (!$loguserid) {
    Kill(__('You must be logged in to edit your avatars.'));
}

CheckPermission('user.editprofile');
CheckPermission('user.editavatars');

MakeCrumbs(
    [pageLink('profile', ['id' => $loguserid, 'name' => $loguser['name']]) => htmlspecialchars($loguser['displayname'] ? $loguser['displayname'] : $loguser['name']),
    actionLink('editavatars') => __('Mood avatars'), ]
);

$ft = ['', 'gif', 'jpg', 'png'];

if (isset($_POST['actionrename']) || isset($_POST['actiondelete']) || isset($_POST['actionadd'])) {
    $mid = (int) $_POST['mid'];
    $fte = (int) $_POST['ext'];
    if ($_POST['actionrename']) {
        $db->update('moodavatars', ['name' => $_POST['name']], ['mid' => $mid, 'uid' => $loguserid]);

        die(header('Location: '.actionLink('editavatars')));
    } elseif ($_POST['actiondelete']) {
        Query('DELETE FROM {moodavatars} WHERE uid={0} AND mid={1}', $loguserid, $mid);
        $db->update('posts', ['mood' => 0], ['user' => $loguserid, 'mood' => $mid]);
        if (file_exists(DATA_DIR.'avatars/'.$loguserid.'_'.$mid.'.'.$ft[$fte])) {
            unlink(DATA_DIR.'avatars/'.$loguserid.'_'.$mid.'.'.$ft[$fte]);
        }

        die(header('Location: '.actionLink('editavatars')));
    } elseif ($_POST['actionadd']) {
        $highest = FetchResult('select mid from {moodavatars} where uid={0} order by mid desc limit 1', $loguserid);
        if ($highest < 1) {
            $highest = 1;
        }
        $mid = $highest + 1;

        //Begin copypasta from edituser/editprofile_avatar...
        if ($fname = $_FILES['picture']['name']) {
            $fext = strtolower(substr($fname, -4));
            $error = '';

            $exts = ['.png', '.jpg', '.gif'];
            $dimx = 200;
            $dimy = 200;
            $dimxs = 60;
            $dimys = 60;
            $size = 61440;

            $validext = false;
            $extlist = '';
            foreach ($exts as $ext) {
                if ($fext == $ext) {
                    $validext = true;
                }
                $extlist .= ($extlist ? ', ' : '').$ext;
            }
            if (!$validext) {
                $error .= '<li>'.__('Invalid file type, must be one of:').' '.$extlist.'</li>';
            }

            if (!$error) {
                $tmpfile = $_FILES['picture']['tmp_name'];
                list($width, $height, $type) = getimagesize($tmpfile);

                $file = DATA_DIR.'avatars/'.$loguserid.'_'.$mid.'.'.$ft[$type];

                if ($_POST['name'] == '') {
                    $_POST['name'] = '#'.$mid;
                }

                $db->insert('moodavatars', ['uid' => $loguserid, 'mid' => $mid, 'name' => $_POST['name'], 'ext' => $type]);

                if ($type == 1) {
                    $img1 = imagecreatefromgif($tmpfile);
                }
                if ($type == 2) {
                    $img1 = imagecreatefromjpeg($tmpfile);
                }
                if ($type == 3) {
                    $img1 = imagecreatefrompng($tmpfile);
                }

                if ($width <= $dimx && $height <= $dimy && $type <= 3) {
                    copy($tmpfile, $file);
                } elseif ($type <= 3) {
                    $r = imagesx($img1) / imagesy($img1);
                    if ($r > 1) {
                        $img2 = imagecreatetruecolor($dimx, floor($dimy / $r));
                        imagecopyresampled($img2, $img1, 0, 0, 0, 0, $dimx, $dimy / $r, imagesx($img1), imagesy($img1));
                    } else {
                        $img2 = imagecreatetruecolor(floor($dimx * $r), $dimy);
                        imagecopyresampled($img2, $img1, 0, 0, 0, 0, $dimx * $r, $dimy, imagesx($img1), imagesy($img1));
                    }
                    imagepng($img2, $file);
                } else {
                    $error .= '<li>Invalid format.</li>';
                }
            }

            if (!$error) {
                die(header('Location: '.actionLink('editavatars')));
            }

            Alert(__('Could not update your avatar for the following reason(s):').'<ul>'.$error.'</ul>');
        }
    }
}

$moodRows = [];
$rMoods = Query('SELECT mid, name, ext FROM {moodavatars} where uid={0} order by mid asc', $loguserid);
while ($mood = Fetch($rMoods)) {
    $row = [];

    $row['avatar'] = '<img src="'.DATA_URL."avatars/{$loguserid}_{$mood['mid']}.{$ft[$mood['ext']]}\" alt=\"\">";

    $row['field'] = '
				<form method="post" action="'.htmlentities(actionLink('editavatars'))."\">
					<input type=\"hidden\" name=\"mid\" value=\"{$mood['mid']}\">
					<input type=\"hidden\" name=\"ext\" value=\"{$mood['ext']}\">
					<input type=\"text\" id=\"name{$mood['mid']}\" name=\"name\" size=80 maxlength=60 value=\"".htmlspecialchars($mood['name']).'"><br>
					<input type="submit" name="actionrename" value="'.__('Rename').'">
					<input type="submit" name="actiondelete" value="'.__('Delete')."\" 
						onclick=\"if(!confirm('".__('Really delete this avatar? All posts using it will be changed to use your default avatar.')."'))return false;\">
				</form>";

    $moodRows[] = $row;
}

$newField = '	<form method="post" action="'.htmlentities(actionLink('editavatars')).'" enctype="multipart/form-data">
					'.__('Name:').' <input type="text" id="newName" name="name" size=80 maxlength=60><br>
					'.__('Image:').' <input type="file" id="pic" name="picture"><br>
					<input type="submit" name="actionadd" value="'.__('Add').'">
				</form>';

RenderTemplate('moodavatars', ['avatars' => $moodRows, 'newField' => $newField]);
