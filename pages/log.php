<?php

if (!defined('BLARG')) {
    die();
}

CheckPermission('admin.viewlog');

MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('log') => __('Log')]);

$full = GetFullURL();
$here = substr($full, 0, strrpos($full, '/')).'/';
$there = './';

$logR = Query('select * from {reports} order by time desc');
$rows = [];
while ($item = Fetch($logR)) {
    $row = [];
    $row['time'] = str_replace(' ', '&nbsp;', TimeUnits(time() - $item['time']));

    $row['event'] = htmlspecialchars($item['text']);
    $row['event'] = str_replace('[g]', '', $row['event']);
    $row['event'] = str_replace('[b]', '', $row['event']);
    $row['event'] = str_replace('[/]', '', $row['event']);
    $row['event'] = str_replace('-&gt;', '&rarr;', $row['event']);

    $row['event'] = str_replace($here, $there, $row['event']);

    $rows[] = $row;
}

RenderTemplate('admin/log', ['rows' => $row]);
