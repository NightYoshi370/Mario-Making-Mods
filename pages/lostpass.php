<?php

if (!defined('BLARG')) {
    die();
}

if (empty(Settings::get('mailResetSender'))) {
    Kill(__('No sender specified for reset emails.'));
}

if (isset($_GET['key']) && isset($_GET['id'])) {
    $user = $db->row('users', ['id' => (int) $_GET['id']]);
    if (!$user) {
        Kill(__('Invalid key.'));
    }

    $sha = doHash($_GET['key'].SALT.$user['pss']);
    if ($user['lostkey'] !== $sha || $user['lostkeytimer'] > (time() - (60 * 60))) {
        Kill(__('Invalid key.'));
    }

    $newsalt = Shake();
    $newPass = randomString(8);
    $sha = doHash($newPass.SALT.$newsalt);

    $db->updateId('users', ['lostkey' => '', 'password' => $sha, 'pss' => $newsalt], 'id', (int) $_GET['id']);
    Kill(format(__('Your password has been reset to <strong>{0}</strong>. You can use this password to log in to the board. We suggest you change it as soon as possible.'), $newPass), __('Password reset'));
} elseif (isset($_POST['action'])) {
    if ($_POST['mail'] != $_POST['mail2']) {
        Alert(__("The e-mail addresses you entered don't match, try again."));
    }

    $user = $db->row('users', ['name' => $_POST['name'], 'email' => $_POST['mail']]);
    if ($user) {
        if ($user['lostkeytimer'] > time() - (60 * 60)) { //wait an hour between attempts
            Kill(__('To prevent abuse, this function can only be used once an hour.'), __('Slow down!'));
        }

        //Make a RANDOM reset key.
        $resetKey = Shake();

        $hashedResetKey = doHash($resetKey.SALT.$user['pss']);

        $from = Settings::get('mailResetSender');
        $to = $user['email'];
        $subject = format(__('Password reset for {0}'), $user['name']);
        $message = format(__('A password reset was requested for your user account on {0}.'), Settings::get('boardname'))."\n".__('If you did not submit this request, this message can be ignored.')."\n\n".__('To reset your password, visit the following URL:')."\n\n".absoluteActionLink('lostpass', $user['id'], "key=$resetKey")."\n\n".__('This link can be used once.');

        $headers = 'From: '.$from."\r\n".'Reply-To: '.$from."\r\n".'X-Mailer: PHP';

        mail($to, $subject, wordwrap($message, 70), $headers);

        $db->updateId('users', ['lostkey' => $hashedResetKey, 'lostkeytimer' => $db->time()], 'id', $user['id']);

        Kill(__('Check your email in a moment and follow the link found therein.'), __('Reset email sent'));
    } else {
        Alert(__('Check the information you entered and try again.'), __('Invalid user name or email address.'));
    }
}

$title = __('Request password reset');
MakeCrumbs([pageLink('login') => __('Log in'), actionLink('lostpass') => __('Request password reset')]);

echo '<form action="'.htmlentities(actionLink('lostpass')).'" method="post">';

$fields = [
    'username' => '<input class="form-control" type="text" placeholder="'.__('User name').'" name="name" maxlength=20 size=24>',
    'email'    => '<input class="form-control" type="text" placeholder="'.__('Email adress').'" name="mail" maxlength=60 size=24>',
    'email2'   => '<input class="form-control" type="text" placeholder="'.__('Confirm your email adress').'" name="mail2" maxlength=60 size=24>',

    'btnSendReset' => '<input type="submit" name="action" value="'.__('Send reset email').'">',
];

RenderTemplate('form_lostpass', ['fields' => $fields]);

echo '</form>';

function randomString($len, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
{
    $s = '';
    for ($i = 0; $i < $len; $i++) {
        $p = rand(0, strlen($chars) - 1);
        $s .= $chars[$p];
    }

    return $s;
}
