<?php

//  AcmlmBoard XD - The Records
//  Access: all
if (!defined('BLARG')) {
    die();
}

MakeCrumbs([actionlink('records') => __('Records')]);
$title = __('Records');

$df = 'l, F jS Y, G:i:s';

$maxUsersText = $misc['maxuserstext'];
if ($maxUsersText[0] == ':') {
    $users = explode(':', $maxUsersText);

    $maxUsersText = '';
    foreach ($users as $user) {
        if (!$user) {
            continue;
        }
        if ($maxUsersText) {
            $maxUsersText .= ', ';
        }
        $maxUsersText .= UserLinkById($user);
    }
}

// Awesome way of calculating the mean birth date.
// I'm not sure if there's any problems with overflows and all.
// But it seems to work fine :3

$sumAge = FetchResult('SELECT SUM(birthday) FROM {users} WHERE birthday != 0');
$countAge = FetchResult('SELECT COUNT(*) FROM {users} WHERE birthday != 0');
$avgAge = (int) ($sumAge / $countAge);
$avgAge = formatBirthday($avgAge);

$rewards = [];
$rewards[] = [
    'title' => __('Highest number of posts in 24 hours'),
    'value' => format(__('<strong>{0}</strong>, on {1} GMT'), $misc['maxpostsday'], gmdate($df, $misc['maxpostsdaydate'])),
];
$rewards[] = [
    'title' => __('Highest number of posts in one hour'),
    'value' => format(__('<strong>{0}</strong>, on {1} GMT'), $misc['maxpostshour'], gmdate($df, $misc['maxpostshourdate'])),
];
$rewards[] = [
    'title' => __('Highest number of users in five minutes'),
    'value' => format(__('<strong>{0}</strong>, on {1} GMT'), $misc['maxusers'], gmdate($df, $misc['maxusersdate'])).'<br>'.$maxUsersText,
];
$rewards[] = [
    'title' => __('Average age of members'),
    'value' => $avgAge,
];

$rStats = Query('show table status');
while ($stat = Fetch($rStats)) {
    $tables[$stat['Name']] = $stat;
}

$tablestats = [];
$tablestatsTotal = [];
$rows = $avg = $datlen = $idx = $datfree = 0;

foreach ($tables as $table) {
    $row = [];

    $row['name'] = $table['Name'];
    $row['rows'] = $table['Rows'];
    $row['avg'] = sp($table['Avg_row_length']);
    $row['datlen'] = sp($table['Data_length']);
    $row['idx'] = sp($table['Index_length']);
    $row['datfree'] = sp($table['Data_free']);
    $row['dattot'] = sp($table['Data_length'] + $table['Index_length']);

    $tablestats[] = $row;

    $tablestatsTotal['rows'] += $table['Rows'];
    $tablestatsTotal['avg'] += $table['Avg_row_length'];
    $tablestatsTotal['datlen'] += $table['Data_length'];
    $tablestatsTotal['datfree'] += $table['Index_length'];
    $tablestatsTotal['dattot'] += $table['Data_free'];
}

// daily stats code
$mydatefmt = 'm-d-Y';
if ($loguserid) {
    $mydatefmt = $loguser['dateformat'];
}

$timewarp = time() - 2592000;

$utotal = FetchResult('SELECT COUNT(*) FROM {users} WHERE regdate<{0}', $timewarp);
$ttotal = FetchResult('SELECT COUNT(*) num FROM {threads} t LEFT JOIN {posts} p ON p.thread=t.id AND p.id=(SELECT MIN(p2.id) FROM {posts} p2 WHERE p2.thread=t.id) WHERE p.date<{0}', $timewarp);
$ptotal = FetchResult('SELECT COUNT(*) FROM {posts} WHERE date<{0}', $timewarp);

$usersperday = Query('SELECT FLOOR(regdate / 86400) day, COUNT(*) num FROM {users} WHERE regdate>={0} GROUP BY day ORDER BY day', $timewarp);
$threadsperday = Query('SELECT FLOOR(p.date / 86400) day, COUNT(*) num FROM {threads} t LEFT JOIN {posts} p ON p.thread=t.id AND p.id=(SELECT MIN(p2.id) FROM {posts} p2 WHERE p2.thread=t.id) WHERE p.date>={0} GROUP BY day ORDER BY day', $timewarp);
$postsperday = Query('SELECT FLOOR(date / 86400) day, COUNT(*) num FROM {posts} WHERE date>={0} GROUP BY day ORDER BY day', $timewarp);

$stats = [];
while ($u = Fetch($usersperday)) {
    $stats[$u['day']]['u'] = $u['num'];
}
while ($t = Fetch($threadsperday)) {
    $stats[$t['day']]['t'] = $t['num'];
}
while ($p = Fetch($postsperday)) {
    $stats[$p['day']]['p'] = $p['num'];
}

$dailystatrows = [];
$end = floor(time() / 86400);
for ($d = floor($timewarp / 86400); $d <= $end; $d++) {
    $rowdata = [];
    if (!isset($stats[$d])) {
        continue;
    }

    $rowdata['date'] = gmdate($mydatefmt, $d * 86400);

    $rowdata['unew'] = (int) $stats[$d]['u'];
    $rowdata['tnew'] = (int) $stats[$d]['t'];
    $rowdata['pnew'] = (int) $stats[$d]['p'];
    $utotal += $rowdata['unew'];
    $ttotal += $rowdata['tnew'];
    $ptotal += $rowdata['pnew'];

    $rowdata['utotal'] = $utotal;
    $rowdata['ttotal'] = $ttotal;
    $rowdata['ptotal'] = $ptotal;

    $dailystatrows[] = $rowdata;
}

RenderTemplate('records', [
    'rewards'         => $rewards,
    'tablestats'      => $tablestats,
    'tablestatsTotal' => $tablestatsTotal,
    'dailystatrows'   => $dailystatrows,
]);

function sp($sz)
{
    return number_format($sz, 0, '.', ',');
}
