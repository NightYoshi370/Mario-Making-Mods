<?php
if (!defined('BLARG')) die();

//Check Stuff
if (!$loguserid) {
    Kill(__('You must be logged in to edit a profile.'));
}

if (isset($_POST['action']) && $loguser['token'] != $_POST['key']) {
    Kill(__('No.'));
}

if (isset($_POST['editusermode']) && $_POST['editusermode'] != 0) {
    $pageParams['id'] = $_POST['userid'];
}

$editUserMode = false;

if (HasPermission('admin.editusers')) {
    $userid = (isset($pageParams['id'])) ? (int) $pageParams['id'] : $loguserid;
    $editUserMode = true;
} else {
    CheckPermission('user.editprofile');
    $userid = $loguserid;
}

$user = $db->row('users', ['id' => $userid]);
$usergroup = $usergroups[$user['primarygroup']];

$isroot = $usergroup['id'] == Settings::get('rootGroup');
$isbanned = $usergroup['id'] == Settings::get('bannedGroup');

if ($editUserMode && $loguserid != $userid && $usergroup['rank'] > $loguserGroup['rank']) {
    Kill(__('You may not edit a user whose rank is above yours.'));
}

//Breadcrumbs
$uname = $user['name'];
if ($user['displayname']) {
    $uname = $user['displayname'];
}

$title = __('Edit profile');

$secondcrumb = [];
if ($mobileLayout) {
    $secondcrumb[actionLink('editavatars')] = ['text' => __('Mood Avatars')];
    $secondcrumb[actionLink('shop')] = ['text' => __('Item Shop')];
}

makeCrumbs(
    [actionLink('memberlist') => __('Members'),
            pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]) => htmlspecialchars($uname),
            pageLink('editprofile', ['id' => $user['id'], 'name' => slugify($user['name'])]) => __('Edit profile'), ],
    $secondcrumb
);

loadRanksets();
$ranksets = $ranksetNames;
$ranksets = array_reverse($ranksets);
$ranksets[''] = __('None');
$ranksets = array_reverse($ranksets);

foreach ($dateformats as $format) {
    $datelist[$format] = ($format ? $format.' ('.cdate($format).')' : '');
}
foreach ($timeformats as $format) {
    $timelist[$format] = ($format ? $format.' ('.cdate($format).')' : '');
}

$sexes = [__('Male'), __('Female'), __('N/A')];

$groups = [];
$r = Query('SELECT id,title FROM {usergroups} WHERE type=0 AND rank<={0} ORDER BY rank', $loguserGroup['rank']);
while ($g = Fetch($r)) {
    $groups[$g['id']] = htmlspecialchars($g['title']);
}

$pltype = Settings::get('postLayoutType');

$epPages = [];
$epCategories = [];
$epFields = [];

// EDITPROFILE TAB -- GENERAL -------------------------------------------------
AddPage('general', __('General'));

AddCategory('general', 'appearance', __('Appearance'));

if ($editUserMode || HasPermission('user.editdisplayname')) {
    AddField('general', 'appearance', 'displayname', __('Display name'), 'text', ['width' => 24, 'length' => 20, 'hint' => __('Leave this empty to use your login name.'), 'callback'=>'HandleDisplayname']);
}

AddField('general', 'appearance', 'rankset', __('Rankset'), 'select', ['options'=>$ranksets]);

if ($editUserMode || (HasPermission('user.edittitle') && (HasPermission('user.havetitle') || $user['posts'] >= Settings::get('customTitleThreshold')))) {
    AddField('general', 'appearance', 'title', __('Title'), 'text', ['width'=>80, 'length'=>255]);
}

if ($editUserMode || HasPermission('user.editavatars')) {
    AddCategory('general', 'avatar', __('Avatar'));

    AddField('general', 'avatar', 'picture', __('Avatar'), 'displaypic', ['hint'=>__('Maximum size is 200x200 pixels.')]);
}

AddCategory('general', 'presentation', __('Presentation'));

AddField('general', 'presentation', 'layout', __('Layout'), 'select', ['options'=>makeLayoutList()]);
AddField('general', 'presentation', 'threadsperpage', __('Threads per page'), 'number', ['min'=>1, 'max'=>99]);
AddField('general', 'presentation', 'postsperpage', __('Posts per page'), 'number', ['min'=>1, 'max'=>99]);
AddField('general', 'presentation', 'dateformat', __('Date format'), 'datetime', ['presets'=>$datelist, 'presetname'=>'presetdate']);
AddField('general', 'presentation', 'timeformat', __('Time format'), 'datetime', ['presets'=>$timelist, 'presetname'=>'presettime']);
AddField('general', 'presentation', 'fontsize', __('Font scale'), 'number', ['min'=>20, 'max'=>200]);

AddCategory('general', 'options', __('Options'));

$blockall = $pltype ? __('Hide post layouts') : __('Hide signatures');
AddField('general', 'options', 'blocklayouts', $blockall, 'checkbox');
AddField('general', 'options', 'hideonline', __('Hide Online Status'), 'checkbox');

// EDITPROFILE TAB -- GAMING PROFILES -------------------------------------------------
AddPage('profiles', __('Profiles'));

AddCategory('profiles', 'profiles', __('Profiles'));

AddField('profiles', 'profiles', 'dID', __('Discord ID'), 'text', ['width' => 24, 'length' => 60]);
AddField('profiles', 'profiles', 'NX', __('Nintendo Switch Friend Code'), 'text', ['width' => 24, 'length' => 60]);
AddField('profiles', 'profiles', 'NNID', __('Nintendo Network ID'), 'text', ['width' => 24, 'length' => 60]);
AddField('profiles', 'profiles', '3DS', __('Nintendo 3DS Friend Code'), 'text', ['width' => 24, 'length' => 60]);

// EDITPROFILE TAB -- PERSONAL ------------------------------------------------
AddPage('personal', __('Personal'));

AddCategory('personal', 'personal', __('Personal information'));

AddField('personal', 'personal', 'sex', __('Gender'), 'radiogroup', ['options'=>$sexes]);
AddField('personal', 'personal', 'realname', __('Real name'), 'text', ['width'=>24, 'length'=>60]);
AddField('personal', 'personal', 'location', __('Location'), 'text', ['width'=>24, 'length'=>60]);
AddField('personal', 'personal', 'birthday', __('Birthday'), 'birthday');

if ($editUserMode || HasPermission('user.editbio')) {
    AddField('personal', 'personal', 'bio', __('Bio'), 'textarea');
}

AddField('personal', 'personal', 'timezone', __('Timezone offset'), 'timezone');

AddCategory('personal', 'contact', __('Contact information'));

AddField('personal', 'contact', 'homepageurl', __('Homepage URL'), 'text', ['width'=>60, 'length'=>60]);
AddField('personal', 'contact', 'homepagename', __('Homepage name'), 'text', ['width'=>60, 'length'=>60]);

// EDITPROFILE TAB -- ACCOUNT -------------------------------------------------
AddPage('account', __('Account settings'));

AddCategory('account', 'confirm', __('Password confirmation'));
AddField('account', 'confirm', 'info', '', 'label', ['value' => __('Enter your password in order to edit account settings.')]);
AddField('account', 'confirm', 'currpassword', __('Password'), 'passwordonce');

AddCategory('account', 'login', __('Login information'));

if ($editUserMode) {
    AddField('account', 'login', 'name', __('User name'), 'text', ['width' => 24, 'length' => 20, 'callback' => 'HandleUsername']);
} else {
    AddField('account', 'login', 'name', __('User name'), 'label', ['value' => htmlspecialchars($user['name'])]);
}

AddField('account', 'login', 'password', __('Password'), 'password', ['callback' => 'HandlePassword']);

AddCategory('account', 'email', __('Email information'));

AddField('account', 'email', 'email', __('Email address'), 'email', ['width' => 24, 'length' => 60]);
AddField('account', 'email', 'showemail', __('Make email address public'), 'checkbox');

if ($editUserMode) {
    AddCategory('account', 'admin', __('Administrative stuff'));

    if ($isroot) {
        AddField('account', 'admin', 'primarygroup', __('Primary group'), 'label', ['value'=>htmlspecialchars($usergroup['title'])]);
    } else {
        AddField('account', 'admin', 'primarygroup', __('Primary group'), 'select', ['options' => $groups]);
    }

    // TODO secondary groups!!

    if ($isbanned && $user['tempbantime']) {
        AddField('account', 'admin', 'dopermaban', __('Make ban permanent'), 'checkbox', ['callback' => 'dummycallback']);
    }

    AddField('account', 'admin', 'globalblock', __('Globally block layout'), 'checkbox');

    $aflags = [0x1 => __('IP banned'), 0x2 => __('Errorbanned')];
    AddField('account', 'admin', 'flags', __('Misc. settings'), 'bitmask', ['options' => $aflags]);
}

// EDITPROFILE TAB -- LAYOUT --------------------------------------------------
if ($editUserMode || HasPermission('user.editpostlayout')) {
    $pltext = $pltype ? __('Post layout') : __('Signature');
    AddPage('postlayout', $pltext);

    AddCategory('postlayout', 'postlayout', $pltext);

    if ($pltype) {
        AddField('postlayout', 'postlayout', 'postheader', __('Post header'), 'textarea', ['rows'=>16]);
    }
    AddField('postlayout', 'postlayout', 'signature', __('Signature'), 'textarea', ['rows'=>16]);

    AddField('postlayout', 'postlayout', 'signsep', __('Show signature separator'), 'checkbox', ['negative'=>true]);

    // TODO make a per-user permission for this one?
    if ($pltype == 2) {
        AddField('postlayout', 'postlayout', 'fulllayout', __('Apply layout to whole post box'), 'checkbox');
    }
}

// EDITPROFILE TAB -- THEME ---------------------------------------------------
AddPage('theme', __('Theme'));

AddCategory('theme', 'theme', __('Themes'));
AddField('theme', 'theme', 'theme', '', 'themeselector');

//Allow plugins to add their own fields
$bucket = 'editprofile'; require BOARD_ROOT.'lib/pluginloader.php';

$_POST['actionsave'] = (isset($_POST['actionsave']) ? $_POST['actionsave'] : '');

/* QUERY PART
 * ----------
 */

$failed = false;

if ($_POST['actionsave']) {
    // catch spamvertisers early
    if ((time() - $user['regdate']) < 300 && preg_match('@^\w+\d+$@', $user['name'])) {
        $lolbio = strtolower($_POST['bio']);

        if ((substr($lolbio, 0, 7) == 'http://'
            || substr($lolbio, 0, 12) == '[url]http://'
            || substr($lolbio, 0, 12) == '[url=http://')
            && ((substr($_POST['email'], 0, strlen($user['name'])) == $user['name'])
            || (substr($user['name'], 0, 6) == 'iphone'))
        ) {
            $db->updateId('users', ['primarygroup' => Settings::get('bannedGroup'), 'title' => __('Banned Permanently: Spamvertising')], 'id', $loguserid);

            die(header('Location: '.pageLink('home')));
        }
    }

    $passwordEntered = false;

    if (!empty($_POST['currpassword'])) {
        $sha = doHash($_POST['currpassword'].SALT.$loguser['pss']);
        if ($loguser['password'] == $sha) {
            $passwordEntered = true;
        } else {
            Alert(__('Invalid password'));
            $failed = true;
            $selectedTab = 'account';

            $epFields['account.confirm']['currpassword']['fail'] = true;
        }
    }

    $sets = [];
    $pluginSettings = unserialize($user['pluginsettings']);

    foreach ($epFields as $catid => $cfields) {
        foreach ($cfields as $field => $item) {
            if (substr($catid, 0, 8) == 'account.' && !$passwordEntered) {
                continue;
            }

            if ($item['callback']) {
                $ret = $item['callback']($field, $item);
                if ($ret === true) {
                    continue;
                } elseif (!empty($ret)) {
                    Alert($ret, __('Error'));
                    $failed = true;
                    $selectedTab = $id;
                    $item['fail'] = true;
                }
            }

            switch ($item['type']) {
            case 'label':
                break;
            case 'text':
            case 'email':
            case 'textarea':
            case 'themeselector':
                $sets[$field] = $_POST[$field];
                break;
            case 'password':
                if ($_POST[$field]) {
                    $sets[$field] = $_POST[$field];
                }
                break;
            case 'select':
                $val = $_POST[$field];
                if (array_key_exists($val, $item['options'])) {
                    $sets[$field] = $val;
                }
                break;
            case 'number':
                $num = (int) $_POST[$field];
                if ($num < 1) {
                    $num = $item['min'];
                } elseif ($num > $item['max']) {
                    $num = $item['max'];
                }
                $sets[$field] = $num;
                break;
            case 'datetime':
                if ($_POST[$item['presetname']] != -1) {
                    $_POST[$field] = $_POST[$item['presetname']];
                }
                $sets[$field] = $_POST[$field];
                break;
            case 'checkbox':
                $val = (int) ($_POST[$field] == 'on');
                if ($item['negative']) {
                    $val = (int) ($_POST[$field] != 'on');
                }
                $sets[$field] = $val;
                break;
            case 'radiogroup':
                if (array_key_exists($_POST[$field], $item['options'])) {
                    $sets[$field] = $_POST[$field];
                }
                break;
            case 'birthday':
                if (!empty($_POST[$field.'M']) && !empty($_POST[$field.'D']) && !empty($_POST[$field.'Y'])) {
                    $val = @mktime(0, 0, 0, (int) $_POST[$field.'M'], (int) $_POST[$field.'D'], (int) $_POST[$field.'Y']);
                    if ($val > time()) {
                        $val = 0;
                    }
                } else {
                    $val = 0;
                }
                $sets[$field] = $val;
                break;
            case 'timezone':
                $val = ((int) $_POST[$field.'H'] * 3600) + ((int) $_POST[$field.'M'] * 60) * ((int) $_POST[$field.'H'] < 0 ? -1 : 1);
                $sets[$field] = $val;
                break;

            case 'displaypic':
                if ($_POST['remove'.$field]) {
                    $res = true;
                    $sets[$field] = '';
                } else {
                    if (empty($_FILES[$field]['name']) || $_FILES[$field]['error'] == UPLOAD_ERR_NO_FILE) {
                        continue;
                    }
                    $usepic = '';
                    $res = HandlePicture($field, $usepic);
                    if ($res === true) {
                        $sets[$field] = $usepic;
                    } else {
                        Alert($res);
                        $failed = true;
                        $item['fail'] = true;
                    }
                }

                // delete the old image if needed
                if ($res === true) {
                    if (substr($user[$field], 0, 6) == '$root/') {
                        // verify that the file they want us to delete is an internal avatar and not something else
                        $path = str_replace('$root/', DATA_DIR, $user[$field]);
                        if (!file_exists($path.'.internal')) {
                            continue;
                        }
                        $hash = file_get_contents($path.'.internal');
                        if ($hash === hash_hmac_file('sha256', $path, $userid.SALT)) {
                            @unlink($path);
                            @unlink($path.'.internal');
                        }
                    }
                }
                break;

            case 'bitmask':
                $val = 0;
                if ($_POST[$field]) {
                    foreach ($_POST[$field] as $bit) {
                        if ($bit && array_key_exists($bit, $item['options'])) {
                            $val |= $bit;
                        }
                    }
                }
                $sets[$field] = (int) $val;
                break;
            }

            $epFields[$catid][$field] = $item;
        }
    }

    //Force theme names to be alphanumeric to avoid possible directory traversal exploits ~Dirbaio
    if (preg_match('/^[a-zA-Z0-9_]+$/', $_POST['theme'])) {
        $sets['theme'] = $_POST['theme'];
    }

    $sets['pluginsettings'] = serialize($pluginSettings);
    if ($editUserMode && ((int) $_POST['primarygroup'] != $user['primarygroup'] || $_POST['dopermaban'])) {
        $sets['tempbantime'] = 0;
        if ((int) $_POST['primarygroup'] != $user['primarygroup']) {
            $sets['tempbanpl'] = (int) $user['primarygroup'];
        }

        Report($user['name']."'s primary group was changed from ".$groups[$user['primarygroup']].' to '.$groups[(int) $_POST['primarygroup']]);
    }

    if (!$failed) {
        $db->updateId('users', $sets, 'id', $userid);

        if ($loguserid == $userid) {
            $his = HisHer($user['sex']);
        } else {
            $his = '[b]'.$user['name']."[/]'s";
        }

        Report('[b]'.$loguser['name']."[/] edited $his profile. -> ".getServerDomainNoSlash().pageLink('profile', ['id' => $userid, 'name' => $user['name']]), 1);
        $theme = $sets['theme'];
        $layout = $sets['layout'];

        if ($layout == 'acmlm') {
            OldRedirect(__('Profile updated.'), pageLink('profile', ['id' => $userid, 'name' => $user['name']]), ($userid == $loguserid ? __('your profile') : format(__("{0}'s profile"), $user['name'])));
        } else {
            die(header('Location: '.pageLink('profile', ['id' => $userid, 'name' => $user['name']])));
        }
    }
}

//If failed, get values from $_POST
//Else, get them from $user

foreach ($epFields as $catid => $cfields) {
    foreach ($cfields as $field => $item) {
        if ($item['type'] == 'label' || $item['type'] == 'password') {
            continue;
        }

        if (!$failed) {
            if (!isset($item['value'])) {
                $item['value'] = $user[$field];
            }
        } else {
            if ($item['type'] == 'checkbox') {
                $item['value'] = ($_POST[$field] == 'on') ^ $item['negative'];
            } elseif ($item['type'] == 'timezone') {
                $item['value'] = ((int) $_POST[$field.'H'] * 3600) + ((int) $_POST[$field.'M'] * 60) * ((int) $_POST[$field.'H'] < 0 ? -1 : 1);
            } elseif ($item['type'] == 'birthday') {
                $item['value'] = @mktime(0, 0, 0, (int) $_POST[$field.'M'], (int) $_POST[$field.'D'], (int) $_POST[$field.'Y']);
            } else {
                $item['value'] = $_POST[$field];
            }
        }

        $epFields[$catid][$field] = $item;
    }
}

if ($failed) {
    $loguser['theme'] = $_POST['theme'];
}

function dummycallback($field, $item)
{
    return true;
}

function HandlePicture($field, &$usepic)
{
    global $userid;

    $extensions = ['.png', '.jpg', '.jpeg', '.gif'];

    $maxDim = 512;
    $maxSize = 600 * 1024;
    $errorname = __('avatar');

    $fileName = $_FILES[$field]['name'];
    $fileSize = $_FILES[$field]['size'];
    $tempFile = $_FILES[$field]['tmp_name'];
    list($width, $height, $fileType) = getimagesize($tempFile);

    $extension = strtolower(strrchr($fileName, '.'));
    if (!in_array($extension, $extensions)) {
        return format(__('Invalid extension used for {0}. Allowed: {1}'), $errorname, implode($extensions, ', '));
    }

    if ($fileSize > $maxSize && !$allowOversize) {
        return format(__('File size for {0} is too high. The limit is {1} bytes, the uploaded image is {2} bytes.'), $errorname, $maxSize, $fileSize).'</li>';
    }

    switch ($fileType) {
    case 1:
        $sourceImage = imagecreatefromgif($tempFile);
        $ext = '.gif';
        break;
    case 2:
        $sourceImage = imagecreatefromjpeg($tempFile);
        $ext = '.jpg';
        break;
    case 3:
        $sourceImage = imagecreatefrompng($tempFile);
        $ext = '.png';
        break;
    }

    $randomcrap = '_'.time();
    $targetFile = false;

    $oversize = ($width > $maxDim || $height > $maxDim);
    $targetFile = 'avatars/'.$userid.$randomcrap.$ext;

    if (!$oversize) {
        //Just copy it over.
        copy($tempFile, DATA_DIR.$targetFile);
    } else {
        //Resample it!
        $ratio = $width / $height;
        if ($ratio > 1) {
            $targetImage = imagecreatetruecolor($maxDim, floor($maxDim / $ratio));
            imagecopyresampled($targetImage, $sourceImage, 0, 0, 0, 0, $maxDim, $maxDim / $ratio, $width, $height);
        } else {
            $targetImage = imagecreatetruecolor(floor($maxDim * $ratio), $maxDim);
            imagecopyresampled($targetImage, $sourceImage, 0, 0, 0, 0, $maxDim * $ratio, $maxDim, $width, $height);
        }
        imagepng($targetImage, DATA_DIR.$targetFile);
        imagedestroy($targetImage);
    }

    // file created to verify that the avatar was created here
    file_put_contents(DATA_DIR.$targetFile.'.internal', hash_hmac_file('sha256', DATA_DIR.$targetFile, $userid.SALT));

    $usepic = '$root/'.$targetFile;

    return true;
}

// Special field-specific callbacks
function HandlePassword($field, $item)
{
    global $sets, $user, $loguser, $loguserid;
    if (!empty($_POST[$field]) && !empty($_POST['repeat'.$field]) && $_POST['repeat'.$field] !== $_POST[$field]) {
        return __('To change your password, you must type it twice without error.');
    }

    if (!empty($_POST[$field]) && empty($_POST['repeat'.$field])) {
        $_POST[$field] = '';
    }

    if ($_POST[$field]) {
        $newsalt = Shake();
        $sha = doHash($_POST[$field].SALT.$newsalt);
        $_POST[$field] = $sha;
        $sets['pss'] = $newsalt;

        //Now logout all the sessions that aren't this one, for security.
        Query('DELETE FROM {sessions} WHERE id != {0} and user = {1}', doHash($_COOKIE['logsession'].SALT), $user['id']);
    }

    return false;
}

function HandleDisplayname($field, $item)
{
    global $user;
    if (IsReallyEmpty($_POST[$field]) || $_POST[$field] == $user['name']) {
        $_POST[$field] = '';
    } else {
        $dispCheck = FetchResult('SELECT COUNT(*) FROM {users} WHERE id != {0} AND (name = {1} OR displayname = {1})', $user['id'], $_POST[$field]);
        if ($dispCheck) {
            return format(__('The display name you entered, "{0}", is already taken.'), SqlEscape($_POST[$field]));
        } elseif ($_POST[$field] !== ($_POST[$field] = preg_replace('/(?! )[\pC\pZ]/u', '', $_POST[$field]))) {
            return __('The display name you entered cannot contain control characters.');
        }
    }
}

function HandleUsername($field, $item)
{
    global $user;
    if (IsReallyEmpty($_POST[$field])) {
        $_POST[$field] = $user[$field];
    }

    $dispCheck = FetchResult('SELECT COUNT(*) FROM {users} WHERE id != {0} AND (name = {1} OR displayname = {1})', $user['id'], $_POST[$field]);
    if ($dispCheck) {
        return format(__('The login name you entered, "{0}", is already taken.'), SqlEscape($_POST[$field]));
    } elseif ($_POST[$field] !== ($_POST[$field] = preg_replace('/(?! )[\pC\pZ]/u', '', $_POST[$field]))) {
        return __('The login name you entered cannot contain control characters.');
    }
}

/* EDITOR PART
 * -----------
 */

//Dirbaio: Rewrote this so that it scans the themes dir.
$dir = 'themes/';
$themeList = '';
$themes = [];

// Open a known directory, and proceed to read its contents
if (is_dir($dir) && $dh = opendir($dir)) {
    while (($file = readdir($dh)) !== false) {
        if (filetype($dir.$file) != 'dir') {
            continue;
        }
        if ($file == '..' || $file == '.') {
            continue;
        }
        $infofile = $dir.$file.'/themeinfo.txt';

        if (file_exists($infofile)) {
            $themeinfo = file_get_contents($infofile);
            $themeinfo = explode("\n", $themeinfo, 2);

            $themes[$file]['name'] = trim($themeinfo[0]);
            $themes[$file]['author'] = trim($themeinfo[1]);
        } else {
            $themes[$file]['name'] = $file;
            $themes[$file]['author'] = '';
        }

        $themes[$file]['num'] = 0;
    }
    closedir($dh);
}

$countdata = Query('SELECT theme, COUNT(id) num FROM {users} GROUP BY theme');
while ($c = Fetch($countdata)) {
    $themes[$c['theme']]['num'] = $c['num'];
}

asort($themes);

$themeList .= '
    <div style="text-align: right;">
        <input type="text" placeholder="'.__('Search').'" id="search" onkeyup="searchThemes(this.value);" />
    </div>';

foreach ($themes as $themeKey => $themeData) {
    if (is_file($dir.$themeKey.'/themeinfo.txt')) {
        $themeName = $themeData['name'];
        $themeAuthor = $themeData['author'];
        $themeCategory = $themeData['category'];
        $numUsers = $themeData['num'];

        $csspreview = false;
        if (is_file($dir.$themeKey.'/preview.css')) {
            $csspreview = true;
            $preview = resourceLink($dir.$themeKey.'/preview.css');
        } elseif (is_file($dir.$themeKey.'/preview.png')) {
            $preview = resourceLink($dir.$themeKey.'/preview.png');
        } else {
            $preview = '';
        }

        $themeList .=
            '<span class="theme" title="'.$themeName.'">
                <input style="display: none;" type="radio" name="theme" value="'.$themeKey.'" 
                        '.($themeKey == $user['theme'] ? 'checked="checked"' : '').'
                        id="'.$themeKey.'" onchange="ChangeTheme(this.value);" />
                <label style="'.($mobileLayout ? 'width: 100%;' : 'width: 350px;').' padding: 5px 5px 5px 5px; vertical-align:top;" class="p'.$themeKey.'back" onmousedown="void();" for="'.$themeKey.'">';

        if ($csspreview) {
            $themeList .= '<link rel="stylesheet" type="text/css" id="theme_preview" href="'.$preview.'">
                        <table class="table table-bordered table-condensed table-striped previewbox p'.$themeKey.'" style="margin-bottom: 0;">
                            <tr class="pheader1"><th><strong>'.$themeName.'</strong></th></tr>'
                            .($themeAuthor ? '<tr class="pcell0"><td>'.parseBBCode($themeAuthor).'</td></tr>' : '')
                            .'<tr class="pcell2"><td>'.Plural($numUsers, 'user').'</td></tr>
                        </table>';
        } else {
            $themeList .= '<table class="table table-bordered table-striped table-condensed">
                            <tr class="header1"><th><strong>'.$themeName.'</strong></th></tr>'
                            .(!empty($preview) ? '<tr class="cell1"><td style="padding: 5px 5px 5px 5px;"><img src="'.$preview.'" alt="'.$themeName.'" style="margin-bottom: 0.5em"></td></tr>' : '')
                            .($themeAuthor ? '<tr class="cell1"><td>'.parseBBCode($themeAuthor).'</td></tr>' : '').
                            '<tr class="cell2"><td>'.Plural($numUsers, 'user').'</td></tr>
                        </table>';
        }

        $themeList .= '
                </label>
            </span>';
    }
}

if (!isset($selectedTab)) {
    $selectedTab = 'general';
    foreach ($epPages as $id => $name) {
        if (isset($_GET[$id])) {
            $selectedTab = $id;
            break;
        }
    }
}

foreach ($epFields as $catid => $cfields) {
    foreach ($cfields as $field => $item) {
        $output = '';

        if (isset($item['fail'])) {
            $item['caption'] = "<span style=\"color:#f44;\">{$item['caption']}</span>";
        }

        switch ($item['type']) {
        case 'label':
            $output .= $item['value']."\n";
            break;

        case 'password':
            $output = '
                    <input type="password" class="form-control" placeholder="'.__('Enter the Password').'" name="'.$field.'" size=24>
                    <input type="password" class="form-control" placeholder="'.__('Confirm the Password').'" name="repeat'.$field.'" size=24>';
            break;
        case 'passwordonce':
            $output = '<input type="password" class="form-control" placeholder="'.__('Enter the Password').'" name="'.$field.'" id="'.$field.'" size=24>';
            break;

        case 'color':
            $output = '<input type="text" name="'.$field.'" id="'.$field.'" value="'.htmlspecialchars($item['value']).'" class="color{required:false}">';
            break;

        case 'birthday':
            if (!$item['value']) {
                $bd = ['', '', ''];
            } else {
                $bd = explode('-', date('m-d-Y', $item['value']));
            }
            $output .= __('Month: ')."<input type=\"text\" name=\"{$field}M\" value=\"{$bd[0]}\" size=4 maxlength=2> ";
            $output .= __('Day: ')."<input type=\"text\" name=\"{$field}D\" value=\"{$bd[1]}\" size=4 maxlength=2> ";
            $output .= __('Year: ')."<input type=\"text\" name=\"{$field}Y\" value=\"{$bd[2]}\" size=4 maxlength=4> ";
            break;

        case 'text':
        case 'email':
            $output .= '<input id="'.$field.'" class="form-control" name="'.$field.'" type="'.$item['type'].'" value="'.htmlspecialchars($item['value']).'"';
            if (isset($item['length'])) {
                $output .= ' maxlength="'.$item['length'].'"';
            }
            if (isset($item['more'])) {
                $output .= ' '.$item['more'];
            }
            if ($item['type'] == 'email') {
                $output .= ' placeholder="'.__('Enter the Email').'"';
            }
            $output .= '>';
            break;

        case 'textarea':
            $output .= '<textarea id="'.$field.'" class="form-control" name="'.$field.'" rows="'.$item['rows']."\">\n".htmlspecialchars($item['value']).'</textarea>';
            break;

        case 'checkbox':
            $output .= '<div class="checkbox"><label><input id="'.$field.'" name="'.$field.'" type="checkbox"';
            if ((isset($item['negative']) && !$item['value']) || (!isset($item['negative']) && $item['value'])) {
                $output .= ' checked="checked"';
            }
            $output .= '> '.$item['caption'].'</label></div>';
            $item['caption'] = '';
            break;

        case 'select':
            $disabled = isset($item['disabled']) ? $item['disabled'] : false;
            $disabled = $disabled ? 'disabled="disabled" ' : '';
            $checks = [];
            $checks[$item['value']] = ' selected="selected"';
            $options = '';
            foreach ($item['options'] as $key => $val) {
                $options .= format('<option value="{0}"{1}>{2}</option>', $key, $checks[$key], $val);
            }
            $output .= format("<select id=\"{0}\" name=\"{0}\" size=\"1\" {2}>\n{1}\n</select>\n", $field, $options, $disabled);
            break;

        case 'radiogroup':
            $checks = [];
            $checks[$item['value']] = ' checked="checked"';
            foreach ($item['options'] as $key => $val) {
                $output .= format('<div class="radio"><label><input type="radio" name="{1}" value="{0}"{2}>{3}</label></div>', $key, $field, $checks[$key], $val).' ';
            }
            break;

        case 'displaypic':
            $output .= '<input type="file" id="'.$field.'" name="'.$field."\">\n";
            $output .= '<label><input type="checkbox" name="remove'.$field.'"> '.__('Remove')."</label>\n";
            break;

        case 'number':
            $output .= '<input type="text" id="'.$field.'" name="'.$field.'" value="'.$item['value'].'" size="6" maxlength="4">';
            break;

        case 'datetime':
            $output .= '<input type="text" id="'.$field.'" name="'.$field.'" value="'.$item['value']."\">\n";
            $output .= __('or preset:')."\n";
            $options = '<option value="-1">'.__('[select]').'</option>';
            foreach ($item['presets'] as $key => $val) {
                $options .= format('<option value="{0}">{1}</option>', $key, $val);
            }
            $output .= format("<select id=\"{0}\" name=\"{0}\" size=\"1\" >\n{1}\n</select>\n", $item['presetname'], $options);
            break;

        case 'timezone':
            $output .= '<input type="text" name="'.$field.'H" size="2" maxlength="3" value="'.(int) ($item['value'] / 3600)."\">\n";
            $output .= ":\n";
            $output .= '<input type="text" name="'.$field.'M" size="2" maxlength="3" value="'.floor(abs($item['value'] / 60) % 60).'">';
            break;

        case 'bitmask':
            foreach ($item['options'] as $key => $val) {
                $output .= format(
                    '<label><input type="checkbox" name="{1}[]" value="{0}"{2}> {3}</label> &nbsp;',
                    $key,
                    $field,
                    ($item['value'] & $key) ? ' checked="checked"' : '',
                    $val
                );
            }
            $item['caption'] = '';
            break;

        case 'themeselector':
            $output .= $themeList;
            break;
        }
        if (isset($item['extra'])) {
            $output .= ' '.$item['extra'];
        }

        $item['html'] = $output;
        $epFields[$catid][$field] = $item;
    }
}

echo '<form action="'.htmlentities(pageLink('editprofile')).'" method="post" enctype="multipart/form-data">';

RenderTemplate(
    'form_editprofile', [
    'pages'          => $epPages,
    'categories'     => $epCategories,
    'fields'         => $epFields,
    'selectedTab'    => $selectedTab,
    'btnEditProfile' => '<input type="submit" id="submit" class="btn btn-primary" name="actionsave" value="'.__('Save').'">', ]
);

echo "
        <input type=\"hidden\" name=\"editusermode\" value=\"1\">
        <input type=\"hidden\" name=\"userid\" value=\"{$userid}\">
        <input type=\"hidden\" name=\"key\" value=\"{$loguser['token']}\">
    </form>";

function IsReallyEmpty($subject)
{
    $trimmed = trim(preg_replace('/&.*;/', '', $subject));

    return strlen($trimmed) == 0;
}

function AddPage($page, $name)
{
    global $epPages, $epCategories;

    $epPages[$page] = $name;
    $epCategories[$page] = [];
}

function AddCategory($page, $cat, $name)
{
    global $epCategories, $epFields;

    $epCategories[$page][$page.'.'.$cat] = $name;
    $epFields[$page.'.'.$cat] = [];
}

function AddField($page, $cat, $id, $label, $type, $misc = null)
{
    global $epFields;

    $field = [
        'caption' => $label,
        'type'    => $type,
    ];

    if ($misc) {
        $field = array_merge($field, $misc);
    }

    $epFields[$page.'.'.$cat][$id] = $field;
}

function HandlePowerlevel($field, $item)
{
    global $user, $loguserid, $userid;
    $id = $userid;
    if ($user['primarygroup'] != (int) $_POST['primarygroup'] && $id != $loguserid) {
        $newPL = (int) $_POST['primarygroup'];
        $oldPL = $user['primarygroup'];

        if ($newPL == 5) {

            //Do nothing -- System won't pick up the phone.
        } elseif ($newPL == -1) {
            SendSystemPM($id, __("If you don't know why this happened, feel free to ask the one most likely to have done this. Calmly, if possible."), __('You have been banned.'));
        } elseif ($newPL == 0) {
            if ($oldPL == -1) {
                SendSystemPM($id, __('Try not to repeat whatever you did that got you banned.'), __('You have been unbanned.'));
            } elseif ($oldPL > 0) {
                SendSystemPM($id, __('Try not to take it personally.'), __('You have been brought down to normal.'));
            }
        } else {
            if ($oldPL == -1) {

                //Do nothing.
            } elseif ($oldPL > $newPL) {
                SendSystemPM($id, __('Try not to take it personally.'), __('You have been demoted.'));
            } elseif ($oldPL < $newPL) {
                SendSystemPM($id, __("Congratulations. Don't forget to review the rules regarding your newfound powers."), __('You have been promoted.'));
            }
        }
    }
}

function makeLayoutList()
{
    $layouts = [];
    $dir = @opendir('layout');
    while ($layout = readdir($dir)) {
        if (!endsWith($layout, '.php') && $layout != '.' && $layout != '..' && file_exists('./layout/'.$layout.'/info.txt')) {
            $layouts[$layout] = @file_get_contents('./layout/'.$layout.'/info.txt');
        }
    }
    closedir($dir);
    ksort($layouts);

    return $layouts;
}

?>
<script type="text/javascript">
var homepagename = "<?php echo addslashes($epFields['personal.contact']['homepagename']['value']); ?>";
setTimeout(function()
{
    // kill Firefox's dumb autofill
    $('#homepagename').val(homepagename);
    $('#currpassword').keyup();
}, 200);

$('#currpassword').keyup(function()
{
    var fields = $('#account').find('input:not(#currpassword),select');
    if (this.value == '')
        fields.attr('disabled', 'disabled');
    else
        fields.removeAttr('disabled');
});
</script>