<?php
if (!defined('BLARG')) die();
CheckPermission('admin.viewips');

$title = __('Visitors History');
MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('visitors') => __('Visitors History')]);

$rGuests = Query('select * from {guests} where date > {0} and bot = 0 order by date desc', (time() - 300));
$rBots = Query('select * from {guests} where date > {0} and bot = 1 order by date desc', (time() - 300));

$guests = listGuests($rGuests);
$bots   = listGuests($rBots);

RenderTemplate('onlinelist', [
    'guests'    => $guests,
    'bots'      => $bots
]);

function FilterURL($url)
{
    $url = htmlspecialchars($url);
    $url = preg_replace('@(&amp;)?(key|token)=[0-9a-f]{40,64}@i', '', $url);

    return $url;
}

function listGuests($rGuests)
{
    $guestList = [];
    $i = 1;
    while ($guest = Fetch($rGuests)) {
        $gdata = [];
        $gdata['num'] = $i++;

        $gdata['userAgent'] = '<span title="'.htmlspecialchars($guest['useragent']).'">'.htmlspecialchars(substr($guest['useragent'], 0, 65)).'</span>';
		$gdata['ip'] = formatIP($guest['ip']);

        $gdata['lastView'] = cdate('d-m-y G:i:s', $guest['date']);

        if ($guest['date']) {
            $gdata['lastURL'] = parseBBCode('[url]'.FilterURL($guest['lasturl']).'[/url]';
        } else {
            $gdata['lastURL'] = __('None');
        }

        $guestList[] = $gdata;
    }

    return $guestList;
}
