<?php

    const SCALE_X = 1;
    const SCALE_Y = 500; // 500

    const SECTOR_H = 50;

    const THRESHOLD = 5000;

    // Legend box with usernames (easily configurable just in case)
    const BOX_X = 60;
    const BOX_Y = 10;
    const BOX_W = 143;

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

    //Get the first registration date (without time info)
    $regdate = fetchResult('SELECT MIN(`regdate`) FROM {users}');
    $regday = floor($regdate / 86400); // Day number
    $regday_ts = $regday * 86400;         // Day timestamp

    $days = ceil((time() - $regday_ts) / 86400); // Days the board has been opened

    $max = ceil((fetchResult('SELECT COUNT(*) FROM {posts}') + 1) / THRESHOLD) * THRESHOLD;

    $alen = (isset($_GET['len']) ? $_GET['len'] : 30);
    $alen = min(max(7, $alen), 90);

    define('IMAGE_X', $days * SCALE_X);
    define('IMAGE_Y', $max / SCALE_Y);

    $img = imagecreatetruecolor(IMAGE_X, IMAGE_Y);

    $c['bg'] = imagecolorallocate($img, 0, 0, 0);
    $c['bg1'] = imagecolorallocate($img, 0, 0, 60);
    $c['bg2'] = imagecolorallocate($img, 0, 0, 80);
    $c['bg3'] = imagecolorallocate($img, 40, 40, 100);
    $c['bg4'] = imagecolorallocate($img, 100, 40, 40);
    $c['mk1'] = imagecolorallocate($img, 60, 60, 130);
    $c['mk2'] = imagecolorallocate($img, 80, 80, 150);
    $c['bar'] = imagecolorallocate($img, 250, 190, 40);
    $c['pt'] = imagecolorallocate($img, 250, 250, 250);

    // Draw background
for ($i = 0; $i < $days; $i++) {
    $ts = $regday_ts + $i * 86400;
    $md = date('m-d', $ts);

    if ($md == '01-01') {
        $num = 3;
    } else {
        $num = substr($md, 0, 2) % 2 + 1;
    } // Alternate between months

    imagefilledrectangle($img, $i * SCALE_X, IMAGE_Y, ($i + 1) * SCALE_X - 2, 0, $c['bg'.$num]);
}

    // Postcount indicator for each sector; with separator lines
    $sect_x2 = SECTOR_H * 2; // yeah
    $digits = strlen((string) $max);
    $r_padding = $digits * 7 + 2; // Font 3 is 7 px wide
for ($y = IMAGE_Y - SECTOR_H; $y >= 0; $y -= SECTOR_H) {
    $color = ($y % $sect_x2) ? $c['mk1'] : $c['mk2']; // Start from mk1 and loop back and forth for each limit
    $posts = (IMAGE_Y - $y) * SCALE_Y;

    imageline($img, 0, $y, IMAGE_X, $y, $color);
    // On both sides here
    imagestring($img, 3, 3, $y + 1, $posts, $c['bg']);
    imagestring($img, 3, 2, $y, $posts, $color);
    imagestring($img, 3, IMAGE_X - $r_padding + 1, $y + 1, sprintf("%{$digits}d", $posts), $c['bg']);
    imagestring($img, 3, IMAGE_X - $r_padding, $y, sprintf("%{$digits}d", $posts), $color);
}

    $users = [
         0 => ['name' => 'Total posts',                        'color' =>  imagecolorallocate($img, 255, 255, 255)],
        -1 => ['name' => $alen.'-day average x '.SCALE_Y,    'color' =>  0xFF8888],
    ];

    $z = count($users);

    const NAME_HEIGHT = 12;
    // Draw the legend background box
    imagerectangle($img, BOX_X + 1, BOX_Y + 1, BOX_X + BOX_W + 1, BOX_Y + 5 + $z * NAME_HEIGHT, $c['bg']);  // Shadow
    imagefilledrectangle($img, BOX_X, BOX_Y, BOX_X + BOX_W, BOX_Y + 4 + $z * NAME_HEIGHT, $c['bg2']); // Background
    imagerectangle($img, BOX_X, BOX_Y, BOX_X + BOX_W, BOX_Y + 4 + $z * NAME_HEIGHT, $c['mk2']); // Border

    // Get the data for the real user IDs (id > 0) if they are present
    $realusers = array_filter(
        array_keys($users), function ($v) {
            return $v > 0;
        }
    );
    $data = $realusers ? getdata($realusers) : [];
    // Get the total as well
    $data += getdata();

    $z = 0;
    foreach ($users as $uid => $userx) {
        // Negative IDs are special and are handled separately
        if ($uid >= 0) {
            drawdata($data[$uid], $userx['color']);
        }
        // 10px Dash next to the name...
        imageline($img, BOX_X + 6, BOX_Y + 9 + $z * NAME_HEIGHT, BOX_X + 6 + 10, BOX_Y + 9 + $z * NAME_HEIGHT, $c['bg']);
        imageline($img, BOX_X + 5, BOX_Y + 8 + $z * NAME_HEIGHT, BOX_X + 5 + 10, BOX_Y + 8 + $z * NAME_HEIGHT, $userx['color']);
        // And the name proper...
        imagestring($img, 2, BOX_X + 21, BOX_Y + 2 + $z * NAME_HEIGHT, $userx['name'], $c['bg']);
        imagestring($img, 2, BOX_X + 20, BOX_Y + 1 + $z * NAME_HEIGHT, $userx['name'], $userx['color']);
        $z++;
    }

    // Draw the line for the average
    $average = getxdata();
    drawdata($average, $users[-1]['color']);

    // Header('Content-type:image/png');
    // ImagePNG($img);
    // ImageDestroy($img);
    error_reporting(E_ALL);

    function getdata($users = null)
    {
        global $regday;

        if ($users !== null) { // Actual users in the list
            // Initialize user total
            $ucount = count($users);
            $total = [];
            for ($i = 0; $i < $ucount; $i++) {
                $total[$users[$i]] = 0;
            }

            $postdays = Query(
                '
			SELECT user, FLOOR(date / 86400) day, COUNT(*) c
			FROM posts 
			WHERE user IN ('.implode(',', $users).') 
			GROUP BY user, day 
			ORDER BY user, day
		'
            );
        } else {
            // Get total of every user regardless of user; so grab all the users as user 0 (which is the key for the total posts in $users)
            $total[0] = 0;
            $postdays = fetch(
                query(
                    '
			SELECT
				0 user, FLOOR(date / 86400) day, COUNT(*) c 
			FROM {posts} 
			GROUP BY day 
			ORDER BY day'
                )
            );
        }

        $resp = [];
        // For every user, return the total
        // Since it's ordered already by day we can safely add to the total without issues
        //while ($x = $sql->fetch($postdays)) {
        foreach ($postdays as $x) {
            $total[$x['user']] += $x['c'];
            $resp[$x['user']][$x['day'] - $regday] = $total[$x['user']] / SCALE_Y;
        }

        return $resp;
    }

    function getxdata()
    {
        global $alen, $regday;

        $nquery = fetch(
            query(
                '
		SELECT 0 user, FLOOR(date / 86400) day, COUNT(*) c 
		FROM {posts} 
		GROUP BY day 
		ORDER BY day'
            )
        );

        $xdata = [];

        // Initialize the totals
        $days = array_column($nquery, 'day');
        $first_day = min($days);
        $total = array_fill($first_day, max($days) - $first_day, 0);

        foreach ($nquery as $n) {
            $total[$n['day']] = $n['c'];

            $min = max($first_day, $n['day'] - $alen); // Never check days before the start
            $real_day = $n['day'] - $regday; // Offset the key appropriately for drawdata()
            $xdata[$real_day] = 0;

            for ($i = $n['day']; $i > $min; $i--) {
                $xdata[$real_day] += $total[$i];
            }
            $xdata[$real_day] = $xdata[$real_day] / $alen / SCALE_Y;
        }

        return $xdata;
    }

    // Draw progression of user's postcount
    function drawdata($p, $color)
    {
        global $days, $img;
        $oldy = IMAGE_Y; // We start from the bottom
        for ($i = 0; $i < $days; $i++) {
            if (!isset($p[$i])) {
                $y = $oldy;
            } else {
                $y = IMAGE_Y - $p[$i];
            }

            $x = $i * SCALE_X;
            imageline($img, $x, $oldy, $x + SCALE_X - 1, $y, $color);
            $oldy = $y;
        }
    }
