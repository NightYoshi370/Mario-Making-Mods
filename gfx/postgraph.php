<?php
    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['u']) {
    $id = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $id = $loguserid;
    }
}
    checknumeric($id);

    $forums = Query('SELECT id, title FROM {forums} ORDER BY id');
    $names = [];
    $posts = [];
while ($forum = Fetch($forums)) {
    $names[] = $forum['title'];
    $posts[] = FetchResult('SELECT COUNT(*) FROM {posts} LEFT JOIN {threads} on {posts}.thread = {threads}.id WHERE {posts}.user = {0} and {threads}.forum = {1}', $id, $forum['id']);
}

    const IMG_X = 320;
    const IMG_Y = 200;

    $cx = 160;
    $cy = 90;
    $sx = 300;
    $sy = 170;
    $sz = 16;

$data_sum = array_sum($posts);
$angle = [];
for ($i = 0; $i <= count($posts); $i++) {
    $angle[$i] = (($posts[$i] / $data_sum) * 360);
    $angle_sum[$i] = array_sum($angle);
}

    $img = imagecreate(IMG_X, IMG_Y);
    $background = imagecolorallocate($img, 255, 255, 255);

for ($i = 0; $i <= count($posts); $i++) {
    $red = rand(100, 255);
    $green = rand(100, 255);
    $blue = rand(100, 255);
    $colors[$i] = imagecolorallocate($img, $red, $green, $blue);
    $colord[$i] = imagecolorallocate($img, ($red / 1.5), ($green / 1.5), ($blue / 1.5));
}

for ($z = 1; $z <= $sz; $z++) {
    imagefilledarc($img, $cx, ($cy + $sz) - $z, $sx, $sy, 0, $angle_sum[0], $colord[0], IMG_ARC_EDGED);
    for ($i = 1; $i <= count($posts); $i++) {
        imagefilledarc($img, $cx, ($cy + $sz) - $z, $sx, $sy, $angle_sum[$i - 1], $angle_sum[$i], $colord[$i], IMG_ARC_NOFILL);
    }
}

imagefilledarc($img, $cx, $cy, $sx, $sy, 0, $angle_sum[0], $colors[0], IMG_ARC_PIE);
for ($i = 1; $i <= count($posts); $i++) {
    imagefilledarc($img, $cx, $cy, $sx, $sy, $angle_sum[$i - 1], $angle_sum[$i], $colors[$i], IMG_ARC_PIE);
}

    header('Content-type: image/png');
    imagepng($img);
    imagedestroy($img);
