<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

    const SCALE_X = 1; // 1
    const SCALE_Y = 20; // 1px to 20 posts
    const SECTOR_H = 50; // Height of a horizontal sector

    // Legend box with usernames (easily configurable just in case)
    const BOX_X = 60;
    const BOX_Y = 10;
    const BOX_W = 125;

    const USER_COUNT = 10; // Number of users to show

    $regdate = fetchResult('SELECT MIN(`regdate`) FROM {users} WHERE regdate > 0');
    $regday = floor($regdate / 86400);
    $regdate_ts = $regday * 86400; // Remove hour/min/sec info

    $days = floor((time() - $regdate_ts) / 86400); // Days the board has been opened

    $max = ceil((fetchResult('SELECT MAX(`posts`) FROM {users}') + 1) / 50) * 50;
    define('IMAGE_Y', $max / SCALE_Y);
    define('IMAGE_X', $days * SCALE_X);

    $img = imagecreatetruecolor(IMAGE_X, IMAGE_Y);
    $c['bg'] = imagecolorallocate($img, 0, 0, 0);
    $c['bg1'] = imagecolorallocate($img, 0, 0, 60);
    $c['bg2'] = imagecolorallocate($img, 0, 0, 80);
    $c['bg3'] = imagecolorallocate($img, 40, 40, 100);
    $c['mk1'] = imagecolorallocate($img, 60, 60, 130);
    $c['mk2'] = imagecolorallocate($img, 80, 80, 150);

for ($i = 0; $i < $days; $i++) {
    $md = date('m-d', $regdate_ts + $i * 86400);
    if ($md == '01-01') {
        $num = 3;
    } else {
        $num = substr($md, 0, 2) % 2 + 1;
    } // Alternate between months

    imagefilledrectangle($img, $i * SCALE_X, IMAGE_Y, ($i + 1) * SCALE_X - 2, 0, $c['bg'.$num]);
}

    // Postcount indicator for each sector; with separator lines
    $sect_x2 = SECTOR_H * 2; // yeah
for ($y = IMAGE_Y - SECTOR_H; $y >= 0; $y -= SECTOR_H) {
    $color = ($y % $sect_x2) ? $c['mk1'] : $c['mk2']; // Start from mk1 and loop back and forth for each limit
    $posts = (IMAGE_Y - $y) * SCALE_Y;

    imageline($img, 0, $y, IMAGE_X, $y, $color);
    imagestring($img, 3, 3, $y + 1, $posts, $c['bg']);
    imagestring($img, 3, 2, $y, $posts, $color);
}

    $users = [];
    $userq = Query('SELECT id, name, displayname FROM {users} ORDER BY posts DESC LIMIT 0, 10');
while ($user = fetch($userq)) {
    $users[$user['id']] = ['name' => (!empty($user['displayname']) ? $user['displayname'] : $user['name']), 'color' => imagecolorallocate($img, rand(100, 255), rand(100, 255), rand(100, 255))];
}

    $z = count($users);
    const NAME_HEIGHT = 12;
    // Draw the legend background box
    imagerectangle($img, BOX_X + 1, BOX_Y + 1, BOX_X + BOX_W + 1, BOX_Y + 5 + $z * NAME_HEIGHT, $c['bg']);  // Shadow
    imagefilledrectangle($img, BOX_X, BOX_Y, BOX_X + BOX_W, BOX_Y + 4 + $z * NAME_HEIGHT, $c['bg2']); // Background
    imagerectangle($img, BOX_X, BOX_Y, BOX_X + BOX_W, BOX_Y + 4 + $z * NAME_HEIGHT, $c['mk2']); // Border

    $z = 0;
    $data = getdata(array_keys($users));
foreach ($users as $uid => $userx) {
    // Draw the post total as a line
    drawdata($data[$uid], $userx['color']);
    // 10px Dash next to the name...
    imageline($img, BOX_X + 6, BOX_Y + 9 + $z * NAME_HEIGHT, BOX_X + 6 + 10, BOX_Y + 9 + $z * NAME_HEIGHT, $c['bg']);
    imageline($img, BOX_X + 5, BOX_Y + 8 + $z * NAME_HEIGHT, BOX_X + 5 + 10, BOX_Y + 8 + $z * NAME_HEIGHT, $userx['color']);
    // And the name proper...
    imagestring($img, 2, BOX_X + 21, BOX_Y + 2 + $z * NAME_HEIGHT, $userx['name'], $c['bg']);
    imagestring($img, 2, BOX_X + 20, BOX_Y + 1 + $z * NAME_HEIGHT, $userx['name'], $userx['color']);
    $z++;
}

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);

// Draw progression of user's postcount
function drawdata($p, $color)
{
    global $days, $img;
    $oldy = IMAGE_Y; // We start from the bottom
    for ($i = 0; $i < $days; $i++) {
        if (!isset($p[$i])) {
            $y = $oldy;
        } else {
            $y = IMAGE_Y - $p[$i];
        }

        $x = $i * SCALE_X;
        imageline($img, $x, $oldy, $x + SCALE_X - 1, $y, $color);
        $oldy = $y;
    }
}

function getdata($users)
{
    global $regday;

    // Initialize user total
    $ucount = count($users);
    $total = [];
    for ($i = 0; $i < $ucount; $i++) {
        $total[$users[$i]] = 0;
    }

    $postdays = Query(
        '
		SELECT user, FLOOR(date / 86400) day, count(*) c
		FROM {posts}
		WHERE user IN ({0c})
		GROUP BY user, day 
		ORDER BY user, day', $users
    );

    $resp = [];
    // For every user, return the total
    // Since it's ordered already by day we can safely add to the total without issues
    while ($x = fetch($postdays)) {
        $total[$x['user']] += $x['c'];
        $resp[$x['user']][$x['day'] - $regday] = $total[$x['user']] / SCALE_Y;
    }

    return $resp;
}
