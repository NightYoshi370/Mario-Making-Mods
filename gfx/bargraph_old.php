<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

    $img = imagecreatetruecolor(690, 20);

    $n = $_GET['n'];
    $b = $_GET['b'];
    $g = $_GET['g'];
    $r = $_GET['r'];
    $z = $_GET['z'];
if ($n == 0) {
    $n = 1;
}

    imagealphablending($img, false);
    imagesavealpha($img, true);

    const FONT = 'verdana.ttf';

    $cblank = imagecolorallocatealpha($img, 0, 0, 0, 127);
    $cuhl = imagecolorallocate($img, cap($r + 80), cap($g + 80), cap($b + 80));
    $cstd = imagecolorallocate($img, $r, $g, $b);
    $cshadow = imagecolorallocate($img, cap($r - 80), cap($g - 80), cap($b - 80));
    $calpha = imagecolorallocatealpha($img, 0, 0, 0, 110);
    $cwhite = imagecolorallocate($img, 255, 255, 255);

    imagefilledrectangle($img, 0, 0, 690, 20, $cblank);

    imagefilledrectangle($img, 0, 0, 600 * $z / $n, 20, $cstd);
    imagefilledrectangle($img, 0, 2, 600 * $z / $n, 3, $cuhl);
    imagefilledrectangle($img, 0, 14, 600 * $z / $n, 20, $cshadow);

    $str = '';
    $str = sprintf("%.1f%% ($z)", 100 * $z / $n);

    imagettftext($img, 10, 0, floor(600 * $z / $n) + 3, 14, $cwhite, FONT, $str);

    imagesavealpha($img, false);
    imagealphablending($img, true);
for ($i = 0; $i < 600; $i += 10) {
    imagefilledrectangle($img, $i + 0, 0, $i + 4, 20, $calpha);
}
    imagealphablending($img, false);
    imagesavealpha($img, true);

    // Header('Content-type: image/png');
    // ImagePNG($img);
    // ImageDestroy($img);

    error_reporting(E_ALL);

function cap($num)
{
    return changewithinrange($num, 255, 0);
}
