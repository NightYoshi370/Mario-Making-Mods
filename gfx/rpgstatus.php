<?php
    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['u']) {
    $u = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $u = $loguserid;
    }
}
    checknumeric($u);

    $user = fetch(
        Query(
            'SELECT
							u.name, u.posts, u.regdate, u.displayname,
							r.* 
						FROM {users} u 
						LEFT JOIN {usersrpg} r ON r.id=u.id
						WHERE u.id = {0}', $u
        )
    );

    $p = $user['posts'];
    $d = (time() - $user['regdate']) / 86400;

    $it = $_GET['it'];
    checknumeric($it);

    $eqitemsR = Query('SELECT * FROM {usersrpg} WHERE id={0}', $user['id']);
    if (NumRows($eqitemsR)) {
        $eq = fetch($eqitemsR);
        $eqitems = Query('SELECT * FROM {items} WHERE id IN ({0c}) OR id = {1}', $eq, $it);

        while ($item = fetch($eqitems)) {
            $items[$item['id']] = $item;
        }
    }

    $ct = $_GET['ct'];
    if ($ct) {
        $GPdif = floor($items[$user['eq'.$ct]]['coins'] * 0.6) - $items[$it]['coins'];
        $user['eq'.$ct] = $it;
    }

    $st = getstats($user, $items);
    $st['GP'] += $GPdif;
    if ($st['lvl'] > 0) {
        $pct = 1 - calcexpleft($st['exp']) / lvlexp($st['lvl']);
    }

    if ($_GET['font'] && file_exists(__DIR__.'/lib/font/'.$_GET['font'].'.png')) {
        $pickfont = $_GET['font'];
    } else {
        $pickfont = 'acmlmboard';
    }

    $img = imagecreate(256, 200);
    if ($_GET['color'] && file_exists(__DIR__.'/lib/themes/'.$_GET['color'].'.php')) {
        include __DIR__.'/lib/themes/'.$_GET['color'].'.php';
    } else {
        include __DIR__.'/lib/themes/default.php';
    }

    if ($pickfont == 'slim') {
        box(0, 0, 1 + strlen(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name'])) * 6, 24);
        twrite($c['font']['W'], 8, 8, 0, mb_convert_encoding(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']), 'ISO-8859-1'));
    } else {
        box(0, 0, 16 + strlen(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name'])) * 8, 24);
        twrite($c['font']['W'], 8, 8, 0, mb_convert_encoding(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']), 'ISO-8859-1'));
    }

    box(0, 30, 256, 32);
    twrite($c['font']['B'], 8, 38, 0, 'HP:');
    twrite($c['font']['R'], 24, 38, 7, $st['HP'] - $user['hp']);
    twrite($c['font']['B'], 85, 38, 0, '/');
    twrite($c['font']['Y'], 88, 38, 5, $st['HP']);
    twrite($c['font']['B'], 8, 46, 0, 'MP:');
    twrite($c['font']['R'], 24, 46, 7, $st['MP'] - $st['mp']);
    twrite($c['font']['B'], 85, 46, 0, '/');
    twrite($c['font']['Y'], 88, 46, 5, $st['MP']);

    box(0, 70, 256, 76);
    for ($i = 2; $i < 9; $i++) {
        twrite($c['font']['B'], 8, (8 + $i) * 8, 0, $stat[$i].':');
        twrite($c['font']['Y'], 32, (8 + $i) * 8, 6, $st[$stat[$i]]);
    }

    box(0, 152, 144, 48);
    twrite($c['font']['B'], 8, 160, 0, 'Level');
    twrite($c['font']['Y'], 104, 160, 4, $st['lvl']);
    twrite($c['font']['B'], 8, 176, 0, 'EXP:');
    twrite($c['font']['Y'], 64, 176, 9, $st['exp']);
    twrite($c['font']['B'], 8, 184, 0, 'Next:');
    twrite($c['font']['Y'], 64, 184, 9, calcexpleft($st['exp']));

    box(152, 152, 104, 48);
    twrite($c['font']['B'], 160, 160, 0, 'Coins:');
    twrite($c['font']['Y'], 160, 176, 0, chr(0));
    twrite($c['font']['Y'], 168, 176, 10, $st['GP']);

    $sc[1] = 1;
    $sc[2] = 5;
    $sc[3] = 25;
    $sc[4] = 100;
    $sc[5] = 250;
    $sc[6] = 500;
    $sc[7] = 1000;
    $sc[8] = 2500;
    $sc[9] = 5000;
    $sc[10] = 10000;
    $sc[11] = 100000;
    $sc[12] = 1000000;
    $sc[13] = 10000000;
    $sc[14] = 100000000;
    $sc[15] = 1000000000;

    RPGbars();

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);

    function RPGbars()
    {
        global $st, $img, $c, $sc, $pct, $stat, $user;

        for ($s = 1; @(max($st['HP'], $st['MP']) / $sc[$s]) > 113; $s++) {
        }
        if (!$sc[$s]) {
            $sc[$s] = 1;
        }

        if ($st['HP'] > 0) {
            imagefilledrectangle($img, 137, 39, 136 + $st['HP'] / $sc[$s], 45, $c['bxb0']);
            imagefilledrectangle($img, 136, 38, 135 + $st['HP'] / $sc[$s], 44, $c['bar'][$s]);
            if ($user['hp'] > 0) {
                $dmg = max($st['HP'] - $user['hp'], 0) / $sc[$s];
                $ctemp = imagecolorsforindex($img, $c['bar'][$s]);
                $df = 0.6;
                imagefilledrectangle($img, 135 + $st['HP'] / $sc[$s], 38, 135 + $dmg, 44, imagecolorallocate($img, $ctemp['red'] * $df, $ctemp['green'] * $df, $ctemp['blue'] * $df));
            }
        }

        if ($st['MP'] > 0) {
            imagefilledrectangle($img, 137, 47, 136 + $st['MP'] / $sc[$s], 53, $c['bxb0']);
            imagefilledrectangle($img, 136, 46, 135 + $st['MP'] / $sc[$s], 52, $c['bar'][$s]);
        }

        for ($i = 2; $i < 9; $i++) {
            $st2[$i] = $st[$stat[$i]];
        }
        for ($s = 1; @(max($st2) / $sc[$s]) > 161; $s++) {
        }
        if (!$sc[$s]) {
            $sc[$s] = 1;
        }
        for ($i = 2; $i < 9; $i++) {
            if (floor($st[$stat[$i]] / $sc[$s]) > 0) {
                imagefilledrectangle($img, 89, 65 + $i * 8, 88 + $st[$stat[$i]] / $sc[$s], 71 + $i * 8, $c['bxb0']);
                imagefilledrectangle($img, 88, 64 + $i * 8, 87 + $st[$stat[$i]] / $sc[$s], 70 + $i * 8, $c['bar'][$s]);
            }
        }

        $e2 = 16 * 8;    // width of bar
        $e1 = $e2 * $pct;
        $y = 168 + 1;
        imagefilledrectangle($img, 9, $y + 1, 8 + $e2, $y + 4, $c['bxb0']);
        imagefilledrectangle($img, 8, $y, 7 + $e2, $y + 3, $c['barE2']);
        imagefilledrectangle($img, 8, $y, 7 + $e1, $y + 3, $c['barE1']);
    }
