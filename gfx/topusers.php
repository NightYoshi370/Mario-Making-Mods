<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

    $t = $_GET['t'];
    $u = $_GET['u'];
if (!$n = $_GET['n']) {
    $n = 50;
}
if (!$s = $_GET['s']) {
    $s = 1;
}
    checknumeric($n);
    checknumeric($u);
    checknumeric($s);

if ($t == 'lv') {
    $val = 'pow('.sqlexpval().',2/7)*100';
}
if ($t == 'ppd') {
    $val = 'posts/('.time().'-regdate)*8640000';
} else {
    $val = 'posts';
}

if ($s < 0) {
    $u = -$s;
    $uval = Fetch(Query('SELECT {0} val FROM users WHERE id = {1}', $val, $u));
    $rank = Fetch(Query('SELECT COUNT(*) FROM users WHERE {0} > {1} AND id != {2}', $val, $uval, $u)) + 1;
    $s = floor($rank - ($n - 1) / 2);
    if ($s < 1) {
        $s = 1;
    }
}

    $users = Query(
        "SELECT id, name, displayname, $val val "
                   .'FROM users '
                   ."ORDER BY val DESC, (id='$u') DESC "
        .'LIMIT '.($s - 1).",$n"
    );

    $img = imagecreate(512, ($n + 2) * 8);
    $urlcolor = $_GET['color'];
    if ($urlcolor) {
        if (file_exists(__DIR__.'/lib/themes/'.$urlcolor.'.php')) {
            include __DIR__.'/lib/themes/'.$urlcolor.'.php';
        } else {
            include __DIR__.'/lib/themes/default.php';
        }
    } else {
        include __DIR__.'/lib/themes/default.php';
    }

    $urlfont = $_GET['font'];
    $pickfont = '';
    if ($urlfont && file_exists(__DIR__.'/lib/font/font'.$urlfont.'.png')) {
        $pickfont = $urlfont;
    }

    $c['hlit'] = imagecolorallocate($img, 47, 63, 191);
    box(0, 0, 64, $n + 2, 0);

    $sc[1] = 1;
    $sc[2] = 5;
    $sc[3] = 25;
    $sc[4] = 100;
    $sc[5] = 250;
    $sc[6] = 500;
    $sc[7] = 1000;
    $sc[8] = 2500;
    $sc[9] = 5000;
    $sc[10] = 10000;
    $sc[11] = 100000;
    $sc[12] = 1000000;
    $sc[13] = 10000000;
    $sc[14] = 100000000;
    $sc[15] = 1000000000;

    for ($i = $s; $user = fetch($users); $i++) {
        if ($user['val'] != $rval) {
            $rank = $i;
            $rval = $user['val'];
        }
        if ($i == $s) {
            $rank = FetchResult("SELECT COUNT(*) FROM {users} WHERE $val > $user[val] AND id != $user[id]") + 1;
            for ($sn = 1; ($user['val'] / $sc[$sn]) > 320; $sn++) {
            }
            $div = $sc[$sn];
            if (!$div) {
                $div = 1;
            }
        }
        $y = $i - $s + 1;
        if ($user['id'] == $u) {
            imagefilledrectangle($img, 8, $y * 8, 503, $y * 8 + 7, $c['hlit']);
            $fontu = $c['font']['Y'];
        } else {
            $fontu = $c['font']['B'];
        }
        twrite($c['font']['W'], 0, $y, 4, $rank, $pickfont);
        if ($pickfont == 2) {
            twrite($fontu, 5, $y, 0, substr(mb_convert_encoding(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']), 'ISO-8859-1'), 0, 16), $pickfont);
        } else {
            twrite($fontu, 5, $y, 0, substr(mb_convert_encoding(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']), 'ISO-8859-1'), 0, 12), $pickfont);
        }
        twrite($c['font']['Y'], 16, $y, 6, floor($user['val']), $pickfont);
        if (($sx = $user['val'] / $div) >= 1) {
            imagefilledrectangle($img, 185, $y * 8 + 1, 184 + $sx, $y * 8 + 7, $c['bxb0']);
            imagefilledrectangle($img, 184, $y * 8, 183 + $sx, $y * 8 + 6, $c['bar'][$sn]);
        }
    }

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);
