<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';
    require __DIR__.'/lib/ringbuf.php';

if ($_GET['u']) {
    $u = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $u = $loguserid;
    }
}
    checknumeric($u);

    $im = imagecreatetruecolor(1100, 340);

    imagealphablending($im, false);
    imagesavealpha($im, true);

    $cblank = imagecolorallocatealpha($im, 0, 0, 0, 127);
    $cwhite = imagecolorallocatealpha($im, 205, 205, 255, 80);
    $ch1 = imagecolorallocatealpha($im, 0, 0, 0, 100);
    $ch2 = imagecolorallocatealpha($im, 255, 255, 255, 120);
    $c1 = imagecolorallocatealpha($im, 50, 50, 255, 76);
    $c2 = imagecolorallocatealpha($im, 100, 100, 255, 0);
    $c3 = imagecolorallocatealpha($im, 100, 100, 255, 96);

    const FONT = 'lib/font/verdana.ttf';
    imagefilledrectangle($im, 0, 0, 1100, 340, $cblank);

    /* background */
    imagealphablending($im, true);
for ($i = 0; $i < 320; $i += 20) {
    imagefilledrectangle($im, 0, $i, 1100, $i + 9, $ch1);
    imagettftext($im, 7, 0, 1034, $i + 14, $cwhite, FONT, (310 - $i) / 5);
    imagefilledrectangle($im, 1024, $i + 10, 1028, $i + 10, $cwhite);
}
for ($i = 0; $i < 1100; $i += 128) {
    imagefilledrectangle($im, $i, 0, $i + 64, 320, $ch1);
}
    imagealphablending($im, false);
    imagefilledrectangle($im, 0, 320, 1100, 340, $ch2);

    $n = Fetch(Query('SELECT name, displayname FROM {users} WHERE id={0}', $u));
if (!empty($n['displayname'])) {
    $namedisplay = $n['displayname'];
} else {
    $namedisplay = $n['name'];
}

    imagettftext($im, 7, 0, 7, 30, $cwhite, FONT, "Activity stats for $namedisplay\n  bold: 8-day average\n  thin: daily postcount");

    $stats = Query(
        'SELECT
						FROM_UNIXTIME(p.date,"%y-%m-%d") date,
						(FLOOR(p.date/(24*60*60))) q,
						COUNT(*) c
					FROM {posts} p
					WHERE p.user={0}
					GROUP BY FLOOR(p.date/(24*60*60))
					UNION SELECT
						FROM_UNIXTIME(UNIX_TIMESTAMP(),"%y-%m-%d") date,
						(FLOOR(UNIX_TIMESTAMP()/(24*60*60))+1) q, 0 c', $u
    );

    $x = 0;
    $y = 0;
    $yold = 0;

    $abuf = new ringbuf();
    $abuf->size = 8;

    imagealphablending($im, true);

    $day = Fetch($stats);
    $yold = $day['c'];
    $q = $day['q'] - 1;

    do {
        $x++;
        $y = $day['c'];
        while ($q + 1 != $day['q']) {
            $q++;
            $x++;
            $abuf->push(0);

            imageline($im, $x, 320 - $abuf->get() * 5, $x, 320, $c1);
            imageline($im, $x - 1, 320 - $yold * 5 - 1, $x - 0, 320 - $abuf->get() * 5 - 1, $c3);
            imageline($im, $x - 1, 320 - $yold * 5 + 1, $x - 0, 320 - $abuf->get() * 5 + 1, $c3);
            imageline($im, $x - 1, 320 - $yold * 5, $x - 0, 320 - $abuf->get() * 5, $c2);
            if (!(($x - 2) % 64)) {
                imagettftext($im, 7, 0, $x, 10, $cwhite, FONT, $day['date']);
            }
            $yold = $abuf->get();
        }
        $q = $day['q'];
        $abuf->push($y);

        imageline($im, $x, 320 - $abuf->get() * 5, $x, 320 - $y * 5, $c1);

        imageline($im, $x - 1, 320 - $yold * 5 - 1, $x - 0, 320 - $abuf->get() * 5 - 1, $c3);
        imageline($im, $x - 1, 320 - $yold * 5 + 1, $x - 0, 320 - $abuf->get() * 5 + 1, $c3);
        imageline($im, $x - 1, 320 - $yold * 5, $x - 0, 320 - $abuf->get() * 5, $c2);

        if (!(($x - 1) % 64)) {
            imagettftext($im, 7, 0, $x, 10, $cwhite, FONT, $day['date']);
        }

        $users = $day['users'];
        $posts = $day['posts'];
        $threads = $day['threads'];
        $views = $day['views'];
        $yold = $abuf->get();
    } while ($day = fetch($stats));

    imagealphablending($im, false);
    imagesavealpha($im, true);

    header('Content-type: image/png');
    imagepng($im);
