<?php
    imagesavealpha($img, true);
    imagealphablending($img, false);
    $c['bg'] = imagecolorallocatealpha($img, 40, 40, 90, 127);
    $c['bxb0'] = imagecolorallocate($img, 0, 0, 0);
    $c['bxb1'] = imagecolorallocate($img, 200, 180, 225);
    $c['bxb2'] = imagecolorallocate($img, 160, 130, 190);
    $c['bxb3'] = imagecolorallocate($img, 90, 110, 130);

for ($i = 0; $i < 100; $i++) {
    $c[$i] = imagecolorallocate($img, 15 + $i / 1.5, 8, 20 + $i);
}

    $c['barE1'] = imagecolorallocate($img, 120, 150, 180);
    $c['barE2'] = imagecolorallocate($img, 30, 60, 90);
    $c['bar'][1] = imagecolorallocate($img, 215, 91, 129);
    $c['bar'][2] = imagecolorallocate($img, 255, 136, 154);
    $c['bar'][3] = imagecolorallocate($img, 255, 139, 89);
    $c['bar'][4] = imagecolorallocate($img, 255, 251, 89);
    $c['bar'][5] = imagecolorallocate($img, 89, 255, 139);
    $c['bar'][6] = imagecolorallocate($img, 89, 213, 255);
    $c['bar'][7] = imagecolorallocate($img, 196, 33, 33);
    $c['bar'][8] = imagecolorallocate($img, 196, 66, 196);
    $c['bar'][9] = imagecolorallocate($img, 100, 0, 155);
    $c['bar'][10] = imagecolorallocate($img, 88, 0, 121);
    $c['bar'][11] = imagecolorallocate($img, 0, 174, 215);
    $c['bar'][12] = imagecolorallocate($img, 0, 99, 151);
    $c['bar'][13] = imagecolorallocate($img, 175, 175, 175);
    $c['bar'][14] = imagecolorallocate($img, 222, 222, 222);
    $c['bar'][15] = imagecolorallocate($img, 255, 255, 255);

    $c['font']['Y'] = fontc(255, 250, 240, 255, 240, 80, 0, 0, 0, $pickfont);
    $c['font']['R'] = fontc(255, 230, 220, 240, 160, 150, 0, 0, 0, $pickfont);
    $c['font']['G'] = fontc(190, 255, 190, 60, 220, 60, 0, 0, 0, $pickfont);
    $c['font']['B'] = fontc(160, 240, 255, 120, 190, 240, 0, 0, 0, $pickfont);
    $c['font']['W'] = fontc(255, 255, 255, 210, 210, 210, 0, 0, 0, $pickfont);
