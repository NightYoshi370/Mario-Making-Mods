<?php
    $c['bg'] = imagecolorallocate($img, 40, 40, 90);
    $c['bxb0'] = imagecolorallocate($img, 0, 0, 0);
    $c['bxb1'] = imagecolorallocate($img, 200, 170, 140);
    $c['bxb2'] = imagecolorallocate($img, 155, 130, 105);
    $c['bxb3'] = imagecolorallocate($img, 110, 90, 70);

for ($i = 0; $i < 100; $i++) {
    $c[$i] = imagecolorallocate($img, 10, 16, 60 + $i / 2);
}

    $c['barE1'] = imagecolorallocate($img, 120, 150, 180);
    $c['barE2'] = imagecolorallocate($img, 30, 60, 90);
    $c['bar'][1] = imagecolorallocate($img, 215, 91, 129);
    $c['bar'][2] = imagecolorallocate($img, 90, 22, 43);
    $c['bar'][3] = imagecolorallocate($img, 255, 136, 154);
    $c['bar'][4] = imagecolorallocate($img, 151, 0, 38);
    $c['bar'][5] = imagecolorallocate($img, 255, 139, 89);
    $c['bar'][6] = imagecolorallocate($img, 125, 37, 0);
    $c['bar'][7] = imagecolorallocate($img, 255, 251, 89);
    $c['bar'][8] = imagecolorallocate($img, 83, 81, 0);
    $c['bar'][9] = imagecolorallocate($img, 89, 255, 139);
    $c['bar'][10] = imagecolorallocate($img, 0, 100, 30);
    $c['bar'][11] = imagecolorallocate($img, 89, 213, 255);
    $c['bar'][12] = imagecolorallocate($img, 0, 66, 93);
    $c['bar'][13] = imagecolorallocate($img, 196, 33, 33);
    $c['bar'][14] = imagecolorallocate($img, 70, 12, 12);
    imagecolortransparent($img, 0);

    $c['font']['Y'] = fontc(255, 250, 240, 255, 240, 80, 0, 0, 0);
    $c['font']['R'] = fontc(255, 230, 220, 240, 160, 150, 0, 0, 0);
    $c['font']['G'] = fontc(190, 255, 190, 60, 220, 60, 0, 0, 0);
    $c['font']['B'] = fontc(160, 240, 255, 120, 190, 240, 0, 0, 0);
    $c['font']['W'] = fontc(255, 255, 255, 210, 210, 210, 0, 0, 0);
