<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['u']) {
    $id = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $id = $loguserid;
    }
}
    checknumeric($id);

    $poster = Fetch(Query('SELECT * FROM {users} WHERE id={0}', $id));

if (!empty($poster['picture'])) {
    $filename = str_replace('$root/', '', $poster['picture']);
    $image_s = imagecreatefromstring(file_get_contents(__DIR__.'/../data/'.$filename));
} else {
    switch ($profile['sex']) {
    case 0:
        $image_s = imagecreatefromstring(file_get_contents(__DIR__.'/lib/bg/male.png'));
        break;
    case 1:
        $image_s = imagecreatefromstring(file_get_contents(__DIR__.'/lib/bg/female.png'));
        break;
    default:
        $image_s = imagecreatefromstring(file_get_contents(__DIR__.'/lib/bg/unspecified.png'));
    }
}

if (imagesx($image_s) < 100) {
    $pfp_height = 100;
    $pfp_width = 100;

    $img_height = 250;
    $img_width = 150;
} else {
    $pfp_height = 200;
    $pfp_width = 200;

    $img_height = 350;
    $img_width = 250;
}

    const FONT = '/gfx/lib/font/verdana.ttf';

    $img = imagecreatetruecolor($img_width, $img_height);

if ($_GET['color'] && file_exists(__DIR__.'/lib/themes/'.$_GET['color'].'.php')) {
    include __DIR__.'/lib/themes/'.$_GET['color'].'.php';
} else {
    include __DIR__.'/lib/themes/default.php';
}

    imagesavealpha($img, true);
    imagealphablending($img, false);

    $cwhite = imagecolorallocate($img, 255, 255, 255);

    imagecopyresampled($img, $image_s, 25, 10, 0, 0, $pfp_width, $pfp_height, imagesx($image_s), imagesy($image_s));

    //create masking
    $mask = imagecreatetruecolor($img_width, $img_height);

    $transparent = imagecolorallocate($mask, 0, 0, 0);
    imagecolortransparent($mask, $transparent);
    box(0, 0, $img_width, $img_height);
    imagefilledellipse($mask, $pfp_width / 2 + 25, $pfp_height / 2 + 10, $pfp_width, $pfp_height, $transparent);

if (imagesx($image_s) < 100) {
    box(10, 120, $img_width - 20, 120);
    imagettftext($mask, 12, 0, 20, 145, $cwhite, FONT, htmlspecialchars($poster['displayname'] ? $poster['displayname'] : $poster['name']));
} else {
    box(10, 220, $img_width - 20, 120);
    imagettftext($mask, 17, 0, 20, 245, $cwhite, FONT, htmlspecialchars($poster['displayname'] ? $poster['displayname'] : $poster['name']));
    imagettftext($mask, 12, 0, 20, 265, $cwhite, FONT, substr(htmlspecialchars($poster['title']), 0, 20));
    imagettftext($mask, 12, 0, 20, 280, $cwhite, FONT, substr(htmlspecialchars($poster['title']), 20, 30));
}

    imagecopymerge($img, $mask, 0, 0, 0, 0, $img_width, $img_height, 100);
    imagefill($img, 0, 0, 0);

    //output, save and free memory
    header('Content-type: image/png');
    imagepng($img);
    imagedestroy($img);
