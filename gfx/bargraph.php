<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['font'] && file_exists(__DIR__.'/lib/font/'.$_GET['font'].'.png')) {
    $pickfont = $_GET['font'];
} else {
    $pickfont = 'acmlmboard';
}

    $img = imagecreatetruecolor(690, 20);
    $n = $_GET['n'];
    $b = $_GET['b'];
    $g = $_GET['g'];
    $r = $_GET['r'];
    $z = $_GET['z'];
if ($n == 0) {
    $n = 1;
}

    imagealphablending($img, false);
    imagesavealpha($img, true);

    $cblank = imagecolorallocatealpha($img, 0, 0, 0, 127);
    $chl = imagecolorallocate($img, cap($r + 50), cap($g + 50), cap($b + 50));
    $cstd = imagecolorallocate($img, $r, $g, $b);
    $calpha = imagecolorallocatealpha($img, 0, 0, 0, 110);
    $calpha2 = imagecolorallocatealpha($img, 255, 255, 255, 105);

    imagefilledrectangle($img, 0, 0, 690, 20, $cblank);

    imagefilledrectangle($img, 0, 5, 600 * $z / $n, 15, $chl);
    imagefilledrectangle($img, 1, 6, 600 * $z / $n - 1, 14, $cstd);

    $str = '';
    $str = sprintf("%.1f%% ($z)", 100 * $z / $n);

    $fontW = fontc(255, 255, 255, 210, 210, 210, 0, 0, 0);
    twrite($fontW, floor(600 * $z / $n) + 5, 7, 0, $str);

    imagesavealpha($img, false);
    imagealphablending($img, true);
for ($i = 0; $i <= 600; $i += 10) {
    imagefilledrectangle($img, $i + 0, 0, $i + 4, 20, $calpha);
    imagefilledrectangle($img, $i + 0, $i % 60 ? 18 : ($i % 300 ? 14 : 0), $i + 0, 20, $calpha2);
}
    imagealphablending($img, false);
    imagesavealpha($img, true);

    header('Content-type:image/png');
    imagepng($img);

function cap($n)
{
    return $n > 255 ? 255 : ($n < 0 ? 0 : $n);
}
