<?php

define('BLARG', 1);
require __DIR__.'/../lib/common.php';
header('Content-type: text/json');

$tid = (int) $_GET['tid'];
if (!$_GET['tid']) die();
$thread = $db->row('threads', ['id' => $_GET['tid']]);
if (!$thread) {
    die(__('Unknown thread ID.'));
}

$forum = $db->row('forums', ['id' => $thread['forum']]);
if (!$forum) {
    die(__('Unknown forum ID.'));
}
if (!HasPermission('forum.viewforum', $thread['forum'])) {
    die(__('You may not access this forum.'));
}

$fid = $thread['forum'];

$ppp = $loguser['threadsperpage'];
    if (!$ppp) {
        $ppp = 50;
    }
    if (isset($_GET['from'])) {
        $from = $_GET['from'];
    } else {
        $from = 0;
    }

$tablerows = [];
while ($dbrow = Fetch(Query("SELECT * FROM {quickthread} WHERE tid = {0u} ORDER BY time DESC LIMIT {1u}, {2u}", $thread['id'], $from, $ppp))) {
	$row = [];

	$user = $db->row('users', ['id' =>$dbrow['user']]);
	$userLink = UserLink($user);

	if ($user['picture']) {
		$avatar = '<img src="'.str_replace('$root/', substr(DATA_URL, 5), $user['picture']).'" class="media-object" style="max-width: 60px; width:60px;">';
	} else {
		$avatar = '<div style="width:60px;"></div>';
	}

	$row['avatar'] = $avatar;
	$row['userLink'] = $userLink;
	$row['time'] = relativedate($dbrow['time']);
	$row['text'] = CleanUpPost($dbrow['text']);

	$tablerows[] = $row;
}

echo json_encode($tablerows);