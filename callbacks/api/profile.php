<?php

define('BLARG', '1');

require __DIR__.'/../lib/common.php';

if ($_GET['id']) {
    $u = (int) $_GET['id'];
} elseif ($_GET['name']) {
    $name = $_GET['name'];
} elseif ($_GET['discord']) {
    $id = (int) $_GET['discord'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $u = $loguserid;
    }
}

$nostrip = false;
if ($_GET['strip']) {
    $nostrip = true;
}

checknumeric($u);

if ($_GET['name']) {
    $user = fetch(
        Query(
            '	SELECT u.*, r.* 
								FROM {users} u 
								LEFT JOIN {usersrpg} r ON r.id=u.id 
								WHERE u.displayname = {0} OR u.name = {0} LIMIT 1', $name
        )
    );
} elseif ($_GET['discord']) {
    $user = fetch(
        Query(
            '	SELECT u.*, r.* 
								FROM {users} u 
								LEFT JOIN {usersrpg} r ON r.id=u.id 
								WHERE u.dID = {0} LIMIT 1', $id
        )
    );
} else {
    $user = Fetch(
        Query(
            '	SELECT u.*, r.* 
								FROM {users} u 
								LEFT JOIN {usersrpg} r ON r.id=u.id 
								WHERE u.id = {0} LIMIT 1', $u
        )
    );
}

    $p = $user['posts'];
    $d = (time() - $user['regdate']) / 86400;

    $eq = Fetch(Query('SELECT * FROM {usersrpg} WHERE id={0}', $user['id']));
    $shops =    Query('SELECT * FROM {itemcateg} ORDER BY corder');
    $eqitems =  Query('SELECT * FROM {items} WHERE id IN ({0c})', $eq);

while ($item = fetch($eqitems)) {
    $items[$item['id']] = $item;
}

    $equipitems = [];
while ($shop = fetch($shops)) {
    $itemdesc = [];

    $id = $eq['eq'.$shop['id']];

    $itemdesc['name'] = $shop['name'];
    $itemdesc['desc'] = '<a href="'.getServerDomainNoSlash().actionLink('shop', $id, 'action=desc').'">'.$items[$id]['name'].'</a>';
    $equipitems[] = $itemdesc;
}

    $st = getstats($user, $items);

header('Content-type: text/json');

$ugroup = $usergroups[$user['primarygroup']];
$usgroups = [];

$res = Query('SELECT groupid FROM {secondarygroups} WHERE userid={0}', $user['id']);
while ($sg = Fetch($res)) {
    $usgroups[] = $usergroups[$sg['groupid']];
}

$glist = htmlspecialchars($ugroup['name']);
foreach ($usgroups as $sgroup) {
    if ($sgroup['display'] > -1) {
        $glist .= ', '.htmlspecialchars($sgroup['name']);
    }
}

$daysKnown = ($db->time() - $user['regdate']) / 86400;
if (!$daysKnown) {
    $daysKnown = 1;
}

$posts = FetchResult('SELECT COUNT(*) FROM {posts} WHERE user={0}', $user['id']);
$threads = FetchResult('SELECT COUNT(*) FROM {threads} WHERE user={0}', $user['id']);
$averagePosts = sprintf('%1.02f', $user['posts'] / $daysKnown);
$averageThreads = sprintf('%1.02f', $threads / $daysKnown);

$totalposts = format('{0} ({1} per day)', $posts, $averagePosts);
$Totalthreads = format('{0} ({1} per day)', $threads, $averageThreads);
$RegisterDate = format('{0} ({1} ago)', formatdate($user['regdate']), TimeUnits($daysKnown * 86400));

$lastPost = Fetch(
    Query(
        '
	SELECT
		p.id as pid, p.date as date,
		{threads}.title AS ttit, {threads}.id AS tid,
		{forums}.title AS ftit, {forums}.id AS fid
	FROM {posts} p
		LEFT JOIN {users} u on u.id = p.user
		LEFT JOIN {threads} on {threads}.id = p.thread
		LEFT JOIN {forums} on {threads}.forum = {forums}.id
	WHERE p.user={0}
	ORDER BY p.date DESC
	LIMIT 0, 1', $user['id']
    )
);

if ($lastPost) {
    $thread = [];
    $thread['title'] = $lastPost['ttit'];
    $thread['id'] = $lastPost['tid'];
    $thread['forum'] = $lastPost['fid'];
    $tags = ParseThreadTags($thread['title']);

    if (!HasPermission('forum.viewforum', $lastPost['fid'])) {
        $place = __('a restricted forum');
    } else {
        $ispublic = HasPermission('forum.viewforum', $lastPost['fid'], true);
        $pid = $lastPost['pid'];
        $place = actionLinkTag($tags[0], 'post', $pid).' ('.actionLinkTag($lastPost['ftit'], 'forum', $lastPost['fid'], '', $ispublic ? $lastPost['ftit'] : '').')';
    }
    $totalposts .= format(' (last post {0} ago', TimeUnits(time() - $lastPost['date'])).__(' in').' '.$place.')';
}

$LastSeen = format('{0} ({1} ago)', formatdate($user['lastactivity']), TimeUnits(time() - $user['lastactivity'])).'<br> at: <a>'.$user['lasturl'].'</a>';

$infofile = 'themes/'.$user['theme'].'/themeinfo.txt';

if (file_exists($infofile)) {
    $themeinfo = file_get_contents($infofile);
    $themeinfo = explode("\n", $themeinfo, 2);

    $themename = htmlspecialchars(trim($themeinfo[0]));
    $themeauthor = htmlspecialchars(trim($themeinfo[1]));
    $themeauthor = parseBBCode($themeauthor);
} else {
    $themename = htmlspecialchars($user['theme']);
    $themeauthor = '';
}

$data = [];

$data['uid'] = $user['id'];
$data['name'] = htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']).($user['displayname'] ? ' ('.htmlspecialchars($user['name']).')' : '');
$data['title'] = preg_replace('@<br.*?>\s*(\S)@i', ' &bull; $1', strip_tags(CleanUpPost($user['title'], '', true), '<b><strong><i><em><span><s><del><img><a><br><br/><small>'));
$data['groups'] = $glist;
$data['Rank'] = $currentRank;
$data['To next rank'] = $toNextRank;
$data['totalposts'] = $totalposts;
$data['Totalthreads'] = $Totalthreads;
$data['LastSeen'] = $LastSeen;
if ($user['picture']) {
    $data['avatar'] = getServerDomainNoSlash().str_replace('$root/', substr(DATA_URL, 5), $user['picture']);
}
if (HasPermission('admin.viewips')) {
    $data['useragent'] = htmlspecialchars($user['lastknownbrowser']);
    $data['LastIP'] = formatIP($user['lastip']);
}
$data['theme'] = $themename.' &middot; '.$themeauthor;
$data['realname'] = htmlspecialchars($user['realname']);
$data['location'] = htmlspecialchars($user['location']);
$data['birthday'] = formatBirthday($user['birthday']);
if ($nostrip) {
    $data['Bio'] = strip_html_tags(CleanUpPost($user['bio']));
} else {
    $data['Bio'] = CleanUpPost($user['bio']);
}
$data['HP'] = $st['HP'];
$data['MP'] = $st['MP'];
$data['level'] = $st['lvl'];
$data['exp'] = $st['exp'];
$data['next'] = calcexpleft($st['exp']);

for ($i = 2; $i < 9; $i++) {
    $data[$stat[$i]] = $st[$stat[$i]];
}

foreach ($equipitems as $equipitem) {
    $data[$equipitem['name']] = $equipitem['desc'];
}

echo json_encode($data);
