<?php

define('BLARG', 1);
require __DIR__.'/../lib/common.php';

$board = $_GET['board'];
if (empty($board) || !isset($forumBoards[$board])) {
    $board = '';
}

$category = '';
if (!empty((int) $_GET['category'])) {
    $category = (int) $_GET['category'];
}

header('Content-type: text/json');
die(json_encode(makeForumListing(0, $board, 'array', $category)));
