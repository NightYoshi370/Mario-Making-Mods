<?php

function chatUsers($userid)
{
    global $db;

    return Query('SELECT * FROM {users} WHERE id != {0u} LIMIT 10', $userid);
    $db->rows('users', ['id' != $userid]);
}

function getUserDetails($userid)
{
    global $db;

    return $db->row('users', ['id' => $userid]);
}

function getUserAvatar($userid)
{
    return str_replace('$root/', '', FetchResult('SELECT picture FROM {users} WHERE id = {0u}', $userid));
}

function updateUserOnline($userId, $online)
{
    global $db;
    $db->updateId('users', ['online' => $online], 'id', $userId);
}

function insertChat($reciever_userid, $user_id, $chat_message)
{
    global $db;
    $result = $db->insert(
        'chat', [
        'reciever_userid' => $reciever_userid, 'sender_userid' => $user_id,
        'message'         => $chat_message, 'status' => 1,
        ]
    );
    $conversation = getUserChat($user_id, $reciever_userid);
    $data = [
        'conversation' => $conversation,
    ];
    echo json_encode($data);
}

function getUserChat($from_user_id, $to_user_id)
{
    $fromUserAvatar = getUserAvatar($from_user_id);
    $toUserAvatar = getUserAvatar($to_user_id);
    $sqlQuery = Query(
        'SELECT * FROM {chat} 
			WHERE (sender_userid = {0u}	AND reciever_userid = {1u}) 
			OR (sender_userid = {1u} AND reciever_userid = {0u}) 
			ORDER BY timestamp ASC
		LIMIT 10', $from_user_id, $to_user_id
    );
    $conversation = '<ul>';
    while ($userChat == Fetch($sqlQuery)) {
        $user_name = '';
        if ($chat['sender_userid'] == $from_user_id) {
            $conversation .= '<li class="sent">';
            $conversation .= '<img width="22px" height="22px" src="userpics/'.$fromUserAvatar.'" alt="" />';
        } else {
            $conversation .= '<li class="replies">';
            $conversation .= '<img width="22px" height="22px" src="userpics/'.$toUserAvatar.'" alt="" />';
        }
        $conversation .= '<p>'.$chat['message'].'</p>';
        $conversation .= '</li>';
    }
    $conversation .= '</ul>';

    return $conversation;
}

function showUserChat($from_user_id, $to_user_id)
{
    global $db;

    $user = getUserDetails($to_user_id);
    $toUserAvatar = $user['avatar'];
    $userSection = '<img src="userpics/'.$user['avatar'].'" alt="" />
		<p>'.$user['username'].'</p>
		<div class="social-media">
			<i class="fa fa-facebook" aria-hidden="true"></i>
			<i class="fa fa-twitter" aria-hidden="true"></i>
			<i class="fa fa-instagram" aria-hidden="true"></i>
		</div>';
    // get user conversation
    $conversation = getUserChat($from_user_id, $to_user_id);
    // update chat user read status
    $db->update('chat', ['status' => 0], ['sender_userid' => $to_user_id, 'reciever_userid' => $from_user_id, 'status' => 1]);
    // update users current chat session
    $db->updateId('users', ['current_session' => $to_user_id], 'userid', $from_user_id);
    $data = [
        'userSection'  => $userSection,
        'conversation' => $conversation,
    ];
    echo json_encode($data);
}

function getUnreadMessageCount($senderUserid, $recieverUserid)
{
    $sqlQuery = Query(
        '	SELECT * FROM {chat}  
						WHERE sender_userid = {0u}
						AND reciever_userid = {1u}
						AND status = 1', $senderUserid, $recieverUserid
    );
    $numRows = NumRows($sqlQuery);
    $output = '';
    if ($numRows > 0) {
        $output = $numRows;
    }

    return $output;
}

function updateTypingStatus($is_type, $loginDetailsId)
{
    global $db;
    $db->updateId('chat_login_details', ['is_typing' => $is_type], 'id', $loginDetailsId);
}

function fetchIsTypeStatus($userId)
{
    $row = Fetch(
        Query(
            'SELECT is_typing FROM {chat_login_details} 
						WHERE userid = {0c}
						ORDER BY last_activity DESC
						LIMIT 1', $userId
        )
    );

    $output = '';
    if ($row['is_typing'] == 'yes') {
        $output = ' - <small><em>Typing...</em></small>';
    }

    return $output;
}

function getUserLastActivity($userId)
{
    $row = Fetch(
        Query(
            '
		SELECT last_activity FROM {chat_login_details}
		WHERE userid = {0c} ORDER BY last_activity DESC LIMIT 1', $userId
        )
    );

    return $row['last_activity'];
}
