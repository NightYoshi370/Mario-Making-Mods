<?php
define('BLARG', 1);
require __DIR__.'/../lib/common.php';

session_start();
require 'header.php';
?>
<title>phpzag.com : Demo Build Live Chat System with Ajax, PHP & MySQL</title>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>
<link href="css/style.css" rel="stylesheet" id="bootstrap-css">
<script src="js/chat.js"></script>
<style>
.modal-dialog {
    width: 400px;
    margin: 30px auto;    
}
</style>
<?php require 'container.php'; ?>
<div class="container">        
    <h1>Example: Build Live Chat System with Ajax, PHP & MySQL</h1>        
    <br>
    <?php if ($loguserid) {
    ?>     
        <div class="chat">    
            <div id="frame">        
                <div id="sidepanel">
                    <div id="profile">
        <?php
                    include 'Chat.php';
    $loggedUser = getUserDetails($loguserid);
    echo '<div class="wrap">';
    $currentSession = $loguserid;
    echo '<img id="profile-img" src="'.str_replace('$root/', substr(DATA_URL, 5), $loguser['picture']).'" class="online" alt="" />';
    echo  '<p>'.$user['username'].'</p>';
    echo '<i class="fa fa-chevron-down expand-button" aria-hidden="true"></i>';
    echo '<div id="status-options">';
    echo '<ul>';
    echo '<li id="status-online" class="active"><span class="status-circle"></span> <p>Online</p></li>';
    echo '<li id="status-away"><span class="status-circle"></span> <p>Away</p></li>';
    echo '<li id="status-busy"><span class="status-circle"></span> <p>Busy</p></li>';
    echo '<li id="status-offline"><span class="status-circle"></span> <p>Offline</p></li>';
    echo '</ul>';
    echo '</div>';
    echo '<div id="expanded">';
    echo '<a href="logout.php">Logout</a>';
    echo '</div>';
    echo '</div>'; ?>
                    </div>
                    <div id="search">
                        <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                        <input type="text" placeholder="Search contacts..." />                    
                    </div>
                    <div id="contacts">    
        <?php
                    echo '<ul>';
    $chatUsers = chatUsers($loguserid);
    while ($user = Fetch($chatUsers)) {
        $status = 'offline';
        if ($user['online']) {
            $status = 'online';
        }
        $activeUser = '';
        if ($user['id'] == $loguserid) {
            $activeUser = 'active';
        }
        echo '<li id="'.$user['id'].'" class="contact '.$activeUser.'" data-touserid="'.$user['id'].'" data-tousername="'.$user['name'].'">';
        echo '<div class="wrap">';
        echo '<span id="status_'.$user['id'].'" class="contact-status '.$status.'"></span>';
        echo '<img src="userpics/'.str_replace('$root/', substr(DATA_URL, 5), $user['picture']).'" alt="" />';
        echo '<div class="meta">';
        echo '<p class="name">'.$user['name'].'<span id="unread_'.$user['userid'].'" class="unread">'.getUnreadMessageCount($user['id'], $loguserid).'</span></p>';
        echo '<p class="preview"><span id="isTyping_'.$user['id'].'" class="isTyping"></span></p>';
        echo '</div>';
        echo '</div>';
        echo '</li>';
    }
    echo '</ul>'; ?>
                    </div>
                    <div id="bottom-bar">    
                        <button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add contact</span></button>
                        <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>                    
                    </div>
                </div>            
                <div class="content" id="content"> 
                    <div class="contact-profile" id="userSection">    
        <?php
                    $user = getUserDetails($loguserid);
    echo '<img src="userpics/'.str_replace('$root/', substr(DATA_URL, 5), $user['picture']).'" alt="" />';
    echo '<p>'.$user['name'].'</p>';
    echo '<div class="social-media">';
    echo '<i class="fa fa-facebook" aria-hidden="true"></i>';
    echo '<i class="fa fa-twitter" aria-hidden="true"></i>';
    echo '<i class="fa fa-instagram" aria-hidden="true"></i>';
    echo '</div>'; ?>                        
                    </div>
                    <div class="messages" id="conversation">        
        <?php
                    echo getUserChat($loguserid, $currentSession); ?>
                    </div>
                    <div class="message-input" id="replySection">                
                        <div class="message-input" id="replyContainer">
                            <div class="wrap">
                                <input type="text" class="chatMessage" id="chatMessage<?php echo $currentSession; ?>" placeholder="Write your message..." />
                                <button class="submit chatButton" id="chatButton<?php echo $currentSession; ?>"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>    
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
        <?php
} else {
                        ?>
        <br>
        <br>
        <strong><a href="login.php"><h3>Login To Access Chat System</h3></a></strong>        
        <?php
                    } ?>
    <br>
    <br>    
    <div style="margin:50px 0px 0px 0px;">
        <a class="btn btn-default read-more" style="background:#3399ff;color:white" href="http://www.phpzag.com/build-live-chat-system-with-ajax-php-mysql/">Back to Tutorial</a>        
    </div>    
</div>    
<?php require 'footer.php'; ?>