<?php

session_start();
require 'Chat.php';
if (!$loguserid) {
    die('Yeah, no!');
}
if ($_POST['action'] == 'update_user_list') {
    $chatUsers = chatUsers($loguserid);
    $data = [
        'profileHTML' => $chatUsers,
    ];
    echo json_encode($data);
}
if ($_POST['action'] == 'insert_chat') {
    insertChat($_POST['to_user_id'], $loguserid, $_POST['chat_message']);
}
if ($_POST['action'] == 'show_chat') {
    showUserChat($loguserid, $_POST['to_user_id']);
}
if ($_POST['action'] == 'update_user_chat') {
    $conversation = getUserChat($loguserid, $_POST['to_user_id']);
    $data = [
        'conversation' => $conversation,
    ];
    echo json_encode($data);
}
if ($_POST['action'] == 'update_unread_message') {
    $count = getUnreadMessageCount($_POST['to_user_id'], $loguserid);
    $data = [
        'count' => $count,
    ];
    echo json_encode($data);
}
if ($_POST['action'] == 'update_typing_status') {
    updateTypingStatus($_POST['is_type'], $_SESSION['login_details_id']);
}
if ($_POST['action'] == 'show_typing_status') {
    $message = fetchIsTypeStatus($_POST['to_user_id']);
    $data = [
        'message' => $message,
    ];
    echo json_encode($data);
}
